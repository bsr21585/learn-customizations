﻿<%@ WebService Language="C#" Class="CourseTrackerService" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Web;
using System.Web.Services;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class CourseTrackerService : System.Web.Services.WebService 
{
    private List<string> serviceAPIKeyList = new List<string> {"#N1CB53rv1c3!"};
    private const string ERRORINVALIDAPIKEY = "Invalid API Key";
    //temporary connection string
    private string sqlConnectionString = "Data Source=ASENTIA-SQL02;Initial Catalog=NICB-CourseTracker;User ID=installer;Password=#fr4nt1c!;Trusted_Connection=False";
    private string msgBeforeStartDate = "Content is attempting to launch before its assigned start date. Please close this window.";
    private string msgAfterEndDate = "Content is attempting to launch after its expiration date. Please close this window.";
    private string msgExceededMaxLaunches = "Content has exceeded maximum unique launches. Please close this window.";
    
    public CourseTrackerService()
    {
        //
        // TODO: Add any constructor code required
        //
    }
    
    [WebMethod]
    public string IsContentAuthorizedToLaunch(string lessonCode, string companyCode, string APIKey)
    {
        if (!isAPIKeyValid(APIKey))
            return ERRORINVALIDAPIKEY;

        //lessonCode = GetDecodedValue(lessonCode);
        //companyCode = GetDecodedValue(companyCode;
        
        //check if content is launching before start date
        if (IsContentBeforeStartDate(lessonCode, companyCode))
            return "false|||" + msgBeforeStartDate;
        
        //check if content is expired
        if (IsContentExpired(lessonCode, companyCode))
            return "false|||" + msgAfterEndDate;
        
        //check if content exceeded Max unique user launches
        if (HasContentExceededMaximumLaunches(lessonCode, companyCode))
            return "false|||" + msgExceededMaxLaunches;

        return "true";
    }

    [WebMethod]
    public string LogUserLaunch(string lessonCode, string companyCode, string userId, string userName, string APIKey)
    {
        string result = "no user launch logged";

        //if (!isAPIKeyValid(APIKey))
        //    return ERRORINVALIDAPIKEY;

        //lessonCode = GetDecodedValue(lessonCode);
        //companyCode = GetDecodedValue(companyCode);
		
		
        userId = GetDecodedValue(userId);
        userName = GetDecodedValue(userName);
        
        SqlConnection sqlConn = new SqlConnection(GetConnectionString());
        SqlCommand sqlComm = new SqlCommand();

        sqlComm.CommandType = CommandType.StoredProcedure;
        sqlComm.Connection = sqlConn;
        sqlComm.CommandText = "[User.LogUserLaunch]";

        SqlParameter lessonCodeParam = new SqlParameter("lessonCode", lessonCode);
        SqlParameter lessonNameParam = new SqlParameter("lessonName", "-");
        SqlParameter companyCodeParam = new SqlParameter("companyCode", companyCode);
        SqlParameter userIdParam = new SqlParameter("lmsUserId", userId);
        SqlParameter userNameParam = new SqlParameter("lmsUsername", userName);

        sqlComm.Parameters.Add(lessonCodeParam);
        sqlComm.Parameters.Add(lessonNameParam);
        sqlComm.Parameters.Add(companyCodeParam);
        sqlComm.Parameters.Add(userIdParam);
        sqlComm.Parameters.Add(userNameParam);

        sqlConn.Open();

        using (SqlDataReader dataReader = sqlComm.ExecuteReader())
        {
            while (dataReader.Read())
            {
                result = (string)dataReader["Result"];
            }
        }

        sqlConn.Close();

        return result;
    }
    
    private bool IsContentExpired(string lessonCode, string companyCode)
    {
        bool isExpired = false;

        SqlConnection sqlConn = new SqlConnection(GetConnectionString());
        SqlCommand sqlComm = new SqlCommand();

        sqlComm.CommandType = CommandType.StoredProcedure;
        sqlComm.Connection = sqlConn;
        sqlComm.CommandText = "[Lesson.GetIsLessonLaunchingAfterEndDate]";

        SqlParameter lessonCodeParam = new SqlParameter("lessonCode", lessonCode);
        SqlParameter companyCodeParam = new SqlParameter("companyCode", companyCode);

        sqlComm.Parameters.Add(lessonCodeParam);
        sqlComm.Parameters.Add(companyCodeParam);

        sqlConn.Open();

        using (SqlDataReader dataReader = sqlComm.ExecuteReader())
        {
            while (dataReader.Read())
            {
                isExpired = (bool)dataReader["Result"];
            }
        }

        sqlConn.Close();

        return isExpired;
    }
    
    private bool IsContentBeforeStartDate(string lessonCode, string companyCode)
    {
        bool isBeforeLaunchDate = false;

        SqlConnection sqlConn = new SqlConnection(GetConnectionString());
        SqlCommand sqlComm = new SqlCommand();

        sqlComm.CommandType = CommandType.StoredProcedure;
        sqlComm.Connection = sqlConn;
        sqlComm.CommandText = "[Lesson.GetIsLessonLaunchingBeforeStartDate]";

        SqlParameter lessonCodeParam = new SqlParameter("lessonCode", lessonCode);
        SqlParameter companyCodeParam = new SqlParameter("companyCode", companyCode);

        sqlComm.Parameters.Add(lessonCodeParam);
        sqlComm.Parameters.Add(companyCodeParam);
        
        sqlConn.Open();

        using (SqlDataReader dataReader = sqlComm.ExecuteReader())
        {
            while (dataReader.Read())
            {
                isBeforeLaunchDate = (bool)dataReader["Result"];
            }
        }

        sqlConn.Close();

        return isBeforeLaunchDate;
    }
    
    private bool HasContentExceededMaximumLaunches(string lessonCode, string companyCode)
    {
        bool hasExceededLaunches = false;

        SqlConnection sqlConn = new SqlConnection(GetConnectionString());
        SqlCommand sqlComm = new SqlCommand();

        sqlComm.CommandType = CommandType.StoredProcedure;
        sqlComm.Connection = sqlConn;
        sqlComm.CommandText = "[Lesson.HasLessonExceededMaxUniqueUserLaunches]";

        SqlParameter lessonCodeParam = new SqlParameter("lessonCode", lessonCode);
        SqlParameter companyCodeParam = new SqlParameter("companyCode", companyCode);

        sqlComm.Parameters.Add(lessonCodeParam);
        sqlComm.Parameters.Add(companyCodeParam);

        sqlConn.Open();

        using (SqlDataReader dataReader = sqlComm.ExecuteReader())
        {
            while (dataReader.Read())
            {
                string hasExceededLaunchesStr = (string)dataReader["Result"];

                if (hasExceededLaunchesStr == "true")
                    hasExceededLaunches = true;
            }
        }

        sqlConn.Close();

        return hasExceededLaunches;
    }
    
    private string GetConnectionString()
    {
        return sqlConnectionString;
    }

    #region IsAPIKeyValid
    /// <summary>
    /// Returns whether the API key is a valid key for performing operations with this service
    /// </summary>
    /// <param name="APIKey"></param>
    /// <returns></returns>
    private bool isAPIKeyValid(string APIKey)
    {
        //decode the key
        string decodedKey = "";
        
        try
        {
            decodedKey = GetDecodedValue(APIKey);
        }
        catch(Exception)
        { }

        //check if the provided key exists in the pool of Keys
        if (decodedKey.Length > 0)
        {
            try
            {
                //check for the key in the list
                if (serviceAPIKeyList.IndexOf(decodedKey) > -1)
                    return true;
            }
            catch (Exception)
            { }
        }

        return false;
        
    } 
    #endregion

    #region GetDecodedValue
    /// <summary>
    /// Decodes Base64 values and returns the decoded value
    /// </summary>
    /// <param name="encString"></param>
    /// <returns></returns>
    private string GetDecodedValue(string encString)
    {
        byte[] data = Convert.FromBase64String(encString);
        string decodedString = System.Text.Encoding.UTF8.GetString(data);

        return decodedString;
    } 
    #endregion
}
