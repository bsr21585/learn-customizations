﻿<%@ WebService Language="C#" Class="SafewayService" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Xml.Linq;
using Newtonsoft.Json;

[WebService(Namespace = "http://safewaydriving.asentialms.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class SafewayService : System.Web.Services.WebService 
{
	private bool useEncryption = false;
    private int attemptLimit = 10;
    private const int NUM_REQUIRED_QUESTIONS = 30;//number of questions the user is required to provide a secret answer for at the beginning of their first launch
	private const string ALTERNATE_SITE_DOMAIN =  "onlinedrivingschooltexas";
	private const string ALTERNATE_SITE_DOMAIN_REROUTE =  "safewaydriving";
	private bool useAlternateSiteDomainReroute = true;
    private const string QUESTION_POOL_FOLDER_PATH_RELATIVE = "~/static/SafewayService/";
    private const string QUESTION_POOL_FILENAME = "SafewayQuestionPool.xml";
    private const string USERS_FOLDER_PATH_RELATIVE = "~/_config/{0}/users/";
    private const string USER_ANSWERS_FILENAME = "VerificationAnswers.xml";
    private const string USER_FAILURES_FILENAME = "VerificationFailures.xml";
    private const string USER_FAILURES_FILE_CONTENTS = 
        @"<failures>
    <failure failureCount=""{0}"" timestamp=""{1}"" />
</failures>";
    //private const string QUESTION_ANSWER_XMLFORMAT = @"<question id=""{0}"" userAnswer=""{1}""/>";
    private const string QUESTION_ANSWER_XMLFORMAT =
        @"<question>
        <questionText><![CDATA[{0}]]></questionText>
        <userAnswer><![CDATA[{1}]]></userAnswer>
    </question>";
    private const string QUESTION_ANSWER_XMLFORMAT_OPEN = "<questions>";
    private const string QUESTION_ANSWER_XMLFORMAT_CLOSE = "</questions>";
    
    private class SafewayQuestion
    {
        public string QuestionId { get; set; }
        public string QuestionText { get; set; }
    }
    
    /// <summary>
    /// User's answer to a verification question
    /// </summary>
    private class UserAnswer
    {
        public string QuestionId { get; set; }
        public string AnswerText { get; set; }
    }
    
    /// <summary>
    /// An secret answer the user provided, i.e. their favorite color ("i" = question Id, "t" = question text, "a" = answer; obfuscated to makes less noticeable in JavaScript)
    /// </summary>
    private class SecretAnswer
    {
        public string i { get; set; }
        public string t { get; set; }
        public string a { get; set; }
    }
    
    private class NumSavedSecretAnswers
    {
        public string NumSavedAnswers { get; set; }
        public string NumSavedAnswersRequired { get; set; }
    }
    
    private class WebServiceResponse
    {
        public string Success { get; set; }
        public string Details { get; set; }
    }
    
    private class FailuresAndAttemptLimit
    {
        public string Failures { get; set; }
        public string AttemptLimit { get; set; }
    }
    
    public SafewayService()
    {

    }  

    #region EvaluateUserAnswer
    [WebMethod]
    public string EvaluateUserAnswer(int userId, string questionText, string answerText)
    {
        string jsonReturnStr = "{{\"IsCorrect\":\"{0}\",\"Attempts\":\"{1}\",\"AttemptLimit\":\"" + attemptLimit.ToString() + "\"}}";

        //get the correctness of the user's answer
        int userAnswerStatus = IsUserAnswerCorrect(userId, questionText, answerText);

        try
        {
            //no information was found for this user
            if (userAnswerStatus == -1)
            {

            }
            //answer is correct
            else if (userAnswerStatus == 1)
            {
                //clear out the VerificationFailures file
                //CreateNewFailuresFile(userId);

                //send back that the answer was correct
                jsonReturnStr = String.Format(jsonReturnStr, userAnswerStatus.ToString(), "0");

                return jsonReturnStr;
            }
            //answer is incorrect
            else if (userAnswerStatus == 0)
            {
                //increment the failurecount
                IncrementFailureCount(userId);
                
                //get the current failure count
                int failureCount = _GetFailureCount(userId);
                
                //if the failure count is at the attempt limit, lock the lesson
                if (failureCount >= attemptLimit)
                {
                    //TO DO: ANY SERVERSIDE LESSON LOCK TASKS
                }
                     
                //return the results
                jsonReturnStr = String.Format(jsonReturnStr, userAnswerStatus.ToString(), failureCount.ToString());
                
                return jsonReturnStr;
            }
        }
        catch (Exception e)
        {
            return e.Message;
        }

        //temp to avoid fxn error
        return jsonReturnStr;
    } 
    #endregion

    #region SaveSecretAnswers
    /// <summary>
    /// Takes in a JSON string of the user's secret answers and stores them to the VerificationAnswers.xml file
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="userAnswersJSON"></param>
    /// <returns></returns>
    [WebMethod]
    public string SaveSecretAnswers(int userId, string userAnswersJSON)
    {
        string answersXml = "";
        string filePath = "";

        try
        {
            //turn the answers into a JSON object
            List<SecretAnswer> secretAnswerList = JsonConvert.DeserializeObject<List<SecretAnswer>>(userAnswersJSON);

            //get the path for this user's file storage
            filePath = GetUserIdFolderRelativePath(userId);
			
			// get the path for storing user verification answers on Asentia SQL server prior to sending the data to Safeway FTP server
            string FTPIntermediatePath = string.Format("\\\\asentia-sql02\\DData\\DataOut\\SafewayDrivingSchool\\User_Verification_Answers\\{0}_{1}", userId.ToString(), USER_ANSWERS_FILENAME);

            //create the target folder if path doesn't exist
            /*if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }*/

            //append the filename for user answers
            filePath += USER_ANSWERS_FILENAME;

            //open the questions tag
            answersXml = QUESTION_ANSWER_XMLFORMAT_OPEN;

            //create XML of secret answers to be saved to VerificationAnswers.xml
            for (int i = 0; i < secretAnswerList.Count; i++)
            {
                SecretAnswer thisAnswer = secretAnswerList[i];

                //get the question text and answer text
                string thisAnswerId = thisAnswer.i;
                string thisQuestionText = thisAnswer.t;
				string thisAnswerText = thisAnswer.a;
				
				//encrypt the answer, if needed
				if (useEncryption)
				{
					string encryptedAnswer = AESEncryptString(true, thisAnswer.a);
					thisAnswerText = encryptedAnswer;
				}

                //create the question xml element
                string xmlElement = String.Format(QUESTION_ANSWER_XMLFORMAT, thisQuestionText, thisAnswerText);

                //add to the main string
                answersXml += "\n\t" + xmlElement;
            }

            //close the questions tag
            answersXml += "\n" + QUESTION_ANSWER_XMLFORMAT_CLOSE;

            //save the data to the files
            File.WriteAllText(filePath, answersXml);
			File.WriteAllText(FTPIntermediatePath, answersXml);

            //report success
            return GetWebServiceResponseSerialized("true", "Wrote answers successfully");
        }
        catch (Exception e)
        {
            return GetWebServiceResponseSerialized("false", e.Message);
        }

    } 
    #endregion
    
    #region GetFailureCount
    /// <summary>
    /// Returns the number of failures for a particular user; returns -1 is no data was found for specified user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [WebMethod]
    public string GetFailureCount(int userId)
    {
        return _GetFailureCount(userId).ToString();
    }
    #endregion

    #region GetFailureCountAndAttemptLimit
    /// <summary>
    /// Returns an object containing both the failure count for the user specified and the attempt limit
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [WebMethod]
    public string GetFailureCountAndAttemptLimit(int userId)
    {
        FailuresAndAttemptLimit failuresAndAttemptLimit = new FailuresAndAttemptLimit();

        try
        {
            //get the failure count for this user (-1 if none found)
            int failureCount = _GetFailureCount(userId);

            //set object properties
            failuresAndAttemptLimit.Failures = failureCount.ToString();
            failuresAndAttemptLimit.AttemptLimit = attemptLimit.ToString();

            //convert to string   
            string objectStr = JsonConvert.SerializeObject(failuresAndAttemptLimit);
            return objectStr;
        }
        catch (Exception e)
        {
            Console.WriteLine("GetFailureCountAndAttempts(): " + e.Message);

            //set object properties
            failuresAndAttemptLimit.Failures = "-1";
            failuresAndAttemptLimit.AttemptLimit = attemptLimit.ToString();

            //convert to string   
            string objectStr = JsonConvert.SerializeObject(failuresAndAttemptLimit);
            return objectStr;
        }

    } 
    #endregion
    
    #region GetNumberOfSavedSecretAnswers
    /// <summary>
    /// Returns the number of secret verification answers the specified user currently has saved in the system as a NumSavedSecretAnswers object, which also shows the required number of saved answers
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [WebMethod]
    public string GetNumberOfSavedSecretAnswers(int userId)
    {
        //create object to hold the data
        NumSavedSecretAnswers userNumSavedAnswers = new NumSavedSecretAnswers
        {
            NumSavedAnswersRequired = NUM_REQUIRED_QUESTIONS.ToString(),
            NumSavedAnswers = "0"
        };
        
        //get the answers xml file
        XElement userAnswersFile = GetUserVerificationAnswersXML(userId);

        //return 0 if no file exists
        if (userAnswersFile == null)
        {
            return JsonConvert.SerializeObject(userNumSavedAnswers);
        }
        
        try
        {
            //otherwise, get a count of how many 'question' elements there are
            int numAnswers = userAnswersFile.Elements().Count();

            //update the num answers and return the object
            userNumSavedAnswers.NumSavedAnswers = numAnswers.ToString();
            return JsonConvert.SerializeObject(userNumSavedAnswers);
        }
        catch(Exception e)
        {
            Console.WriteLine("GetNumberOfSavedSecretAnswers(): " + e.Message);
        }

        return JsonConvert.SerializeObject(userNumSavedAnswers);
    } 
    #endregion

    //THIS ISN'T USED ANYMORE BECAUSE IF THE QUESTIONS CHANGE, THEN THE IDS WON'T MATCH UP ANYMORE
    #region GetSavedSecretResponseIds
    /// <summary>
    /// Pulls a string of comma-delimited question Ids that the user has created a secret answer for
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [WebMethod]
    public string GetSavedSecretResponseIds(int userId)
    {
        List<string> currentUserResponseIdsList = new List<string>();
        string currentUserResponseIds = "";

        //get the XML file where their answers are stored
        XElement userAnswersFile = GetUserVerificationAnswersXML(userId);

        //return false if no file found for this user
        if (userAnswersFile == null)
            return "";

        try
        {
            //grab all the question elements
            IEnumerable<XElement> userAnswerElementList = userAnswersFile.Descendants();

            //add the ids to the list
            for (int i = 0; i < userAnswerElementList.Count(); i++)
            {
                XElement thisElement = userAnswerElementList.ElementAt(i);

                string thisElementId = thisElement.Attribute("id").Value;
                currentUserResponseIdsList.Add(thisElementId);
            }

            //stringify the list and delimit with a comma
            currentUserResponseIds = String.Join(",", currentUserResponseIdsList);
        }
        catch (Exception e)
        {
            Console.WriteLine("GetSavedSecretResponseIds(): " + e.Message);
        }

        return currentUserResponseIds;
    } 
    #endregion

    #region GetQuestionPool
    /// <summary>
    /// Returns the questions currently in the Safeway questions pool
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string GetQuestionPool()
    {
        string questionPoolPath = GetQuestionPoolFilePath();
        string questionListStr = "";
        List<SafewayQuestion> questionList = new List<SafewayQuestion>();

        try
        {
            //grab the file contents
            XElement questionPoolDoc = XElement.Load(@questionPoolPath);
            
            //grab out the questions
            IEnumerable<XElement> questionPoolItems = questionPoolDoc.Elements();

            //push the question data to objects
            for (int i = 0; i < questionPoolItems.Count(); i++)
            {
                XElement thisQuestionElement = questionPoolItems.ElementAt(i);

                SafewayQuestion thisQuestionObject = new SafewayQuestion
                {
                    QuestionId = thisQuestionElement.Attribute("id").Value,
                    QuestionText = thisQuestionElement.DescendantNodes().OfType<XCData>().ElementAt(0).Value
                };
                
                //add the question to the list
                questionList.Add(thisQuestionObject);
            }
            
            //convert the list to string
            questionListStr = JsonConvert.SerializeObject(questionList);
        }
        catch(Exception e)
        {
            Console.WriteLine(e.Message);
        }
        
        return questionListStr;
    } 
    #endregion

    #region GetRandomQuestions
    /// <summary>
    /// Pulls random questions from the question pool until NUM_REQUIRED_QUESTIONS is reached (or no questions are left)
    /// </summary>
    /// <returns></returns>
    [WebMethod]
    public string GetRandomQuestions()
    {
        string questionListStr = "";

        try
        {
            List<SafewayQuestion> mainQuestionPool = JsonConvert.DeserializeObject<List<SafewayQuestion>>(GetQuestionPool());
            List<SafewayQuestion> chosenQuestionsList = new List<SafewayQuestion>();
            Random randomGen = new Random();

            for (int i = 0; i < NUM_REQUIRED_QUESTIONS && mainQuestionPool.Count > 0; i++)
            {
                //choose a random question
                int randQuestionIndex = randomGen.Next(mainQuestionPool.Count);

                //add the question to chosen questions
                chosenQuestionsList.Add(mainQuestionPool.ElementAt(randQuestionIndex));

                //remove that question from the main list
                mainQuestionPool.RemoveAt(randQuestionIndex);
            }

            //create a string from the collected questions
            questionListStr = JsonConvert.SerializeObject(chosenQuestionsList);
        }
        catch (Exception e)
        {
            Console.WriteLine("GetRandomQuestions(): " + e.Message);
        }

        return questionListStr;
    } 
    #endregion


    #region GetQuestionsByUserId
    /// <summary>
    /// Check the VerificationAnswers.xml file for specified user and return SafewayQuestion objects for each match id as JSON string
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [WebMethod]
    public string GetQuestionsByUserId(int userId)
    {
        List<string> questionTextList = new List<string>();
        List<SafewayQuestion> targetQuestionList = new List<SafewayQuestion>();
        
        //get the VerificationAnswers.xml file where their answers are stored
        XElement userAnswersFile = GetUserVerificationAnswersXML(userId);

        //return empty if no file found for this user
        if (userAnswersFile == null)
            return "[]";

        Random randomGen = new Random();
        
        try
        {
            //grab all the descendents
            IEnumerable<XElement> questionNodes = userAnswersFile.Elements();
            
            for (int i = 0; i < questionNodes.Count(); i++)
            {
                //grab the question ids from each node
                XElement thisQuestionNode = questionNodes.ElementAt(i);

                //get the question text so it can be compared to the overall question pool and matching questions can be pulled
                string thisQuestionText = thisQuestionNode.DescendantNodes().OfType<XCData>().ElementAt(0).Value;
                
                //add the question Id to the list
                questionTextList.Add(thisQuestionText);
            }
            
            //get the full question pool so questions with target questions can be pulled
            string questionPoolSerializedJSON = GetQuestionPool();
            
            //deserialize the question pool to a list
            List<SafewayQuestion> questionPoolList = JsonConvert.DeserializeObject<List<SafewayQuestion>>(questionPoolSerializedJSON);

            //iterate thru and grab the target question objects
            for (int j = 0; j < questionTextList.Count; j++)
            {
                //choose a random question
                int randQuestionIndex = randomGen.Next(questionTextList.Count);

                SafewayQuestion thisTargetQuestion = questionPoolList.Find(x => x.QuestionText == questionTextList.ElementAt(randQuestionIndex));

                //add the question to the target list
                targetQuestionList.Add(thisTargetQuestion);

                //remove that question from the main list
                questionTextList.RemoveAt(randQuestionIndex);
            }
            
            //serialize the list
            string targetQuestionListSerialized = JsonConvert.SerializeObject(targetQuestionList);

            return targetQuestionListSerialized;
        }
        catch(Exception e)
        {
            Console.WriteLine("GetQuestionsByUserId(): " + e.Message);
        }

        return "[]";
    }
    #endregion

    #region ResetFailuresFile
    /// <summary>
    /// Sets an existing failures file back to 0 attempts for the target user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    [WebMethod]
    public bool ResetFailuresFile(int userId)
    {
        //check for the user folder
        string fullPath = GetUsersFolderRelativePath();

        //check if a folder exists for this user
        string userIdPath = fullPath + userId.ToString();

        try
        {
            //no action needed if the path doesn't exist
            if (!DoesPathExist(userIdPath))
            {
                return false;
            }

            //build the file path
            string filePath = userIdPath + "/" + USER_FAILURES_FILENAME;

            //build the file contents
            string xmlContent = String.Format(USER_FAILURES_FILE_CONTENTS, "0", "");

            //write the file to a blank slate
            File.WriteAllText(filePath, xmlContent);

            return true;

        }
        catch (Exception e)
        {
            Console.WriteLine("ResetFailuresFile(): " + e.Message);
        }

        return false;
    }
    #endregion
    
    #region IncrementFailureCount
    /// <summary>
    /// Increments the failure count for a particular user in their VerificationFailures.xml file; Returns boolean indicating success
    /// </summary>
    /// <param name="userId"></param>
    private bool IncrementFailureCount(int userId)
    {
        //build the path to the failure count xml file
        string fullPath = GetUsersFolderRelativePath();

        //build path to this user
        string userIdPath = fullPath + userId.ToString();
        
        //build the target file path/name for VerificationFailures.xml
        string filePath = userIdPath + "/" + USER_FAILURES_FILENAME;
        
        //get the failures file
        XElement failuresFile = GetFailuresFile(userId);

        //if no file was found, create one and set the failure count to 1 with timestamp
        if (failuresFile == null)
        {        
            try
            {
                //create the path if it doesn't exist
                /*if (!DoesPathExist(userIdPath))
                {
                    Directory.CreateDirectory(userIdPath);
                }*/
                
                //create the file contents
                string newFileContents = String.Format(USER_FAILURES_FILE_CONTENTS, "1", DateTime.UtcNow.ToString());
                
                //write the file
                File.WriteAllText(filePath, newFileContents);

                return true;
            }
            catch(Exception e)
            {
                Console.WriteLine("IncrementFailureCount(): " + e.Message);
            }

            return false;
        }
        
        //if the file does exist, update the failure count and timestamp
        try
        {
            //get the current failure count
            int failureCount = Convert.ToInt32(failuresFile.Descendants().ElementAt(0).Attribute("failureCount").Value);

            //increment the number
            failureCount++;

            //rewrite the xml with the updated values inserted for failure count and timestamp
            string newXmlContent = String.Format(USER_FAILURES_FILE_CONTENTS, failureCount.ToString(), DateTime.UtcNow.ToString());

            //write the new file
            File.WriteAllText(filePath, newXmlContent);

            return true;
        }
        catch (Exception e)
        {
            Console.WriteLine("IncrementFailureCount(): " + e.Message);
        }

        return false;
    } 
    #endregion

    #region CreateNewFailuresFile
    /// <summary>
    /// Creates a new VerificationFailures.xml file and returns true if it was created successfully, and false if not
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    private bool CreateNewFailuresFile(int userId)
    {
        //check for the user folder
        string fullPath = GetUsersFolderRelativePath();
        
        //check if a folder exists for this user
        string userIdPath = fullPath + userId.ToString();

        try
        {
            //create the path if it doesn't exist
            /*if (!DoesPathExist(userIdPath))
            {
                Directory.CreateDirectory(userIdPath);
            }*/

            //build the file path
            string filePath = userIdPath + "/" + USER_FAILURES_FILENAME;
            
            //build the file contents
            //string xmlContent = @"<failures><failure failureCount=""0"" timestamp="""" /></failures>";
            string xmlContent = String.Format(USER_FAILURES_FILE_CONTENTS, "0", "");
            
            //create/overwrite the file
            File.WriteAllText(filePath, xmlContent);
            
            return true;

        }
        catch (Exception e)
        {
            Console.WriteLine("CreateNewFailuresFile(): " + e.Message);
        }

        return false;
    }
    #endregion

    #region _GetFailureCount
    /// <summary>
    /// Returns the number of failures for a particular user; returns -1 is no data was found for specified user
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    private int _GetFailureCount(int userId)
    {
        XElement failuresDoc = GetFailuresFile(userId);
        int failureCount = -1;

        //return -1 if no folder found
        if (failuresDoc == null)
            return failureCount;

        //get the failure info
        XElement failureNode = failuresDoc.Descendants().ElementAt(0);

        //get the number of failures
        try
        {
            failureCount = Convert.ToInt32(failureNode.Attribute("failureCount").Value);
        }
        catch (Exception e)
        {
            Console.WriteLine("GetFailureCount(): " + e.Message);
            return failureCount;
        }

        return failureCount;
    } 
    #endregion
    
    #region GetFailuresFile
    /// <summary>
    /// Gets the VerificationFailures.xml file for the specified user Id as XElement; returns null if none found
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    private XElement GetFailuresFile(int userId)
    {
        XElement failuresDoc = null;

        //build the path to the failure count xml file
        string fullPath = GetUsersFolderRelativePath();

        //check if a folder exists for this user
        string userIdPath = fullPath + userId.ToString();

        //return the null instance if no folder exists
        if (!DoesPathExist(userIdPath))
            return failuresDoc;

        //build failures xml file path
        string failuresFilePath = userIdPath + "/" + USER_FAILURES_FILENAME;

        try
        {
            //get the failure file
            failuresDoc = XElement.Load(@failuresFilePath);
        }
        catch (Exception e)
        {
            Console.WriteLine("GetFailureCount(): " + e.Message);
        }

        return failuresDoc;
    } 
    #endregion
    
    #region IsUserAnswerCorrect
    /// <summary>
    /// Returns 0 or 1 based on whether the answerText matches for the provided questionId and userId; returns -1 if no data was found for the user Id
    /// </summary>
    /// <param name="userId"></param>
    /// <param name="questionId"></param>
    /// <param name="answerText"></param>
    /// <returns></returns>
    private int IsUserAnswerCorrect(int userId, string questionText, string answerText)
    {
        //check if a folder exists for this user
        string userIdPath = GetUserIdFolderRelativePath(userId);

        //return -1 if their folder doesn't exist
        if (!DoesPathExist(userIdPath))
        {
            return -1;
        }

        //get the XML file where their answers are stored
        XElement userAnswersFile = GetUserVerificationAnswersXML(userId);
        
        //return false if no file found for this user
        if (userAnswersFile == null)
            return -1;

        //grab the question element for the matching question from the xml file
        XElement questionElement = null;

        try
        {
            //questionElement = userAnswersFile.Descendants().Where(x => x.DescendantNodes().OfType<XCData>().ElementAt(0).Value == questionText).ElementAt(0);

            //iterate thru <question> tags to find the one with the matching questionText
            for (int i = 0; i < userAnswersFile.Elements().Count(); i++)
            {
                XElement thisQuestionElement = userAnswersFile.Elements().ElementAt(i);

                string thisQuestionText = thisQuestionElement.Element("questionText").Value;

                if (thisQuestionText == questionText)
                    questionElement = thisQuestionElement;
            }

            if (questionElement == null)
                throw new Exception("No matching question was found");
        }
        catch(Exception e)
        {
            Console.WriteLine("IsUserAnswerCorrect: " + e.Message);
            return -1; 
        }
        
        if (questionElement == null)
            return -1;
        
        //get the correct answer for this question
        //string correctAnswer = questionElement.DescendantNodes().OfType<XCData>().ElementAt(0).Value;
        string correctAnswer = questionElement.Element("userAnswer").Value;
        
        //decrypt the retrieved answer if needed
		if (useEncryption)
		{
			string decryptedCorrectAnswer = AESEncryptString(false, correctAnswer);
			
			//check that it decrypted correctly
			if (decryptedCorrectAnswer == null)
			    return -1;

			correctAnswer = decryptedCorrectAnswer;
		}
          
        //IGNORING CASE, check if the correctAnswer matches the user's answer
        try
        {
            if (correctAnswer.ToLower() == answerText.ToLower())
                return 1;
            else if (correctAnswer.ToLower() != answerText.ToLower())
                return 0;
        }
        catch(Exception e)
        {
            Console.WriteLine("IsUserAnswerCorrect(): " + e.Message);
        }
        
        return -1;
    } 
    #endregion

    #region GetUserVerificationAnswersXML
    /// <summary>
    /// Retuns the XML file for a specific user's answers as XElement
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    private XElement GetUserVerificationAnswersXML(int userId)
    {
        string filePath = GetUserIdFolderRelativePath(userId);
        XElement userAnswersFile;

        //append the xml filename
        filePath += USER_ANSWERS_FILENAME;

        try
        {
            userAnswersFile = XElement.Load(@filePath);
            return userAnswersFile;
        }
        catch (Exception e)
        {
            Console.WriteLine("GetUserVerificationAnswersXML: " + e.Message);
        }

        return null;
    } 
    #endregion
    
    #region GetUsersFolderRelativePath
    /// <summary>
    /// Returns the path to the current users folder based on the portal name (i.e. /_config/ics-jpack/users/)
    /// </summary>
    /// <returns></returns>
    private string GetUsersFolderRelativePath()
    {
        string finalFolderPath = "";

        //get the current portal name by getting the subdomain, i.e. http://ics-user.asentialms.com/ gives ics-user
        string subDomain = GetCurrentSubdomain();
		
		//special case if login site is on a different domain than the LMS portal: ie https://safewaydriving.asentialms.com/ is the LMS site but users log in to https://www.onlinedrivingschooltexas.com/
		if (useAlternateSiteDomainReroute)
		{
			if (subDomain == ALTERNATE_SITE_DOMAIN)
				subDomain = ALTERNATE_SITE_DOMAIN_REROUTE;
		}
		
        //insert the current portal name into the config path
        finalFolderPath = String.Format(USERS_FOLDER_PATH_RELATIVE, subDomain);

        finalFolderPath = Server.MapPath(finalFolderPath);

        return finalFolderPath;
    }
    #endregion

    #region GetUserIdFolderRelativePath
    /// <summary>
    /// Returns relative file path for the target user id; path ends in a \
    /// </summary>
    /// <param name="userId"></param>
    /// <returns></returns>
    private string GetUserIdFolderRelativePath(int userId)
    {
        string filePath = "";

        //construct the target folder path
        filePath += GetUsersFolderRelativePath();

        //append the user id folder
        filePath += userId.ToString() + "/";

        return filePath;
    }
    #endregion
    
    #region GetQuestionPoolFilePath
    /// <summary>
    /// Returns the path and filename for where the question pool xml is stored
    /// </summary>
    /// <returns></returns>
    private string GetQuestionPoolFilePath()
    {
        return GetQuestionPoolFolderRelativePath() + QUESTION_POOL_FILENAME;
    } 
    #endregion
    
    #region DoesPathExist
    /// <summary>
    /// Returns whether the target path exists on the server's drive
    /// </summary>
    /// <param name="targetPath"></param>
    /// <returns></returns>
    private bool DoesPathExist(string targetPath)
    {
        if (Directory.Exists(targetPath))
            return true;

        return false;
    } 
    #endregion

    #region GetQuestionPoolFolderRelativePath()
    /// <summary>
    /// Returns the relative location for storing the Question pool xml file, i.e. ~/static/SafewayService/
    /// </summary>
    /// <returns></returns>
    private string GetQuestionPoolFolderRelativePath()
    {
        try
        {
            //return the relative path for the question pool xml
            var folderPath = Server.MapPath(QUESTION_POOL_FOLDER_PATH_RELATIVE);

            return folderPath;
        }
        catch (Exception e)
        {
            Console.WriteLine("GetQuestionPoolFolderRelativePath(): " + e.Message);
        }

        return null;
    } 
    #endregion
    
    #region GetCurrentProtocolAndDomain
    /// <summary>
    /// Returns a string for the current protocol and domain, i.e. https://ics-user.asentialms.com
    /// </summary>
    /// <returns></returns>
    private string GetCurrentProtocolAndDomain()
    {
        string protocolAndDomain = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);

        return protocolAndDomain;
    } 
    #endregion

    #region GetCurrentSubdomain
    /// <summary>
    /// Returns the current subdomain as a string
    /// </summary>
    /// <returns></returns>
    private string GetCurrentSubdomain()
    {
        string folderPath = GetCurrentProtocolAndDomain();

        Uri thisDomain = new Uri(folderPath);
        string subdomain = GetSubdomainFromUri(thisDomain);

        return subdomain;
    } 
    #endregion

    #region GetSubdomainFromUri
    /// <summary>
    /// Returns the subdomain from the provided Uri
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    private string GetSubdomainFromUri(Uri url)
    {

        if (url.HostNameType == UriHostNameType.Dns)
        {

            string host = url.Host;

            var nodes = host.Split('.');
            int startNode = 0;
            if (nodes[0] == "www") startNode = 1;

            return nodes[startNode];
        }

        return null;
    }
    #endregion

    #region GetWebServiceResponseSerialized
    /// <summary>
    /// Returns a new serialized WebServiceResponse object with the provided success and message strings
    /// </summary>
    /// <param name="success"></param>
    /// <param name="message"></param>
    /// <returns></returns>
    private string GetWebServiceResponseSerialized(string success, string message)
    {
        WebServiceResponse response = new WebServiceResponse
        {
            Success = success,
            Details = message
        };

        return JsonConvert.SerializeObject(response);
    }
    #endregion

    #region AESEncryptString
    /// <summary>
    /// Uses Asentia's AESEncryption to encrypt/decrypt a string
    /// </summary>
    /// <param name="doEncrypt"></param>
    /// <param name="targetString"></param>
    /// <returns></returns>
    private string AESEncryptString(bool doEncrypt, string targetString)
    {
        Asentia.Common.AsentiaAESEncryption encrytor = new Asentia.Common.AsentiaAESEncryption();
        string resultStr = "";

        try
        {
            if (doEncrypt == true)
            {
                //perform an encryption
                resultStr = encrytor.Encrypt(targetString);
            }
            else if (doEncrypt == false)
            {
                //perform a decryption
                resultStr = encrytor.Decrypt(targetString);
            }

            return resultStr;
        }
        catch (Exception e)
        {
            Console.WriteLine("AESEncryptString(): " + e.Message);

            //return so we know it didn't work
            return null;
        }
    } 
    #endregion
    
}
