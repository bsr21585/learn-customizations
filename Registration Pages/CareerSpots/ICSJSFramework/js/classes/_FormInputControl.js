FormInputControl = function(
	label, 
	id,
	appendTo, 
	isRequired
	){
		
	this.ObjectType = "FormInputControl";
	this.Inputs = [];
	
	this.RootElement = DOM.c(
		this, 
		"DIV",
		{
			"id": (id) ? id : "",
			"class":"input-container-outer"
		},
		appendTo	
	);
	
	this.Label = DOM.c(
		this, 
		"DIV", 
		{
			"class":"label"
		},
		this.RootElement, 
		label
	);
	
	if (isRequired == true) 
		DOM.addClass(this.Label, "required");
	
	this.InputContainer = DOM.c(
		this,
		"DIV", 
		{
			"class":"input-container-inner"
		},
		this.RootElement
	);
	
	this.ErrorContainer = DOM.c(
		this, 
		"DIV", 
		{
			"class":"error"
		},
		this.RootElement
	);
	
}

FormInputControl.prototype.GetData = function(){
	
	if (this.Inputs.length == 0){return true;}
	
	return "'" + this.Inputs[0].name + "':'" + this.GetValue() + "'";
	
}

FormInputControl.prototype.GetValue = function(){
	
	if (this.Inputs.length == 0){return true;}

	switch(this.Inputs[0].type){
		case "text":
		case "password":		
		
			return this.Inputs[0].value.replace("'", "\\'");
			break;
		
		case "select-one":
			
			return this.Inputs[0].value.replace("'", "\\'");
			break;
	}
}

FormInputControl.prototype.PushData = function(d){
	
	if (this.Inputs.length == 0){return true;}
	if (d == null){return true;}
	
	switch(this.Inputs[0].type){
		case "text":
		case "password":
		
			this.Inputs[0].value = d;
			break;
			
		case "select":
		
			alert("Class: FormInputControl; Method: PushData (Not complete yet).");
			break;
			
		case "radio":
			
			for (var i = 0; i < this.Inputs.length; i++){
				
				if (this.Inputs[i].value == d){
					this.Inputs[i].checked = true;
				}
			}
			break;
			
		case "checkbox":
		
			var a = d.split(",");
			
			for (var i = 0; i < this.Inputs.length; i++){
				
				for (var j = 0; j < a.length; j++){
					
					if (this.Inputs[i].value == a[j]){
						this.Inputs[i].checked = true;
					}
					
				}
				
			}
			
			break;		
	}
	
	
}

FormInputControl.prototype.AddInput = function(e, caption){
	
	this.Inputs.push(e);
	
	var local = DOM.c(null, "DIV", {"class":"single-input"});
		local.appendChild(e);
		
	if (caption){
		switch(e.type){
			case "radio":
			case "checkbox":
				// change the class
				DOM.addClass(local, "single-input-selectable");
				// add the custom check/radio
				local.appendChild(
					DOM.c(
						null, 
						"LABEL",
						{
							"class":"custom-" + e.type,
							"for":e.id
						}
					)
				);
				
				switch(typeof caption){
					case "object": // its already formatted as a DOM object. must be a "label". just attach it.
						if(caption.nodeName != "LABEL"){
							alert("Error: 'caption' argument must be DOM element type 'LABEL' [FormControlInput:AddInput]")
						}
						local.appendChild(caption);
						break;
					default: // string
						// add the caption as a label
						local.appendChild(
							DOM.c(
								null, 
								"LABEL", 
								{
									"class":"class"
								}, 
								null, 
								caption
							)
						);
						break;
				}
				
				local.childNodes[2].setAttribute("for", e.id);
				break;
			default:
				local.appendChild(
					DOM.c(
						null, 
						"label", 
						{
							"class":"class"
						}, 
						null, 
						caption
					)
				);
				break;
		}
	}
	
	this.InputContainer.appendChild(local);
	
}

FormInputControl.prototype.AddError = function(s){
	
	this.ClearErrors();
	DOM.addClass(this.RootElement, "error");
	this.ErrorContainer.appendChild(DOM.text(s));
	
}

FormInputControl.prototype.ClearErrors = function(){
	
	DOM.removeClass(this.RootElement, "error");
	while(this.ErrorContainer.childNodes.length > 0){
		this.ErrorContainer.removeChild(this.ErrorContainer.childNodes[0]);
	}
	
}

FormInputControl.prototype.HasErrors = function(){
	
	return (this.ErrorContainer.childNodes.length > 0) ? true : false;
	
}

