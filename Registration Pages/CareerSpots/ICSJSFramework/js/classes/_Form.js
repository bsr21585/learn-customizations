Form = function(
	controller, 
	id, 
	name, 
	className, 
	appendTo, 
	workingCaption
	){
	
	this.RootElement = DOM.c(
		controller, 
		"FORM", 
		{
			"id":(id) ? id : "",
			"name":(name) ? name : "",
			"class":"form"
		},
		appendTo
	);
	
	if (className){
		DOM.addClass(this.RootElement, className);
	}
	
	this.RootElement.InputControls = [];
	
	// to be overloaded
	this.RootElement.GetURL = null;
	this.RootElement.PostURL = null;
	
	// Methods
	
	this.RootElement.Get = function(guid){
	
		// clear all the errors
		
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		// set form to "working" 
		
		DOM.addClass(this, "working");
		
		// Load
		
		AJAXHelper.Post(
			this.GetURL, 
			"{guid:'" + guid + "'}",
			"json",
			"get-complete", 
			this
		);
		
	}

	this.RootElement.GetComplete = function(e){
	
		console.dir(e.detail.data[0]);
		
		for (var p in e.detail.data[0]){ // for each property in the data
			
			for (var i = 0; i < this.InputControls.length; i++){ // loop each input control
				
				if (this.InputControls[i].Inputs[0].name == p){
					
					this.InputControls[i].PushData(e.detail.data[0][p]);
					
				}
			}
			
		}
		
		DOM.removeClass(this, "working");
		
	}
	
	this.RootElement.Post = function(){
	
		// move into view
		DOM.scrollTo(this);
		
		// clear all the errors
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		switch(this.PostType){
			case "json":
			case "xml":
				break;
			default:
				alert("Error: PostType is invalid. [Class.Method: _Form.Post]");
				break;
		}
		
		// set form to "working" 
		DOM.addClass(this, "working");
		
		// post
		AJAXHelper.Post(
			this.PostURL, 
			this.GetData(),
			this.PostType,
			"post-complete", 
			this
		);
		
	}

	this.RootElement.PostComplete = function(e){
		
	    // set the ReturnData so it's accessible from calling code
	    this.ReturnData = e.detail.d;

        // process the return
		switch(this.PostType){
			case "json":
				switch(e.detail.d.status.code){
					case 1:
					case "1":
						this.DispatchSuccess();
						break;
					case 0:
					case "0":						
						FormHelper.DoErrors(this.InputControls, e.detail.d.status.messages);
						DOM.removeClass(this, "working");
						break;
				}
				
				break;
			
			case "xml":
			
				switch(InquisiqXMLReader.GetStatus(e.detail)){
					case "success":
						this.DispatchSuccess();
						break;
					case "fail":
						FormHelper.DoErrors(this.InputControls, InquisiqXMLReader.GetStatusMessagesAsJSON(e.detail));
						DOM.removeClass(this, "working");
						break;
				}
				
				break;
		
		}
		
	}

	this.RootElement.Cancel = function(){
	
		try{
			this.CancelEventTarget.dispatchEvent(
				new Event(
					this.CancelEventName
				)
			);
		} catch(e){}
		
	}

	this.RootElement.DispatchSuccess = function(){
	
		// if no event set, remove working class and exit
		
		try{
			this.SuccessEventTarget.dispatchEvent(
				new CustomEvent(
					this.SuccessEventName
				)
			);
		} catch(e){
			console.dir(e);
			console.log("Class:_Form; Method:DispatchSuccess :: No dispatch target or event set.");
		}
		
	}
	
	this.RootElement.SetSuccessEvent = function(eventName, eventTarget){
	
		this.SuccessEventTarget = eventTarget;
		this.SuccessEventName = eventName;
		
	}
	
	this.RootElement.SetCancelEvent = function(eventName, eventTarget){
	
		this.CancelEventTarget = eventTarget;
		this.CancelEventName = eventName;
		
	}

	this.RootElement.PushData = function(){
		
	}
	
	this.RootElement.GetData = function(){

		var s = "";
		
		s = s + "{";
		
		for (var i = 0; i < this.InputControls.length; i++){
			
			if (i > 0){
				
				s = s + ",";
				
			}
			
			s = s + this.InputControls[i].GetData();
			
		}
		
		s = s + "}";
		
		return s;
		
	}

	// style to modify the working caption
	
	this.RootElement.StyleSheet = DOM.c(
		null, 
		"style", 
		{
			"type":"text/css"
		},
		document.head, 
		""
		);
	
	this.RootElement.StyleSheet.sheet.insertRule(".form.working::after{content: \"" + workingCaption + "\";", 0);
		
	// Event Listeners
	
	this.RootElement.addEventListener(
		"post-complete",
		function(e){
			this.PostComplete(e);
		}
	);
	
	this.RootElement.addEventListener(
		"get-complete",
		function(e){
			this.GetComplete(e);
		}
	);
	
}










	



