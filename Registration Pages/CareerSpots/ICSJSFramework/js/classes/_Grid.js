/**

Class		: Grid
Argument	: identifier (string) 
Desc		: Creates a TABLE element with methods attached (to the TABLE element).
			: DO NOT add methods directly to this class; they should be attached to the DOM element (table)
			
-----

Property: 	Table
Desc:		The HTML table.

Property: 	Table.THead
Desc: 		The HTML THEAD element.

Property: 	Table.TBody
Desc: 		The HTML TBODY element.

-----

Method		: Initialize
Atrguments	: E (HTML element) the element that is being initialized.
			  row (HTML element) the parent row if the element is a child element of a TR.
Desc		: Executed for the grid and cascaded to all child elements on initialization and when added.
			  Sets the this.Grid property to point to the Grid object so that sub-elements can easily call Grid methods.
			  If E is a TR element, adds GetCellByIndex and RemoveRow methods to the TR for ease of use.
			  If E is a child element of a TR, sets the this.Row property to point to the TR element so that cells can refer to their parent row easily.

Method		: AppendRow
Argument	: row (HTML row element)
Desc		: Appends the row to the end of the TABLE.

Method		: ReplaceRowByIdentifier
Argument	: row (HTML container element)
			  identifier (string)
Desc		: Replaces the row specified by the identifier with the row provided.

Method		: InsertRowAtIndex
Argument	: row (HTML container element)
			  index (number)
Desc		: Inserts the row provided at the index position.

Method		: OnBeforeRowAdd
Desc		: Available to be overloaded. Executed before row any add. When overloading, return FALSE to CANCEL the row add

Method		: OnAfterRowAdd
Desc		: Available to be overloaded. Executed after any row add.

Method		: OnAfterRowRemove
Desc		: Available to be overloaded. Executed after any row remove.

Method		: RemoveAllRows
Desc		: Inserts the row provided at the index position.

Method		: RemoveRowAtIndex
Arguments	: index (number)
Desc		: Removes the row at the specified index position.

Method		: RemoveRowsWithIdentifiers
Arguments	: list (string - comma separated list) 
Desc		: Removes all rows whose identifier is in the list.

Method		: RemoveRowWithIdentifier
Arguments	: identifier (string)
Desc		: Removes the row specified by the identifier

Method		: ToggleSelectAll
Arguments	: input (HTML input element)
Desc		: Toggles the selector for all rows based on the checked state of the provided input argument.

Method		: GetSelectedIdentifiers
Desc		: Returns a comma separated list of the identifiers for all selected rows.

Method		: GetAllIdentifiers
Desc		: Returns a comma separated list of all identifiers.

Method		: GetRowByIndex
Arguments	: index (number)
Desc		: Returns the row element (TR) found at the index position provided.

Method		: GetRowByIdentifier
Arguments	: identifier (string)
Desc		: Returns the row element (TR) with the specified identifier.

Method		: RowCount
Desc		: Returns the number of rows in the table.

Method		: GetRowIndexByIdentifier
Arguments	: identifier (string)
Desc		: Returns the index position of the row with the specified identifier.

Method		: InsertNoneRowIfNeeded
Desc		: Creates and inserts the "no results" row only if the table is empty.

Method		: RemoveNoneRow
Desc		: Removes the "none" row if it exists.

Method		: SortByColumn
Arguments	: index (number)
			  isAscending (boolean)
Desc		: Orders the table rows by the values contained within the column at the index position specified in ascending or descending order as specified.

Method		: UpdateCounters
Desc		: Updates the row counters (the 1., 2., 3., etc that can appear within each row for ordering.)

*/

Grid = function(
	creator,
	identifier,
	appendTo
	){
	
	this.Parent = creator;
	this.ObjectType = "Grid";
	
	this.Table = DOM.c(
		this, 
		"table", 
		{
			"name": (identifier) ? identifier : "",
			"class": "grid"
		},
		appendTo
	);
	
	this.Table.STATES = {
		READY	: "ready",
		WORKING	: "working"
	}
	
	this.Table.THead = DOM.c(
		this, 
		"thead", 
		null,
		this.Table
	);
	
	this.Table.TBody = DOM.c(
		this, 
		"tbody", 
		null, 
		this.Table
	);
	
	
	
	/*
	table.Identifier 	= table.id;
	table.TBody 		= table.getElementsByTagName("tbody")[0];
	table.THead 		= table.getElementsByTagName("thead")[0];
	table.Phrases		= phrasesObject;
	
	// create the sort button, both need to be defined or sorts will not occur.
	if (sortColumn != null && isAscending != null){
		
		table.SortedColumn 	= sortColumn;
		table.isAscending 	= isAscending;
		table.SortArrow 	= document.createElement("button");
		table.SortArrow.type = "button";
		
		if (table.isAscending == true){
			table.THead.getElementsByTagName("th")[table.SortedColumn].className = "column-sortable open";
			table.SortArrow.className = "toggle toggle-close";
		} else {
			table.THead.getElementsByTagName("th")[table.SortedColumn].className = "column-sortable closed";
			table.SortArrow.className = "toggle toggle-open";
		}
		
		table.THead.getElementsByTagName("th")[table.SortedColumn].appendChild(table.SortArrow);
	}
	*/
	this.Table.AppendRow = function(row){
		
		if (this.OnBeforeRowAdd() != true){
			
			return false;
			
		}
		
		this.RemoveNoneRow();
		this.TBody.appendChild(row);
		this.Initialize(row);
		this.SortByColumn(this.SortedColumn, this.isAscending);
		this.UpdateCounters();
		this.OnAfterRowAdd();
		
	}
	
	this.Table.ReplaceRowByIdentifier = function(row, identifier){
		
		if(!this.GetRowByIdentifier(identifier)){ // adding the row, not replacing it
			
			this.AppendRow(row);
			return true;
		}
		
		this.TBody.insertBefore(row, this.GetRowByIdentifier(identifier).nextSibling);
		this.RemoveRowWithIdentifier(identifier); // will only remove the first instance (the old one)
		this.Initialize(row);
		this.SortByColumn(this.SortedColumn, this.isAscending);
		this.UpdateCounters();
		this.OnAfterRowAdd();
		
	}
	
	this.Table.InsertRowAtIndex = function(row, index){
		
		if (index <= 0){ index = 0; }
		
		if (index >= this.RowCount()){	
			
			this.AppendRow(row);
			return true;
			
		}
		
		if (this.OnBeforeRowAdd() != true){
			
			return false;
			
		}
		
		this.RemoveNoneRow();
		this.TBody.insertBefore(row, this.GetRowByIndex(index));
		this.Initialize(row);
		this.SortByColumn(this.SortedColumn, this.isAscending);
		this.UpdateCounters();
		this.OnAfterRowAdd();
		
	}
	
	this.Table.OnBeforeRowAdd = function(){
	
		// available to be overloaded
		// when overloading, return false to CANCEL the row add
		return true;
		
	}
	
	this.Table.OnAfterRowAdd = function(){
		
		// available to be overloaded
		
	}
	
	this.Table.OnAfterRowRemove = function(){
		
		// available to be overloaded
		
	}
	
	this.Table.RemoveAllRows = function(){
		
		while(this.TBody.childNodes.length > 0){
			this.TBody.removeChild(this.TBody.childNodes[0]);
		}
		
	}
	
	this.Table.RemoveRowAtIndex = function(index){
			
		this.TBody.removeChild(this.GetRowByIndex(index));
		this.InsertNoneRowIfNeeded();
		this.UpdateCounters();
		
	}
	
	this.Table.RemoveRowsWithIdentifiers = function(str){
	
		var a = str.split(",");
		
		for (var i = 0; i < a.length; i++){
			
			this.RemoveRowWithIdentifier(a[i]);
			
		}
		
		this.UpdateCounters();
		
	}
		
	this.Table.RemoveRowWithIdentifier = function(identifier){
		
		if(this.GetRowByIdentifier(identifier)){
			this.TBody.removeChild(this.GetRowByIdentifier(identifier));
		}
		
		if(identifier != "none"){
			this.InsertNoneRowIfNeeded();
		}
		
		this.UpdateCounters();
		
	}
	
	this.Table.ToggleSelectAll = function(input){
		
		var inputs = this.TBody.getElementsByTagName("input");
		
		for (var i = 0; i < inputs.length; i++){
			if (!inputs[i].disabled){
				inputs[i].checked = input.checked;
			}
		}
		
	}
	
	this.Table.GetSelectedIdentifiers = function(){
		
		var s 		= "";
		var inputs 	= this.getElementsByTagName("input");
		
		for(var i = 0; i < inputs.length; i++){
			
			if (inputs[i].checked == true){
				
				if (s.length != 0){ s = s + ",";}
				
				s = s + inputs[i].value;
				
			}
		}
		
		return s;
		
	}
	
	this.Table.GetAllIdentifiers = function(){
		
		var s 		= "";
		
		for (var i = 0; i < this.RowCount(); i++){
			
			if (s.length != 0){ s = s + ",";}
			
			if (this.GetRowByIndex(i).getAttribute("identifier")){
			
				if (this.GetRowByIndex(i).getAttribute("identifier") != "none"){
					
					s = s + this.GetRowByIndex(i).getAttribute("identifier");
					
				}
			
			}
			
		}
		
		return s;
		
	}
	
	this.Table.GetRowByIndex = function(index){
		
		return this.TBody.getElementsByTagName("tr")[index];
		
	}
	
	this.Table.GetRowByIdentifier = function(identifier){
		
		var trs = this.TBody.getElementsByTagName("tr");
		
		for (var i = 0; i < trs.length; i++){
		
			if (trs[i].getAttribute("identifier") == identifier){
				
				return trs[i];
				
			}
		}
		
	}
	
	this.Table.RowCount = function(){
		
		return this.TBody.getElementsByTagName("TR").length;
		
	}
	
	this.Table.GetRowIndexByIdentifier = function(identifier){
		
		for (var i = 0; i < this.RowCount(); i ++){
			
			if (this.GetRowByIndex(i).getAttribute("identifier") == identifier){
				
				return i;
				
			}
				
		}
		
	}
	
	this.Table.InsertNoneRowIfNeeded = function(){
		
		if (this.RowCount() > 0){
		
			return true;
			
		}
		
		var tr = document.createElement("tr");
			tr.setAttribute("identifier", "none");
			tr.id = this.Identifier + "-none";
			
		var td = document.createElement("td");
			td.className = "none";
			td.colSpan = 100;
			try {
				td.appendChild(document.createTextNode(this.Phrases.Get("no records found")));
			} catch(e){
				td.appendChild(document.createTextNode("-"));
			}
			
			
		tr.appendChild(td);
		this.TBody.appendChild(tr);
		
	}
	
	this.Table.RemoveNoneRow = function(){
		
		this.RemoveRowWithIdentifier("none");
		
	}
	
	this.Table.ClickSort = function(index){
		
		if (this.SortedColumn == index){
			this.SortByColumn(index, !this.isAscending);
		}else{
			this.SortByColumn(index, true);
		}
		
	}
	
	this.Table.SortByColumn = function(index, isAscending){
			
		if (index == null){ return true; }
		
		var sortArray = new Array();
		
		// for each row, grab the n-index cell value
		
		for (var i = 0; i < this.RowCount(); i++){
			
			var tr = this.GetRowByIndex(i);
			var td = tr.GetCellByIndex(index);
			
			if (td){
				if (td.textContent){
					sortArray.push(new Array(td.textContent.toLowerCase(), tr));
				} else{
					if (td.innerText){
						sortArray.push(new Array(td.innerText.toLowerCase(), tr));
					}else{
						sortArray.push(new Array("", tr));
					}
				}
			}
		}
		
		// sort
		
		if (isAscending == true){
			sortArray.sort(
				function grid_sort_ascending(a,b){
					(a[0] > b[0]) ? i = 1 : (b[0] > a[0]) ? i = -1 : i = 0 ;
					return i;
				}
			);
		} else {
			sortArray.sort(
				function grid_sort_descending(a,b){
					(a[0] > b[0]) ? i = -1 : (b[0] > a[0]) ? i = 1 : i = 0 ;
					return i;
				}
			);
		}
		
		// remove the sort arrow
		this.THead.getElementsByTagName("th")[this.SortedColumn].className = "column-sortable";
		this.THead.getElementsByTagName("th")[this.SortedColumn].removeChild(this.SortArrow);
		
		// set properties
		this.SortedColumn 	= index;
		this.isAscending 	= isAscending;
		
		// remove all
		this.RemoveAllRows();
		
		// append the rows
		for (var i = 0; i < sortArray.length; i++){
			this.TBody.appendChild(sortArray[i][1]);
		}
		
		// append the sort arrow
		if (table.isAscending == true){
			table.THead.getElementsByTagName("th")[this.SortedColumn].className = "column-sortable open";
			table.SortArrow.className = "toggle toggle-close";
		} else {
			table.THead.getElementsByTagName("th")[this.SortedColumn].className = "column-sortable closed";
			table.SortArrow.className = "toggle toggle-open";
		}
		
		table.THead.getElementsByTagName("th")[index].appendChild(table.SortArrow);
		
	}
	
	this.Table.UpdateCounters = function(){
		
		for (i = 0; i < this.RowCount(); i++){
		
			var row = this.GetRowByIndex(i);
			var s = row.getElementsByTagName("span");
			
			for (var j = 0; j < s.length; j++){
				if (s[j].className == "counter"){
					s[j].innerHTML = i + 1 + ".";
				}
			}
			
		}
			
	}
	
	this.Table.SetState = function(state){
		
		switch(state){
			case this.STATES.WORKING:
				DOM.addClass(this, "working");
				break;
			default:
				DOM.removeClass(this, "working");
				break;
		}
	}
	
	this.Table.Initialize = function(e, row){
		
		if (e != this){ e.Grid = this; }
	
		if (!row){
			//nothing
		}else {
			e.Row = row;
		}
		
		// tr - GetCell method
		
		if (e.tagName == "TR"){
			
			e.GetCellByIndex = function(index){
				
				return this.getElementsByTagName("td")[index];
				
			}
			
			e.RemoveRow = function(){
			
				try{
					if (confirm(this.Grid.Phrases.Get("remove row confirmation")))
						this.Grid.RemoveRowWithIdentifier(this.getAttribute("identifier"));
				} catch(e){
					this.Grid.RemoveRowWithIdentifier(this.getAttribute("identifier"));
				}
				
			}
			
			row = e; // we have hit a row. pass it down.
			
		}
		
		for (var i = 0; i < e.childNodes.length; i++){
			
			this.Initialize(e.childNodes[i], row);
			
		}
		
	}
	
	this.Table.Initialize(this.Table);
	
}
