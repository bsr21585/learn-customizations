function RemoveSpacesFromInput(inputElement) {				
		inputElement.value = inputElement.value.replace(" ", "");
}

RegistrationFormControl = function(appendTo){
	
	this.ObjectType = "CareerSpotsUserRegistrationForm";
	this.Container 	= appendTo;
	
	this.RootElement = new Form(
		this, 
		null, 
		null, 
		"registration-form",
		null,
		"Please wait while we create your CareerSpots LMS user account."
	).RootElement;
		
	this.RootElement.PostURL 	= "/static/CareerSpots/svc/RegistrationForm.asmx/Register";
	this.RootElement.PostType = "json";
	this.RootElement.ReturnData = null;
	
	// Create the instructions container.
	DOM.c(
		null, 
		"DIV", 
		{
			"id":"registrationinstructions",			
			"class":"registrationinstructions"
		}, 
		this.RootElement, 
		"Fill out the form fields below to register for an account."
	);	
	
	// Create the groupings.
	
	this.NameGroup 					= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.EmailGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.EmailConfirmGroup 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.PasswordGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.YearInSchoolGroup 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.MajorGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.InstitutionGroup 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.InstructorLastNameGroup 	= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.AccessCodeGroup 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.ButtonGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	
	// Create the inputs.
	
	this.FirstNameInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"firstname", 			"name":"firstname", 			"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.LastNameInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"lastname", 			"name":"lastname", 				"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.EmailInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"email", 				"name":"email", 				"type":"text", 		"maxlength":512, "class":"InputLong"});
	this.EmailConfirm 				= DOM.c(this.RootElement, "INPUT", 	{"id":"emailConfirm", 		"name":"emailConfirm", 			"type":"text", 		"maxlength":512, "class":"InputLong", "autocomplete":"new-password"});
	this.PasswordInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"password", 			"name":"password", 				"type":"password", 	"maxlength":255, "class":"InputNormal"});
	this.ConfirmInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"confirm", 			"name":"confirm", 				"type":"password", 	"maxlength":255, "class":"InputNormal"});
	this.YearInSchoolInput 			= DOM.c(this.RootElement, "SELECT", {"id":"yearinschool", 		"name":"yearinschool", 			"type":"select", 	"maxlength":255, "class":"InputNormal"});
	this.MajorInput 				= DOM.c(this.RootElement, "SELECT", {"id":"major", 				"name":"major", 				"type":"select", 	"maxlength":255, "class":"InputNormal"});
	this.InstitutionInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"institution", 		"name":"institution", 			"type":"text", 		"maxlength":255, "class":"InputLong"});
	this.InstructorLastNameInput 	= DOM.c(this.RootElement, "INPUT", 	{"id":"instructorlastname", "name":"instructorlastname", 	"type":"text", 		"maxlength":255, "class":"InputLong", "onkeyup":"RemoveSpacesFromInput(this);"});
	this.AccessCodeInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"accesscode", 		"name":"accesscode", 			"type":"text", 		"maxlength":255, "class":"InputNormal"});
	
	this.SaveButton = DOM.c(
		this, 
		"INPUT",
		{
			"type"	: "button",
			"id"	: "save",
			"name"	: "save", 
			"class"	: "default",
			"value"	: "Create Account"
		}
	)
	
	this.SaveButton.onclick = function(){
		this.Controller.RootElement.Post();
	}
	
	// Create the controls.
	
	this.FirstNameControl 				= new FormInputControl("First Name",				"firstname",			this.NameGroup,					true);
	this.LastNameControl 				= new FormInputControl("Last Name",					"lastname",				this.NameGroup,					true);
	this.EmailControl 					= new FormInputControl("Email",						"email",				this.EmailGroup, 				true);	
	this.EmailConfirmControl 			= new FormInputControl("Confirm Email",				"emailConfirm",			this.EmailConfirmGroup, 		true);	
	this.PasswordControl 				= new FormInputControl("Password",					"password",				this.PasswordGroup,				true);
	this.ConfirmControl 				= new FormInputControl("Confirm Password",			"confirm",				this.PasswordGroup,				true);
	this.YearInSchoolControl 			= new FormInputControl("Year in School", 			"yearinschool", 		this.YearInSchoolGroup,			true);
	this.MajorControl 					= new FormInputControl("Major", 					"major", 				this.MajorGroup,				true);
	this.InstitutionControl				= new FormInputControl("Institution",				"institution",			this.InstitutionGroup,			true);
	this.InstructorLastNameControl		= new FormInputControl("Instructor's Last Name",	"instructorlastname",	this.InstructorLastNameGroup,	false);
	this.AccessCodeControl				= new FormInputControl("Access Code",				"accesscode",			this.AccessCodeGroup,			true);
	
	// Log the controls into the Form object.
	// Each control represents a parameter value that will be sent to/received from the server.
	
	this.RootElement.InputControls.push(this.FirstNameControl);
	this.RootElement.InputControls.push(this.LastNameControl);
	this.RootElement.InputControls.push(this.EmailControl);	
	this.RootElement.InputControls.push(this.EmailConfirmControl);	
	this.RootElement.InputControls.push(this.PasswordControl);
	this.RootElement.InputControls.push(this.ConfirmControl);
	this.RootElement.InputControls.push(this.YearInSchoolControl);
	this.RootElement.InputControls.push(this.MajorControl);
	this.RootElement.InputControls.push(this.InstitutionControl);
	this.RootElement.InputControls.push(this.InstructorLastNameControl);
	this.RootElement.InputControls.push(this.AccessCodeControl);
	
	// Attach items to the select inputs.
	
	this.YearInSchoolInput.AddItem("Freshman", "Freshman", true);
	this.YearInSchoolInput.AddItem("Sophomore", "Sophomore", false);
	this.YearInSchoolInput.AddItem("Junior", "Junior", false);
	this.YearInSchoolInput.AddItem("Senior", "Senior", false);
	this.YearInSchoolInput.AddItem("Graduate Student", "Graduate Student", false);	
	this.YearInSchoolInput.AddItem("Other", "Other", false);
	
	this.MajorInput.AddItem("Accounting", "Accounting", true);
	this.MajorInput.AddItem("Administrative and Clerical", "Administrative and Clerical", false);
	this.MajorInput.AddItem("Advertising/Public Relations", "Advertising/Public Relations", false);
	this.MajorInput.AddItem("Agricultural Sciences", "Agricultural Sciences", false);
	this.MajorInput.AddItem("Anthropology/Archaeology", "Anthropology/Archaeology", false);
	this.MajorInput.AddItem("Architecture", "Architecture", false);
	this.MajorInput.AddItem("Arts Management", "Arts Management", false);
	this.MajorInput.AddItem("Athletic Training", "Athletic Training", false);
	this.MajorInput.AddItem("Biochemistry and Molecular Biology", "Biochemistry and Molecular Biology", false);
	this.MajorInput.AddItem("Biology", "Biology", false);
	this.MajorInput.AddItem("Building and Construction", "Building and Construction", false);
	this.MajorInput.AddItem("Business/Management", "Business/Management", false);
	this.MajorInput.AddItem("Chemical Engineering", "Chemical Engineering", false);
	this.MajorInput.AddItem("Chemistry", "Chemistry", false);
	this.MajorInput.AddItem("Communications", "Communications", false);
	this.MajorInput.AddItem("Computer Science", "Computer Science", false);
	this.MajorInput.AddItem("Criminal Justice", "Criminal Justice", false);
	this.MajorInput.AddItem("Culinary Arts and Food Service", "Culinary Arts and Food Service", false);
	this.MajorInput.AddItem("Dental", "Dental", false);
	this.MajorInput.AddItem("Digital Media, Arts, and Technology", "Digital Media, Arts, and Technology", false);
	this.MajorInput.AddItem("Earth and Environmental Sciences", "Earth and Environmental Sciences", false);
	this.MajorInput.AddItem("Economics", "Economics", false);
	this.MajorInput.AddItem("Education", "Education", false);
	this.MajorInput.AddItem("Engineering", "Engineering", false);
	this.MajorInput.AddItem("English", "English", false);
	this.MajorInput.AddItem("Film, Video, and Photography", "Film, Video, and Photography", false);
	this.MajorInput.AddItem("Finance", "Finance", false);
	this.MajorInput.AddItem("Food and Nutrition", "Food and Nutrition", false);
	this.MajorInput.AddItem("Foreign Languages", "Foreign Languages", false);
	this.MajorInput.AddItem("General Studies", "General Studies", false);
	this.MajorInput.AddItem("Global and International Studies", "Global and International Studies", false);
	this.MajorInput.AddItem("Graphic Design", "Graphic Design", false);
	this.MajorInput.AddItem("Health Professions", "Health Professions", false);
	this.MajorInput.AddItem("History", "History", false);
	this.MajorInput.AddItem("Hospitality Management", "Hospitality Management", false);
	this.MajorInput.AddItem("Humanities", "Humanities", false);
	this.MajorInput.AddItem("Industrial Engineering", "Industrial Engineering", false);
	this.MajorInput.AddItem("Information Sciences and Technology", "Information Sciences and Technology", false);
	this.MajorInput.AddItem("International Relations", "International Relations", false);
	this.MajorInput.AddItem("Journalism", "Journalism", false);
	this.MajorInput.AddItem("Kinesiology", "Kinesiology", false);
	this.MajorInput.AddItem("Legal Studies", "Legal Studies", false);
	this.MajorInput.AddItem("Liberal Arts", "Liberal Arts", false);
	this.MajorInput.AddItem("Management Information Systems", "Management Information Systems", false);
	this.MajorInput.AddItem("Marketing", "Marketing", false);
	this.MajorInput.AddItem("Mathematics", "Mathematics", false);
	this.MajorInput.AddItem("Mechanical Engineering", "Mechanical Engineering", false);
	this.MajorInput.AddItem("Medical Assistant/Technician", "Medical Assistant/Technician", false);
	this.MajorInput.AddItem("Music", "Music", false);
	this.MajorInput.AddItem("Nursing", "Nursing", false);
	this.MajorInput.AddItem("Other", "Other", false);
	this.MajorInput.AddItem("Performing Arts", "Performing Arts", false);
	this.MajorInput.AddItem("Pharmacy", "Pharmacy", false);
	this.MajorInput.AddItem("Philosophy", "Philosophy", false);
	this.MajorInput.AddItem("Physics", "Physics", false);
	this.MajorInput.AddItem("Political Science", "Political Science", false);
	this.MajorInput.AddItem("Protective Services", "Protective Services", false);
	this.MajorInput.AddItem("Psychology", "Psychology", false);
	this.MajorInput.AddItem("Public Policy", "Public Policy", false);
	this.MajorInput.AddItem("Rehabilitation and Therapy", "Rehabilitation and Therapy", false);
	this.MajorInput.AddItem("Religious Studies/Theology", "Religious Studies/Theology", false);
	this.MajorInput.AddItem("Risk Management", "Risk Management", false);
	this.MajorInput.AddItem("Science", "Science", false);
	this.MajorInput.AddItem("Sociology", "Sociology", false);
	this.MajorInput.AddItem("Sports Management", "Sports Management", false);
	this.MajorInput.AddItem("Trades and Personal Services", "Trades and Personal Services", false);
	this.MajorInput.AddItem("Undecided", "Undecided", false);
	this.MajorInput.AddItem("Veterinary Studies", "Veterinary Studies", false);
	this.MajorInput.AddItem("Visual Arts", "Visual Arts", false);
	this.MajorInput.AddItem("Women's Studies", "Women's Studies", false);

	// Attach the inputs to the controls.
		
	this.FirstNameControl.AddInput(this.FirstNameInput);
	this.LastNameControl.AddInput(this.LastNameInput);
	this.EmailControl.AddInput(this.EmailInput);	
	this.EmailConfirmControl.AddInput(this.EmailConfirm);
	this.PasswordControl.AddInput(this.PasswordInput);
	this.ConfirmControl.AddInput(this.ConfirmInput);	
	this.YearInSchoolControl.AddInput(this.YearInSchoolInput);
	this.MajorControl.AddInput(this.MajorInput);
	this.InstitutionControl.AddInput(this.InstitutionInput);
	this.InstructorLastNameControl.AddInput(this.InstructorLastNameInput);
	this.AccessCodeControl.AddInput(this.AccessCodeInput);
	
	// Buttons
	
	this.ButtonGroup.appendChild(this.SaveButton);
	
	// Append the form to the container.
	
	try{
		this.Container.appendChild(this.RootElement);
	}catch(e){ }
	
	// Client-side error checking.
	
	this.RootElement.UpdatePasswordConfirm = function(){
		if(this.Controller.ConfirmInput.value.length > 0){
			if (this.Controller.ConfirmInput.value == this.Controller.PasswordInput.value){
				this.Controller.ConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.ConfirmControl.HasErrors()){
					this.Controller.ConfirmControl.AddError("Confirmation does not match.");
				}
			}
		}else{
			this.Controller.ConfirmControl.ClearErrors();
		}
	}
	
	this.RootElement.UpdateEmailConfirm = function(){
		if(this.Controller.EmailConfirm.value.length > 0){
			if (this.Controller.EmailConfirm.value == this.Controller.EmailInput.value){
				this.Controller.EmailConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.EmailConfirmControl.HasErrors()){
					this.Controller.EmailConfirmControl.AddError("Email confirmation does not match.");
				}
			}
		}else{
			this.Controller.EmailConfirmControl.ClearErrors();
		}
	}
	
	this.PasswordInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	this.ConfirmInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	
	this.EmailInput.onkeyup = function(e){
		this.Controller.UpdateEmailConfirm();
	}
	this.EmailConfirm.onkeyup = function(e){
		this.Controller.UpdateEmailConfirm();
	}
	
	// Set Success Actions
	
	this.RootElement.addEventListener(
		"post-successful", 
		function(){
			this.Controller.CompleteRegistration();
		}
	);
	
	this.RootElement.SetSuccessEvent("post-successful", this.RootElement);
	
	// Overloads
	
	this.RootElement.Post = function(){
	
		// move into view
		
		DOM.scrollTo(this);
		
		// clear all the errors
		
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		// set form to "working" 
		DOM.addClass(this, "working");		
		
		if(!this.Controller.DoEasyErrors()){
			DOM.removeClass(this, "working");
			return false;
		}
		
		// post		
		AJAXHelper.Post(
			this.PostURL, 
			this.Controller.RootElement.GetData(),
			this.PostType,
			"post-complete", 
			this
		);
		
	}
}


RegistrationFormControl.prototype.DoEasyErrors = function(){
	
	var s = true;
	
	if(this.FirstNameInput.value.length == 0){
		s = false;
		this.FirstNameControl.AddError("First Name is required.")
	}
	
	if(this.LastNameInput.value.length == 0){
		s = false;
		this.LastNameControl.AddError("Last Name is required.")
	}
	
	if(this.EmailInput.value.length == 0){
		s = false;
		this.EmailControl.AddError("Email is required.")
	}
	
	if(this.EmailConfirm.value != this.EmailInput.value){
		s = false;
		this.EmailConfirmControl.AddError("Email confirmation does not match.")
	}	
	
	if(this.PasswordInput.value.length == -0){
		s = false;
		this.PasswordControl.AddError("Password is required.")
	}
	
	if(this.ConfirmInput.value != this.PasswordInput.value){
		s = false;
		this.ConfirmControl.AddError("Confirmation does not match.")
	}	
	
	if(this.InstitutionInput.value.length == 0){
		s = false;
		this.InstitutionControl.AddError("Institution is required.")
	}
	
	if(this.AccessCodeInput.value.length == 0){
		s = false;
		this.AccessCodeControl.AddError("Company is required.")
	}	
		
	return s;
	
}


RegistrationFormControl.prototype.CompleteRegistration = function () {
    
    // if we got an SSO token, redirect; otherwise, just proceed with displaying the success message
    if (this.RootElement.ReturnData.token != null) {
        window.location.href = "https://careerspots.asentialms.com/_util/DoSSO.aspx?token=" + this.RootElement.ReturnData.token;
    }
    else {
        this.RootElement.addEventListener(
            "animationend",
            function () {
                this.Controller.Container.appendChild(
                    new UserRegistrationCompletedControl(
                        this.Controller.FirstNameControl.GetValue(),
                        this.Controller.EmailControl.GetValue()
                        )
                );
                DOM.scrollTo(this.Controller.Container);
                this.Controller.Container.removeChild(this);
            }
        );

        DOM.addClass(this.RootElement, "vanish");
    }
}


UserRegistrationCompletedControl = function(
	name,
	email
	){
	
	this.RootElement = DOM.c(
		this, 
		"DIV",
		{
			"class":"info-control evolve"
		}
	);
	
	this.Something = DOM.c(
		null, 
		"DIV", 
		{
			"class":"primary"
		}, 
		this.RootElement, 
		"Thank you for registering for a CareerSpots LMS account, " + name + ". Your LMS username is: " + email
		+ ". You may log into the LMS by going to "
	);
	
	DOM.c(
		null, 
		"A", 
		{
			"href":"https://careerspots.asentialms.com"			
		}, 
		this.Something, 
		"https://careerspots.asentialms.com"
	);
	
	return this.RootElement;
	
}

