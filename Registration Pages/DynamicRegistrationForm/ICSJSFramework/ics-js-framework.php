<?php
/**
	* Plugin Name: 		[ICS] JavaScript Framework
	* Description: 		Includes all the file necessary for the ICS JS Framework in the page headers.
	* Plugin URI:       http://www.icslearninggroup.com
	* Author:           ICS Learning Group
	* Author URI:		tech@icslearninggroup.com
*/
add_action( 'wp_head', 'ics_js_header_scripts' );
function ics_js_header_scripts(){
  ?>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/script.js" type="text/javascript"></script>
  
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/util/AJAXHelper.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/util/DOM.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/util/FormHelper.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/util/InputHelper.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/util/PageOverlay.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/util/InquisiqXMLReader.js" type="text/javascript"></script>
  
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_AlertModal.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_Carousel.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_Form.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_FormInputControl.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_Grid.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_Modal.js" type="text/javascript"></script>
  <script src="<?php echo plugin_dir_url( __FILE__ ) ?>js/classes/_TabsControl.js" type="text/javascript"></script>
  
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/AlertModal.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Carousel.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Forms.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Grid.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Inputs.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Modal.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/PageOverlay.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Style.css" type="text/css" media="all">
  <link rel="stylesheet" href="<?php echo plugin_dir_url( __FILE__ ) ?>css/Tabs.css" type="text/css" media="all">
  
  <?php
}

?>