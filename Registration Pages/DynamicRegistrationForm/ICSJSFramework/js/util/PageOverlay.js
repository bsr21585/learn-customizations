PageOverlay = function(){}

PageOverlay.On = function(){
	
	DOM.addClass(DOM.get("page-inner"), "blur");
	DOM.addClass(DOM.get("pageoverlay"), "on");
	
}

PageOverlay.Off = function(){
	
	DOM.removeClass(DOM.get("page-inner"), "blur");
	DOM.removeClass(DOM.get("pageoverlay"), "on");
	
}