AJAXHelper = function(){
		
}

AJAXHelper.Get = function(
	url, 
	dataType, 
	target
	){
	
	jQuery.ajax({
		"type": "GET",
		"url": url,
		"dataType": dataType,
		"success": function(response){target.OnGetSuccess(response);},
		"error": function(response){target.OnGetError(response);}
    });
	
}

AJAXHelper.Post = function(
	url, 
	data,
	dataType,
	eventName, 
	eventTarget
	){
	
	jQuery.ajax({
		"type": "POST",
		"url": url,
		"data": data,
		"contentType": "application/json; charset=utf-8",
		"dataType": dataType,
		"timeout": 0,
		"success": function (response) {
			//console.dir(response);
			console.log("AJAX Successful.");
			eventTarget.dispatchEvent(
				new CustomEvent(
					eventName, 	
					{detail:response} // dispatch our complete object(including status)
				)
			);
		},
		"error": function(response){
			//console.dir(response);
			try{ //.NET, webservice type error
				var errorAlert = new AlertModal(
					response.responseJSON.ExceptionType,
					response.responseJSON.Message,
					null,
					response.responseJSON.StackTrace
				);
				console.log("AJAX Failed. .NET-type error returned.");
			} catch(e){ // dumb errors
				console.log("AJAX Failed. Basic error returned.");
				var errorAlert = new AlertModal(
					response.statusCode, // title
					response.statusText, // heading
					null, // message
					null // details
				);
			}
		} // AJAX Error handling should go here.
		
	});
	
}
