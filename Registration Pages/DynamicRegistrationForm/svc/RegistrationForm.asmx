﻿<%@ WebService Language="C#" Class="DynamicRegistrationForm" %>

using System;
using System.IO;
using System.Collections.Generic;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Xml;

[WebService(Description = "Asentia Dynamic Registration Web Service", Namespace = "https://www.asentialms.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class DynamicRegistrationForm : WebService
{
    string PORTAL_NAME = String.Empty;
    readonly string API_KEY = "#c0v!d!gtr@in!n94p1!";
    readonly string[] courseCodesArr = new string[] { "001", "002", "003" };

    public DynamicRegistrationForm()
    {}

    #region Register
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ReturnObject Register(string firstname, string lastname, string email, string emailConfirm, string company, string username, string password, string confirm, string timezone, string portal)
    {
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

        string stepThroughLog = String.Empty;

        ReturnObject retObj = new ReturnObject();

        retObj.status.result = "success";
        retObj.status.code = "1";
        retObj.status.messages = new List<object>();
        retObj.token = null;

        PORTAL_NAME = portal;

        // REGISTER THE USER USING THE ASENTIA API
        try
        {
            // USER ACCOUNT REGISTRATION
            stepThroughLog += DateTime.Now.ToString() + " - User registration with API.\r\n";

            try
            { this._DoUserRegistration(firstname, lastname, email, company, username, password, timezone); }
            catch (WebException webEx) // RETRY 1 TIME
            { this._DoUserRegistration(firstname, lastname, email, company, username, password, timezone); }

            stepThroughLog += DateTime.Now.ToString() + " - User registration with API finished.\r\n";

            // GET THE REGISTERED USER'S ID AND ENROLL INTO COURSES IF THERE ARE COURSES TO ENROLL IN

            // GET THE USER ID
            int idUser = 0;

            stepThroughLog += DateTime.Now.ToString() + " - Getting user ID with API.\r\n";

            try
            { idUser = this._DoGetUserId(username); }
            catch (WebException webEx) // RETRY 1 TIME
            { idUser = this._DoGetUserId(username); }

            stepThroughLog += DateTime.Now.ToString() + " - Getting user ID with API finished.\r\n";

            if (idUser > 0)
            {
                if (courseCodesArr != null)
                {
                    // LOOP THROUGH COURSE CODES, GET COURSE IDS, AND ENROLL
                    for (int i = 0; i < courseCodesArr.Length; i++)
                    {
                        int idCourse = 0;

                        stepThroughLog += DateTime.Now.ToString() + " - Getting course ID with API.\r\n";

                        try
                        { idCourse = this._DoGetCourseId(courseCodesArr[i]); }
                        catch (WebException webEx) // RETRY 1 TIME
                        { idCourse = this._DoGetCourseId(courseCodesArr[i]); }

                        stepThroughLog += DateTime.Now.ToString() + " - Getting course ID with API finished.\r\n";

                        if (idCourse > 0)
                        {
                            // ENROLL IN THE COURSE
                            stepThroughLog += DateTime.Now.ToString() + " - Enrolling in course with API.\r\n";

                            try
                            { this._DoEnrollment(idUser, idCourse, timezone); }
                            catch (WebException webEx) // RETRY 1 TIME
                            { this._DoEnrollment(idUser, idCourse, timezone); }

                            stepThroughLog += DateTime.Now.ToString() + " - Enrolling in course with API finished.\r\n";
                        }
                    }
                }

                // LOG THE USER INTO ASENTIA AND GET SSO TOKEN FOR REDIRECT
                stepThroughLog += DateTime.Now.ToString() + " - Logging user into Asentia with API.\r\n";

                try
                { retObj.token = this._GetUserSSOToken(idUser); }
                catch (WebException webEx) // RETRY 1 TIME
                { retObj.token = this._GetUserSSOToken(idUser); }

                stepThroughLog += DateTime.Now.ToString() + " - Logging user into Asentia with API finished.\r\n";
            }
        }
        catch (WebException ex)
        {
            // write the exception to a log with the response
            if (ex.Response != null)
            {
                WebResponse errResp = ex.Response;

                using (Stream respStream = errResp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(respStream);
                    string text = reader.ReadToEnd();

                    this._WriteExceptionToLog(ex.Message, ex.StackTrace, text, stepThroughLog);
                }
            }
            else // write the exception to a log without the response
            {
                this._WriteExceptionToLog(ex.Message, ex.StackTrace, null, stepThroughLog);
            }

            // bubble up the exception
            throw new Exception("A fatal error occurred when contacting the user registration service. Please contact an administrator.");
        }
        catch (Exception ex)
        {
            retObj.status.result = "fail";
            retObj.status.code = "0";
            retObj.status.messages = new List<object>();

            string[] exceptionMessages = ex.Message.Split('|');

            foreach (string s in exceptionMessages)
            {
                MessageObject msgObj = new MessageObject();
                msgObj.identifier = "general";
                msgObj.text = s;
                retObj.status.messages.Add(msgObj);
            }
        }

        // return
        return retObj;
    }
    #endregion

    #region _DoUserRegistration
    /// <summary>
    /// Creates a user using the API.
    /// </summary>
    /// <param name="firstname">First Name</param>
    /// <param name="lastname">Last Name</param>
    /// <param name="email">Email</param>
    /// <param name="company">Company</param>
    /// <param name="username">Username</param>
    /// <param name="password">Password</param>
    /// <param name="timezone">Timezone ID</param>
    private void _DoUserRegistration(string firstname, string lastname, string email, string company, string username, string password, string timezone)
    {
        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key><![CDATA[" + API_KEY + "]]></key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<user id=\\\"0\\\">";
        xmlPayload += "<firstName><![CDATA[" + firstname + "]]></firstName>";
        xmlPayload += "<lastName><![CDATA[" + lastname + "]]></lastName>";
        xmlPayload += "<email><![CDATA[" + email + "]]></email>";
        xmlPayload += "<username><![CDATA[" + username + "]]></username>";
        xmlPayload += "<password><![CDATA[" + password + "]]></password>";
        xmlPayload += "<mustChangePassword>0</mustChangePassword>";
        xmlPayload += "<idTimezone><![CDATA[" + timezone + "]]></idTimezone>";
        xmlPayload += "<languageString>en-US</languageString>";
        xmlPayload += "<isActive>true</isActive>";
        xmlPayload += "<company><![CDATA[" + company + "]]></company>";
        xmlPayload += "</user>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + PORTAL_NAME + ".asentialms.com/_util/API/Default.asmx/SaveUser");

        // encode payload into bytes
        byte[] bytes;
        bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // throw an error if there is one
        if (statusNode.InnerText == "Error")
        { throw new Exception(descriptionNode.InnerText); }
    }
    #endregion


    #region _DoGetUserId
    /// <summary>
    /// Retrieves a User ID for a username using the API.
    /// </summary>
    /// <param name="username">username</param>
    private int _DoGetUserId(string username)
    {
        int idUser = 0;

        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key><![CDATA[" + API_KEY + "]]></key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<getUser>";
        xmlPayload += "<username><![CDATA[" + username + "]]></username>";
        xmlPayload += "</getUser>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + PORTAL_NAME + ".asentialms.com/_util/API/Default.asmx/GetUser");

        // encode payload into bytes                
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // do nothing if there is an error, it just means that we cannot register the user in courses, it should not be a fatal exception
        if (statusNode.InnerText == "Error")
        { }
        else // no error on getUser, proceed
        {
            if (xmlRoot.SelectSingleNode("response/payload/Users/User/Id") != null)
            {
                idUser = Convert.ToInt32(xmlRoot.SelectSingleNode("response/payload/Users/User/Id").InnerText);
            }
        }

        // return the user id
        return idUser;
    }
    #endregion

    #region _DoGetCourseId
    /// <summary>
    /// Retrieves a Course ID for a course with the specified Course Code using the API.
    /// </summary>
    /// <param name="courseCode">Course Code</param>
    private int _DoGetCourseId(string courseCode)
    {
        int idCourse = 0;

        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key><![CDATA[" + API_KEY + "]]></key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<getCourse>";
        xmlPayload += "<code><![CDATA[" + courseCode + "]]></code>";
        xmlPayload += "</getCourse>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + PORTAL_NAME + ".asentialms.com/_util/API/Default.asmx/GetCourse");

        // encode payload into bytes                
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // do nothing if there is an error, it just means that we cannot register the user in courses, it should not be a fatal exception
        if (statusNode.InnerText == "Error")
        { }
        else // no error on getCourse, proceed
        {
            if (xmlRoot.SelectSingleNode("response/payload/Courses/Course/Id") != null)
            {
                idCourse = Convert.ToInt32(xmlRoot.SelectSingleNode("response/payload/Courses/Course/Id").InnerText);
            }
        }

        // return the course id                      
        return idCourse;
    }
    #endregion

    #region _DoEnrollment
    /// <summary>
    /// Enrolls a user in a course using the API.
    /// </summary>
    /// <param name="idUser">User ID</param>
    /// <param name="idCourse">Course ID</param>
    /// <param name="timezone">Timezone ID</param>
    private void _DoEnrollment(int idUser, int idCourse, string timezone)
    {
        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key><![CDATA[" + API_KEY + "]]></key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<courseEnrollment>";
        xmlPayload += "<idUser>" + idUser.ToString() + "</idUser>";
        xmlPayload += "<idCourse>" + idCourse.ToString() + "</idCourse>";
        xmlPayload += "<isLockedByPrerequisites>false</isLockedByPrerequisites>";
        xmlPayload += "<dtStart>" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss") + "</dtStart>";
        xmlPayload += "<idTimezone><![CDATA[" + timezone + "]]></idTimezone>";
        xmlPayload += "</courseEnrollment>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + PORTAL_NAME + ".asentialms.com/_util/API/Default.asmx/EnrollCourse");

        // encode payload into bytes                
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // do nothing if there is an error, it just means that we cannot register the user in courses, it should not be a fatal exception
        if (statusNode.InnerText == "Error")
        { }
    }
    #endregion

    #region _GetUserSSOToken
    /// <summary>
    /// Retrieves a SSO token using the API which will be used in a redirect link.
    /// </summary>
    /// <param name="idUser">User ID</param>
    private string _GetUserSSOToken(int idUser)
    {
        string token = null;

        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key><![CDATA[" + API_KEY + "]]></key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<getSSOToken>";
        xmlPayload += "<id>" + idUser.ToString() + "</id>";
        xmlPayload += "</getSSOToken>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://" + PORTAL_NAME + ".asentialms.com/_util/API/Default.asmx/GetSSOToken");

        // encode payload into bytes                
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // do nothing if there is an error, it just means that we cannot re-direct the user into the LMS, it should not be a fatal exception
        if (statusNode.InnerText == "Error")
        { }
        else // no error on getSSOToken, proceed with redirect
        {
            token = xmlRoot.SelectSingleNode("response/payload/SSOToken/Token").InnerText;
        }

        // return the token
        return token;

    }
    #endregion

    #region _WriteExceptionToLog
    /// <summary>
    /// Writes exceptions to a log file stored on the server.
    /// </summary>
    /// <param name="message">Message</param>
    /// <param name="stackTrace">Stack Trace</param>
    /// <param name="response">Response</param>
    /// <param name="stepThroughLog">Step-Through Log</param>
    private void _WriteExceptionToLog(string message, string stackTrace, string response, string stepThroughLog)
    {
        // write the exception to a file
        DateTime dtNow = DateTime.Now;
        string exceptionfileName = "RegistrationException_" + String.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}.log", dtNow);
        string exceptionFileFullPath = Path.Combine(HttpRuntime.AppDomainAppPath + @"static\DynamicRegistrationForm\ExceptionLogs\", exceptionfileName);

        using (StreamWriter file = new StreamWriter(exceptionFileFullPath))
        {
            file.WriteLine("EXCEPTION OCCURED ON " + dtNow.ToString());
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION MESSAGE:");
            file.WriteLine(message);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION STACK TRACE:");
            file.WriteLine(stackTrace);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("API RESPONSE:");

            if (!String.IsNullOrWhiteSpace(response))
            { file.WriteLine(response); }
            else
            { file.WriteLine("NO RESPONSE RECEIVED FROM THE API"); }

            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("STEP THROUGH LOG:");
            file.WriteLine(stepThroughLog);
        }
    }
    #endregion

    #region Helper Structs
    public struct ReturnObject
    {
        public StatusObject status;
        public string token;
    }

    public struct StatusObject
    {
        public string result;
        public string code;
        public List<object> messages;
    }

    public struct MessageObject
    {
        public string identifier;
        public string text;
    }
    #endregion
}
