var PORTAL_NAME = GetPortalName();

// Dynamically set the portal name based on the hostname
function GetPortalName() {
    var hostname = parent.window.location.hostname;     // https://covid19training.asentialms.com

    var portalName = hostname.substring(                // ... /{portalname}. ...
        hostname.indexOf("/") + 1,
        hostname.indexOf("."));

    portalName.replace('/', '');                        // remove the first '/' if it is captured for some reason

    return portalName;                                  // returns covid19training
}

RegistrationFormControl = function(appendTo){
	
	this.ObjectType = "DynamicRegistrationForm";
	this.Container 	= appendTo;
	
	this.RootElement = new Form(
		this, 
		null, 
		null, 
		"registration-form",
		null,
		"Please wait while we create your COVID-19 Training LMS user account."
	).RootElement;
		
	this.RootElement.PostURL 	= "/static/DynamicRegistrationForm/svc/RegistrationForm.asmx/Register";
	this.RootElement.PostType = "json";
	this.RootElement.ReturnData = null;
	
	// Create the instructions container.
	DOM.c(
		null, 
		"DIV", 
		{
			"id":"registrationinstructions",			
			"class":"registrationinstructions"
		}, 
		this.RootElement, 
		"Fill out the form fields below to register for a COVID-19 Training LMS account."
	);	
	
	// Create the groupings.
	
	this.NameGroup 					= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.EmailGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
    this.EmailConfirmGroup          = DOM.c(this, "DIV", {"class":"input-group"},   this.RootElement);
    this.UsernameGroup              = DOM.c(this, "DIV", {"class":"input-group"},   this.RootElement);
	this.PasswordGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.TimezoneGroup	 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
    this.CompanyGroup          		= DOM.c(this, "DIV", {"class":"input-group"},   this.RootElement);
	this.ButtonGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	
	// Create the inputs.
	
	this.FirstNameInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"firstname", 			"name":"firstname", 			"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.LastNameInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"lastname", 			"name":"lastname", 				"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.EmailInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"email", 				"name":"email", 				"type":"text", 		"maxlength":512, "class":"InputLong"});
	this.EmailConfirm 				= DOM.c(this.RootElement, "INPUT", 	{"id":"emailConfirm", 		"name":"emailConfirm", 			"type":"text", 		"maxlength":512, "class":"InputLong", "autocomplete":"new-password"});
    this.UsernameInput              = DOM.c(this.RootElement, "INPUT",  {"id":"username",           "name":"username",              "type":"text",      "maxlength":512, "class":"InputLong"});
    this.PasswordInput              = DOM.c(this.RootElement, "INPUT",  {"id":"password",           "name":"password",              "type":"password",  "maxlength":255, "class":"InputNormal"});
	this.ConfirmInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"confirm", 			"name":"confirm", 				"type":"password", 	"maxlength":255, "class":"InputNormal"});
	this.TimezoneInput 				= DOM.c(this.RootElement, "SELECT", {"id":"timezone", 			"name":"timezone", 				"type":"select", 	"maxlength":255, "class":"InputLong"});
	this.CompanyInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"company", 	    	"name":"company", 				"type":"text", 		"maxlength":255, "class":"InputLong"});

    // PortalInput is a hidden input whose value is set to the portal name retrieved from the current hostname URL
    this.PortalInput                = DOM.c(this.RootElement, "INPUT",  {"id":"portal",             "name":"portal",                "type":"text",      "maxlength":255});
    this.PortalInput.value = PORTAL_NAME;

	this.SaveButton = DOM.c(
		this, 
		"INPUT",
		{
			"type"	: "button",
			"id"	: "save",
			"name"	: "save", 
			"class"	: "default",
			"value"	: "Create Account"
		}
	)
	
	this.SaveButton.onclick = function(){
		this.Controller.RootElement.Post();
	}
	
	// Create the controls.
	
	this.FirstNameControl 				= new FormInputControl("First Name",				"firstname",			this.NameGroup,					true);
	this.LastNameControl 				= new FormInputControl("Last Name",					"lastname",				this.NameGroup,					true);
	this.EmailControl 					= new FormInputControl("Email",						"email",				this.EmailGroup, 				true);	
	this.EmailConfirmControl 			= new FormInputControl("Confirm Email",				"emailConfirm",			this.EmailConfirmGroup, 		true);	
    this.UsernameControl                = new FormInputControl("Username",                  "username",             this.UsernameGroup,             true);	
    this.PasswordControl                = new FormInputControl("Password",                  "password",             this.PasswordGroup,             true);
	this.ConfirmControl 				= new FormInputControl("Confirm Password",			"confirm",				this.PasswordGroup,				true);
	this.TimezoneControl 				= new FormInputControl("Timezone", 					"timezone", 			this.TimezoneGroup,				true);
	this.CompanyControl           	 	= new FormInputControl("Company",              		"company",         		this.CompanyGroup,         		true);
	this.PortalControl				    = new FormInputControl("Portal",					"portal",				null,				            true);
	
	
	// Log the controls into the Form object.
	// Each control represents a parameter value that will be sent to/received from the server.
	
	this.RootElement.InputControls.push(this.FirstNameControl);
	this.RootElement.InputControls.push(this.LastNameControl);
	this.RootElement.InputControls.push(this.EmailControl);	
    this.RootElement.InputControls.push(this.EmailConfirmControl);	
    this.RootElement.InputControls.push(this.UsernameControl);	
	this.RootElement.InputControls.push(this.PasswordControl);
	this.RootElement.InputControls.push(this.ConfirmControl);
	this.RootElement.InputControls.push(this.TimezoneControl);
	this.RootElement.InputControls.push(this.CompanyControl);
	this.RootElement.InputControls.push(this.PortalControl);
	
	// Attach timezone items to the select input.

	this.TimezoneInput.AddItem("1", "(GMT-12:00) International Date Line West", false);
	this.TimezoneInput.AddItem("2", "(GMT-11:00) Coordinated Universal Time-11", false);
	this.TimezoneInput.AddItem("3", "(GMT-10:00) Hawaii", false);
	this.TimezoneInput.AddItem("4", "(GMT-09:00) Alaska", false);
	this.TimezoneInput.AddItem("5", "(GMT-08:00) Baja California", false);
	this.TimezoneInput.AddItem("6", "(GMT-08:00) Pacific Time (US & Canada)", false);
	this.TimezoneInput.AddItem("7", "(GMT-07:00) Arizona", false);
	this.TimezoneInput.AddItem("8", "(GMT-07:00) Chihuahua, La Paz, Mazatlan", false);
	this.TimezoneInput.AddItem("9", "(GMT-07:00) Mountain Time (US & Canada)", false);
	this.TimezoneInput.AddItem("10", "(GMT-06:00) Central America", false);
	this.TimezoneInput.AddItem("11", "(GMT-06:00) Central Time (US & Canada)", false);
	this.TimezoneInput.AddItem("12", "(GMT-06:00) Guadalajara, Mexico City, Monterrey", false);
	this.TimezoneInput.AddItem("13", "(GMT-06:00) Saskatchewan", false);
	this.TimezoneInput.AddItem("14", "(GMT-05:00) Bogota, Lima, Quito", false);
	this.TimezoneInput.AddItem("15", "(GMT-05:00) Eastern Time (US & Canada)", true);
	this.TimezoneInput.AddItem("16", "(GMT-05:00) Indiana (East)", false);
	this.TimezoneInput.AddItem("17", "(GMT-04:30) Caracas", false);
	this.TimezoneInput.AddItem("18", "(GMT-04:00) Asuncion", false);
	this.TimezoneInput.AddItem("19", "(GMT-04:00) Atlantic Time (Canada)", false);
	this.TimezoneInput.AddItem("20", "(GMT-04:00) Cuiaba", false);
	this.TimezoneInput.AddItem("21", "(GMT-04:00) Georgetown, La Paz, Manaus, San Juan", false);
	this.TimezoneInput.AddItem("22", "(GMT-04:00) Santiago", false);
	this.TimezoneInput.AddItem("23", "(GMT-03:30) Newfoundland", false);
	this.TimezoneInput.AddItem("24", "(GMT-03:00) Brasilia", false);
	this.TimezoneInput.AddItem("25", "(GMT-03:00) Buenos Aires", false);
	this.TimezoneInput.AddItem("26", "(GMT-03:00) Cayenne, Fortaleza", false);
	this.TimezoneInput.AddItem("27", "(GMT-03:00) Greenland", false);
	this.TimezoneInput.AddItem("28", "(GMT-03:00) Montevideo", false);
	this.TimezoneInput.AddItem("29", "(GMT-03:00) Salvador", false);
	this.TimezoneInput.AddItem("30", "(GMT-02:00) Coordinated Universal Time-02", false);
	this.TimezoneInput.AddItem("31", "(GMT-02:00) Mid-Atlantic", false);
	this.TimezoneInput.AddItem("32", "(GMT-01:00) Azores", false);
	this.TimezoneInput.AddItem("33", "(GMT-01:00) Cape Verde Is.", false);
	this.TimezoneInput.AddItem("34", "(GMT) Casablanca", false);
	this.TimezoneInput.AddItem("35", "(GMT) Coordinated Universal Time", false);
	this.TimezoneInput.AddItem("36", "(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London", false);
	this.TimezoneInput.AddItem("37", "(GMT) Monrovia, Reykjavik", false);
	this.TimezoneInput.AddItem("38", "(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna", false);
	this.TimezoneInput.AddItem("39", "(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague", false);
	this.TimezoneInput.AddItem("40", "(GMT+01:00) Brussels, Copenhagen, Madrid, Paris", false);
	this.TimezoneInput.AddItem("41", "(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb", false);
	this.TimezoneInput.AddItem("42", "(GMT+01:00) Tripoli", false);
	this.TimezoneInput.AddItem("43", "(GMT+01:00) West Central Africa", false);
	this.TimezoneInput.AddItem("44", "(GMT+01:00) Windhoek", false);
	this.TimezoneInput.AddItem("45", "(GMT+02:00) Athens, Bucharest", false);
	this.TimezoneInput.AddItem("46", "(GMT+02:00) Beirut", false);
	this.TimezoneInput.AddItem("47", "(GMT+02:00) Cairo", false);
	this.TimezoneInput.AddItem("48", "(GMT+02:00) Damascus", false);
	this.TimezoneInput.AddItem("49", "(GMT+02:00) E. Europe", false);
	this.TimezoneInput.AddItem("50", "(GMT+02:00) Harare, Pretoria", false);
	this.TimezoneInput.AddItem("51", "(GMT+02:00) Helsinki, Kyiv, Riga, Sofia, Tallinn, Vilnius", false);
	this.TimezoneInput.AddItem("52", "(GMT+02:00) Istanbul", false);
	this.TimezoneInput.AddItem("53", "(GMT+02:00) Jerusalem", false);
	this.TimezoneInput.AddItem("54", "(GMT+03:00) Amman", false);
	this.TimezoneInput.AddItem("55", "(GMT+03:00) Baghdad", false);
	this.TimezoneInput.AddItem("56", "(GMT+03:00) Kaliningrad, Minsk", false);
	this.TimezoneInput.AddItem("57", "(GMT+03:00) Kuwait, Riyadh", false);
	this.TimezoneInput.AddItem("58", "(GMT+03:00) Nairobi", false);
	this.TimezoneInput.AddItem("59", "(GMT+03:30) Tehran", false);
	this.TimezoneInput.AddItem("60", "(GMT+04:00) Abu Dhabi, Muscat", false);
	this.TimezoneInput.AddItem("61", "(GMT+04:00) Baku", false);
	this.TimezoneInput.AddItem("62", "(GMT+04:00) Moscow, St. Petersburg, Volgograd", false);
	this.TimezoneInput.AddItem("63", "(GMT+04:00) Port Louis", false);
	this.TimezoneInput.AddItem("64", "(GMT+04:00) Tbilisi", false);
	this.TimezoneInput.AddItem("65", "(GMT+04:00) Yerevan", false);
	this.TimezoneInput.AddItem("66", "(GMT+04:30) Kabul", false);
	this.TimezoneInput.AddItem("67", "(GMT+05:00) Ashgabat, Tashkent", false);
	this.TimezoneInput.AddItem("68", "(GMT+05:00) Islamabad, Karachi", false);
	this.TimezoneInput.AddItem("69", "(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi", false);
	this.TimezoneInput.AddItem("70", "(GMT+05:30) Sri Jayawardenepura", false);
	this.TimezoneInput.AddItem("71", "(GMT+05:45) Kathmandu", false);
	this.TimezoneInput.AddItem("72", "(GMT+06:00) Astana", false);
	this.TimezoneInput.AddItem("73", "(GMT+06:00) Dhaka", false);
	this.TimezoneInput.AddItem("74", "(GMT+06:00) Ekaterinburg", false);
	this.TimezoneInput.AddItem("75", "(GMT+06:30) Yangon (Rangoon)", false);
	this.TimezoneInput.AddItem("76", "(GMT+07:00) Bangkok, Hanoi, Jakarta", false);
	this.TimezoneInput.AddItem("77", "(GMT+07:00) Novosibirsk", false);
	this.TimezoneInput.AddItem("78", "(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi", false);
	this.TimezoneInput.AddItem("79", "(GMT+08:00) Krasnoyarsk", false);
	this.TimezoneInput.AddItem("80", "(GMT+08:00) Kuala Lumpur, Singapore", false);
	this.TimezoneInput.AddItem("81", "(GMT+08:00) Perth", false);
	this.TimezoneInput.AddItem("82", "(GMT+08:00) Taipei", false);
	this.TimezoneInput.AddItem("83", "(GMT+08:00) Ulaanbaatar", false);
	this.TimezoneInput.AddItem("84", "(GMT+09:00) Irkutsk", false);
	this.TimezoneInput.AddItem("85", "(GMT+09:00) Osaka, Sapporo, Tokyo", false);
	this.TimezoneInput.AddItem("86", "(GMT+09:00) Seoul", false);
	this.TimezoneInput.AddItem("87", "(GMT+09:30) Adelaide", false);
	this.TimezoneInput.AddItem("88", "(GMT+09:30) Darwin", false);
	this.TimezoneInput.AddItem("89", "(GMT+10:00) Brisbane", false);
	this.TimezoneInput.AddItem("90", "(GMT+10:00) Canberra, Melbourne, Sydney", false);
	this.TimezoneInput.AddItem("91", "(GMT+10:00) Guam, Port Moresby", false);
	this.TimezoneInput.AddItem("92", "(GMT+10:00) Hobart", false);
	this.TimezoneInput.AddItem("93", "(GMT+10:00) Yakutsk", false);
	this.TimezoneInput.AddItem("94", "(GMT+11:00) Solomon Is., New Caledonia", false);
	this.TimezoneInput.AddItem("95", "(GMT+11:00) Vladivostok", false);
	this.TimezoneInput.AddItem("96", "(GMT+12:00) Auckland, Wellington", false);
	this.TimezoneInput.AddItem("97", "(GMT+12:00) Coordinated Universal Time+12", false);
	this.TimezoneInput.AddItem("98", "(GMT+12:00) Fiji", false);
	this.TimezoneInput.AddItem("99", "(GMT+12:00) Magadan", false);
	this.TimezoneInput.AddItem("100", "(GMT+12:00) Petropavlovsk-Kamchatsky - Old", false);
	this.TimezoneInput.AddItem("101", "(GMT+13:00) Nuku'alofa", false);
	this.TimezoneInput.AddItem("102", "(GMT+13:00) Samoa", false);		

	// Attach the inputs to the controls.
		
	this.FirstNameControl.AddInput(this.FirstNameInput);
	this.LastNameControl.AddInput(this.LastNameInput);
	this.EmailControl.AddInput(this.EmailInput);	
    this.EmailConfirmControl.AddInput(this.EmailConfirm);
    this.UsernameControl.AddInput(this.UsernameInput);
	this.PasswordControl.AddInput(this.PasswordInput);
	this.ConfirmControl.AddInput(this.ConfirmInput);	
	this.TimezoneControl.AddInput(this.TimezoneInput);
    this.CompanyControl.AddInput(this.CompanyInput);
	this.PortalControl.AddInput(this.PortalInput);
	
	// Buttons
	
	this.ButtonGroup.appendChild(this.SaveButton);
	
	// Append the form to the container.
	
	try{
		this.Container.appendChild(this.RootElement);
	}catch(e){ }
	
	// Client-side error checking.
	
	this.RootElement.UpdatePasswordConfirm = function(){
		if(this.Controller.ConfirmInput.value.length > 0){
			if (this.Controller.ConfirmInput.value == this.Controller.PasswordInput.value){
				this.Controller.ConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.ConfirmControl.HasErrors()){
					this.Controller.ConfirmControl.AddError("Password confirmation does not match.");
				}
			}
		}else{
			this.Controller.ConfirmControl.ClearErrors();
		}
	}
	
	this.RootElement.UpdateEmailConfirm = function(){
		if(this.Controller.EmailConfirm.value.length > 0){
			if (this.Controller.EmailConfirm.value == this.Controller.EmailInput.value){
				this.Controller.EmailConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.EmailConfirmControl.HasErrors()){
					this.Controller.EmailConfirmControl.AddError("Email confirmation does not match.");
				}
			}
		}else{
			this.Controller.EmailConfirmControl.ClearErrors();
		}
	}
	
	this.PasswordInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	this.ConfirmInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	
	this.EmailInput.onkeyup = function(e){
		this.Controller.UpdateEmailConfirm();
	}
	this.EmailConfirm.onkeyup = function(e){
		this.Controller.UpdateEmailConfirm();
	}
	
	// Set Success Actions
	
	this.RootElement.addEventListener(
		"post-successful", 
		function(){
			this.Controller.CompleteRegistration();
		}
	);
	
	this.RootElement.SetSuccessEvent("post-successful", this.RootElement);
	
	// Overloads
	
	this.RootElement.Post = function(){
	
		// move into view
		
		DOM.scrollTo(this);
		
		// clear all the errors
		
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		// set form to "working" 
		DOM.addClass(this, "working");		
		
		if(!this.Controller.DoEasyErrors()){
			DOM.removeClass(this, "working");
			return false;
		}
		
		// post		
		AJAXHelper.Post(
			this.PostURL, 
			this.Controller.RootElement.GetData(),
			this.PostType,
			"post-complete", 
			this
		);
		
	}
}


RegistrationFormControl.prototype.CompleteRegistration = function () {
	
	var redirect;
	
	// if we got an SSO token, set the redirect link to automatically log the user in
    if (this.RootElement.ReturnData.token != null) {
        redirect = "https://" + PORTAL_NAME + ".asentialms.com/_util/DoSSO.aspx?token=" + this.RootElement.ReturnData.token;
    }
    // otherwise, set the redirect link to the portal's homepage
	else {
		redirect = "https://" + PORTAL_NAME + ".asentialms.com/";
	}
	
	this.RootElement.addEventListener(
	"animationend",
	function () {
		this.Controller.Container.appendChild(
		new UserRegistrationCompletedControl(
		this.Controller.FirstNameControl.GetValue(),
		redirect
		)
		);
		DOM.scrollTo(this.Controller.Container);
		this.Controller.Container.removeChild(this);
		}
        );

        DOM.addClass(this.RootElement, "vanish");
    }


RegistrationFormControl.prototype.DoEasyErrors = function(){
	
	var s = true;
	
	if(this.FirstNameInput.value.length == 0){
		s = false;
		this.FirstNameControl.AddError("First Name is required.")
	}
	
	if(this.LastNameInput.value.length == 0){
		s = false;
		this.LastNameControl.AddError("Last Name is required.")
	}
	
	if(this.EmailInput.value.length == 0){
		s = false;
		this.EmailControl.AddError("Email is required.")
	}
	
	if(this.EmailConfirm.value != this.EmailInput.value){
		s = false;
		this.EmailConfirmControl.AddError("Email confirmation does not match.")
    }	

    if (this.UsernameInput.value.length == 0) {
        s = false;
        this.UsernameControl.AddError("Username is required.")
    }
	
	if(this.PasswordInput.value.length == -0){
		s = false;
		this.PasswordControl.AddError("Password is required.")
	}
	
	if(this.ConfirmInput.value != this.PasswordInput.value){
		s = false;
		this.ConfirmControl.AddError("Confirmation does not match.")
	}	
	
	if(this.TimezoneInput.value == 0){
		s = false;
		this.TimezoneControl.AddError("Timezone is required.")
	}
	
    if (this.CompanyInput.value.length == 0) {
        s = false;
        this.CompanyControl.AddError("Company is required.")
    }
		
	return s;
	
}


UserRegistrationCompletedControl = function(name, redirect){
		
	var title = document.getElementById('title');
	title.remove(); 
	
	this.RootElement = DOM.c(
		this, 
		"DIV",
		{
			"class":"info-control evolve"
		}
	);
	
	this.Something = DOM.c(
		null, 
		"DIV", 
		{
			"class":"success"
		}, 
		this.RootElement, 
		"Thank you for registering for a COVID-19 Training LMS account, " + name + "."
	);
	
	this.SomethingElse = DOM.c(
		null, 
		"DIV", 
		{
			"class":"success"
		}, 
		this.RootElement, 
		"You will be automatically enrolled in three COVID-19 Preparedness courses. " +
		"Each course can be viewed by clicking on the green arrow on the course tile once you are logged in."
	);
	
	this.GoButtonGroup = DOM.c(this, "DIV", {"class":"success"},	this.RootElement);
	
	this.GoButton = DOM.c(
		null, 
		"INPUT",
		{
			"type"	: "button",
			"id"	: "go",
			"name"	: "go", 
			"class"	: "default",
			"value"	: "Continue to LMS"
		},
		this.RootElement,
	)
	
	this.GoButtonGroup.appendChild(this.GoButton);
	
	this.GoButton.onclick = function(){
		if (redirect != null) {
			window.location.href = redirect;
		}	
	}
	
	return this.RootElement;
	
}

