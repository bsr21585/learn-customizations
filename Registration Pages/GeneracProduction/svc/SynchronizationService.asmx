﻿<%@ WebService Language="C#" Class="SynchronizationService" %>

using System;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.WebControls;
using System.Xml;

[WebService(Description = "Web services example.", Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class SynchronizationService : System.Web.Services.WebService 
{
    public SynchronizationService()
    {}
	
	#region Syncronize
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ReturnObject Synchronize()
    {
        
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		string stepThroughLog = String.Empty;
        
        ReturnObject retObj = new ReturnObject();
        
		
		retObj.status.result = "success";
		retObj.status.code = "1";
		retObj.status.messages = new List<object>();
        retObj.token = null;

        

        //try
        //{
            
            // REGISTER THE USER USING THE ASENTIA API
            //try
            //{
               	
                // USER ACCOUNT REGISTRATION
				stepThroughLog += DateTime.Now.ToString() + " - User synchronization with API.\r\n";
                
                // Read from batch file
                string filename = Server.MapPath(@"~/static/GeneracProduction/svc/UserUpdates.txt");
                
                DataTable datatable = new DataTable();
                StreamReader streamreader = new StreamReader(filename);
                char[] delimiter = new char[] { '\t' };
               
                string[] columnheaders = streamreader.ReadLine().Split(delimiter);
                foreach (string columnheader in columnheaders)
                {
                    datatable.Columns.Add(columnheader); // I've added the column headers here.
                }
                
                while (streamreader.Peek() > 0)
                {
                    DataRow datarow = datatable.NewRow();
                    datarow.ItemArray = streamreader.ReadLine().Split(delimiter);
                    datatable.Rows.Add(datarow);
                }

                string id = "";
                string username = "";
                string firstname = "";
                string middlename = "";
                string lastname = "";
                string jobtitle = "";
                string email = "";
                string streetaddress1 = "";
                string streetaddress2 = "";
                string city = "";
                string country = "";
                string state = "";
                string zip = "";
                string primaryphone = "";
                string mobilephone = "";
                string company = "";
                
                foreach (DataRow row in datatable.Rows)
                {

                    foreach (DataColumn column in datatable.Columns)
                    {
                        switch (column.ColumnName)
                        {
                            case "id":
                                id = row[column].ToString();
                                break;
                            case "username":
                                username = row[column].ToString();
                                break;
                            case "firstname":
                                firstname = row[column].ToString();
                                break;
                            case "middlename":
                                middlename = row[column].ToString();
                                break;
                            case "lastname":
                                lastname = row[column].ToString();
                                break;
                            case "jobtitle":
                                jobtitle = row[column].ToString();
                                break;
                            case "email":
                                email = row[column].ToString();
                                break;
                            case "streetaddress":
                                string[] separator = {","};
                                if(!String.IsNullOrWhiteSpace(row[column].ToString())){
                                    string[] streetAddressArray = row[column].ToString().Split(separator, 2, StringSplitOptions.RemoveEmptyEntries);
                                    streetaddress1 = streetAddressArray[0];
                                    if (streetAddressArray.Length > 1)
                                    {
                                        streetaddress2 = streetAddressArray[1];
                                    }
                                    else
                                    {
                                        streetaddress2 = "";
                                    }
                                }else{
                                    streetaddress1 = "";
                                    streetaddress2 = "";
                                }
                                break;
                            case "city":
                                city = row[column].ToString();
                                break;
                            case "state":
                                state = row[column].ToString();
                                break;
                            case "zip":
                                zip = row[column].ToString();
                                break;
                            case "country":
                                country = row[column].ToString();
                                break;
                            case "primaryphone":
                                primaryphone = row[column].ToString();
                                break;
                            case "mobilephone":
                                mobilephone = row[column].ToString();
                                break;
                            case "company":
                                company = row[column].ToString();
                                break;
                            default:
                                break;
                                
                        }
                        
                    }
                    
                    string crmResponse = "";
                    string techid = null;
                    string dealerid = null;
                    
                    //try
                    //{
                        // CALL EXTERNAL API to get techid and dealer id
                        crmResponse = _CallPIWrapper(firstname, middlename, lastname, jobtitle, email, streetaddress1, streetaddress2, city, state, zip, country, primaryphone, mobilephone, company);
                    //}
                    //catch (WebException webEx)
                    //{
                      //  throw new Exception("An error occurred when contacting the CRM Service. Please contact an administrator.");
                    //}

                    stepThroughLog += DateTime.Now.ToString() + " - Call external API finished.\r\n";


                    CRMData responseObject = Newtonsoft.Json.JsonConvert.DeserializeObject<CRMData>(crmResponse);

                    /*if (responseObject.ReturnMessage[0].MessageStatus == "E")
                    {
                        throw new Exception(responseObject.ReturnMessage[0].Message);
                    }*/

                    techid = responseObject.TechID;
                    dealerid = responseObject.DealerID;

                    //try
                    /*{*/ this._UpdateUser(id, firstname, lastname, username, techid, dealerid); /*}
                    catch (WebException webEx) // RETRY 1 TIME
                    { stepThroughLog += DateTime.Now.ToString() + " - Exception Updating User - " + webEx.Message; }*/

                    stepThroughLog += DateTime.Now.ToString() + " - User registration with API finished.\r\n";
                }
            
            //Console.ReadLine();

                
            /*}
            catch (WebException ex)
            {
                // write the exception to a log with the response
				if (ex.Response != null)
				{
					WebResponse errResp = ex.Response;
				
					using (Stream respStream = errResp.GetResponseStream())
					{
						StreamReader reader = new StreamReader(respStream);
						string text = reader.ReadToEnd();

                        this._WriteExceptionToLog(ex.Message, ex.StackTrace, text, stepThroughLog);	
					}
				}
                else // write the exception to a log without the response
				{
                    this._WriteExceptionToLog(ex.Message, ex.StackTrace, null, stepThroughLog);	
				}

                retObj.status.result = "fail";
                retObj.status.code = "0";
                
                return retObj;
				// bubble up the exception
                throw new Exception("A fatal error occurred when contacting the user registration service. Please contact an administrator.");
            }
        }        
        catch (Exception ex)
        {
            retObj.status.result = "fail";
            retObj.status.code = "0";
            retObj.status.messages = new List<object>();

            string[] exceptionMessages = ex.Message.Split('|');

            foreach (string s in exceptionMessages)
            {
                MessageObject msgObj = new MessageObject();
                msgObj.identifier = "general";
                msgObj.text = s;
                retObj.status.messages.Add(msgObj);
            }
            return retObj;
        }*/
		
		// return
		return retObj;
    }
    #endregion

    #region _UpdateUser
    private void _UpdateUser(string id, string firstname, string lastname, string username, string techid, string dealerid)
    {
        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key>1234567890</key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<user id=\\\"" + id + "\\\">";
        xmlPayload += "<firstName><![CDATA[" + firstname + "]]></firstName>";
        xmlPayload += "<lastName><![CDATA[" + lastname + "]]></lastName>";
        xmlPayload += "<username><![CDATA[" + username + "]]></username>";
        xmlPayload += "<password></password>";
        xmlPayload += "<mustChangePassword>0</mustChangePassword>";
        xmlPayload += "<idTimezone>15</idTimezone>";
        xmlPayload += "<languageString>en-US</languageString>";
        xmlPayload += "<isActive>true</isActive>";
        xmlPayload += "<field00><![CDATA[" + techid + "]]></field00>";
        xmlPayload += "<field15><![CDATA[" + dealerid + "]]></field15>";
        xmlPayload += "</user>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://generac-fs.asentialms.com/_util/API/Default.asmx/SaveUser");
        

        // encode payload into bytes
        byte[] bytes;
        bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // throw an error if there is one
        /*if (statusNode.InnerText == "Error")
        { throw new Exception(descriptionNode.InnerText); }*/
    }
    #endregion
	
	#region _CallPIWrapper
    private string _CallPIWrapper(string firstname, string middlename, string lastname, string jobtitle, string email, string streetaddress1, string streetaddress2, string city, string state, string zip, string country, string primaryphone, string mobilephone, string company)
    {
        string responseString = String.Empty;
        string payload = String.Empty;
        //try
        //{
            // prepare the payload
            payload = "{";
            payload += "\"Technician\": ";
            payload += "{";
            payload += "\"FirstName\":\"" + firstname + "\", ";
            payload += "\"LastName\":\"" + lastname + "\", ";
            payload += "\"MiddleName\":\"" + middlename + "\", ";
            payload += "\"JobTitle\":\"" + jobtitle + "\", ";
            payload += "\"StreetAddress1\":\"" + streetaddress1 + "\", ";
            payload += "\"StreetAddress2\":\"" + streetaddress2 + "\", ";
            payload += "\"ZipCode\":\"" + zip + "\", ";
            payload += "\"City\":\"" + city + "\", ";
            payload += "\"Country\":\"" + country + "\", ";
            payload += "\"State\":\"" + state + "\", ";
            payload += "\"Phone\":\"" + primaryphone + "\", ";
            payload += "\"Phone2\":\"" + mobilephone + "\", ";
            payload += "\"Email\":\"" + email + "\"";
            payload += "}, ";
            payload += "\"Dealer\": ";
            payload += "{ ";
            payload += "\"CompanyName\":\"" + company + "\", ";
            payload += "\"StreetAddress1\":\"" + streetaddress1 + "\", ";
            payload += "\"StreetAddress2\":\"" + streetaddress2 + "\", ";
            payload += "\"ZipCode\":\"" + zip + "\", ";
            payload += "\"City\":\"" + city + "\", ";
            payload += "\"Country\":\"" + country + "\", ";
            payload += "\"State\":\"" + state + "\" ";
            payload += "}";
            payload += "}";


            // create the request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://soa.generac.com/WebServices/SAPCRMTechnician/api/Technician/Create");
            
            // encode payload into bytes
            byte[] bytes;
            bytes = System.Text.Encoding.UTF8.GetBytes(payload);

            // set the payload type, length, and method
            request.ContentType = "application/json; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Headers["auth-key"] = "9C4E50DD-657D-4697-8B64-9D658BB0D071";
            request.Method = "POST";

            // write the request stream (payload)
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            // get the response
            
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    { responseString = new StreamReader(responseStream).ReadToEnd(); }
                }
                else
                { /*throw new Exception(response.StatusDescription);*/ }
            }
        //}
        /*catch (Exception webEx)
        {
            throw new Exception("An error occurred when contacting the CRM Service: " + webEx.Message + " , " + webEx.InnerException + " , " + webEx.StackTrace );
        }*/

        return responseString;
    }
    #endregion

    #region _WriteExceptionToLog
    private void _WriteExceptionToLog(string message, string stackTrace, string response, string stepThroughLog)
    {
        // write the exception to a file
        DateTime dtNow = DateTime.Now;
        string exceptionfileName = "SynchronizationException_" + String.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}.log", dtNow);
        string exceptionFileFullPath = Path.Combine(HttpRuntime.AppDomainAppPath + @"static\GeneracProduction\ExceptionLogs\", exceptionfileName);

        using (StreamWriter file = new StreamWriter(exceptionFileFullPath))
        {
            file.WriteLine("EXCEPTION OCCURED ON " + dtNow.ToString());
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION MESSAGE:");
            file.WriteLine(message);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION STACK TRACE:");
            file.WriteLine(stackTrace);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("API RESPONSE:");

            if (!String.IsNullOrWhiteSpace(response))
            { file.WriteLine(response); }
            else
            { file.WriteLine("NO RESPONSE RECEIVED FROM THE API"); }
			
			file.WriteLine("");
            file.WriteLine("");
			file.WriteLine("STEP THROUGH LOG:");
            file.WriteLine(stepThroughLog);            
        }
    }
    #endregion

    #region Helper Structs
    public struct ReturnObject
    { 
        public StatusObject status;
        public string token;
    }

    public struct StatusObject
    {
        public string result;
        public string code;
        public List<object> messages;
    }       
	
	public struct MessageObject
    {
        public string identifier;
        public string text;
    }
    
    public struct CRMData
    {
        public string DealerID;
        public string TechID;
        public CRMReturnMessage[] ReturnMessage;
    }        	
    
    public struct CRMReturnMessage
    {
        public string MessageStatus;
        public string Message;
    }
    
    #endregion
}
