// assumes that it will be attached to the DOM element "alert" and will call the page overlay automatically.

AlertModal = function(
	title, 
	heading,
	message,
	details
	){
	
	this.Title = title;
	//this.XMLDOM = new DOMParser();
	//this.XML = this.XMLDOM.parseFromString(message, "text/xml");
		
	this.RootElement = DOM.c(
		this, 
		"DIV"
	);
	
	//instantiate the titlebar
	this.TitleBar = new AlertModalTitleBar(
		this
	)
	
	// attach the titlebar to the DOM
	this.RootElement.appendChild(this.TitleBar.RootElement);
	
	this.ContentContainer = DOM.c(
		this, 
		"DIV",
		{
			"class":"content-container"
		},
		this.RootElement
	);
	
	// heading
	if(heading){
		this.Heading = DOM.c(
			this, 
			"DIV",
			{
				"class":"heading"
			},
			this.ContentContainer,
			heading
		);
	}
	
	// message
	if(message){
		this.Message = DOM.c(
			this, 
			"DIV",
			{
				"class":"message"
			},
			this.ContentContainer,
			message
		);
	}
	
	// details
	if(details){
		this.Details = DOM.c(
			this, 
			"DIV",
			{
				"class":"details"
			},
			this.ContentContainer,
			details
		);
	}
	
	// append to the page
	
	if(!DOM.get("alert")){
		DOM.c(
			null, 
			"div", 
			{
				"id":"alert"
			},
			document.body
		);
	}
	
	DOM.get("alert").appendChild(this.RootElement);
	
	// trigger overlay
	PageOverlay.On();
	
	// trigger display
	DOM.addClass(DOM.get("alert"), "alert-modal");
	DOM.addClass(DOM.get("alert"), this.ClassName);
	
	//scroll into view
	
	DOM.scrollTo(this.RootElement);
	
	// Assign the cleanup (for when closed).
	
	DOM.get("alert").Cleanup = function(){
		DOM.get("alert").removeChild(DOM.get("alert").childNodes[0]);
		DOM.removeAllClasses(DOM.get("alert"));
		this.removeEventListener("animationend", DOM.get("alert").Cleanup);
	}
	
}

AlertModal.prototype.Close = function(){
	
	
	DOM.addClass(DOM.get("alert"), "alert-out");
	
	// trigger overlay
	PageOverlay.Off();
	
	DOM.get("alert").addEventListener(
		"animationend", 
		DOM.get("alert").Cleanup
	);
}
/*
Modal.prototype.Cleanup = function(){
	
	DOM.get("modal").removeChild(this.RootElement);
	DOM.removeClass(DOM.get("modal"), "modal");
	DOM.removeClass(DOM.get("modal"), this.ClassName);
	
}
*/
AlertModalTitleBar = function(
	controller
	){
	
	this.Controller = controller;
	
	this.RootElement = DOM.c(
		this, 
		"DIV", 
		{
			"class":"title-container"
		}
	);
	
	this.Title = DOM.c(
		this,
		"DIV",
		{
			"class":"title"
		},
		this.RootElement,
		this.Controller.Title
	);
	
	this.CloseButton = DOM.c(
		this,
		"IMG",
		{
			"class":"close-x",
			"src":"/static/DFHV/ICSJSFramework/images/modal-x.png"	
		},
		this.RootElement
	);
	
	this.CloseButton.onclick = function(){
		
		this.Controller.Controller.Close();
		
	}
	
}
