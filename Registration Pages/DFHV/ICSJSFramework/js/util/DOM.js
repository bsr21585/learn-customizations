/**

Class: 		DOM

----

Method		: c
Arguments	: 
Desc		: Creates and returns an HTML element as specified.

Method		: get
Arguments	: identifier (string)
Desc		: Returns the document element with the identifier specified

Method		: textarea
Arguments	: 
Desc		: Creates and returns a TEXTAREA element with the specified parameters.

Method		: input
Arguments	: 
Desc		: Creates and returns an INPUT element with the specified parameters.

Method		: select
Arguments	: 
Desc		: Creates and returns a SELECT element with the specified parameters.

Method		: text
Arguments	: s (string)
Desc		: Creates and returns a DOM textnode

Method		: addClass
Arguments	: E (object)
			  className (string)
Desc		: Adds the class name specified to the element.

Method		: removeClass
Arguments	: E (object)
			  className (string)
Desc		: Removes the class name specified from the element.




*/

DOM = function(){
	// shortcuts for creating DOM elements
}

DOM.c = function(
	controller, 
	type, 
	data, 
	appendTo, 
	contents
	){
	
	var E = document.createElement(type);
	
	// set data
	
	try{
		if (data){
			for (var prop in data){
				E.setAttribute(prop, data[prop]);
			}
		}
	} catch(e){
		alert("failed to set data");
	}
	
	// set controller
	
	if (controller){
		try{
			E.Controller = controller;
		}catch(e){
			alert("failed to set controller");
		}
	}
	
	// insert text
	
	if (contents){
		switch(typeof contents){
			case "string":
				E.appendChild(document.createTextNode(contents));
				break;
			default:
				try{
					E.appendChild(contents);
				}catch(e){}
				break;
		}
	}
	
	// append
	
	if (appendTo){
		try{
			E.Parent = appendTo;
			appendTo.appendChild(E);
		} catch(e){
			alert("failed to append");
		}
	}
	
	if (type == "SELECT") {
		
		/* METHOD: AddItem
		*/
		E.AddItem = function(
			value, 
			caption,
			selected
			){
			
			var o 			= new Option;
				o.value 	= value;
				o.text 		= caption;
				o.selected 	= selected;
				
			this.options[this.options.length] = o;
			
		}

		/* METHOD: AddItemsFromJSONString

		[
			{Value:XXX,Caption:YYY},
			{Value:AAA,Caption:BBB},
		]

		*/
		E.AddItemsFromJSONString = function(
			data
			){
			
			if (typeof data != "string"){
				return false;
			}
			
			var Data = JSON.parse(data);
			
			for (var i = 0; i < Data.length; i++){
				this.AddItem(Data[i].Value, Data[i].Caption, Data[i].Selected);
			}
			
		}
		
		/* METHOD: RemoveItemAtIndex
		*/
		E.RemoveItemAtIndex = function(
			index
			){
			this.options.remove(index);
		}

		/* METHOD: RemoveAllItems
		*/
		E.RemoveAllItems = function(){
			this.options.length = 0;
		}
		
		/* METHOD: RemoveSelectedItems
		*/
		E.RemoveSelectedItems = function(
			){
			for (var i = this.options.length-1; i >= 0; i--){
				if (this.options[i].selected){
					this.RemoveItemAtIndex(i);
				}
			}
		}

		/* METHOD: GetItemsAsJSONString
		*/
		E.GetItemsAsJSONString = function(){
		
			var v = "";
			
			for (var i = 0; i < this.options.length; i++){
				if (v.length != 0){	v = v + ","; }
				v = v + "{\"Value\":\"" + this.options[i].value + "\",\"Caption\":\"" + this.options[i].text + "\",\"Selected\":\"" + this.options[i].selected + "\"}";
			}
			
			return "[" + v + "]";
			
		}

		/* METHOD: GetSelectedItemsAsJSONString
		*/
		E.GetSelectedItemsAsJSONString = function(){
		
			var v = "";
			
			for (var i = 0; i < this.options.length; i++){
				if (this.options[i].selected){
					if (v.length != 0){	v = v + ","; }
					v = v + "{\"Value\":\"" + this.options[i].value + "\",\"Caption\":\"" + this.options[i].text + "\",\"Selected\":\"" + this.options[i].selected + "\"}";
				}
			}
			
			return "[" + v + "]";
			
		}
		
		/* METHOD: GetValue
		*/
		E.GetValue = function(){
			
			var v = "";
			
			for (var i = 0; i < this.options.length; i++){
				if(this.options[i].selected){
					if (v.length == 0){ 
						v = this.options[i].value;
					} else {
						v = v + "," + this.options[i].value;
					}
				}
			}
			
			return v;
			
		}

		/* METHOD: SelectItemAtIndex
		*/
		E.SelectItemAtIndex = function(index){
			this.options.selectedIndex = index;
		}
		
		/* METHOD: SelectItemWithValue
		*/
		E.SelectItemWithValue = function(val){
			for (var i = 0; i < this.options.length; i++){
				if (this.options[i].value == val){
					this.options.selectedIndex = i;
					return true;
				}
			}
		}
		
		/* METHOD: SortItemsByCaption
		*/
		E.SortItemsByCaption = function(){
		}
		
		/* METHOD: SortItemsByValue
		*/
		E.SortItemsByValue = function(){
		}
		
	}
	
	return E;
	
}

DOM.get = function(identifier){
	
	return document.getElementById(identifier);
	
}

DOM.textarea = function(
	controller, 
	id, 
	name,
	className, 
	appendTo, 
	rows,
	disabled, 
	maxlength,
	value
	){
	
	var E = document.createElement("textarea");
	
	if((typeof name == "string") && (name != null)){
		E.name = name;
	}
	
	if((typeof id == "string") && (id != null)){
		E.id = id;
	}
	
	if(typeof className == "string"){
		E.className = className;
	}
	
	E.disabled 	= disabled;
	E.rows		= rows;
	
	if(typeof value == "string"){
		E.appendChild(DOM.text(null, null, value));
	}
	
	if (maxlength > 0) {
		E.setAttribute("maxlength", maxlength);
	}else{
		E.removeAttribute("maxlength");
	}
	
	try{
		E.Controller = controller;
	}catch(e){}
	
	try{
		E.Parent = appendTo;
		appendTo.appendChild(E);
	}catch(e){}
	
	return E;
	
}

DOM.select = function(
	controller, 
	id, 
	name, 
	className, 
	appendTo,
	disabled, 
	isMultiple, 
	rows,
	data
	){

	var E = document.createElement("select");
	
	if((typeof name == "string") && (name != null)){
		E.name = name;
	}
	
	if((typeof id == "string") && (id != null)){
		E.id = id;
	}
	
	if(typeof className == "string"){
		E.className = className;
	}
	
	E.disabled 	= disabled;
	
	if (isMultiple == true) {
		E.setAttribute("multiple", "true");
	}else{
		E.removeAttribute("multiple");
	}
	
	try{
		E.setAttribute("size", rows);
	}catch(e){}
	
	try{
		E.Controller = controller;
	}catch(e){}
	
	try{
		E.Parent = appendTo;
		appendTo.appendChild(E);
	}catch(e){}
		
	/* METHOD: AddItem
	*/
	E.AddItem = function(
		value, 
		caption,
		selected
		){
		
		var o 			= new Option;
			o.value 	= value;
			o.text 		= caption;
			o.selected 	= selected;
			
		this.options[this.options.length] = o;
		
	}

	/* METHOD: AddItemsFromJSONString

	[
		{Value:XXX,Caption:YYY},
		{Value:AAA,Caption:BBB},
	]

	*/
	E.AddItemsFromJSONString = function(
		data
		){
		
		if (typeof data != "string"){
			return false;
		}
		
		var Data = JSON.parse(data);
		
		for (var i = 0; i < Data.length; i++){
			this.AddItem(Data[i].Value, Data[i].Caption, Data[i].Selected);
		}
		
	}
	
	/* METHOD: RemoveItemAtIndex
	*/
	E.RemoveItemAtIndex = function(
		index
		){
		this.options.remove(index);
	}

	/* METHOD: RemoveAllItems
	*/
	E.RemoveAllItems = function(){
		this.options.length = 0;
	}
	
	/* METHOD: RemoveSelectedItems
	*/
	E.RemoveSelectedItems = function(
		){
		for (var i = this.options.length-1; i >= 0; i--){
			if (this.options[i].selected){
				this.RemoveItemAtIndex(i);
			}
		}
	}

	/* METHOD: GetItemsAsJSONString
	*/
	E.GetItemsAsJSONString = function(){
	
		var v = "";
		
		for (var i = 0; i < this.options.length; i++){
			if (v.length != 0){	v = v + ","; }
			v = v + "{\"Value\":\"" + this.options[i].value + "\",\"Caption\":\"" + this.options[i].text + "\",\"Selected\":\"" + this.options[i].selected + "\"}";
		}
		
		return "[" + v + "]";
		
	}

	/* METHOD: GetSelectedItemsAsJSONString
	*/
	E.GetSelectedItemsAsJSONString = function(){
	
		var v = "";
		
		for (var i = 0; i < this.options.length; i++){
			if (this.options[i].selected){
				if (v.length != 0){	v = v + ","; }
				v = v + "{\"Value\":\"" + this.options[i].value + "\",\"Caption\":\"" + this.options[i].text + "\",\"Selected\":\"" + this.options[i].selected + "\"}";
			}
		}
		
		return "[" + v + "]";
		
	}
	
	/* METHOD: GetValue
	*/
	E.GetValue = function(){
		
		var v = "";
		
		for (var i = 0; i < this.options.length; i++){
			if(this.options[i].selected){
				if (v.length == 0){ 
					v = this.options[i].value;
				} else {
					v = v + "," + this.options[i].value;
				}
			}
		}
		
		return v;
		
	}

	/* METHOD: SelectItemAtIndex
	*/
	E.SelectItemAtIndex = function(index){
		this.options.selectedIndex = index;
	}
	
	/* METHOD: SelectItemWithValue
	*/
	E.SelectItemWithValue = function(val){
		for (var i = 0; i < this.options.length; i++){
			if (this.options[i].value == val){
				this.options.selectedIndex = i;
				return true;
			}
		}
	}
	
	/* METHOD: SortItemsByCaption
	*/
	E.SortItemsByCaption = function(){
	}
	
	/* METHOD: SortItemsByValue
	*/
	E.SortItemsByValue = function(){
	}
	
	E.AddItemsFromJSONString(data);
	
	return E;
	
}

DOM.text = function(s){
	
	return document.createTextNode(s);
	
}

/*

DOM.image = function(
	controller, 
	name, 
	id, 
	className, 
	appendTo, 
	src	
	){
	
	var E = DOM.c(
		controller, 
		"IMG", 
		{
			"id":(id) ? id : "",
			"name":(name) ? name : "",
			"class":(className) ? className : "",
		},
		appendTo, // parent DOM element
		null
	)
	
	E.setAttribute("src", src);
	
	return E;
	
}
*/

DOM.addClass = function(E, className){
	
	if (!E){return true;}
	
	if (E.className.length == 0){
		E.className = className;
		return true;
	}	
	
	var A = E.className.split(" ");
	
	A.push(className);
	
	E.className = A.join(" ");
	
}

DOM.removeAllClasses = function(E){
	
	E.className = "";
	
}

DOM.removeClass = function(E, className){
	
	// if no element, exit
	
	if(!E){
		
		return;
		
	}
	
	if (E.className == className){
		E.className = "";
		return true;
	}
	
	var A = E.className.split(" ");
	
	for (var i = A.length - 1; i >= 0; i--){ // go in reverse since we are removing
		
		if (A[i] == className){
			
			A.splice(i, 1);
			
		}
		
	}
	
	E.className = A.join(" ");
	
}

/*

Get the actual width of an element

*/
DOM.width = function(E){
	
	try{
		return E.offsetWidth;
	} catch(e){
		return 0;
	}
	
}
/*

Get the actual height of an element

*/
DOM.height = function(E){
	
	try{
		return E.offsetHeight;
	} catch(e){
		return 0;
	}
	
}

DOM.scrollTo = function(E){
	
	try{
		E.scrollIntoView({"behavior":"smooth"});
	} catch(e){
		alert(e);
	}
	
}