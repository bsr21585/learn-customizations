﻿<%@ WebService Language="C#" Class="DFHVRegistrationForm" %>

using System;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.WebControls;
using System.Xml;

[WebService(Description = "DFHV Registration Web Service", Namespace = "https://dfhv.dc.gov/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class DFHVRegistrationForm : System.Web.Services.WebService 
{
    public DFHVRegistrationForm()
    {}
	
	#region Register
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ReturnObject Register(string firstname, string lastname, string email, string username, string facecardid, string checkbox, string password, string confirm, string phonenumber)
    {
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		string stepThroughLog = String.Empty;
		
        ReturnObject retObj = new ReturnObject();		
		retObj.status.result = "success";
		retObj.status.code = "1";
		retObj.status.messages = new List<object>();
        retObj.token = null;
		
		string verificationStatus = String.Empty;
		
		if (facecardid == String.Empty)
		{
			verificationStatus = "valid";
		}
		else
		{
			verificationStatus = VerifyDriver(facecardid);
		}
		
		if (verificationStatus == "valid")
		{
			try
			{
				// REGISTER THE USER USING THE ASENTIA API
				try
				{				
					// USER ACCOUNT REGISTRATION
					stepThroughLog += DateTime.Now.ToString() + " - User registration with API.\r\n";
					
					try
					{ this._DoUserRegistration(firstname, lastname, email, username, facecardid, password, confirm, phonenumber); }
					catch (WebException webEx) // RETRY 1 TIME
					{ this._DoUserRegistration(firstname, lastname, email, username, facecardid, password, confirm, phonenumber); }
					
					stepThroughLog += DateTime.Now.ToString() + " - User registration with API finished.\r\n";
				}
				catch (WebException ex)
				{
					// write the exception to a log with the response
					if (ex.Response != null)
					{
						WebResponse errResp = ex.Response;
					
						using (Stream respStream = errResp.GetResponseStream())
						{
							StreamReader reader = new StreamReader(respStream);
							string text = reader.ReadToEnd();

							this._WriteExceptionToLog(ex.Message, ex.StackTrace, text, stepThroughLog);	
						}
					}
					else // write the exception to a log without the response
					{
						this._WriteExceptionToLog(ex.Message, ex.StackTrace, null, stepThroughLog);	
					}
					
					// bubble up the exception
					throw new Exception("A fatal error occurred when contacting the user registration service. Please contact an administrator.");
				}
			}        
			catch (Exception ex)
			{
				retObj.status.result = "fail";
				retObj.status.code = "0";
				retObj.status.messages = new List<object>();

				string[] exceptionMessages = ex.Message.Split('|');

				foreach (string s in exceptionMessages)
				{
					MessageObject msgObj = new MessageObject();
					msgObj.identifier = "general";
					msgObj.text = s;
					retObj.status.messages.Add(msgObj);
				}
			}
		}
		
		else if (verificationStatus == "invalid")
		{
			retObj.status.result = "fail";
            retObj.status.code = "0";
            retObj.status.messages = new List<object>();

            MessageObject msgObj = new MessageObject();
            msgObj.identifier = "general";
            msgObj.text = "FaceCard Id is invalid.";
            retObj.status.messages.Add(msgObj);
		}
		
		else if (verificationStatus == "suspended")
		{
			retObj.status.result = "fail";
            retObj.status.code = "0";
            retObj.status.messages = new List<object>();

            MessageObject msgObj = new MessageObject();
            msgObj.identifier = "general";
            msgObj.text = "FaceCard Id is suspended.";
            retObj.status.messages.Add(msgObj);
		}

		// return
		return retObj;
    }
    #endregion
	
	#region VerifyDriver
	[WebMethod]
	public string VerifyDriver(string facecardid)
	{
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
	
		// create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://dctcdata.azure-api.net/verification/prod/v2/drivers/" + facecardid);

        request.PreAuthenticate = true;
        request.Headers.Add("Ocp-Apim-Subscription-Key", "3b026c7bb6f34d9ab601379729156ed4");
        request.Accept = "application/xml";
        request.Method = "GET";
		
        // get the response
        string responseString = String.Empty;

		try
		{
			using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
			{
				if (response.StatusCode == HttpStatusCode.OK)
				{
					using (Stream responseStream = response.GetResponseStream())
					{ responseString = new StreamReader(responseStream).ReadToEnd(); }
				}
			
				else
				{ throw new WebException(); }
			}
		}
		catch (Exception ex)
		{
			return("invalid");
		}

        // parse the xml string to a xml object
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        XmlNamespaceManager nsmgr = new XmlNamespaceManager(xmlRoot.NameTable);
        nsmgr.AddNamespace("a", "http://schemas.datacontract.org/2004/07/TransitIQ.Taxi.Verification.API.Models");
        XmlNodeList nodes = xmlRoot.SelectNodes("//a:APIDriver", nsmgr);

        string dlStatus = String.Empty;
        string flStatus = String.Empty;

        foreach (XmlNode node in nodes)
        {
            dlStatus = node["DriverLicenseStatus"].InnerText;		// driver license status
            flStatus = node["FaceLicenseStatus"].InnerText;			// face license status
        }
		
        if ((dlStatus != "Active") || (flStatus != "Active"))
        {
            return "suspended";
        }
		else
		{
			return "valid";
		}
	}
	#endregion


    #region _DoUserRegistration
    private void _DoUserRegistration(string firstname, string lastname, string email, string username, string facecardid, string password, string confirm, string phonenumber)
    {
		
		string field00 = String.Empty;
		
		if (facecardid == String.Empty)
		{
			field00 = "No";
		}
		else
		{
			field00 = "Yes";
		}

        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key>1234567890</key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<user id=\\\"0\\\">";
        xmlPayload += "<firstName><![CDATA[" + firstname + "]]></firstName>";
        xmlPayload += "<lastName><![CDATA[" + lastname + "]]></lastName>";
        xmlPayload += "<email><![CDATA[" + email + "]]></email>";
        xmlPayload += "<username><![CDATA[" + username + "]]></username>";
        xmlPayload += "<password><![CDATA[" + password + "]]></password>";
		xmlPayload += "<mustChangePassword>0</mustChangePassword>";
        xmlPayload += "<idTimezone>15</idTimezone>";
        xmlPayload += "<languageString>en-US</languageString>";
		xmlPayload += "<isActive>true</isActive>";
        xmlPayload += "<phonePrimary><![CDATA[" + phonenumber + "]]></phonePrimary>";
		xmlPayload += "<field00><![CDATA[" + field00 + "]]></field00>";
        xmlPayload += "</user>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://dfhv.asentialms.com/_util/API/Default.asmx/SaveUser");

        // encode payload into bytes
        byte[] bytes;
        bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // throw an error if there is one
        if (statusNode.InnerText == "Error")
        { throw new Exception(descriptionNode.InnerText); }
		
    }
    #endregion

    #region _WriteExceptionToLog
    private void _WriteExceptionToLog(string message, string stackTrace, string response, string stepThroughLog)
    {
        // write the exception to a file
        DateTime dtNow = DateTime.Now;
        string exceptionfileName = "RegistrationException_" + String.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}.log", dtNow);
        string exceptionFileFullPath = Path.Combine(HttpRuntime.AppDomainAppPath + @"static\DFHV\ExceptionLogs\", exceptionfileName);

        using (StreamWriter file = new StreamWriter(exceptionFileFullPath))
        {
            file.WriteLine("EXCEPTION OCCURED ON " + dtNow.ToString());
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION MESSAGE:");
            file.WriteLine(message);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION STACK TRACE:");
            file.WriteLine(stackTrace);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("API RESPONSE:");

            if (!String.IsNullOrWhiteSpace(response))
            { file.WriteLine(response); }
            else
            { file.WriteLine("NO RESPONSE RECEIVED FROM THE API"); }
			
			file.WriteLine("");
            file.WriteLine("");
			file.WriteLine("STEP THROUGH LOG:");
            file.WriteLine(stepThroughLog);            
        }
    }
    #endregion

    #region Helper Structs
    public struct ReturnObject
    { 
        public StatusObject status;
        public string token;
    }

    public struct StatusObject
    {
        public string result;
        public string code;
        public List<object> messages;
    }       
	
	public struct MessageObject
    {
        public string identifier;
        public string text;
    }        	
    #endregion
}
