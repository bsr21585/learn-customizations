function CopyFaceCardIdToUsername() {
	if (document.getElementById('facecardidInput').value.length > 0)
	{
		document.getElementById('usernameInput').value = document.getElementById('facecardidInput').value + 'a';
	}
	else
	{
		document.getElementById('usernameInput').value = '';
	}  
}

function DisableFaceCardId() {
	if (document.getElementById('checkboxInput').checked == true)
	{
		document.getElementById('facecardidInput').disabled = true
		document.getElementById('usernameInput').disabled = false
		
		document.getElementById('facecardidInput').value = '';
		document.getElementById('usernameInput').value = '';
	}
	else
	{
		document.getElementById('facecardidInput').disabled = false
		document.getElementById('usernameInput').disabled = true
		
		document.getElementById('usernameInput').value = '';
	}  
}

RegistrationFormControl = function(appendTo){
	
	this.ObjectType = "DFHVUserRegistrationForm";
	this.Container 	= appendTo;
	
	this.RootElement = new Form(
		this, 
		null, 
		null, 
		"registration-form",
		null,
		"Please wait while we create your DFHV LMS user account."
	).RootElement;
		
	this.RootElement.PostURL 	= "/static/DFHV/svc/RegistrationForm.asmx/Register";
	this.RootElement.PostType   = "json";
	this.RootElement.ReturnData = null;
	
	// Create the instructions container.
	
	DOM.c(
		null, 
		"DIV", 
		{
			"id":"registrationinstructions",			
			"class":"registrationinstructions"
		}, 
		this.RootElement, 
		"Fill out the form fields below to register for an account."
	);	
	
	// Create the groupings.
	
	this.NameGroup 					= DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement);
	this.EmailGroup 				= DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement);
	this.CheckBoxGroup 				= DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement);
	this.FaceCardIdGroup            = DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement);
	this.UsernameGroup              = DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement, "Please note that if you entered a FaceCard ID, then your Username is your FaceCard ID appended with letter 'a'. If you don't have a FaceCard ID, please create your Username on your own.");
	this.PasswordGroup              = DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement, "Your password must be at least 6 characters and no more than 25 characters. It cannot contain 'password', '123456', '654321' or contain the username.");
	this.PhoneNumberGroup           = DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement);	
	this.ButtonGroup 				= DOM.c(this, "DIV", { "class": "input-group" }, this.RootElement);
	
	// Create the inputs.
	
	this.FirstNameInput 			= DOM.c(this.RootElement, "INPUT", { "id": "firstnameInput",         "name": "firstname",        "type": "text",         "maxlength": 255,       "class": "InputNormal" 					  });
	this.LastNameInput 				= DOM.c(this.RootElement, "INPUT", { "id": "lastnameInput",          "name": "lastname",         "type": "text",         "maxlength": 255,       "class": "InputNormal" 					  });
	this.EmailInput 				= DOM.c(this.RootElement, "INPUT", { "id": "emailInput",             "name": "email",            "type": "text",         "maxlength": 512,       "class": "InputLong"   					  });
	this.CheckboxInput           	= DOM.c(this.RootElement, "INPUT", { "id": "checkboxInput",       	 "name": "checkbox",      	 "type": "checkbox",  		"value": "empty",					 "class": "InputNormal", "onclick":"DisableFaceCardId();" 					  });
	this.FaceCardIdInput 			= DOM.c(this.RootElement, "INPUT", { "id": "facecardidInput", 		"name": "facecardid", 		"type": "text", 		"maxlength": 512, 		"class": "InputLong", 	 "onkeyup":"CopyFaceCardIdToUsername();" });
	this.UsernameInput              = DOM.c(this.RootElement, "INPUT", { "id": "usernameInput",          "name": "username",         "type": "text",         "maxlength": 512,       "class": "InputLong",	 "disabled": true });
	this.PasswordInput 				= DOM.c(this.RootElement, "INPUT", { "id": "passwordInput",          "name": "password",         "type": "password",     "maxlength": 255,       "class": "InputNormal" 					  });
	this.ConfirmInput 				= DOM.c(this.RootElement, "INPUT", { "id": "confirmInput",           "name": "confirm",          "type": "password",     "maxlength": 255,       "class": "InputNormal" 					  });
	this.PhoneNumberInput           = DOM.c(this.RootElement, "INPUT", { "id": "phonenumberInput",       "name": "phonenumber",      "type": "text",         "maxlength": 255,       "class": "InputNormal" 					  });
	
	this.SaveButton = DOM.c(
		this, 
		"INPUT",
		{
			"type"	: "button",
			"id"	: "save",
			"name"	: "save", 
			"class"	: "default",
			"value"	: "Create Account"
		}
	)

	this.SaveButton.onclick = function(){
		this.Controller.RootElement.Post();
	}
	
	// Create the controls.
	
	this.FirstNameControl 				= new FormInputControl("First Name",										"firstname",		this.NameGroup,					true);
	this.LastNameControl 				= new FormInputControl("Last Name",											"lastname",			this.NameGroup,					true);
	this.EmailControl 					= new FormInputControl("Email",												"email",			this.EmailGroup, 				true);	
	this.CheckboxControl             	= new FormInputControl("Check this box if you do not have a FaceCard ID",	"checkbox",			this.CheckBoxGroup,				true);
	this.FaceCardIdControl              = new FormInputControl("FaceCard Id",               						"facecardid",       this.FaceCardIdGroup,           false);
	this.UsernameControl                = new FormInputControl("Username",                  						"username",        	this.UsernameGroup,             true);
	this.PasswordControl 				= new FormInputControl("Password",											"password",			this.PasswordGroup,				true);
	this.ConfirmControl 				= new FormInputControl("Confirm Password",									"confirm",			this.PasswordGroup,				true);
	this.PhoneNumberControl             = new FormInputControl("Phone Number",              						"phonenumber",		this.PhoneNumberGroup,          true);

	// Log the controls into the Form object.
	// Each control represents a parameter value that will be sent to/received from the server.
	
	this.RootElement.InputControls.push(this.FirstNameControl);
	this.RootElement.InputControls.push(this.LastNameControl);
	this.RootElement.InputControls.push(this.EmailControl);	
	this.RootElement.InputControls.push(this.CheckboxControl);
	this.RootElement.InputControls.push(this.FaceCardIdControl);
	this.RootElement.InputControls.push(this.UsernameControl);
	this.RootElement.InputControls.push(this.PasswordControl);
	this.RootElement.InputControls.push(this.ConfirmControl);
	this.RootElement.InputControls.push(this.PhoneNumberControl);

	// Attach the inputs to the controls.
	
	this.FirstNameControl.AddInput(this.FirstNameInput);
	this.LastNameControl.AddInput(this.LastNameInput);
	this.EmailControl.AddInput(this.EmailInput);	
	this.CheckboxControl.AddInput(this.CheckboxInput);
	this.FaceCardIdControl.AddInput(this.FaceCardIdInput);
	this.UsernameControl.AddInput(this.UsernameInput);	
	this.PasswordControl.AddInput(this.PasswordInput);
	this.ConfirmControl.AddInput(this.ConfirmInput);
	this.PhoneNumberControl.AddInput(this.PhoneNumberInput);
	
	// Buttons
	
	this.ButtonGroup.appendChild(this.SaveButton);
	
	// Append the form to the container.
	
	try{
		this.Container.appendChild(this.RootElement);
	}catch(e){ }
	
	// Client-side error checking.
	
	this.RootElement.UpdatePasswordConfirm = function(){
		if(this.Controller.ConfirmInput.value.length > 0){
		    if (this.Controller.ConfirmInput.value == this.Controller.PasswordInput.value) {
				this.Controller.ConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.ConfirmControl.HasErrors()){
					this.Controller.ConfirmControl.AddError("Confirmation does not match.");
				}
			}
		}else{
			this.Controller.ConfirmControl.ClearErrors();
		}
	}
	
	this.PasswordInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	this.ConfirmInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	
	// Set Success Actions
	
	this.RootElement.addEventListener(
		"post-successful", 
		function(){
			this.Controller.CompleteRegistration();
		}
	);
	
	this.RootElement.SetSuccessEvent("post-successful", this.RootElement);
	
	// Overloads
	
	this.RootElement.Post = function(){
	
		// move into view
		
		DOM.scrollTo(this);
		
		// clear all the errors
		
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		// set form to "working" 
		DOM.addClass(this, "working");		
		
		if(!this.Controller.DoEasyErrors()){
			DOM.removeClass(this, "working");
			return false;
		}
		
		// post		
		AJAXHelper.Post(
			this.PostURL, 
			this.Controller.RootElement.GetData(),
			this.PostType,
			"post-complete", 
			this
		);
		
	}
}


RegistrationFormControl.prototype.DoEasyErrors = function(){
	
	var s = true;
	
	if(this.FirstNameInput.value.length == 0){
		s = false;
		this.FirstNameControl.AddError("First Name is required.")
	}
	
	if(this.LastNameInput.value.length == 0){
		s = false;
		this.LastNameControl.AddError("Last Name is required.")
	}
	
	if(this.EmailInput.value.length == 0){
		s = false;
		this.EmailControl.AddError("Email is required.")
	}

	if (this.UsernameInput.value.length == 0) {
	    s = false;
	    this.UsernameControl.AddError("Username is required.")
	}

	if (this.FaceCardIdInput.value.length == 0) {
		if (this.UsernameInput.value.length == 0) {
			 s = false;
			this.FaceCardIdControl.AddError("FaceCard Id is required.")
		}
	}
	
	if(this.PasswordInput.value.length == -0){
		s = false;
		this.PasswordControl.AddError("Password is required.")
	}
	
	if(this.ConfirmInput.value != this.PasswordInput.value){
		s = false;
		this.ConfirmControl.AddError("Confirmation does not match.")
	}

	if (this.PhoneNumberInput.value.length == 0) {
	    s = false;
	    this.PhoneNumberControl.AddError("Phone Number is required.")
	}
		
	return s;
	
}


RegistrationFormControl.prototype.CompleteRegistration = function () {
    
    // if we got an SSO token, redirect; otherwise, just proceed with displaying the success message
    if (this.RootElement.ReturnData.token != null) {
        window.location.href = "https://dfhv.asentialms.com/_util/DoSSO.aspx?token=" + this.RootElement.ReturnData.token;
    }
    else {
        this.RootElement.addEventListener(
            "animationend",
            function () {
                this.Controller.Container.appendChild(
                    new UserRegistrationCompletedControl(
                        this.Controller.FirstNameControl.GetValue(),
                        this.Controller.UsernameControl.GetValue()
                        )
                );
                DOM.scrollTo(this.Controller.Container);
                this.Controller.Container.removeChild(this);
            }
        );

        DOM.addClass(this.RootElement, "vanish");
    }
}


UserRegistrationCompletedControl = function(
	name,
	username
	){
	
	this.RootElement = DOM.c(
		this, 
		"DIV",
		{
			"class":"info-control evolve"
		}
	);
	
	this.Something = DOM.c(
		null, 
		"DIV", 
		{
			"class":"primary"
		}, 
		this.RootElement, 
		"Thank you for registering for a DFHV LMS account, " + name + ". Your LMS username is: " + username
		+ ". You may log into the LMS by going to "
	);
	
	DOM.c(
		null, 
		"A", 
		{
		    "href": "https://dfhv.asentialms.com"
		}, 
		this.Something, 
		"https://dfhv.asentialms.com"
	);
	
	return this.RootElement;
	
}