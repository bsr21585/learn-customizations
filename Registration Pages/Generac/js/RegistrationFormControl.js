function RemoveSpacesFromInput(inputElement) {				
		inputElement.value = inputElement.value.replace(" ", "");
}

RegistrationFormControl = function(appendTo){
	
	this.ObjectType = "GeneracUserRegistrationForm";
	this.Container 	= appendTo;
	
	this.RootElement = new Form(
		this, 
		null, 
		null, 
		"registration-form",
		null,
		"Please wait while we create your Generac LMS user account."
	).RootElement;
		
	this.RootElement.PostURL 	= "/static/Generac/svc/RegistrationForm.asmx/Register";
	this.RootElement.PostType = "json";
	this.RootElement.ReturnData = null;
	
	// Create the instructions container.
	DOM.c(
		null, 
		"DIV", 
		{
			"id":"registrationinstructions",			
			"class":"registrationinstructions"
		}, 
		this.RootElement, 
		"Fill out the form fields below to register for an account."
	);	
	
	// Create the groupings.
	
	// Account Groupings
	this.FirstNameGroup 			= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.MiddleNameGroup 			= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.LastNameGroup	 			= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.EmailGroup					= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.UsernameGroup				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.PasswordGroup 				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.ConfirmGroup 				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	
	// Contact Groupings
	this.Address1Group				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.Address2Group				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.CityGroup					= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.CountryGroup				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.StateGroup					= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.ZipGroup					= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	
	this.PrimaryPhoneGroup			= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.MobilePhoneGroup			= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	
	// Employee Groupings
	this.JobTitleGroup				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	
	// Other Groupings
	
	this.CompanyGroup				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	this.ButtonGroup 				= DOM.c(this, "DIV", {"class":"FormFieldContainer"},	this.RootElement);
	
	// Create the inputs.
	
	// Account Inputs
	this.FirstNameInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"firstname", 			"name":"firstname", 			"type":"text", 		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.MiddleNameInput			= DOM.c(this.RootElement, "INPUT",  {"id":"middlename",			"name":"middlename",			"type":"text",		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.LastNameInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"lastname", 			"name":"lastname", 				"type":"text", 		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.EmailInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"email", 				"name":"email",		 			"type":"text", 		"maxlength":512, "class":"InputLong FormFieldInputContainer"});
	this.UsernameInput				= DOM.c(this.RootElement, "INPUT",	{"id":"username",			"name":"username",				"type":"text",		"maxlenght":255, "class":"InputLong FormFieldInputContainer"});
	this.PasswordInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"password", 			"name":"password", 				"type":"password", 	"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.ConfirmInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"confirm", 			"name":"confirm", 				"type":"password", 	"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	
	// Contact Inputs
	this.StreetAddress1Input		= DOM.c(this.RootElement, "INPUT",  {"id":"streetaddress1",		"name":"streetaddress1",		"type":"text",		"maxlength":255, "class":"InputLong FormFieldInputContainer"});
	this.StreetAddress2Input		= DOM.c(this.RootElement, "INPUT",  {"id":"streetaddress2",		"name":"streetaddress2",		"type":"text",		"maxlength":255, "class":"InputLong FormFieldInputContainer"});
	this.CityInput					= DOM.c(this.RootElement, "INPUT",  {"id":"city",				"name":"city",					"type":"text",		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.StateInput					= DOM.c(this.RootElement, "SELECT", {"id":"state", 				"name":"state", 				"type":"text",	 	"maxlength":255, "class":"InputNormal FormFieldInputContainer", "autocomplete":"arandomstring"});
	this.ZipInput					= DOM.c(this.RootElement, "INPUT", 	{"id":"zip",	 			"name":"zip",	 				"type":"text", 		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.CountryInput				= DOM.c(this.RootElement, "SELECT", {"id":"country", 			"name":"country", 				"type":"text", 		"maxlength":255, "class":"InputNormal FormFieldInputContainer", "onchange":"changeCountry();"});
	this.PrimaryPhoneInput			= DOM.c(this.RootElement, "INPUT", 	{"id":"primaryphone", 		"name":"primaryphone", 			"type":"text", 		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	this.MobilePhoneInput			= DOM.c(this.RootElement, "INPUT", 	{"id":"mobilephone", 		"name":"mobilephone", 			"type":"text",	 	"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	
	// Employee Inputs
	this.JobTitleInput				= DOM.c(this.RootElement, "INPUT", 	{"id":"jobtitle", 			"name":"jobtitle", 				"type":"text", 		"maxlength":255, "class":"InputNormal FormFieldInputContainer"});
	
	// Other Inputs
	this.CompanyInput				= DOM.c(this.RootElement, "INPUT", 	{"id":"company", 			"name":"company",		 		"type":"text",	 	"maxlength":255, "class":"InputLong FormFieldInputContainer"});
	
	//this.OptionContainer			= DOM.c(this.RootElement, "SPAN",	{"id":"option-container",	"name":"option-container",		"type":"text",		"maxlength":255, "class":"InputLong FormFieldInputContainer", "style":"visibility: hidden; position:absolute;"});
	// STATES
	
//COUNTRY - AE
//AJ
option = document.createElement("option");
option.text = "AJ - Adschman"
option.value = "AJ"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//AZ
option = document.createElement("option");
option.text = "AZ - Abu Dhabi"
option.value = "AZ"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//DU
option = document.createElement("option");
option.text = "DU - Dubai"
option.value = "DU"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//FU
option = document.createElement("option");
option.text = "FU - Fudshaira"
option.value = "FU"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//RK
option = document.createElement("option");
option.text = "RK - Al-Chaima"
option.value = "RK"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//SH
option = document.createElement("option");
option.text = "SH - Schardscha"
option.value = "SH"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//SHJ
option = document.createElement("option");
option.text = "SHJ - SHARJAH"
option.value = "SHJ"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//UQ
option = document.createElement("option");
option.text = "UQ - Umm al-Qaiwain"
option.value = "UQ"
option.setAttribute("country", "AE");
this.StateInput.add(option);

//COUNTRY - AG
//BB
option = document.createElement("option");
option.text = "BB - Barbuda"
option.value = "BB"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//GE
option = document.createElement("option");
option.text = "GE - Saint George"
option.value = "GE"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//JO
option = document.createElement("option");
option.text = "JO - Saint John"
option.value = "JO"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Saint Mary"
option.value = "MA"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Saint Paul"
option.value = "PA"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Saint Peter"
option.value = "PE"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//PH
option = document.createElement("option");
option.text = "PH - Saint Philip"
option.value = "PH"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//RD
option = document.createElement("option");
option.text = "RD - Redonda"
option.value = "RD"
option.setAttribute("country", "AG");
this.StateInput.add(option);

//COUNTRY - AI
//BP
option = document.createElement("option");
option.text = "BP - Blowing Point"
option.value = "BP"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//EE
option = document.createElement("option");
option.text = "EE - East End"
option.value = "EE"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//GH
option = document.createElement("option");
option.text = "GH - George Hill"
option.value = "GH"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//IH
option = document.createElement("option");
option.text = "IH - Island Harbour"
option.value = "IH"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//NH
option = document.createElement("option");
option.text = "NH - North Hill"
option.value = "NH"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//NS
option = document.createElement("option");
option.text = "NS - North Side"
option.value = "NS"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//SG
option = document.createElement("option");
option.text = "SG - Sandy Ground"
option.value = "SG"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//SH
option = document.createElement("option");
option.text = "SH - Sandy Hill"
option.value = "SH"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - South Hill"
option.value = "SO"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//ST
option = document.createElement("option");
option.text = "ST - Stoney Ground"
option.value = "ST"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//TF
option = document.createElement("option");
option.text = "TF - The Farrington"
option.value = "TF"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//TQ
option = document.createElement("option");
option.text = "TQ - The Quarter"
option.value = "TQ"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//TV
option = document.createElement("option");
option.text = "TV - The Valley"
option.value = "TV"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//WE
option = document.createElement("option");
option.text = "WE - West End"
option.value = "WE"
option.setAttribute("country", "AI");
this.StateInput.add(option);

//COUNTRY - AR
//0
option = document.createElement("option");
option.text = "0 - Capital Federal"
option.value = "0"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//1
option = document.createElement("option");
option.text = "1 - Buenos Aires"
option.value = "1"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Catamarca"
option.value = "2"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Cordoba"
option.value = "3"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Corrientes"
option.value = "4"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Entre Rios"
option.value = "5"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Jujuy"
option.value = "6"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Mendoza"
option.value = "7"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - La Rioja"
option.value = "8"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Salta"
option.value = "9"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - San Juan"
option.value = "10"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - San Luis"
option.value = "11"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Santa Fe"
option.value = "12"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Santiago del Estero"
option.value = "13"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Tucuman"
option.value = "14"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Chaco"
option.value = "16"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Chubut"
option.value = "17"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Formosa"
option.value = "18"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Misiones"
option.value = "19"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Neuquen"
option.value = "20"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - La Pampa"
option.value = "21"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Rio Negro"
option.value = "22"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Santa Cruz"
option.value = "23"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Tierra de Fuego"
option.value = "24"
option.setAttribute("country", "AR");
this.StateInput.add(option);

//COUNTRY - AT
//B
option = document.createElement("option");
option.text = "B - Burgenland"
option.value = "B"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//K
option = document.createElement("option");
option.text = "K - Carinthia"
option.value = "K"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//NOE
option = document.createElement("option");
option.text = "NOE - Lower Austria"
option.value = "NOE"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//OOE
option = document.createElement("option");
option.text = "OOE - Upper Austria"
option.value = "OOE"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//S
option = document.createElement("option");
option.text = "S - Salzburg"
option.value = "S"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//ST
option = document.createElement("option");
option.text = "ST - Styria"
option.value = "ST"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//T
option = document.createElement("option");
option.text = "T - Tyrol"
option.value = "T"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//V
option = document.createElement("option");
option.text = "V - Vorarlberg"
option.value = "V"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//W
option = document.createElement("option");
option.text = "W - Vienna"
option.value = "W"
option.setAttribute("country", "AT");
this.StateInput.add(option);

//COUNTRY - AU
//ACT
option = document.createElement("option");
option.text = "ACT - Aust Capital Terr"
option.value = "ACT"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//NSW
option = document.createElement("option");
option.text = "NSW - New South Wales"
option.value = "NSW"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//NT
option = document.createElement("option");
option.text = "NT - Northern Territory"
option.value = "NT"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//QLD
option = document.createElement("option");
option.text = "QLD - Queensland"
option.value = "QLD"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - South Australia"
option.value = "SA"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//TAS
option = document.createElement("option");
option.text = "TAS - Tasmania"
option.value = "TAS"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//VIC
option = document.createElement("option");
option.text = "VIC - Victoria"
option.value = "VIC"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Western Australia"
option.value = "WA"
option.setAttribute("country", "AU");
this.StateInput.add(option);

//COUNTRY - AW
//1
option = document.createElement("option");
option.text = "1 - Noord / Tanki Leende"
option.value = "1"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Oranjestad West"
option.value = "2"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Oranjestad Oost"
option.value = "3"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Paradera"
option.value = "4"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - San Nicolas Noord"
option.value = "5"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - San Nicolas Zuid"
option.value = "6"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Santa Cruz"
option.value = "7"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Savaneta"
option.value = "8"
option.setAttribute("country", "AW");
this.StateInput.add(option);

//COUNTRY - BB
//1
option = document.createElement("option");
option.text = "1 - Christ Church"
option.value = "1"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Saint Andrew"
option.value = "2"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Saint George"
option.value = "3"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Saint James"
option.value = "4"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Saint John"
option.value = "5"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Saint Joseph"
option.value = "6"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Saint Lucy"
option.value = "7"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Saint Michael"
option.value = "8"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Saint Peter"
option.value = "9"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Saint Philip"
option.value = "10"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Saint Thomas"
option.value = "11"
option.setAttribute("country", "BB");
this.StateInput.add(option);

//COUNTRY - BD
//13
option = document.createElement("option");
option.text = "13 - Dhaka"
option.value = "13"
option.setAttribute("country", "BD");
this.StateInput.add(option);

//COUNTRY - BE
//1
option = document.createElement("option");
option.text = "1 - Antwerp"
option.value = "1"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Brabant (Flemish)"
option.value = "2"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Hainaut"
option.value = "3"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Liege"
option.value = "4"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Limburg"
option.value = "5"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Luxembourg"
option.value = "6"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Namur"
option.value = "7"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Oost-Vlaanderen"
option.value = "8"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - West-Vlaanderen"
option.value = "9"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Brabant (Walloon)"
option.value = "10"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Brussels (Capital)"
option.value = "11"
option.setAttribute("country", "BE");
this.StateInput.add(option);

//COUNTRY - BF
//1
option = document.createElement("option");
option.text = "1 - Boucle du Mouhoun"
option.value = "1"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Cascades"
option.value = "2"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Centre"
option.value = "3"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Centre Est"
option.value = "4"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Centre Nord"
option.value = "5"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Centre Quest"
option.value = "6"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Centre Sud"
option.value = "7"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Est"
option.value = "8"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Hauts Bassins"
option.value = "9"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Nord"
option.value = "10"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Plateau Central"
option.value = "11"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Sahel"
option.value = "12"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Sud-Quest"
option.value = "13"
option.setAttribute("country", "BF");
this.StateInput.add(option);

//COUNTRY - BG
//1
option = document.createElement("option");
option.text = "1 - Burgas"
option.value = "1"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Grad Sofiya"
option.value = "2"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Khaskovo"
option.value = "3"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Lovech"
option.value = "4"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Montana"
option.value = "5"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Plovdiv"
option.value = "6"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Ruse"
option.value = "7"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Sofiya"
option.value = "8"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Varna"
option.value = "9"
option.setAttribute("country", "BG");
this.StateInput.add(option);

//COUNTRY - BH
//13
option = document.createElement("option");
option.text = "13 - Capital Governorate"
option.value = "13"
option.setAttribute("country", "BH");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Southern Governorate"
option.value = "14"
option.setAttribute("country", "BH");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Muharraq Governorate"
option.value = "15"
option.setAttribute("country", "BH");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Northern Governorate"
option.value = "17"
option.setAttribute("country", "BH");
this.StateInput.add(option);

//COUNTRY - BM
//DE
option = document.createElement("option");
option.text = "DE - Devonshire"
option.value = "DE"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//HA
option = document.createElement("option");
option.text = "HA - Hamilton"
option.value = "HA"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//HC
option = document.createElement("option");
option.text = "HC - Hamilton Municipalit"
option.value = "HC"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Paget"
option.value = "PA"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Pembroke"
option.value = "PE"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Sandys"
option.value = "SA"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//SC
option = document.createElement("option");
option.text = "SC - Saint Georges"
option.value = "SC"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//SG
option = document.createElement("option");
option.text = "SG - Saint George Municip"
option.value = "SG"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//SM
option = document.createElement("option");
option.text = "SM - Smiths"
option.value = "SM"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Southampton"
option.value = "SO"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Warwick"
option.value = "WA"
option.setAttribute("country", "BM");
this.StateInput.add(option);

//COUNTRY - BO
//CB
option = document.createElement("option");
option.text = "CB - Cochabamba"
option.value = "CB"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//CQ
option = document.createElement("option");
option.text = "CQ - Chuquisaca"
option.value = "CQ"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//EB
option = document.createElement("option");
option.text = "EB - Beni"
option.value = "EB"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//LP
option = document.createElement("option");
option.text = "LP - La Paz"
option.value = "LP"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//OR
option = document.createElement("option");
option.text = "OR - Oruro"
option.value = "OR"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Pando"
option.value = "PA"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//PO
option = document.createElement("option");
option.text = "PO - Potosi"
option.value = "PO"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//SC
option = document.createElement("option");
option.text = "SC - Santa Cruz"
option.value = "SC"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//TR
option = document.createElement("option");
option.text = "TR - Tarija"
option.value = "TR"
option.setAttribute("country", "BO");
this.StateInput.add(option);

//COUNTRY - BQ
//1
option = document.createElement("option");
option.text = "1 - "
option.value = "1"
option.setAttribute("country", "BQ");
this.StateInput.add(option);

//COUNTRY - BR
//AC
option = document.createElement("option");
option.text = "AC - Acre"
option.value = "AC"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//AL
option = document.createElement("option");
option.text = "AL - Alagoas"
option.value = "AL"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//AM
option = document.createElement("option");
option.text = "AM - Amazon"
option.value = "AM"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//AP
option = document.createElement("option");
option.text = "AP - Amapa"
option.value = "AP"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Bahia"
option.value = "BA"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//CE
option = document.createElement("option");
option.text = "CE - Ceara"
option.value = "CE"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//DF
option = document.createElement("option");
option.text = "DF - Brasilia"
option.value = "DF"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//ES
option = document.createElement("option");
option.text = "ES - Espirito Santo"
option.value = "ES"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//GO
option = document.createElement("option");
option.text = "GO - Goias"
option.value = "GO"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Maranhao"
option.value = "MA"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//MG
option = document.createElement("option");
option.text = "MG - Minas Gerais"
option.value = "MG"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//MS
option = document.createElement("option");
option.text = "MS - Mato Grosso do Sul"
option.value = "MS"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Mato Grosso"
option.value = "MT"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Para"
option.value = "PA"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//PB
option = document.createElement("option");
option.text = "PB - Paraiba"
option.value = "PB"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Pernambuco"
option.value = "PE"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//PI
option = document.createElement("option");
option.text = "PI - Piaui"
option.value = "PI"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//PR
option = document.createElement("option");
option.text = "PR - Parana"
option.value = "PR"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//RJ
option = document.createElement("option");
option.text = "RJ - Rio de Janeiro"
option.value = "RJ"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//RN
option = document.createElement("option");
option.text = "RN - Rio Grande do Norte"
option.value = "RN"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//RO
option = document.createElement("option");
option.text = "RO - Rondonia"
option.value = "RO"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//RR
option = document.createElement("option");
option.text = "RR - Roraima"
option.value = "RR"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//RS
option = document.createElement("option");
option.text = "RS - Rio Grande do Sul"
option.value = "RS"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//SC
option = document.createElement("option");
option.text = "SC - Santa Catarina"
option.value = "SC"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//SE
option = document.createElement("option");
option.text = "SE - Sergipe"
option.value = "SE"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//SP
option = document.createElement("option");
option.text = "SP - Sao Paulo"
option.value = "SP"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//TO
option = document.createElement("option");
option.text = "TO - Tocantins"
option.value = "TO"
option.setAttribute("country", "BR");
this.StateInput.add(option);

//COUNTRY - BS
//AB
option = document.createElement("option");
option.text = "AB - Abaco"
option.value = "AB"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//AK
option = document.createElement("option");
option.text = "AK - Acklins Island"
option.value = "AK"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//AN
option = document.createElement("option");
option.text = "AN - Andros"
option.value = "AN"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//BI
option = document.createElement("option");
option.text = "BI - Bimini Islands"
option.value = "BI"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//CI
option = document.createElement("option");
option.text = "CI - Cat Island"
option.value = "CI"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//CK
option = document.createElement("option");
option.text = "CK - Crooked Islands"
option.value = "CK"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//EL
option = document.createElement("option");
option.text = "EL - Eleuthera"
option.value = "EL"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//EX
option = document.createElement("option");
option.text = "EX - Exuma"
option.value = "EX"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//GB
option = document.createElement("option");
option.text = "GB - Grand Bahama"
option.value = "GB"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//HB
option = document.createElement("option");
option.text = "HB - Harbour Island"
option.value = "HB"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//IN
option = document.createElement("option");
option.text = "IN - Inagua"
option.value = "IN"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//LI
option = document.createElement("option");
option.text = "LI - Long Island"
option.value = "LI"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//MG
option = document.createElement("option");
option.text = "MG - Mayaguana"
option.value = "MG"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//NW
option = document.createElement("option");
option.text = "NW - New Providence"
option.value = "NW"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//RI
option = document.createElement("option");
option.text = "RI - Ragged Islands"
option.value = "RI"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//SS
option = document.createElement("option");
option.text = "SS - San Salvador"
option.value = "SS"
option.setAttribute("country", "BS");
this.StateInput.add(option);

//COUNTRY - BW
//GA
option = document.createElement("option");
option.text = "GA - GABORONE"
option.value = "GA"
option.setAttribute("country", "BW");
this.StateInput.add(option);

//COUNTRY - BZ
//BZ
option = document.createElement("option");
option.text = "BZ - Belize"
option.value = "BZ"
option.setAttribute("country", "BZ");
this.StateInput.add(option);

//CY
option = document.createElement("option");
option.text = "CY - Cayo"
option.value = "CY"
option.setAttribute("country", "BZ");
this.StateInput.add(option);

//CZ
option = document.createElement("option");
option.text = "CZ - Corozal"
option.value = "CZ"
option.setAttribute("country", "BZ");
this.StateInput.add(option);

//OW
option = document.createElement("option");
option.text = "OW - Orange Walk"
option.value = "OW"
option.setAttribute("country", "BZ");
this.StateInput.add(option);

//SC
option = document.createElement("option");
option.text = "SC - Stann Creek"
option.value = "SC"
option.setAttribute("country", "BZ");
this.StateInput.add(option);

//TO
option = document.createElement("option");
option.text = "TO - Toledo"
option.value = "TO"
option.setAttribute("country", "BZ");
this.StateInput.add(option);

//COUNTRY - CA
//AB
option = document.createElement("option");
option.text = "AB - Alberta"
option.value = "AB"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//BC
option = document.createElement("option");
option.text = "BC - British Columbia"
option.value = "BC"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//MB
option = document.createElement("option");
option.text = "MB - Manitoba"
option.value = "MB"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//NB
option = document.createElement("option");
option.text = "NB - New Brunswick"
option.value = "NB"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//NL
option = document.createElement("option");
option.text = "NL - Newfoundland & Labr."
option.value = "NL"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//NS
option = document.createElement("option");
option.text = "NS - Nova Scotia"
option.value = "NS"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//NT
option = document.createElement("option");
option.text = "NT - Northwest Terr."
option.value = "NT"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//NU
option = document.createElement("option");
option.text = "NU - Nunavut"
option.value = "NU"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//ON
option = document.createElement("option");
option.text = "ON - Ontario"
option.value = "ON"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Prince Edward Island"
option.value = "PE"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//QC
option = document.createElement("option");
option.text = "QC - Quebec"
option.value = "QC"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//SK
option = document.createElement("option");
option.text = "SK - Saskatchewan"
option.value = "SK"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//YT
option = document.createElement("option");
option.text = "YT - Yukon Territory"
option.value = "YT"
option.setAttribute("country", "CA");
this.StateInput.add(option);

//COUNTRY - CH
//AG
option = document.createElement("option");
option.text = "AG - Aargau"
option.value = "AG"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//AI
option = document.createElement("option");
option.text = "AI - Inner-Rhoden"
option.value = "AI"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//AR
option = document.createElement("option");
option.text = "AR - Ausser-Rhoden"
option.value = "AR"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//BE
option = document.createElement("option");
option.text = "BE - Bern"
option.value = "BE"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//BL
option = document.createElement("option");
option.text = "BL - Basel Land"
option.value = "BL"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//BS
option = document.createElement("option");
option.text = "BS - Basel Stadt"
option.value = "BS"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//FR
option = document.createElement("option");
option.text = "FR - Fribourg"
option.value = "FR"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//GE
option = document.createElement("option");
option.text = "GE - Geneva"
option.value = "GE"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//GL
option = document.createElement("option");
option.text = "GL - Glarus"
option.value = "GL"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//GR
option = document.createElement("option");
option.text = "GR - Graubuenden"
option.value = "GR"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//JU
option = document.createElement("option");
option.text = "JU - Jura"
option.value = "JU"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//LU
option = document.createElement("option");
option.text = "LU - Lucerne"
option.value = "LU"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//NE
option = document.createElement("option");
option.text = "NE - Neuchatel"
option.value = "NE"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//NW
option = document.createElement("option");
option.text = "NW - Nidwalden"
option.value = "NW"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//OW
option = document.createElement("option");
option.text = "OW - Obwalden"
option.value = "OW"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//SG
option = document.createElement("option");
option.text = "SG - St. Gallen"
option.value = "SG"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//SH
option = document.createElement("option");
option.text = "SH - Schaffhausen"
option.value = "SH"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Solothurn"
option.value = "SO"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//SZ
option = document.createElement("option");
option.text = "SZ - Schwyz"
option.value = "SZ"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//TG
option = document.createElement("option");
option.text = "TG - Thurgau"
option.value = "TG"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//TI
option = document.createElement("option");
option.text = "TI - Ticino"
option.value = "TI"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//UR
option = document.createElement("option");
option.text = "UR - Uri"
option.value = "UR"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//VD
option = document.createElement("option");
option.text = "VD - Vaud"
option.value = "VD"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//VS
option = document.createElement("option");
option.text = "VS - Valais"
option.value = "VS"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//ZG
option = document.createElement("option");
option.text = "ZG - Zug"
option.value = "ZG"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//ZH
option = document.createElement("option");
option.text = "ZH - Zurich"
option.value = "ZH"
option.setAttribute("country", "CH");
this.StateInput.add(option);

//COUNTRY - CI
//AB
option = document.createElement("option");
option.text = "AB - ABIDJAN"
option.value = "AB"
option.setAttribute("country", "CI");
this.StateInput.add(option);

//COUNTRY - CL
//1
option = document.createElement("option");
option.text = "1 - I - Iquique"
option.value = "1"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - II - Antofagasta"
option.value = "2"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - III - Copiapo"
option.value = "3"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - IV - La Serena"
option.value = "4"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - V - Valparaiso"
option.value = "5"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - VI - Rancagua"
option.value = "6"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - VII - Talca"
option.value = "7"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - VIII - Concepcion"
option.value = "8"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - IX - Temuco"
option.value = "9"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - X - Puerto Montt"
option.value = "10"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - XI - Coyhaique"
option.value = "11"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - XII - Punta Arenas"
option.value = "12"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - RM - Santiago"
option.value = "13"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//LR
option = document.createElement("option");
option.text = "LR - XIV- LOS RIOS"
option.value = "LR"
option.setAttribute("country", "CL");
this.StateInput.add(option);

//COUNTRY - CN
//10
option = document.createElement("option");
option.text = "10 - Beijing"
option.value = "10"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Shanghai"
option.value = "20"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Tianjin"
option.value = "30"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Nei Mongol"
option.value = "40"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Shanxi"
option.value = "50"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//60
option = document.createElement("option");
option.text = "60 - Hebei"
option.value = "60"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//70
option = document.createElement("option");
option.text = "70 - Liaoning"
option.value = "70"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//80
option = document.createElement("option");
option.text = "80 - Jilin"
option.value = "80"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//90
option = document.createElement("option");
option.text = "90 - Heilongjiang"
option.value = "90"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//100
option = document.createElement("option");
option.text = "100 - Jiangsu"
option.value = "100"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//110
option = document.createElement("option");
option.text = "110 - Anhui"
option.value = "110"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//120
option = document.createElement("option");
option.text = "120 - Shandong"
option.value = "120"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//130
option = document.createElement("option");
option.text = "130 - Zhejiang"
option.value = "130"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//140
option = document.createElement("option");
option.text = "140 - Jiangxi"
option.value = "140"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//150
option = document.createElement("option");
option.text = "150 - Fujian"
option.value = "150"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//160
option = document.createElement("option");
option.text = "160 - Hunan"
option.value = "160"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//170
option = document.createElement("option");
option.text = "170 - Hubei"
option.value = "170"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//180
option = document.createElement("option");
option.text = "180 - Henan"
option.value = "180"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//190
option = document.createElement("option");
option.text = "190 - Guangdong"
option.value = "190"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//200
option = document.createElement("option");
option.text = "200 - Hainan"
option.value = "200"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//210
option = document.createElement("option");
option.text = "210 - Guangxi"
option.value = "210"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//220
option = document.createElement("option");
option.text = "220 - Guizhou"
option.value = "220"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//230
option = document.createElement("option");
option.text = "230 - Sichuan"
option.value = "230"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//240
option = document.createElement("option");
option.text = "240 - Yunnan"
option.value = "240"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//250
option = document.createElement("option");
option.text = "250 - Shaanxi"
option.value = "250"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//260
option = document.createElement("option");
option.text = "260 - Gansu"
option.value = "260"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//270
option = document.createElement("option");
option.text = "270 - Ningxia"
option.value = "270"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//280
option = document.createElement("option");
option.text = "280 - Qinghai"
option.value = "280"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//290
option = document.createElement("option");
option.text = "290 - Xinjiang"
option.value = "290"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//300
option = document.createElement("option");
option.text = "300 - Xizang"
option.value = "300"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//320
option = document.createElement("option");
option.text = "320 - Chong Qing"
option.value = "320"
option.setAttribute("country", "CN");
this.StateInput.add(option);

//COUNTRY - CO
//5
option = document.createElement("option");
option.text = "5 - ANTIOQUIA"
option.value = "5"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - ATLANTICO"
option.value = "8"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - BOGOTA"
option.value = "11"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - BOLIVAR"
option.value = "13"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - BOYACA"
option.value = "15"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - CALDAS"
option.value = "17"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - CAQUETA"
option.value = "18"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - CAUCA"
option.value = "19"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - CESAR"
option.value = "20"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - CORDOBA"
option.value = "23"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - CUNDINAMARCA"
option.value = "25"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - CHOCO"
option.value = "27"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - HUILA"
option.value = "41"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - LA GUAJIRA"
option.value = "44"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - MAGDALENA"
option.value = "47"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - META"
option.value = "50"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - NARINO"
option.value = "52"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//54
option = document.createElement("option");
option.text = "54 - NORTE SANTANDER"
option.value = "54"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//63
option = document.createElement("option");
option.text = "63 - QUINDIO"
option.value = "63"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//66
option = document.createElement("option");
option.text = "66 - RISARALDA"
option.value = "66"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//68
option = document.createElement("option");
option.text = "68 - SANTANDER"
option.value = "68"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//70
option = document.createElement("option");
option.text = "70 - SUCRE"
option.value = "70"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//73
option = document.createElement("option");
option.text = "73 - TOLIMA"
option.value = "73"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//76
option = document.createElement("option");
option.text = "76 - VALLE"
option.value = "76"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//81
option = document.createElement("option");
option.text = "81 - ARAUCA"
option.value = "81"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//85
option = document.createElement("option");
option.text = "85 - CASANARE"
option.value = "85"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//86
option = document.createElement("option");
option.text = "86 - PUTUMAYO"
option.value = "86"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//88
option = document.createElement("option");
option.text = "88 - SAN ANDRES"
option.value = "88"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//91
option = document.createElement("option");
option.text = "91 - AMAZONAS"
option.value = "91"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//94
option = document.createElement("option");
option.text = "94 - GUAINIA"
option.value = "94"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//95
option = document.createElement("option");
option.text = "95 - GUAVIARE"
option.value = "95"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//97
option = document.createElement("option");
option.text = "97 - VAUPES"
option.value = "97"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//99
option = document.createElement("option");
option.text = "99 - VICHADA"
option.value = "99"
option.setAttribute("country", "CO");
this.StateInput.add(option);

//COUNTRY - CR
//A
option = document.createElement("option");
option.text = "A - Alajuela"
option.value = "A"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//C
option = document.createElement("option");
option.text = "C - Cartago"
option.value = "C"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//G
option = document.createElement("option");
option.text = "G - Guanacaste"
option.value = "G"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//H
option = document.createElement("option");
option.text = "H - Heredia"
option.value = "H"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//L
option = document.createElement("option");
option.text = "L - Limón"
option.value = "L"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//P
option = document.createElement("option");
option.text = "P - Puntarenas"
option.value = "P"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - San José"
option.value = "SJ"
option.setAttribute("country", "CR");
this.StateInput.add(option);

//COUNTRY - CW
//CW
option = document.createElement("option");
option.text = "CW - "
option.value = "CW"
option.setAttribute("country", "CW");
this.StateInput.add(option);

//COUNTRY - CY
//1
option = document.createElement("option");
option.text = "1 - NICOSIA"
option.value = "1"
option.setAttribute("country", "CY");
this.StateInput.add(option);

//COUNTRY - CZ
//11
option = document.createElement("option");
option.text = "11 - Praha"
option.value = "11"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Stredocesky"
option.value = "21"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Jihocesky"
option.value = "31"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Plzensky"
option.value = "32"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Karlovarsky"
option.value = "41"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Ustecky"
option.value = "42"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//51
option = document.createElement("option");
option.text = "51 - Liberecky"
option.value = "51"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - Kralovehradecky"
option.value = "52"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//53
option = document.createElement("option");
option.text = "53 - Pardubicky"
option.value = "53"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//61
option = document.createElement("option");
option.text = "61 - Vysocina"
option.value = "61"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//62
option = document.createElement("option");
option.text = "62 - Jihomoravsky"
option.value = "62"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//71
option = document.createElement("option");
option.text = "71 - Olomoucky"
option.value = "71"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//72
option = document.createElement("option");
option.text = "72 - Zlinsky"
option.value = "72"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//81
option = document.createElement("option");
option.text = "81 - Moravskoslezsky"
option.value = "81"
option.setAttribute("country", "CZ");
this.StateInput.add(option);

//COUNTRY - DE
//1
option = document.createElement("option");
option.text = "1 - Schleswig-Holstein"
option.value = "1"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Hamburg"
option.value = "2"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Lower Saxony"
option.value = "3"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Bremen"
option.value = "4"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Nrth Rhine Westfalia"
option.value = "5"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Hessen"
option.value = "6"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Rhineland Palatinate"
option.value = "7"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Baden-Wurttemberg"
option.value = "8"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Bavaria"
option.value = "9"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Saarland"
option.value = "10"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Berlin"
option.value = "11"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Brandenburg"
option.value = "12"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Mecklenburg-Vorpomm."
option.value = "13"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Saxony"
option.value = "14"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Saxony-Anhalt"
option.value = "15"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Thuringia"
option.value = "16"
option.setAttribute("country", "DE");
this.StateInput.add(option);

//COUNTRY - DK
//1
option = document.createElement("option");
option.text = "1 - Danish Capital Reg."
option.value = "1"
option.setAttribute("country", "DK");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Central Jutland"
option.value = "2"
option.setAttribute("country", "DK");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - North Jutland"
option.value = "3"
option.setAttribute("country", "DK");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Zealand"
option.value = "4"
option.setAttribute("country", "DK");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - South Denmark"
option.value = "5"
option.setAttribute("country", "DK");
this.StateInput.add(option);

//COUNTRY - DM
//4
option = document.createElement("option");
option.text = "4 - SAINT GEORGE"
option.value = "4"
option.setAttribute("country", "DM");
this.StateInput.add(option);

//COUNTRY - DO
//1
option = document.createElement("option");
option.text = "1 - Distrito Nacional"
option.value = "1"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Azua"
option.value = "2"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Bahoruco"
option.value = "3"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Barahona"
option.value = "4"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Dajabón"
option.value = "5"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Duarte"
option.value = "6"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Elías Piña"
option.value = "7"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - El Seibo"
option.value = "8"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Espaillat"
option.value = "9"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Independencia"
option.value = "10"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - La Altagracia"
option.value = "11"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - La Romana"
option.value = "12"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - La Vega"
option.value = "13"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - María Trinidad Sánch"
option.value = "14"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Monte Cristi"
option.value = "15"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Pedernales"
option.value = "16"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Peravia"
option.value = "17"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Puerto Plata"
option.value = "18"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Hermanas Mirabal"
option.value = "19"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Samaná"
option.value = "20"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - San Cristóbal"
option.value = "21"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - San Juan"
option.value = "22"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - San Pedro de Macorís"
option.value = "23"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Sánchez Ramírez"
option.value = "24"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Santiago"
option.value = "25"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Santiago Rodríguez"
option.value = "26"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Valverde"
option.value = "27"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Monseñor Nouel"
option.value = "28"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Monte Plata"
option.value = "29"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Hato Mayor"
option.value = "30"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - San José de Ocoa"
option.value = "31"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Santo Domingo"
option.value = "32"
option.setAttribute("country", "DO");
this.StateInput.add(option);

//COUNTRY - DZ
//1
option = document.createElement("option");
option.text = "1 - Adrar"
option.value = "1"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Chlef"
option.value = "2"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Laghouat"
option.value = "3"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Oum el Bouaghi"
option.value = "4"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Batna"
option.value = "5"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Béjaïa"
option.value = "6"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Biskra"
option.value = "7"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Béchar"
option.value = "8"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Blida"
option.value = "9"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Bouira"
option.value = "10"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Tamanghasset"
option.value = "11"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Tébessa"
option.value = "12"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Tlemcen"
option.value = "13"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Tiaret"
option.value = "14"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Tizi Ouzou"
option.value = "15"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Alger"
option.value = "16"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Djelfa"
option.value = "17"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Jijel"
option.value = "18"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Sétif"
option.value = "19"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Saïda"
option.value = "20"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Skikda"
option.value = "21"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Sidi Bel Abbès"
option.value = "22"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Annaba"
option.value = "23"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Guelma"
option.value = "24"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Constantine"
option.value = "25"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Médéa"
option.value = "26"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Mostaganem"
option.value = "27"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Msila"
option.value = "28"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Mascara"
option.value = "29"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Ouargla"
option.value = "30"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Oran"
option.value = "31"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - El Bayadh"
option.value = "32"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Illizi"
option.value = "33"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Bordj Bou Arréridj"
option.value = "34"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Boumerdès"
option.value = "35"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - El Tarf"
option.value = "36"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Tindouf"
option.value = "37"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Tissemsilt"
option.value = "38"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - El Oued"
option.value = "39"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Khenchela"
option.value = "40"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Souk Ahras"
option.value = "41"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Tipaza"
option.value = "42"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Mila"
option.value = "43"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Aïn Defla"
option.value = "44"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Naama"
option.value = "45"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Aïn Témouchent"
option.value = "46"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Ghardaïa"
option.value = "47"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Relizane"
option.value = "48"
option.setAttribute("country", "DZ");
this.StateInput.add(option);

//COUNTRY - EC
//A
option = document.createElement("option");
option.text = "A - Azuay"
option.value = "A"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//B
option = document.createElement("option");
option.text = "B - Bolívar"
option.value = "B"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//C
option = document.createElement("option");
option.text = "C - Carchi"
option.value = "C"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//D
option = document.createElement("option");
option.text = "D - Orellana"
option.value = "D"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//E
option = document.createElement("option");
option.text = "E - Esmeraldas"
option.value = "E"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//F
option = document.createElement("option");
option.text = "F - Cañar"
option.value = "F"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//G
option = document.createElement("option");
option.text = "G - Guayas"
option.value = "G"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//H
option = document.createElement("option");
option.text = "H - Chimborazo"
option.value = "H"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//I
option = document.createElement("option");
option.text = "I - Imbabura"
option.value = "I"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//L
option = document.createElement("option");
option.text = "L - Loja"
option.value = "L"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//M
option = document.createElement("option");
option.text = "M - Manabí"
option.value = "M"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//N
option = document.createElement("option");
option.text = "N - Napo"
option.value = "N"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//O
option = document.createElement("option");
option.text = "O - El Oro"
option.value = "O"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//P
option = document.createElement("option");
option.text = "P - Pichincha"
option.value = "P"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//R
option = document.createElement("option");
option.text = "R - Los Ríos"
option.value = "R"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//S
option = document.createElement("option");
option.text = "S - Morona-Santiago"
option.value = "S"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//SD
option = document.createElement("option");
option.text = "SD - Santo Domingo de los"
option.value = "SD"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//SE
option = document.createElement("option");
option.text = "SE - Santa Elena"
option.value = "SE"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//T
option = document.createElement("option");
option.text = "T - Tungurahua"
option.value = "T"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//U
option = document.createElement("option");
option.text = "U - Sucumbíos"
option.value = "U"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//W
option = document.createElement("option");
option.text = "W - Galápagos"
option.value = "W"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//X
option = document.createElement("option");
option.text = "X - Cotopaxi"
option.value = "X"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//Y
option = document.createElement("option");
option.text = "Y - Pastaza"
option.value = "Y"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//Z
option = document.createElement("option");
option.text = "Z - Zamora-Chinchipe"
option.value = "Z"
option.setAttribute("country", "EC");
this.StateInput.add(option);

//COUNTRY - EE
//37
option = document.createElement("option");
option.text = "37 - HARJU"
option.value = "37"
option.setAttribute("country", "EE");
this.StateInput.add(option);

//COUNTRY - EG
//ALX
option = document.createElement("option");
option.text = "ALX - Al Iskandar#yah"
option.value = "ALX"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//ASN
option = document.createElement("option");
option.text = "ASN - Asw#n"
option.value = "ASN"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//AST
option = document.createElement("option");
option.text = "AST - Asy#t"
option.value = "AST"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Al Bahr al Ahmar"
option.value = "BA"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//BH
option = document.createElement("option");
option.text = "BH - Al Buhayrah"
option.value = "BH"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//BNS
option = document.createElement("option");
option.text = "BNS - Ban# Suwayf"
option.value = "BNS"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//C
option = document.createElement("option");
option.text = "C - Al Q#hirah"
option.value = "C"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//DK
option = document.createElement("option");
option.text = "DK - Ad Daqahl#yah"
option.value = "DK"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//DT
option = document.createElement("option");
option.text = "DT - Dumy#t"
option.value = "DT"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//FYM
option = document.createElement("option");
option.text = "FYM - Al Fayy#m"
option.value = "FYM"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//GH
option = document.createElement("option");
option.text = "GH - Al Gharb#yah"
option.value = "GH"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//GZ
option = document.createElement("option");
option.text = "GZ - Al J#zah"
option.value = "GZ"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//IS
option = document.createElement("option");
option.text = "IS - Al Ism#`#l#yah"
option.value = "IS"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//JS
option = document.createElement("option");
option.text = "JS - Jan#b S#n#'"
option.value = "JS"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//KB
option = document.createElement("option");
option.text = "KB - Al Qaly#b#yah"
option.value = "KB"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//KFS
option = document.createElement("option");
option.text = "KFS - Kafr ash Shaykh"
option.value = "KFS"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//KN
option = document.createElement("option");
option.text = "KN - Qin#"
option.value = "KN"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//LX
option = document.createElement("option");
option.text = "LX - Al Uqsur"
option.value = "LX"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//MN
option = document.createElement("option");
option.text = "MN - Al Miny#"
option.value = "MN"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//MNF
option = document.createElement("option");
option.text = "MNF - Al Min#f#yah"
option.value = "MNF"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Matr#h"
option.value = "MT"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//PTS
option = document.createElement("option");
option.text = "PTS - B#r Sa`#d"
option.value = "PTS"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//SHG
option = document.createElement("option");
option.text = "SHG - S#h#j"
option.value = "SHG"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//SHR
option = document.createElement("option");
option.text = "SHR - Ash Sharq#yah"
option.value = "SHR"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//SIN
option = document.createElement("option");
option.text = "SIN - Shamal S#n#'"
option.value = "SIN"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//SUZ
option = document.createElement("option");
option.text = "SUZ - As Suways"
option.value = "SUZ"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//WAD
option = document.createElement("option");
option.text = "WAD - Al W#d# al Jad#d"
option.value = "WAD"
option.setAttribute("country", "EG");
this.StateInput.add(option);

//COUNTRY - ES
//1
option = document.createElement("option");
option.text = "1 - Alava"
option.value = "1"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Albacete"
option.value = "2"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Alicante"
option.value = "3"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Almeria"
option.value = "4"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Avila"
option.value = "5"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Badajoz"
option.value = "6"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Baleares"
option.value = "7"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Barcelona"
option.value = "8"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Burgos"
option.value = "9"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Caceres"
option.value = "10"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Cadiz"
option.value = "11"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Castellon"
option.value = "12"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Ciudad Real"
option.value = "13"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Cordoba"
option.value = "14"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - La Coruna"
option.value = "15"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Cuenca"
option.value = "16"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Gerona"
option.value = "17"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Granada"
option.value = "18"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Guadalajara"
option.value = "19"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Guipuzcoa"
option.value = "20"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Huelva"
option.value = "21"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Huesca"
option.value = "22"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Jaen"
option.value = "23"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Leon"
option.value = "24"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Lerida"
option.value = "25"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - La Rioja"
option.value = "26"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Lugo"
option.value = "27"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Madrid"
option.value = "28"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Malaga"
option.value = "29"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Murcia"
option.value = "30"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Navarra"
option.value = "31"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Orense"
option.value = "32"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Asturias"
option.value = "33"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Palencia"
option.value = "34"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Las Palmas"
option.value = "35"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Pontevedra"
option.value = "36"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Salamanca"
option.value = "37"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Sta. Cruz Tenerife"
option.value = "38"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Cantabria"
option.value = "39"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Segovia"
option.value = "40"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Sevilla"
option.value = "41"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Soria"
option.value = "42"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Tarragona"
option.value = "43"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Teruel"
option.value = "44"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Toledo"
option.value = "45"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Valencia"
option.value = "46"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Valladolid"
option.value = "47"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Vizcaya"
option.value = "48"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - Zamora"
option.value = "49"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Zaragoza"
option.value = "50"
option.setAttribute("country", "ES");
this.StateInput.add(option);

//COUNTRY - FI
//1
option = document.createElement("option");
option.text = "1 - Ahvenanmaa"
option.value = "1"
option.setAttribute("country", "FI");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Southern Finnland"
option.value = "2"
option.setAttribute("country", "FI");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Eastern Finnland"
option.value = "3"
option.setAttribute("country", "FI");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Lappi"
option.value = "4"
option.setAttribute("country", "FI");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Western Finnland"
option.value = "5"
option.setAttribute("country", "FI");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Oulu"
option.value = "6"
option.setAttribute("country", "FI");
this.StateInput.add(option);

//COUNTRY - FJ
//3
option = document.createElement("option");
option.text = "3 - CAKAUDROVE"
option.value = "3"
option.setAttribute("country", "FJ");
this.StateInput.add(option);

//COUNTRY - FM
//KSA
option = document.createElement("option");
option.text = "KSA - KOSRAE"
option.value = "KSA"
option.setAttribute("country", "FM");
this.StateInput.add(option);

//COUNTRY - FR
//1
option = document.createElement("option");
option.text = "1 - Ain"
option.value = "1"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Aisne"
option.value = "2"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Allier"
option.value = "3"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Alpes (Hte-Provence)"
option.value = "4"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Alpes (Hautes)"
option.value = "5"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Alpes-Maritimes"
option.value = "6"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Ardeche"
option.value = "7"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Ardennes"
option.value = "8"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Ariege"
option.value = "9"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Aube"
option.value = "10"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Aude"
option.value = "11"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Aveyron"
option.value = "12"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Bouches-du-Rhone"
option.value = "13"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Calvados"
option.value = "14"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Cantal"
option.value = "15"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Charente"
option.value = "16"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Charente-Maritime"
option.value = "17"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Cher"
option.value = "18"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Correze"
option.value = "19"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Cote-d'Or"
option.value = "21"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Cotes-d'Armor"
option.value = "22"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Creuse"
option.value = "23"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Dordogne"
option.value = "24"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Doubs"
option.value = "25"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Drome"
option.value = "26"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Eure"
option.value = "27"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Eure-et-Loir"
option.value = "28"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Finistere"
option.value = "29"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//2A
option = document.createElement("option");
option.text = "2A - Corse-du-Sud"
option.value = "2A"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//2B
option = document.createElement("option");
option.text = "2B - Corse-du-Nord"
option.value = "2B"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Gard"
option.value = "30"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Garonne (Haute)"
option.value = "31"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Gers"
option.value = "32"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Gironde"
option.value = "33"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Herault"
option.value = "34"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Ille-et-Vilaine"
option.value = "35"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Indre"
option.value = "36"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Indre-et-Loire"
option.value = "37"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Isere"
option.value = "38"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Jura"
option.value = "39"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Landes"
option.value = "40"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Loir-et-Cher"
option.value = "41"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Loire"
option.value = "42"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Loire (Haute)"
option.value = "43"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Loire-Atlantique"
option.value = "44"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Loiret"
option.value = "45"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Lot"
option.value = "46"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Lot-et-Garonne"
option.value = "47"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Lozere"
option.value = "48"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - Maine-et-Loire"
option.value = "49"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Manche"
option.value = "50"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//51
option = document.createElement("option");
option.text = "51 - Marne"
option.value = "51"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - Marne (Haute)"
option.value = "52"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//53
option = document.createElement("option");
option.text = "53 - Mayenne"
option.value = "53"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//54
option = document.createElement("option");
option.text = "54 - Meurthe-et-Moselle"
option.value = "54"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//55
option = document.createElement("option");
option.text = "55 - Meuse"
option.value = "55"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//56
option = document.createElement("option");
option.text = "56 - Morbihan"
option.value = "56"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//57
option = document.createElement("option");
option.text = "57 - Moselle"
option.value = "57"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//58
option = document.createElement("option");
option.text = "58 - Nievre"
option.value = "58"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//59
option = document.createElement("option");
option.text = "59 - Nord"
option.value = "59"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//60
option = document.createElement("option");
option.text = "60 - Oise"
option.value = "60"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//61
option = document.createElement("option");
option.text = "61 - Orne"
option.value = "61"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//62
option = document.createElement("option");
option.text = "62 - Pas-de-Calais"
option.value = "62"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//63
option = document.createElement("option");
option.text = "63 - Puy-de-Dome"
option.value = "63"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//64
option = document.createElement("option");
option.text = "64 - Pyrenees-Atlantiques"
option.value = "64"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//65
option = document.createElement("option");
option.text = "65 - Pyrenees (Hautes)"
option.value = "65"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//66
option = document.createElement("option");
option.text = "66 - Pyrenees-Orientales"
option.value = "66"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//67
option = document.createElement("option");
option.text = "67 - Bas-Rhin"
option.value = "67"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//68
option = document.createElement("option");
option.text = "68 - Haut-Rhin"
option.value = "68"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//69
option = document.createElement("option");
option.text = "69 - Rhone"
option.value = "69"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//70
option = document.createElement("option");
option.text = "70 - Saone (Haute)"
option.value = "70"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//71
option = document.createElement("option");
option.text = "71 - Saone-et-Loire"
option.value = "71"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//72
option = document.createElement("option");
option.text = "72 - Sarthe"
option.value = "72"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//73
option = document.createElement("option");
option.text = "73 - Savoie"
option.value = "73"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//74
option = document.createElement("option");
option.text = "74 - Savoie (Haute)"
option.value = "74"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//75
option = document.createElement("option");
option.text = "75 - Paris"
option.value = "75"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//76
option = document.createElement("option");
option.text = "76 - Seine-Maritime"
option.value = "76"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//77
option = document.createElement("option");
option.text = "77 - Seine-et-Marne"
option.value = "77"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//78
option = document.createElement("option");
option.text = "78 - Yvelines"
option.value = "78"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//79
option = document.createElement("option");
option.text = "79 - Sevres (Deux)"
option.value = "79"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//80
option = document.createElement("option");
option.text = "80 - Somme"
option.value = "80"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//81
option = document.createElement("option");
option.text = "81 - Tarn"
option.value = "81"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//82
option = document.createElement("option");
option.text = "82 - Tarn-et-Garonne"
option.value = "82"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//83
option = document.createElement("option");
option.text = "83 - Var"
option.value = "83"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//84
option = document.createElement("option");
option.text = "84 - Vaucluse"
option.value = "84"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//85
option = document.createElement("option");
option.text = "85 - Vendee"
option.value = "85"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//86
option = document.createElement("option");
option.text = "86 - Vienne"
option.value = "86"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//87
option = document.createElement("option");
option.text = "87 - Vienne (Haute)"
option.value = "87"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//88
option = document.createElement("option");
option.text = "88 - Vosges"
option.value = "88"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//89
option = document.createElement("option");
option.text = "89 - Yonne"
option.value = "89"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//90
option = document.createElement("option");
option.text = "90 - Territ.-de-Belfort"
option.value = "90"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//91
option = document.createElement("option");
option.text = "91 - Essonne"
option.value = "91"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//92
option = document.createElement("option");
option.text = "92 - Hauts-de-Seine"
option.value = "92"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//93
option = document.createElement("option");
option.text = "93 - Seine-Saint-Denis"
option.value = "93"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//94
option = document.createElement("option");
option.text = "94 - Val-de-Marne"
option.value = "94"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//95
option = document.createElement("option");
option.text = "95 - Val-d'Oise"
option.value = "95"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//97
option = document.createElement("option");
option.text = "97 - D.O.M.-T.O.M."
option.value = "97"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//971
option = document.createElement("option");
option.text = "971 - Guadeloupe"
option.value = "971"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//972
option = document.createElement("option");
option.text = "972 - Martinique"
option.value = "972"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//973
option = document.createElement("option");
option.text = "973 - Guyane"
option.value = "973"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//974
option = document.createElement("option");
option.text = "974 - Reunion"
option.value = "974"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//975
option = document.createElement("option");
option.text = "975 - Saint-Pierre-et-Miq."
option.value = "975"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//976
option = document.createElement("option");
option.text = "976 - Wallis-et-Futuna"
option.value = "976"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//99
option = document.createElement("option");
option.text = "99 - Hors-France"
option.value = "99"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//SBH
option = document.createElement("option");
option.text = "SBH - SAINT BARTHELEMY"
option.value = "SBH"
option.setAttribute("country", "FR");
this.StateInput.add(option);

//COUNTRY - GB
//AB
option = document.createElement("option");
option.text = "AB - Aberdeenshire"
option.value = "AB"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//AG
option = document.createElement("option");
option.text = "AG - Argyllshire"
option.value = "AG"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//AL
option = document.createElement("option");
option.text = "AL - Anglesey"
option.value = "AL"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//AM
option = document.createElement("option");
option.text = "AM - Armagh"
option.value = "AM"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//AN
option = document.createElement("option");
option.text = "AN - Angus/Forfarshire"
option.value = "AN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//AT
option = document.createElement("option");
option.text = "AT - Antrim"
option.value = "AT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//AY
option = document.createElement("option");
option.text = "AY - Ayrshire"
option.value = "AY"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BE
option = document.createElement("option");
option.text = "BE - Bedfordshire"
option.value = "BE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BF
option = document.createElement("option");
option.text = "BF - Banffshire"
option.value = "BF"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BK
option = document.createElement("option");
option.text = "BK - Berkshire"
option.value = "BK"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BR
option = document.createElement("option");
option.text = "BR - Brecknockshire"
option.value = "BR"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BS
option = document.createElement("option");
option.text = "BS - Bath&NthEstSomerset"
option.value = "BS"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BT
option = document.createElement("option");
option.text = "BT - Buteshire"
option.value = "BT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BU
option = document.createElement("option");
option.text = "BU - Buckinghamshire"
option.value = "BU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//BW
option = document.createElement("option");
option.text = "BW - Berwickshire"
option.value = "BW"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - Cambridgeshire"
option.value = "CA"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CB
option = document.createElement("option");
option.text = "CB - Carmarthenshire"
option.value = "CB"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CD
option = document.createElement("option");
option.text = "CD - Cardiganshire"
option.value = "CD"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CF
option = document.createElement("option");
option.text = "CF - Caernarfonshire"
option.value = "CF"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CH
option = document.createElement("option");
option.text = "CH - Cheshire"
option.value = "CH"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CM
option = document.createElement("option");
option.text = "CM - Cromartyshire"
option.value = "CM"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CN
option = document.createElement("option");
option.text = "CN - Clackmannanshire"
option.value = "CN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CO
option = document.createElement("option");
option.text = "CO - Cornwall"
option.value = "CO"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CT
option = document.createElement("option");
option.text = "CT - Caithness"
option.value = "CT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//CU
option = document.createElement("option");
option.text = "CU - Cumberland"
option.value = "CU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DB
option = document.createElement("option");
option.text = "DB - Derbyshire"
option.value = "DB"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DD
option = document.createElement("option");
option.text = "DD - Denbighshire"
option.value = "DD"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DF
option = document.createElement("option");
option.text = "DF - Dumfriesshire"
option.value = "DF"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DN
option = document.createElement("option");
option.text = "DN - Down"
option.value = "DN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DO
option = document.createElement("option");
option.text = "DO - Dorset"
option.value = "DO"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DT
option = document.createElement("option");
option.text = "DT - Dunbartonshire"
option.value = "DT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DU
option = document.createElement("option");
option.text = "DU - Durham"
option.value = "DU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//DV
option = document.createElement("option");
option.text = "DV - Devon"
option.value = "DV"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//EL
option = document.createElement("option");
option.text = "EL - East Lothian"
option.value = "EL"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//ES
option = document.createElement("option");
option.text = "ES - Essex"
option.value = "ES"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//FI
option = document.createElement("option");
option.text = "FI - Fife"
option.value = "FI"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//FL
option = document.createElement("option");
option.text = "FL - Flintshire"
option.value = "FL"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//FM
option = document.createElement("option");
option.text = "FM - Fermanagh"
option.value = "FM"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//GL
option = document.createElement("option");
option.text = "GL - Gloucestershire"
option.value = "GL"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//HA
option = document.createElement("option");
option.text = "HA - Hampshire"
option.value = "HA"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//HT
option = document.createElement("option");
option.text = "HT - Hertfordshire"
option.value = "HT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//HU
option = document.createElement("option");
option.text = "HU - Huntingdonshire"
option.value = "HU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//HW
option = document.createElement("option");
option.text = "HW - Hereford and Worcs."
option.value = "HW"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//IN
option = document.createElement("option");
option.text = "IN - Invernesshire"
option.value = "IN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//IW
option = document.createElement("option");
option.text = "IW - Isle of Wight"
option.value = "IW"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//KE
option = document.createElement("option");
option.text = "KE - Kent"
option.value = "KE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//KI
option = document.createElement("option");
option.text = "KI - Kincardineshire"
option.value = "KI"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//KK
option = document.createElement("option");
option.text = "KK - Kirkcudbrightshire"
option.value = "KK"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//KN
option = document.createElement("option");
option.text = "KN - Kinross-shire"
option.value = "KN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//LA
option = document.createElement("option");
option.text = "LA - Lancashire"
option.value = "LA"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//LD
option = document.createElement("option");
option.text = "LD - Londonderry"
option.value = "LD"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//LE
option = document.createElement("option");
option.text = "LE - Leicestershire"
option.value = "LE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//LI
option = document.createElement("option");
option.text = "LI - Lincolnshire"
option.value = "LI"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//LN
option = document.createElement("option");
option.text = "LN - Lanarkshire"
option.value = "LN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//MD
option = document.createElement("option");
option.text = "MD - Midlothian"
option.value = "MD"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//ME
option = document.createElement("option");
option.text = "ME - Merioneth"
option.value = "ME"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//MG
option = document.createElement("option");
option.text = "MG - Mid Glamorgan"
option.value = "MG"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//MM
option = document.createElement("option");
option.text = "MM - Monmouthshire"
option.value = "MM"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//MR
option = document.createElement("option");
option.text = "MR - Morayshire"
option.value = "MR"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Montgomeryshire"
option.value = "MT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//MX
option = document.createElement("option");
option.text = "MX - Middlesex"
option.value = "MX"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//NH
option = document.createElement("option");
option.text = "NH - Northamptonshire"
option.value = "NH"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//NK
option = document.createElement("option");
option.text = "NK - Norfolk"
option.value = "NK"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//NR
option = document.createElement("option");
option.text = "NR - Nairnshire"
option.value = "NR"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//NT
option = document.createElement("option");
option.text = "NT - Nottinghamshire"
option.value = "NT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//NU
option = document.createElement("option");
option.text = "NU - Northumberland"
option.value = "NU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//OR
option = document.createElement("option");
option.text = "OR - Orkney"
option.value = "OR"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//OX
option = document.createElement("option");
option.text = "OX - Oxfordshire"
option.value = "OX"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Peeblesshire"
option.value = "PE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//PM
option = document.createElement("option");
option.text = "PM - Pembrokeshire"
option.value = "PM"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//PR
option = document.createElement("option");
option.text = "PR - Perthshire"
option.value = "PR"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//RA
option = document.createElement("option");
option.text = "RA - Radnorshire"
option.value = "RA"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//RE
option = document.createElement("option");
option.text = "RE - Renfrewshire"
option.value = "RE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//RO
option = document.createElement("option");
option.text = "RO - Ross-shire"
option.value = "RO"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//RU
option = document.createElement("option");
option.text = "RU - Rutland"
option.value = "RU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//RX
option = document.createElement("option");
option.text = "RX - Roxburghshire"
option.value = "RX"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SE
option = document.createElement("option");
option.text = "SE - East Sussex"
option.value = "SE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SF
option = document.createElement("option");
option.text = "SF - Selkirkshire"
option.value = "SF"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SG
option = document.createElement("option");
option.text = "SG - South Glamorgan"
option.value = "SG"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SH
option = document.createElement("option");
option.text = "SH - Shropshire"
option.value = "SH"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SK
option = document.createElement("option");
option.text = "SK - Suffolk"
option.value = "SK"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SL
option = document.createElement("option");
option.text = "SL - Shetland"
option.value = "SL"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Somerset"
option.value = "SO"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//ST
option = document.createElement("option");
option.text = "ST - Staffordshire"
option.value = "ST"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SU
option = document.createElement("option");
option.text = "SU - Sutherland"
option.value = "SU"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SV
option = document.createElement("option");
option.text = "SV - Stirlingshire"
option.value = "SV"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SW
option = document.createElement("option");
option.text = "SW - West Sussex"
option.value = "SW"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//SY
option = document.createElement("option");
option.text = "SY - Surrey"
option.value = "SY"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//TY
option = document.createElement("option");
option.text = "TY - Tyrone"
option.value = "TY"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Warwickshire"
option.value = "WA"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WC
option = document.createElement("option");
option.text = "WC - Worcestershire"
option.value = "WC"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WE
option = document.createElement("option");
option.text = "WE - Westmorland"
option.value = "WE"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WG
option = document.createElement("option");
option.text = "WG - West Glamorgan"
option.value = "WG"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WI
option = document.createElement("option");
option.text = "WI - Wiltshire"
option.value = "WI"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WK
option = document.createElement("option");
option.text = "WK - West Lothian"
option.value = "WK"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//WT
option = document.createElement("option");
option.text = "WT - Wigtownshire"
option.value = "WT"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//YN
option = document.createElement("option");
option.text = "YN - North Yorkshire"
option.value = "YN"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//YS
option = document.createElement("option");
option.text = "YS - South Yorkshire"
option.value = "YS"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//YW
option = document.createElement("option");
option.text = "YW - West Yorkshire"
option.value = "YW"
option.setAttribute("country", "GB");
this.StateInput.add(option);

//COUNTRY - GD
//AN
option = document.createElement("option");
option.text = "AN - Saint Andrew"
option.value = "AN"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - Carriacou"
option.value = "CA"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//DA
option = document.createElement("option");
option.text = "DA - Saint David"
option.value = "DA"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//GE
option = document.createElement("option");
option.text = "GE - Saint George"
option.value = "GE"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//JO
option = document.createElement("option");
option.text = "JO - Saint John"
option.value = "JO"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Saint Mark"
option.value = "MA"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Saint Patrick"
option.value = "PA"
option.setAttribute("country", "GD");
this.StateInput.add(option);

//COUNTRY - GE
//AB
option = document.createElement("option");
option.text = "AB - Abkhazia"
option.value = "AB"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//AJ
option = document.createElement("option");
option.text = "AJ - Ajaria"
option.value = "AJ"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//GU
option = document.createElement("option");
option.text = "GU - Guria"
option.value = "GU"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//IM
option = document.createElement("option");
option.text = "IM - Imereti"
option.value = "IM"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//KA
option = document.createElement("option");
option.text = "KA - K'akheti"
option.value = "KA"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//KK
option = document.createElement("option");
option.text = "KK - Kvemo Kartli"
option.value = "KK"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//MM
option = document.createElement("option");
option.text = "MM - Mtskheta-Mtianeti"
option.value = "MM"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//RL
option = document.createElement("option");
option.text = "RL - Rach'a-Lechkhumi-Kve"
option.value = "RL"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - Samtskhe-Javakheti"
option.value = "SJ"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//SK
option = document.createElement("option");
option.text = "SK - Shida Kartli"
option.value = "SK"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//SZ
option = document.createElement("option");
option.text = "SZ - Samegrelo-Zemo Svane"
option.value = "SZ"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//TB
option = document.createElement("option");
option.text = "TB - Tbilisi"
option.value = "TB"
option.setAttribute("country", "GE");
this.StateInput.add(option);

//COUNTRY - GH
//AA
option = document.createElement("option");
option.text = "AA - Greater Accra"
option.value = "AA"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//AH
option = document.createElement("option");
option.text = "AH - Ashanti"
option.value = "AH"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Brong-Ahafo"
option.value = "BA"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//CP
option = document.createElement("option");
option.text = "CP - Central"
option.value = "CP"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//EP
option = document.createElement("option");
option.text = "EP - Eastern"
option.value = "EP"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//NP
option = document.createElement("option");
option.text = "NP - Northern"
option.value = "NP"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//TV
option = document.createElement("option");
option.text = "TV - Volta"
option.value = "TV"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//UE
option = document.createElement("option");
option.text = "UE - Upper East"
option.value = "UE"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//UW
option = document.createElement("option");
option.text = "UW - Upper West"
option.value = "UW"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//WP
option = document.createElement("option");
option.text = "WP - Western"
option.value = "WP"
option.setAttribute("country", "GH");
this.StateInput.add(option);

//COUNTRY - GP
//1
option = document.createElement("option");
option.text = "1 - Basse-Terre"
option.value = "1"
option.setAttribute("country", "GP");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Grande-Terre"
option.value = "2"
option.setAttribute("country", "GP");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Marie-Galante"
option.value = "3"
option.setAttribute("country", "GP");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - La Désirade"
option.value = "4"
option.setAttribute("country", "GP");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Îles des Saintes"
option.value = "5"
option.setAttribute("country", "GP");
this.StateInput.add(option);

//COUNTRY - GR
//1
option = document.createElement("option");
option.text = "1 - Aitolia kai Akarnan."
option.value = "1"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Akhaia"
option.value = "2"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Argolis"
option.value = "3"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Arkadhia"
option.value = "4"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Arta"
option.value = "5"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Attiki"
option.value = "6"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Dhodhekanisos"
option.value = "7"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Dhrama"
option.value = "8"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Evritania"
option.value = "9"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Evros"
option.value = "10"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Evvoia"
option.value = "11"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Florina"
option.value = "12"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Fokis"
option.value = "13"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Fthiotis"
option.value = "14"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Grevena"
option.value = "15"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Ilia"
option.value = "16"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Imathia"
option.value = "17"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Ioannina"
option.value = "18"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Iraklion"
option.value = "19"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Kardhitsa"
option.value = "20"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Kastoria"
option.value = "21"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Kavala"
option.value = "22"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Kefallinia"
option.value = "23"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Kerkira"
option.value = "24"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Khalkidhiki"
option.value = "25"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Khania"
option.value = "26"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Khios"
option.value = "27"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Kikladhes"
option.value = "28"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Kilkis"
option.value = "29"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Korinthia"
option.value = "30"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Kozani"
option.value = "31"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Lakonia"
option.value = "32"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Larisa"
option.value = "33"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Lasithi"
option.value = "34"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Lesvos"
option.value = "35"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Levkas"
option.value = "36"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Magnisia"
option.value = "37"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Messinia"
option.value = "38"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Pella"
option.value = "39"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Pieria"
option.value = "40"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Piraievs"
option.value = "41"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Preveza"
option.value = "42"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Rethimni"
option.value = "43"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Rodhopi"
option.value = "44"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Samos"
option.value = "45"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Serrai"
option.value = "46"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Thesprotia"
option.value = "47"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Thessaloniki"
option.value = "48"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - Trikala"
option.value = "49"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Voiotia"
option.value = "50"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//51
option = document.createElement("option");
option.text = "51 - Xanthi"
option.value = "51"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - Zakinthos"
option.value = "52"
option.setAttribute("country", "GR");
this.StateInput.add(option);

//COUNTRY - GT
//AB
option = document.createElement("option");
option.text = "AB - Alta Verapaz"
option.value = "AB"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//BV
option = document.createElement("option");
option.text = "BV - Baja Verapaz"
option.value = "BV"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//CM
option = document.createElement("option");
option.text = "CM - Chimaltenango"
option.value = "CM"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//CQ
option = document.createElement("option");
option.text = "CQ - Chiquimula"
option.value = "CQ"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//ES
option = document.createElement("option");
option.text = "ES - Escuintla"
option.value = "ES"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//GU
option = document.createElement("option");
option.text = "GU - Guatemala"
option.value = "GU"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//HU
option = document.createElement("option");
option.text = "HU - Huehuetenango"
option.value = "HU"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//IZ
option = document.createElement("option");
option.text = "IZ - Izabel"
option.value = "IZ"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//JA
option = document.createElement("option");
option.text = "JA - Jalapa"
option.value = "JA"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//JU
option = document.createElement("option");
option.text = "JU - Jutiapa"
option.value = "JU"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Peten"
option.value = "PE"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//PR
option = document.createElement("option");
option.text = "PR - El Progresso"
option.value = "PR"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//QC
option = document.createElement("option");
option.text = "QC - Quiche"
option.value = "QC"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//QZ
option = document.createElement("option");
option.text = "QZ - Quetzaltenango"
option.value = "QZ"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//RE
option = document.createElement("option");
option.text = "RE - Retalhuleu"
option.value = "RE"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Sacatepequez"
option.value = "SA"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//SM
option = document.createElement("option");
option.text = "SM - San Marcos"
option.value = "SM"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Solola"
option.value = "SO"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//SR
option = document.createElement("option");
option.text = "SR - Santa Rosa"
option.value = "SR"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//SU
option = document.createElement("option");
option.text = "SU - Suchitepequez"
option.value = "SU"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//TO
option = document.createElement("option");
option.text = "TO - Totonicapan"
option.value = "TO"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//ZA
option = document.createElement("option");
option.text = "ZA - Zacapa"
option.value = "ZA"
option.setAttribute("country", "GT");
this.StateInput.add(option);

//COUNTRY - GU
//AH
option = document.createElement("option");
option.text = "AH - Agana Heights"
option.value = "AH"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//AN
option = document.createElement("option");
option.text = "AN - Hagåtña"
option.value = "AN"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//AS
option = document.createElement("option");
option.text = "AS - Asan"
option.value = "AS"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//AT
option = document.createElement("option");
option.text = "AT - Agat"
option.value = "AT"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Barrigada"
option.value = "BA"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//CP
option = document.createElement("option");
option.text = "CP - Chalan-Pago-Ordot"
option.value = "CP"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//DD
option = document.createElement("option");
option.text = "DD - Dededo"
option.value = "DD"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//IN
option = document.createElement("option");
option.text = "IN - Inarajan"
option.value = "IN"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Mangilao"
option.value = "MA"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//ME
option = document.createElement("option");
option.text = "ME - Merizo"
option.value = "ME"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Mongmong-Toto-Maite"
option.value = "MT"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//PI
option = document.createElement("option");
option.text = "PI - Piti"
option.value = "PI"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - Sinajana"
option.value = "SJ"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//SR
option = document.createElement("option");
option.text = "SR - Santa Rita"
option.value = "SR"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//TF
option = document.createElement("option");
option.text = "TF - Talofofo"
option.value = "TF"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//TM
option = document.createElement("option");
option.text = "TM - Tamuning"
option.value = "TM"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//UM
option = document.createElement("option");
option.text = "UM - Umatac"
option.value = "UM"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//YG
option = document.createElement("option");
option.text = "YG - Yigo"
option.value = "YG"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//YN
option = document.createElement("option");
option.text = "YN - Yona"
option.value = "YN"
option.setAttribute("country", "GU");
this.StateInput.add(option);

//COUNTRY - GY
//BA
option = document.createElement("option");
option.text = "BA - Barima-Waini"
option.value = "BA"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//CU
option = document.createElement("option");
option.text = "CU - Cuyuni-Mazaruni"
option.value = "CU"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//DE
option = document.createElement("option");
option.text = "DE - Demerara-Mahaica"
option.value = "DE"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//EB
option = document.createElement("option");
option.text = "EB - East Bergice-Corenty"
option.value = "EB"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//ES
option = document.createElement("option");
option.text = "ES - Essequibo Islands-We"
option.value = "ES"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Mahaica-Berbice"
option.value = "MA"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//PM
option = document.createElement("option");
option.text = "PM - Pomeroon-Supenaam"
option.value = "PM"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//PT
option = document.createElement("option");
option.text = "PT - Potaro-Siparuni"
option.value = "PT"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//UD
option = document.createElement("option");
option.text = "UD - Upper Demerara-Berbi"
option.value = "UD"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//UT
option = document.createElement("option");
option.text = "UT - Upper Takutu-Upper E"
option.value = "UT"
option.setAttribute("country", "GY");
this.StateInput.add(option);

//COUNTRY - HK
//HK
option = document.createElement("option");
option.text = "HK - Hong Kong Island"
option.value = "HK"
option.setAttribute("country", "HK");
this.StateInput.add(option);

//KLN
option = document.createElement("option");
option.text = "KLN - Kowloon"
option.value = "KLN"
option.setAttribute("country", "HK");
this.StateInput.add(option);

//NT
option = document.createElement("option");
option.text = "NT - New Territories"
option.value = "NT"
option.setAttribute("country", "HK");
this.StateInput.add(option);

//COUNTRY - HN
//AT
option = document.createElement("option");
option.text = "AT - Atlántida"
option.value = "AT"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//CH
option = document.createElement("option");
option.text = "CH - Choluteca"
option.value = "CH"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//CL
option = document.createElement("option");
option.text = "CL - Colón"
option.value = "CL"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//CM
option = document.createElement("option");
option.text = "CM - Comayagua"
option.value = "CM"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//CP
option = document.createElement("option");
option.text = "CP - Copán"
option.value = "CP"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//CR
option = document.createElement("option");
option.text = "CR - Cortés"
option.value = "CR"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//EP
option = document.createElement("option");
option.text = "EP - El Paraíso"
option.value = "EP"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//FM
option = document.createElement("option");
option.text = "FM - Francisco Morazán"
option.value = "FM"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//GD
option = document.createElement("option");
option.text = "GD - Gracias a Dios"
option.value = "GD"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//IB
option = document.createElement("option");
option.text = "IB - Islas de la Bahía"
option.value = "IB"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//IN
option = document.createElement("option");
option.text = "IN - Intibucá"
option.value = "IN"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//LE
option = document.createElement("option");
option.text = "LE - Lempira"
option.value = "LE"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//LP
option = document.createElement("option");
option.text = "LP - La Paz"
option.value = "LP"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//OC
option = document.createElement("option");
option.text = "OC - Ocotepeque"
option.value = "OC"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//OL
option = document.createElement("option");
option.text = "OL - Olancho"
option.value = "OL"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//SB
option = document.createElement("option");
option.text = "SB - Santa Bárbara"
option.value = "SB"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//VA
option = document.createElement("option");
option.text = "VA - Valle"
option.value = "VA"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//YO
option = document.createElement("option");
option.text = "YO - Yoro"
option.value = "YO"
option.setAttribute("country", "HN");
this.StateInput.add(option);

//COUNTRY - HR
//A00
option = document.createElement("option");
option.text = "A00 - Zagrebacka"
option.value = "A00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//B00
option = document.createElement("option");
option.text = "B00 - Krapinsko-zagorska"
option.value = "B00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//C00
option = document.createElement("option");
option.text = "C00 - Sisacko-moslavacka"
option.value = "C00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//D00
option = document.createElement("option");
option.text = "D00 - Karlovacka"
option.value = "D00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//E00
option = document.createElement("option");
option.text = "E00 - Varazdinska"
option.value = "E00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//F00
option = document.createElement("option");
option.text = "F00 - Koprivnicko-krizevac"
option.value = "F00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//G00
option = document.createElement("option");
option.text = "G00 - Bjelovarsko-bilogors"
option.value = "G00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//H00
option = document.createElement("option");
option.text = "H00 - Rijecko-goranska"
option.value = "H00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//I00
option = document.createElement("option");
option.text = "I00 - Licko-senjska"
option.value = "I00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//J00
option = document.createElement("option");
option.text = "J00 - Viroviticko-podravac"
option.value = "J00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//K00
option = document.createElement("option");
option.text = "K00 - Pozesko-slavonska"
option.value = "K00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//L00
option = document.createElement("option");
option.text = "L00 - Slavonskobrodska"
option.value = "L00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//M00
option = document.createElement("option");
option.text = "M00 - Zadarska"
option.value = "M00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//N00
option = document.createElement("option");
option.text = "N00 - Osjecko-baranjska"
option.value = "N00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//O00
option = document.createElement("option");
option.text = "O00 - Sibensko-kninska"
option.value = "O00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//P00
option = document.createElement("option");
option.text = "P00 - Vukovarsko-srijemska"
option.value = "P00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//R00
option = document.createElement("option");
option.text = "R00 - Splitsko-dalmatinska"
option.value = "R00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//S00
option = document.createElement("option");
option.text = "S00 - Istarska"
option.value = "S00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//T00
option = document.createElement("option");
option.text = "T00 - Dubrovacko-neretvans"
option.value = "T00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//U00
option = document.createElement("option");
option.text = "U00 - Medjimurska"
option.value = "U00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//V00
option = document.createElement("option");
option.text = "V00 - Zagreb"
option.value = "V00"
option.setAttribute("country", "HR");
this.StateInput.add(option);

//COUNTRY - HT
//AR
option = document.createElement("option");
option.text = "AR - L'Artibonite"
option.value = "AR"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//CE
option = document.createElement("option");
option.text = "CE - Centre"
option.value = "CE"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//GA
option = document.createElement("option");
option.text = "GA - Grand' Anse"
option.value = "GA"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//ND
option = document.createElement("option");
option.text = "ND - Nord"
option.value = "ND"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//NE
option = document.createElement("option");
option.text = "NE - Nord-Est"
option.value = "NE"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//NI
option = document.createElement("option");
option.text = "NI - Nippes"
option.value = "NI"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//NO
option = document.createElement("option");
option.text = "NO - Nord-Ouest"
option.value = "NO"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//OU
option = document.createElement("option");
option.text = "OU - Ouest"
option.value = "OU"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//SD
option = document.createElement("option");
option.text = "SD - Sud"
option.value = "SD"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//SE
option = document.createElement("option");
option.text = "SE - Sud-Est"
option.value = "SE"
option.setAttribute("country", "HT");
this.StateInput.add(option);

//COUNTRY - HU
//1
option = document.createElement("option");
option.text = "1 - Bacs-Kiskun"
option.value = "1"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Baranya"
option.value = "2"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Bekes"
option.value = "3"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Bekescsaba"
option.value = "4"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Borsod-Abauj-Zemplen"
option.value = "5"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Budapest"
option.value = "6"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Csongrad"
option.value = "7"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Debrecen"
option.value = "8"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Dunaujvaros"
option.value = "9"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Eger"
option.value = "10"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Fejer"
option.value = "11"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Gyor"
option.value = "12"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Gyor-Moson-Sopron"
option.value = "13"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Hajdu-Bihar"
option.value = "14"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Heves"
option.value = "15"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Hodmezovasarhely"
option.value = "16"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Jasz-Nagykun-Szolnok"
option.value = "17"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Kaposvar"
option.value = "18"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Kecskemet"
option.value = "19"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Komarom-Esztergom"
option.value = "20"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Miskolc"
option.value = "21"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Nagykanizsa"
option.value = "22"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Nograd"
option.value = "23"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Nyiregyhaza"
option.value = "24"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Pecs"
option.value = "25"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Pest"
option.value = "26"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Somogy"
option.value = "27"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Sopron"
option.value = "28"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Szabolcs-Szat.-Bereg"
option.value = "29"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Szeged"
option.value = "30"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Szekesfehervar"
option.value = "31"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Szolnok"
option.value = "32"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Szombathely"
option.value = "33"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Tatabanya"
option.value = "34"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Tolna"
option.value = "35"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Vas"
option.value = "36"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Veszprem"
option.value = "37"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Zala"
option.value = "38"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Zalaegerszeg"
option.value = "39"
option.setAttribute("country", "HU");
this.StateInput.add(option);

//COUNTRY - ID
//1
option = document.createElement("option");
option.text = "1 - DKI Jakarta Jakarta"
option.value = "1"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Jawa Barat West Java"
option.value = "2"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Jawa Tengah Central"
option.value = "3"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Jawa Timur East Java"
option.value = "4"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - DI Yogyakarta Yogyak"
option.value = "5"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - DI Aceh Aceh"
option.value = "6"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Sumatera Utara North"
option.value = "7"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Sumatera Barat West"
option.value = "8"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Riau Riau"
option.value = "9"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Jambi Jambi"
option.value = "10"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Sumatera Selatan Sou"
option.value = "11"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Bengkulu Bengkulu"
option.value = "12"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Lampung Lampung"
option.value = "13"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Kalimantan Selatan S"
option.value = "14"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Kalimantan Barat Wes"
option.value = "15"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Kalimantan Tengah Ce"
option.value = "16"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Kalimantan Timur Eas"
option.value = "17"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Sulawesi Selatan Sou"
option.value = "18"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Sulawesi Tenggara So"
option.value = "19"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Sulawesi Tengah Cent"
option.value = "20"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Sulawesi Utara North"
option.value = "21"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Bali Bali"
option.value = "22"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Nusa Tenggara Barat"
option.value = "23"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Nusa Tenggara Timur"
option.value = "24"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Maluku Maluku"
option.value = "25"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Irian Jaya Irian Jay"
option.value = "26"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Timor Timur East Tim"
option.value = "27"
option.setAttribute("country", "ID");
this.StateInput.add(option);

//COUNTRY - IE
//1
option = document.createElement("option");
option.text = "1 - DOWN"
option.value = "1"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//CK
option = document.createElement("option");
option.text = "CK - Cork"
option.value = "CK"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//CL
option = document.createElement("option");
option.text = "CL - Clare"
option.value = "CL"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//CV
option = document.createElement("option");
option.text = "CV - Cavan"
option.value = "CV"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//CW
option = document.createElement("option");
option.text = "CW - Carlow"
option.value = "CW"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//DB
option = document.createElement("option");
option.text = "DB - Dublin"
option.value = "DB"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//DG
option = document.createElement("option");
option.text = "DG - Donegal"
option.value = "DG"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//GW
option = document.createElement("option");
option.text = "GW - Galway"
option.value = "GW"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//KD
option = document.createElement("option");
option.text = "KD - Kildare"
option.value = "KD"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//KK
option = document.createElement("option");
option.text = "KK - Kilkenny"
option.value = "KK"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//KY
option = document.createElement("option");
option.text = "KY - Kerry"
option.value = "KY"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//LF
option = document.createElement("option");
option.text = "LF - Longford"
option.value = "LF"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//LI
option = document.createElement("option");
option.text = "LI - Limerick"
option.value = "LI"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//LM
option = document.createElement("option");
option.text = "LM - Leitrim"
option.value = "LM"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//LS
option = document.createElement("option");
option.text = "LS - Laois"
option.value = "LS"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//LT
option = document.createElement("option");
option.text = "LT - Louth"
option.value = "LT"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//MH
option = document.createElement("option");
option.text = "MH - Monaghan"
option.value = "MH"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Meath"
option.value = "MT"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//MY
option = document.createElement("option");
option.text = "MY - Mayo"
option.value = "MY"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//OF
option = document.createElement("option");
option.text = "OF - Offaly"
option.value = "OF"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//RC
option = document.createElement("option");
option.text = "RC - Rosscommon"
option.value = "RC"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//SG
option = document.createElement("option");
option.text = "SG - Sligo"
option.value = "SG"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//TP
option = document.createElement("option");
option.text = "TP - Tipperary"
option.value = "TP"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//WF
option = document.createElement("option");
option.text = "WF - Waterford"
option.value = "WF"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//WK
option = document.createElement("option");
option.text = "WK - Wicklow"
option.value = "WK"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//WM
option = document.createElement("option");
option.text = "WM - Westmeath"
option.value = "WM"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//WX
option = document.createElement("option");
option.text = "WX - Wexford"
option.value = "WX"
option.setAttribute("country", "IE");
this.StateInput.add(option);

//COUNTRY - IL
//1
option = document.createElement("option");
option.text = "1 - Central"
option.value = "1"
option.setAttribute("country", "IL");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Haifa"
option.value = "2"
option.setAttribute("country", "IL");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Jerusalem"
option.value = "3"
option.setAttribute("country", "IL");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Northern"
option.value = "4"
option.setAttribute("country", "IL");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Southern"
option.value = "5"
option.setAttribute("country", "IL");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Tel Aviv"
option.value = "6"
option.setAttribute("country", "IL");
this.StateInput.add(option);

//COUNTRY - IN
//1
option = document.createElement("option");
option.text = "1 - Andra Pradesh"
option.value = "1"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Arunachal Pradesh"
option.value = "2"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Assam"
option.value = "3"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Bihar"
option.value = "4"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Goa"
option.value = "5"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Gujarat"
option.value = "6"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Haryana"
option.value = "7"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Himachal Pradesh"
option.value = "8"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Jammu und Kashmir"
option.value = "9"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Karnataka"
option.value = "10"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Kerala"
option.value = "11"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Madhya Pradesh"
option.value = "12"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Maharashtra"
option.value = "13"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Manipur"
option.value = "14"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Megalaya"
option.value = "15"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Mizoram"
option.value = "16"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Nagaland"
option.value = "17"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Orissa"
option.value = "18"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Punjab"
option.value = "19"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Rajasthan"
option.value = "20"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Sikkim"
option.value = "21"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Tamil Nadu"
option.value = "22"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Tripura"
option.value = "23"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Uttar Pradesh"
option.value = "24"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - West Bengal"
option.value = "25"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Andaman und Nico.In."
option.value = "26"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Chandigarh"
option.value = "27"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Dadra und Nagar Hav."
option.value = "28"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Daman und Diu"
option.value = "29"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Delhi"
option.value = "30"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Lakshadweep"
option.value = "31"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Pondicherry"
option.value = "32"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Chhaattisgarh"
option.value = "33"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Jharkhand"
option.value = "34"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Uttaranchal"
option.value = "35"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//A1
option = document.createElement("option");
option.text = "A1 - "
option.value = "A1"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//A2
option = document.createElement("option");
option.text = "A2 - "
option.value = "A2"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//A3
option = document.createElement("option");
option.text = "A3 - "
option.value = "A3"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//A4
option = document.createElement("option");
option.text = "A4 - "
option.value = "A4"
option.setAttribute("country", "IN");
this.StateInput.add(option);

//COUNTRY - IQ
//AN
option = document.createElement("option");
option.text = "AN - Al Anbar"
option.value = "AN"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//AR
option = document.createElement("option");
option.text = "AR - Erbil"
option.value = "AR"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Basra"
option.value = "BA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//BB
option = document.createElement("option");
option.text = "BB - Babil"
option.value = "BB"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//BG
option = document.createElement("option");
option.text = "BG - Baghdad"
option.value = "BG"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//DA
option = document.createElement("option");
option.text = "DA - Duhok"
option.value = "DA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//DI
option = document.createElement("option");
option.text = "DI - Diyala"
option.value = "DI"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//DQ
option = document.createElement("option");
option.text = "DQ - Dhi Qar"
option.value = "DQ"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//HA
option = document.createElement("option");
option.text = "HA - Halabja"
option.value = "HA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//KA
option = document.createElement("option");
option.text = "KA - Karbala"
option.value = "KA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//KI
option = document.createElement("option");
option.text = "KI - Kirkuk"
option.value = "KI"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Maysan"
option.value = "MA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//MU
option = document.createElement("option");
option.text = "MU - Muthanna"
option.value = "MU"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//NA
option = document.createElement("option");
option.text = "NA - Najaf"
option.value = "NA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//NI
option = document.createElement("option");
option.text = "NI - Nineveh"
option.value = "NI"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//QA
option = document.createElement("option");
option.text = "QA - Al-Qadisiyyah"
option.value = "QA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//SD
option = document.createElement("option");
option.text = "SD - Saladin"
option.value = "SD"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//SU
option = document.createElement("option");
option.text = "SU - Sulaymaniyah"
option.value = "SU"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Wasit"
option.value = "WA"
option.setAttribute("country", "IQ");
this.StateInput.add(option);

//COUNTRY - IT
//42
option = document.createElement("option");
option.text = "42 - LIGURIA"
option.value = "42"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AG
option = document.createElement("option");
option.text = "AG - Agriento"
option.value = "AG"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AL
option = document.createElement("option");
option.text = "AL - Alessandria"
option.value = "AL"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AN
option = document.createElement("option");
option.text = "AN - Ancona"
option.value = "AN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AO
option = document.createElement("option");
option.text = "AO - Aosta"
option.value = "AO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AP
option = document.createElement("option");
option.text = "AP - Ascoli Piceno"
option.value = "AP"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AQ
option = document.createElement("option");
option.text = "AQ - L'Aquila"
option.value = "AQ"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AR
option = document.createElement("option");
option.text = "AR - Arezzo"
option.value = "AR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AT
option = document.createElement("option");
option.text = "AT - Asti"
option.value = "AT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//AV
option = document.createElement("option");
option.text = "AV - Avellino"
option.value = "AV"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Bari"
option.value = "BA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BG
option = document.createElement("option");
option.text = "BG - Bergamo"
option.value = "BG"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BI
option = document.createElement("option");
option.text = "BI - Biella"
option.value = "BI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BL
option = document.createElement("option");
option.text = "BL - Belluno"
option.value = "BL"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BN
option = document.createElement("option");
option.text = "BN - Benevento"
option.value = "BN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BO
option = document.createElement("option");
option.text = "BO - Bologna"
option.value = "BO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BR
option = document.createElement("option");
option.text = "BR - Brindisi"
option.value = "BR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BS
option = document.createElement("option");
option.text = "BS - Brescia"
option.value = "BS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//BZ
option = document.createElement("option");
option.text = "BZ - Bolzano"
option.value = "BZ"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - Cagliari"
option.value = "CA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CB
option = document.createElement("option");
option.text = "CB - Campobasso"
option.value = "CB"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CE
option = document.createElement("option");
option.text = "CE - Caserta"
option.value = "CE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CH
option = document.createElement("option");
option.text = "CH - Chieti"
option.value = "CH"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CI
option = document.createElement("option");
option.text = "CI - Carbonia-Iglesias"
option.value = "CI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CL
option = document.createElement("option");
option.text = "CL - Caltanisetta"
option.value = "CL"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CN
option = document.createElement("option");
option.text = "CN - Cuneo"
option.value = "CN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CO
option = document.createElement("option");
option.text = "CO - Como"
option.value = "CO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CR
option = document.createElement("option");
option.text = "CR - Cremona"
option.value = "CR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CS
option = document.createElement("option");
option.text = "CS - Cosenza"
option.value = "CS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CT
option = document.createElement("option");
option.text = "CT - Catania"
option.value = "CT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//CZ
option = document.createElement("option");
option.text = "CZ - Catanzaro"
option.value = "CZ"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//EE
option = document.createElement("option");
option.text = "EE - Stati Esteri"
option.value = "EE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//EN
option = document.createElement("option");
option.text = "EN - Enna"
option.value = "EN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//FC
option = document.createElement("option");
option.text = "FC - Forli/Cesana"
option.value = "FC"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//FE
option = document.createElement("option");
option.text = "FE - Ferrara"
option.value = "FE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//FG
option = document.createElement("option");
option.text = "FG - Foggia"
option.value = "FG"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//FI
option = document.createElement("option");
option.text = "FI - Florence"
option.value = "FI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//FR
option = document.createElement("option");
option.text = "FR - Frosinone"
option.value = "FR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//FU
option = document.createElement("option");
option.text = "FU - Fiume"
option.value = "FU"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//GE
option = document.createElement("option");
option.text = "GE - Genova"
option.value = "GE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//GO
option = document.createElement("option");
option.text = "GO - Gorizia"
option.value = "GO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//GR
option = document.createElement("option");
option.text = "GR - Grosseto"
option.value = "GR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//IM
option = document.createElement("option");
option.text = "IM - Imperia"
option.value = "IM"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//IS
option = document.createElement("option");
option.text = "IS - Isernia"
option.value = "IS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//KR
option = document.createElement("option");
option.text = "KR - Crotone"
option.value = "KR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//LC
option = document.createElement("option");
option.text = "LC - Lecco"
option.value = "LC"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//LE
option = document.createElement("option");
option.text = "LE - Lecce"
option.value = "LE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//LI
option = document.createElement("option");
option.text = "LI - Livorno"
option.value = "LI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//LO
option = document.createElement("option");
option.text = "LO - Lodi"
option.value = "LO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//LT
option = document.createElement("option");
option.text = "LT - Latina"
option.value = "LT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//LU
option = document.createElement("option");
option.text = "LU - Lucca"
option.value = "LU"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MB
option = document.createElement("option");
option.text = "MB - Monza e Brianza"
option.value = "MB"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MC
option = document.createElement("option");
option.text = "MC - Macerata"
option.value = "MC"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//ME
option = document.createElement("option");
option.text = "ME - Messina"
option.value = "ME"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MI
option = document.createElement("option");
option.text = "MI - Milan"
option.value = "MI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MN
option = document.createElement("option");
option.text = "MN - Mantova"
option.value = "MN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MO
option = document.createElement("option");
option.text = "MO - Modena"
option.value = "MO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MS
option = document.createElement("option");
option.text = "MS - Massa Carrara"
option.value = "MS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Matera"
option.value = "MT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//NA
option = document.createElement("option");
option.text = "NA - Naples"
option.value = "NA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//NO
option = document.createElement("option");
option.text = "NO - Novara"
option.value = "NO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//NU
option = document.createElement("option");
option.text = "NU - Nuoro"
option.value = "NU"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//OG
option = document.createElement("option");
option.text = "OG - Ogliastra"
option.value = "OG"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//OR
option = document.createElement("option");
option.text = "OR - Oristano"
option.value = "OR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//OT
option = document.createElement("option");
option.text = "OT - Olbia-Tempio"
option.value = "OT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Palermo"
option.value = "PA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PC
option = document.createElement("option");
option.text = "PC - Piacenza"
option.value = "PC"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PD
option = document.createElement("option");
option.text = "PD - Padova"
option.value = "PD"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Pescara"
option.value = "PE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PG
option = document.createElement("option");
option.text = "PG - Perugia"
option.value = "PG"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PI
option = document.createElement("option");
option.text = "PI - Pisa"
option.value = "PI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PL
option = document.createElement("option");
option.text = "PL - Pola"
option.value = "PL"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PN
option = document.createElement("option");
option.text = "PN - Pordenone"
option.value = "PN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PO
option = document.createElement("option");
option.text = "PO - Prato"
option.value = "PO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PR
option = document.createElement("option");
option.text = "PR - Parma"
option.value = "PR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PT
option = document.createElement("option");
option.text = "PT - Pistoia"
option.value = "PT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PU
option = document.createElement("option");
option.text = "PU - Pesaro-Urbino"
option.value = "PU"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PV
option = document.createElement("option");
option.text = "PV - Pavia"
option.value = "PV"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//PZ
option = document.createElement("option");
option.text = "PZ - Potenza"
option.value = "PZ"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RA
option = document.createElement("option");
option.text = "RA - Ravenna"
option.value = "RA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RC
option = document.createElement("option");
option.text = "RC - Reggio Calabria"
option.value = "RC"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RE
option = document.createElement("option");
option.text = "RE - Reggio Emilia"
option.value = "RE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RG
option = document.createElement("option");
option.text = "RG - Ragusa"
option.value = "RG"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RI
option = document.createElement("option");
option.text = "RI - Rieti"
option.value = "RI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RM
option = document.createElement("option");
option.text = "RM - Rome"
option.value = "RM"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RN
option = document.createElement("option");
option.text = "RN - Rimini"
option.value = "RN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//RO
option = document.createElement("option");
option.text = "RO - Rovigo"
option.value = "RO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Salerno"
option.value = "SA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SI
option = document.createElement("option");
option.text = "SI - Siena"
option.value = "SI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Sondrio"
option.value = "SO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SP
option = document.createElement("option");
option.text = "SP - La Spezia"
option.value = "SP"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SR
option = document.createElement("option");
option.text = "SR - Siracusa"
option.value = "SR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SS
option = document.createElement("option");
option.text = "SS - Sassari"
option.value = "SS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//SV
option = document.createElement("option");
option.text = "SV - Savona"
option.value = "SV"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TA
option = document.createElement("option");
option.text = "TA - Taranto"
option.value = "TA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TE
option = document.createElement("option");
option.text = "TE - Teramo"
option.value = "TE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TN
option = document.createElement("option");
option.text = "TN - Trento"
option.value = "TN"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TO
option = document.createElement("option");
option.text = "TO - Turin"
option.value = "TO"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TP
option = document.createElement("option");
option.text = "TP - Trapani"
option.value = "TP"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TR
option = document.createElement("option");
option.text = "TR - Terni"
option.value = "TR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TS
option = document.createElement("option");
option.text = "TS - Trieste"
option.value = "TS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//TV
option = document.createElement("option");
option.text = "TV - Treviso"
option.value = "TV"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//UD
option = document.createElement("option");
option.text = "UD - Udine"
option.value = "UD"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VA
option = document.createElement("option");
option.text = "VA - Varese"
option.value = "VA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VB
option = document.createElement("option");
option.text = "VB - Verbania"
option.value = "VB"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VC
option = document.createElement("option");
option.text = "VC - Vercelli"
option.value = "VC"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VE
option = document.createElement("option");
option.text = "VE - Venice"
option.value = "VE"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VI
option = document.createElement("option");
option.text = "VI - Vicenza"
option.value = "VI"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VR
option = document.createElement("option");
option.text = "VR - Verona"
option.value = "VR"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VS
option = document.createElement("option");
option.text = "VS - Medio Campidano"
option.value = "VS"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VT
option = document.createElement("option");
option.text = "VT - Viterbo"
option.value = "VT"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//VV
option = document.createElement("option");
option.text = "VV - Vibo Valentia"
option.value = "VV"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//ZA
option = document.createElement("option");
option.text = "ZA - Zara"
option.value = "ZA"
option.setAttribute("country", "IT");
this.StateInput.add(option);

//COUNTRY - JM
//CL
option = document.createElement("option");
option.text = "CL - Clarendon"
option.value = "CL"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//HA
option = document.createElement("option");
option.text = "HA - Hanover"
option.value = "HA"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//KI
option = document.createElement("option");
option.text = "KI - Kingston"
option.value = "KI"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Manchester"
option.value = "MA"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//PO
option = document.createElement("option");
option.text = "PO - Portland"
option.value = "PO"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//SC
option = document.createElement("option");
option.text = "SC - Saint Catherine"
option.value = "SC"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//SD
option = document.createElement("option");
option.text = "SD - Saint Andrews"
option.value = "SD"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//SE
option = document.createElement("option");
option.text = "SE - Saint Elizabeth"
option.value = "SE"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - Saint James"
option.value = "SJ"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//SM
option = document.createElement("option");
option.text = "SM - Saint Mary"
option.value = "SM"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//SN
option = document.createElement("option");
option.text = "SN - Saint Ann"
option.value = "SN"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//ST
option = document.createElement("option");
option.text = "ST - Saint Thomas"
option.value = "ST"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//TR
option = document.createElement("option");
option.text = "TR - Trelawny"
option.value = "TR"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//WE
option = document.createElement("option");
option.text = "WE - Westmoreland"
option.value = "WE"
option.setAttribute("country", "JM");
this.StateInput.add(option);

//COUNTRY - JO
//AM
option = document.createElement("option");
option.text = "AM - AMMAN"
option.value = "AM"
option.setAttribute("country", "JO");
this.StateInput.add(option);

//COUNTRY - JP
//1
option = document.createElement("option");
option.text = "1 - Hokkaido"
option.value = "1"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Aomori"
option.value = "2"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Iwate"
option.value = "3"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Miyagi"
option.value = "4"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Akita"
option.value = "5"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Yamagata"
option.value = "6"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Fukushima"
option.value = "7"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Ibaraki"
option.value = "8"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Tochigi"
option.value = "9"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Gunma"
option.value = "10"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Saitama"
option.value = "11"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Chiba"
option.value = "12"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Tokyo"
option.value = "13"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Kanagawa"
option.value = "14"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Niigata"
option.value = "15"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Toyama"
option.value = "16"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Ishikawa"
option.value = "17"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Fukui"
option.value = "18"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Yamanashi"
option.value = "19"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Nagano"
option.value = "20"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Gifu"
option.value = "21"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Shizuoka"
option.value = "22"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Aichi"
option.value = "23"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Mie"
option.value = "24"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Shiga"
option.value = "25"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Kyoto"
option.value = "26"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Osaka"
option.value = "27"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Hyogo"
option.value = "28"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Nara"
option.value = "29"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Wakayama"
option.value = "30"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Tottori"
option.value = "31"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Shimane"
option.value = "32"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Okayama"
option.value = "33"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Hiroshima"
option.value = "34"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Yamaguchi"
option.value = "35"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Tokushima"
option.value = "36"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Kagawa"
option.value = "37"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Ehime"
option.value = "38"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Kochi"
option.value = "39"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Fukuoka"
option.value = "40"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Saga"
option.value = "41"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Nagasaki"
option.value = "42"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Kumamoto"
option.value = "43"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Oita"
option.value = "44"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Miyazaki"
option.value = "45"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Kagoshima"
option.value = "46"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Okinawa"
option.value = "47"
option.setAttribute("country", "JP");
this.StateInput.add(option);

//COUNTRY - KE
//30
option = document.createElement("option");
option.text = "30 - NAIROBI"
option.value = "30"
option.setAttribute("country", "KE");
this.StateInput.add(option);

//COUNTRY - KN
//1
option = document.createElement("option");
option.text = "1 - "
option.value = "1"
option.setAttribute("country", "KN");
this.StateInput.add(option);

//COUNTRY - KR
//1
option = document.createElement("option");
option.text = "1 - Cheju-do"
option.value = "1"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Cholla-bukto"
option.value = "2"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Cholla-namdo"
option.value = "3"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Ch´ungch´ong-bukto"
option.value = "4"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Ch´ungch´ong-namdo"
option.value = "5"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Inch´on-jikhalsi"
option.value = "6"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Kangwon-do"
option.value = "7"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Kwangju-jikhalsi"
option.value = "8"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Kyonggi-do"
option.value = "9"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Kyongsang-bukto"
option.value = "10"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Kyongsang-namdo"
option.value = "11"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Pusan-jikhalsi"
option.value = "12"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Soul-t´ukpyolsi"
option.value = "13"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Taegu-jikhalsi"
option.value = "14"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Taejon-jikhalsi"
option.value = "15"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - NORTH GYEONGSANG"
option.value = "47"
option.setAttribute("country", "KR");
this.StateInput.add(option);

//COUNTRY - KW
//AH
option = document.createElement("option");
option.text = "AH - Al Ahmadi"
option.value = "AH"
option.setAttribute("country", "KW");
this.StateInput.add(option);

//FA
option = document.createElement("option");
option.text = "FA - Al Farw#n#yah"
option.value = "FA"
option.setAttribute("country", "KW");
this.StateInput.add(option);

//HA
option = document.createElement("option");
option.text = "HA - Hawall#"
option.value = "HA"
option.setAttribute("country", "KW");
this.StateInput.add(option);

//JA
option = document.createElement("option");
option.text = "JA - Al Jahrah"
option.value = "JA"
option.setAttribute("country", "KW");
this.StateInput.add(option);

//KU
option = document.createElement("option");
option.text = "KU - Al Kuwayt"
option.value = "KU"
option.setAttribute("country", "KW");
this.StateInput.add(option);

//MU
option = document.createElement("option");
option.text = "MU - Mubarak Al-Kabir"
option.value = "MU"
option.setAttribute("country", "KW");
this.StateInput.add(option);

//COUNTRY - KY
//BT
option = document.createElement("option");
option.text = "BT - Bodden Town"
option.value = "BT"
option.setAttribute("country", "KY");
this.StateInput.add(option);

//EE
option = document.createElement("option");
option.text = "EE - East End"
option.value = "EE"
option.setAttribute("country", "KY");
this.StateInput.add(option);

//GT
option = document.createElement("option");
option.text = "GT - George Town"
option.value = "GT"
option.setAttribute("country", "KY");
this.StateInput.add(option);

//NS
option = document.createElement("option");
option.text = "NS - North Side"
option.value = "NS"
option.setAttribute("country", "KY");
this.StateInput.add(option);

//SI
option = document.createElement("option");
option.text = "SI - Sister Islands"
option.value = "SI"
option.setAttribute("country", "KY");
this.StateInput.add(option);

//WB
option = document.createElement("option");
option.text = "WB - West Bay"
option.value = "WB"
option.setAttribute("country", "KY");
this.StateInput.add(option);

//COUNTRY - KZ
//0
option = document.createElement("option");
option.text = "0 - Almatynskaia"
option.value = "0"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//1
option = document.createElement("option");
option.text = "1 - Kostanaiskaia"
option.value = "1"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Severo-Kazakhstansk"
option.value = "2"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Pavlodarskaia"
option.value = "3"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Akmolinskaia"
option.value = "4"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Aktubinskaia"
option.value = "5"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Atyrauskaia"
option.value = "6"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Zapadno-Kazakhst"
option.value = "7"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Mangystayskaia"
option.value = "8"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Karagandinskaia"
option.value = "9"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Vostochno-Kazakhstan"
option.value = "10"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Gambilskaia"
option.value = "11"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Kyzilordinskaia"
option.value = "12"
option.setAttribute("country", "KZ");
this.StateInput.add(option);

//COUNTRY - LB
//BA
option = document.createElement("option");
option.text = "BA - BEIRUT"
option.value = "BA"
option.setAttribute("country", "LB");
this.StateInput.add(option);

//JA
option = document.createElement("option");
option.text = "JA - SOUTH"
option.value = "JA"
option.setAttribute("country", "LB");
this.StateInput.add(option);

//COUNTRY - LC
//AR
option = document.createElement("option");
option.text = "AR - Anse-la-Raye"
option.value = "AR"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//CH
option = document.createElement("option");
option.text = "CH - Choiseul"
option.value = "CH"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//CN
option = document.createElement("option");
option.text = "CN - Canaries"
option.value = "CN"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//CO
option = document.createElement("option");
option.text = "CO - Soufriere"
option.value = "CO"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//CS
option = document.createElement("option");
option.text = "CS - Castries"
option.value = "CS"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//DE
option = document.createElement("option");
option.text = "DE - Dennery"
option.value = "DE"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//GI
option = document.createElement("option");
option.text = "GI - Gros Islet"
option.value = "GI"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//LB
option = document.createElement("option");
option.text = "LB - Laborie"
option.value = "LB"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//MI
option = document.createElement("option");
option.text = "MI - Micoud"
option.value = "MI"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//VF
option = document.createElement("option");
option.text = "VF - Vieux Fort"
option.value = "VF"
option.setAttribute("country", "LC");
this.StateInput.add(option);

//COUNTRY - LK
//1
option = document.createElement("option");
option.text = "1 - WESTERN PROVINCE"
option.value = "1"
option.setAttribute("country", "LK");
this.StateInput.add(option);

//COUNTRY - LR
//BG
option = document.createElement("option");
option.text = "BG - Bong"
option.value = "BG"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//BM
option = document.createElement("option");
option.text = "BM - Bomi"
option.value = "BM"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//CM
option = document.createElement("option");
option.text = "CM - Grand Cape Mount"
option.value = "CM"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//GB
option = document.createElement("option");
option.text = "GB - Grand Bassa"
option.value = "GB"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//GG
option = document.createElement("option");
option.text = "GG - Grand Gedeh"
option.value = "GG"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//GK
option = document.createElement("option");
option.text = "GK - Grand Kru"
option.value = "GK"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//GP
option = document.createElement("option");
option.text = "GP - Gbarpolu"
option.value = "GP"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//LO
option = document.createElement("option");
option.text = "LO - Lofa"
option.value = "LO"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//MG
option = document.createElement("option");
option.text = "MG - Margibi"
option.value = "MG"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//MO
option = document.createElement("option");
option.text = "MO - Montserrado"
option.value = "MO"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//MY
option = document.createElement("option");
option.text = "MY - Maryland"
option.value = "MY"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//NI
option = document.createElement("option");
option.text = "NI - Nimba"
option.value = "NI"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//RG
option = document.createElement("option");
option.text = "RG - River Gee"
option.value = "RG"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//RI
option = document.createElement("option");
option.text = "RI - River Cess"
option.value = "RI"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//SI
option = document.createElement("option");
option.text = "SI - Sinoe"
option.value = "SI"
option.setAttribute("country", "LR");
this.StateInput.add(option);

//COUNTRY - LT
//AL
option = document.createElement("option");
option.text = "AL - Alytaus Apskritis"
option.value = "AL"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//KL
option = document.createElement("option");
option.text = "KL - Klaip#dos Apskritis"
option.value = "KL"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//KU
option = document.createElement("option");
option.text = "KU - Kauno Apskritis"
option.value = "KU"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//MR
option = document.createElement("option");
option.text = "MR - Marijampol#s Apskrit"
option.value = "MR"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//PN
option = document.createElement("option");
option.text = "PN - Panev#žio Apskritis"
option.value = "PN"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Šiauli# Apskritis"
option.value = "SA"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//TA
option = document.createElement("option");
option.text = "TA - Taurag#s Apskritis"
option.value = "TA"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//TE
option = document.createElement("option");
option.text = "TE - Telši# Apskritis"
option.value = "TE"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//UT
option = document.createElement("option");
option.text = "UT - Utenos Apskritis"
option.value = "UT"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//VL
option = document.createElement("option");
option.text = "VL - Vilniaus Apskritis"
option.value = "VL"
option.setAttribute("country", "LT");
this.StateInput.add(option);

//COUNTRY - LU
//1
option = document.createElement("option");
option.text = "1 - GRAND DUCHY"
option.value = "1"
option.setAttribute("country", "LU");
this.StateInput.add(option);

//COUNTRY - LV
//RIX
option = document.createElement("option");
option.text = "RIX - RIGA"
option.value = "RIX"
option.setAttribute("country", "LV");
this.StateInput.add(option);

//COUNTRY - MF
//0
option = document.createElement("option");
option.text = "0 - "
option.value = "0"
option.setAttribute("country", "MF");
this.StateInput.add(option);

//COUNTRY - MH
//ZZ
option = document.createElement("option");
option.text = "ZZ - Majuro"
option.value = "ZZ"
option.setAttribute("country", "MH");
this.StateInput.add(option);

//COUNTRY - MK
//1
option = document.createElement("option");
option.text = "1 - "
option.value = "1"
option.setAttribute("country", "MK");
this.StateInput.add(option);

//COUNTRY - ML
//BKO
option = document.createElement("option");
option.text = "BKO - Bomako"
option.value = "BKO"
option.setAttribute("country", "ML");
this.StateInput.add(option);

//COUNTRY - MQ
//1
option = document.createElement("option");
option.text = "1 - "
option.value = "1"
option.setAttribute("country", "MQ");
this.StateInput.add(option);

//COUNTRY - MT
//NR
option = document.createElement("option");
option.text = "NR - Northern Region"
option.value = "NR"
option.setAttribute("country", "MT");
this.StateInput.add(option);

//COUNTRY - MX
//AGS
option = document.createElement("option");
option.text = "AGS - Aguascalientes"
option.value = "AGS"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//BC
option = document.createElement("option");
option.text = "BC - Baja California"
option.value = "BC"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//BCS
option = document.createElement("option");
option.text = "BCS - Baja California S"
option.value = "BCS"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//CDM
option = document.createElement("option");
option.text = "CDM - Ciudad de Mexico"
option.value = "CDM"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//CHI
option = document.createElement("option");
option.text = "CHI - Chihuahua"
option.value = "CHI"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//CHS
option = document.createElement("option");
option.text = "CHS - Chiapas"
option.value = "CHS"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//CMP
option = document.createElement("option");
option.text = "CMP - Campeche"
option.value = "CMP"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//COA
option = document.createElement("option");
option.text = "COA - Coahuila"
option.value = "COA"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//COL
option = document.createElement("option");
option.text = "COL - Colima"
option.value = "COL"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//DGO
option = document.createElement("option");
option.text = "DGO - Durango"
option.value = "DGO"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//GRO
option = document.createElement("option");
option.text = "GRO - Guerrero"
option.value = "GRO"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//GTO
option = document.createElement("option");
option.text = "GTO - Guanajuato"
option.value = "GTO"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//HGO
option = document.createElement("option");
option.text = "HGO - Hidalgo"
option.value = "HGO"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//JAL
option = document.createElement("option");
option.text = "JAL - Jalisco"
option.value = "JAL"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//MCH
option = document.createElement("option");
option.text = "MCH - Michoacan"
option.value = "MCH"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//MEX
option = document.createElement("option");
option.text = "MEX - Estado de Mexico"
option.value = "MEX"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//MOR
option = document.createElement("option");
option.text = "MOR - Morelos"
option.value = "MOR"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//NAY
option = document.createElement("option");
option.text = "NAY - Nayarit"
option.value = "NAY"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//NL
option = document.createElement("option");
option.text = "NL - Nuevo Leon"
option.value = "NL"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//OAX
option = document.createElement("option");
option.text = "OAX - Oaxaca"
option.value = "OAX"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//PUE
option = document.createElement("option");
option.text = "PUE - Puebla"
option.value = "PUE"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//QR
option = document.createElement("option");
option.text = "QR - Quintana Roo"
option.value = "QR"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//QRO
option = document.createElement("option");
option.text = "QRO - Queretaro"
option.value = "QRO"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//SIN
option = document.createElement("option");
option.text = "SIN - Sinaloa"
option.value = "SIN"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//SLP
option = document.createElement("option");
option.text = "SLP - San Luis Potosi"
option.value = "SLP"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//SON
option = document.createElement("option");
option.text = "SON - Sonora"
option.value = "SON"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//TAB
option = document.createElement("option");
option.text = "TAB - Tabasco"
option.value = "TAB"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//TLX
option = document.createElement("option");
option.text = "TLX - Tlaxcala"
option.value = "TLX"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//TMS
option = document.createElement("option");
option.text = "TMS - Tamaulipas"
option.value = "TMS"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//VER
option = document.createElement("option");
option.text = "VER - Veracruz"
option.value = "VER"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//YUC
option = document.createElement("option");
option.text = "YUC - Yucatan"
option.value = "YUC"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//ZAC
option = document.createElement("option");
option.text = "ZAC - Zacatecas"
option.value = "ZAC"
option.setAttribute("country", "MX");
this.StateInput.add(option);

//COUNTRY - MY
//JOH
option = document.createElement("option");
option.text = "JOH - Johor"
option.value = "JOH"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//KED
option = document.createElement("option");
option.text = "KED - Kedah"
option.value = "KED"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//KEL
option = document.createElement("option");
option.text = "KEL - Kelantan"
option.value = "KEL"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//KUL
option = document.createElement("option");
option.text = "KUL - Kuala Lumpur"
option.value = "KUL"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//LAB
option = document.createElement("option");
option.text = "LAB - Labuan"
option.value = "LAB"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//MEL
option = document.createElement("option");
option.text = "MEL - Melaka"
option.value = "MEL"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//PAH
option = document.createElement("option");
option.text = "PAH - Pahang"
option.value = "PAH"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//PEL
option = document.createElement("option");
option.text = "PEL - Perlis"
option.value = "PEL"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//PER
option = document.createElement("option");
option.text = "PER - Perak"
option.value = "PER"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//PIN
option = document.createElement("option");
option.text = "PIN - Pulau Pinang"
option.value = "PIN"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//PSK
option = document.createElement("option");
option.text = "PSK - Wil. Persekutuan"
option.value = "PSK"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//SAB
option = document.createElement("option");
option.text = "SAB - Sabah"
option.value = "SAB"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//SAR
option = document.createElement("option");
option.text = "SAR - Sarawak"
option.value = "SAR"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//SEL
option = document.createElement("option");
option.text = "SEL - Selangor"
option.value = "SEL"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//SER
option = document.createElement("option");
option.text = "SER - Negeri Sembilan"
option.value = "SER"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//TRE
option = document.createElement("option");
option.text = "TRE - Trengganu"
option.value = "TRE"
option.setAttribute("country", "MY");
this.StateInput.add(option);

//COUNTRY - MZ
//MPM
option = document.createElement("option");
option.text = "MPM - MAPUTO CITY"
option.value = "MPM"
option.setAttribute("country", "MZ");
this.StateInput.add(option);

//COUNTRY - NG
//AB
option = document.createElement("option");
option.text = "AB - Abia"
option.value = "AB"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//AD
option = document.createElement("option");
option.text = "AD - Adamawa"
option.value = "AD"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//AK
option = document.createElement("option");
option.text = "AK - Akwa Ibom"
option.value = "AK"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//AN
option = document.createElement("option");
option.text = "AN - Anambra"
option.value = "AN"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//BA
option = document.createElement("option");
option.text = "BA - Bauchi"
option.value = "BA"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//BE
option = document.createElement("option");
option.text = "BE - Benue"
option.value = "BE"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//BO
option = document.createElement("option");
option.text = "BO - Borno"
option.value = "BO"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//BY
option = document.createElement("option");
option.text = "BY - Bayelsa"
option.value = "BY"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//CR
option = document.createElement("option");
option.text = "CR - Cross River"
option.value = "CR"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//DE
option = document.createElement("option");
option.text = "DE - Delta"
option.value = "DE"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//EB
option = document.createElement("option");
option.text = "EB - Ebonyi"
option.value = "EB"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//ED
option = document.createElement("option");
option.text = "ED - Edo"
option.value = "ED"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//EK
option = document.createElement("option");
option.text = "EK - Ekiti"
option.value = "EK"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//EN
option = document.createElement("option");
option.text = "EN - Enugu"
option.value = "EN"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//FC
option = document.createElement("option");
option.text = "FC - Abuja"
option.value = "FC"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//GO
option = document.createElement("option");
option.text = "GO - Gombe"
option.value = "GO"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//IM
option = document.createElement("option");
option.text = "IM - Imo"
option.value = "IM"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//JI
option = document.createElement("option");
option.text = "JI - Jigawa"
option.value = "JI"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//KD
option = document.createElement("option");
option.text = "KD - Kaduna"
option.value = "KD"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//KE
option = document.createElement("option");
option.text = "KE - Kebbi"
option.value = "KE"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//KN
option = document.createElement("option");
option.text = "KN - Kano"
option.value = "KN"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//KO
option = document.createElement("option");
option.text = "KO - Kogi"
option.value = "KO"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//KT
option = document.createElement("option");
option.text = "KT - Katsina"
option.value = "KT"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//KW
option = document.createElement("option");
option.text = "KW - Kwara"
option.value = "KW"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//LA
option = document.createElement("option");
option.text = "LA - Lagos"
option.value = "LA"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//NA
option = document.createElement("option");
option.text = "NA - Nassarawa"
option.value = "NA"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//NI
option = document.createElement("option");
option.text = "NI - Niger"
option.value = "NI"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//OG
option = document.createElement("option");
option.text = "OG - Ogun"
option.value = "OG"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//OS
option = document.createElement("option");
option.text = "OS - Osun"
option.value = "OS"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//OY
option = document.createElement("option");
option.text = "OY - Oyo"
option.value = "OY"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//PL
option = document.createElement("option");
option.text = "PL - Plateau"
option.value = "PL"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//RI
option = document.createElement("option");
option.text = "RI - Rivers"
option.value = "RI"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Sokoto"
option.value = "SO"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//TA
option = document.createElement("option");
option.text = "TA - Taraba"
option.value = "TA"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//YO
option = document.createElement("option");
option.text = "YO - Yobe"
option.value = "YO"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//ZA
option = document.createElement("option");
option.text = "ZA - Zamfara"
option.value = "ZA"
option.setAttribute("country", "NG");
this.StateInput.add(option);

//COUNTRY - NI
//AN
option = document.createElement("option");
option.text = "AN - Región Autónoma del"
option.value = "AN"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//AS
option = document.createElement("option");
option.text = "AS - Región Autónoma del"
option.value = "AS"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//BO
option = document.createElement("option");
option.text = "BO - Boaco"
option.value = "BO"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - Carazo"
option.value = "CA"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//CI
option = document.createElement("option");
option.text = "CI - Chinandega"
option.value = "CI"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//CO
option = document.createElement("option");
option.text = "CO - Chontales"
option.value = "CO"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//ES
option = document.createElement("option");
option.text = "ES - Estelí"
option.value = "ES"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//GR
option = document.createElement("option");
option.text = "GR - Granada"
option.value = "GR"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//JI
option = document.createElement("option");
option.text = "JI - Jinotega"
option.value = "JI"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//LE
option = document.createElement("option");
option.text = "LE - León"
option.value = "LE"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//MD
option = document.createElement("option");
option.text = "MD - Madriz"
option.value = "MD"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//MN
option = document.createElement("option");
option.text = "MN - Managua"
option.value = "MN"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//MS
option = document.createElement("option");
option.text = "MS - Masaya"
option.value = "MS"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Matagalpa"
option.value = "MT"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//NS
option = document.createElement("option");
option.text = "NS - Nueva Segovia"
option.value = "NS"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//RI
option = document.createElement("option");
option.text = "RI - Rivas"
option.value = "RI"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - Río San Juan"
option.value = "SJ"
option.setAttribute("country", "NI");
this.StateInput.add(option);

//COUNTRY - NL
//1
option = document.createElement("option");
option.text = "1 - Drenthe"
option.value = "1"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Flevoland"
option.value = "2"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Friesland"
option.value = "3"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Gelderland"
option.value = "4"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Groningen"
option.value = "5"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Limburg"
option.value = "6"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Noord-Brabant"
option.value = "7"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Noord-Holland"
option.value = "8"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Overijssel"
option.value = "9"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Utrecht"
option.value = "10"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Zeeland"
option.value = "11"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Zuid-Holland"
option.value = "12"
option.setAttribute("country", "NL");
this.StateInput.add(option);

//COUNTRY - NO
//1
option = document.createElement("option");
option.text = "1 - Ostfold County"
option.value = "1"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Akershus County"
option.value = "2"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Oslo"
option.value = "3"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Hedmark County"
option.value = "4"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Oppland County"
option.value = "5"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Buskerud County"
option.value = "6"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Vestfold County"
option.value = "7"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Telemark County"
option.value = "8"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Aust-Agder County"
option.value = "9"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Vest-Agder County"
option.value = "10"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Rogaland County"
option.value = "11"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Hordaland County"
option.value = "12"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Sogn og Fjordane C."
option.value = "14"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - More og Romsdal C."
option.value = "15"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Sor-Trondelag County"
option.value = "16"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Nord-Trondelag Cnty"
option.value = "17"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Nordland County"
option.value = "18"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Troms County"
option.value = "19"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Finnmark County"
option.value = "20"
option.setAttribute("country", "NO");
this.StateInput.add(option);

//COUNTRY - NZ
//AKL
option = document.createElement("option");
option.text = "AKL - Auckland"
option.value = "AKL"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//BOP
option = document.createElement("option");
option.text = "BOP - Bay of Plenty"
option.value = "BOP"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//CAN
option = document.createElement("option");
option.text = "CAN - Canterbury"
option.value = "CAN"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//HAB
option = document.createElement("option");
option.text = "HAB - Hawke´s Bay"
option.value = "HAB"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//MAN
option = document.createElement("option");
option.text = "MAN - Manawatu-Wanganui"
option.value = "MAN"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//NTL
option = document.createElement("option");
option.text = "NTL - Northland"
option.value = "NTL"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//OTA
option = document.createElement("option");
option.text = "OTA - Otago"
option.value = "OTA"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//STL
option = document.createElement("option");
option.text = "STL - Southland"
option.value = "STL"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//TAR
option = document.createElement("option");
option.text = "TAR - Taranaki"
option.value = "TAR"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//WAI
option = document.createElement("option");
option.text = "WAI - Waikato"
option.value = "WAI"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//WEC
option = document.createElement("option");
option.text = "WEC - West Coast"
option.value = "WEC"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//WLG
option = document.createElement("option");
option.text = "WLG - Wellington"
option.value = "WLG"
option.setAttribute("country", "NZ");
this.StateInput.add(option);

//COUNTRY - OM
//BJ
option = document.createElement("option");
option.text = "BJ - Jan#b al B##inah"
option.value = "BJ"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//BS
option = document.createElement("option");
option.text = "BS - Sham#l al B##inah"
option.value = "BS"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//BU
option = document.createElement("option");
option.text = "BU - Al Buraym#"
option.value = "BU"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//DA
option = document.createElement("option");
option.text = "DA - Ad D#khil#yah"
option.value = "DA"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Masqa#"
option.value = "MA"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//MU
option = document.createElement("option");
option.text = "MU - Musandam"
option.value = "MU"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - Jan#b ash Sharq#yah"
option.value = "SJ"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//SS
option = document.createElement("option");
option.text = "SS - Sham#l ash Sharq#yah"
option.value = "SS"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//WU
option = document.createElement("option");
option.text = "WU - Al Wus#á"
option.value = "WU"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//ZA
option = document.createElement("option");
option.text = "ZA - Az# Z##hirah"
option.value = "ZA"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//ZU
option = document.createElement("option");
option.text = "ZU - Z#uf#r"
option.value = "ZU"
option.setAttribute("country", "OM");
this.StateInput.add(option);

//COUNTRY - PA
//1
option = document.createElement("option");
option.text = "1 - Bocas del Toro"
option.value = "1"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Coclé"
option.value = "2"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Colón"
option.value = "3"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Chiriquí"
option.value = "4"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Darién"
option.value = "5"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Herrera"
option.value = "6"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Los Santos"
option.value = "7"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Panamá"
option.value = "8"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Veraguas"
option.value = "9"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//EM
option = document.createElement("option");
option.text = "EM - Emberá"
option.value = "EM"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//KY
option = document.createElement("option");
option.text = "KY - Kuna Yala"
option.value = "KY"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//NB
option = document.createElement("option");
option.text = "NB - Ngäbe Buglé"
option.value = "NB"
option.setAttribute("country", "PA");
this.StateInput.add(option);

//COUNTRY - PE
//1
option = document.createElement("option");
option.text = "1 - Tumbes"
option.value = "1"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Piura"
option.value = "2"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Lambayeque"
option.value = "3"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - La Libertad"
option.value = "4"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Ancash"
option.value = "5"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Lima y Callao"
option.value = "6"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Ica"
option.value = "7"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Arequipa"
option.value = "8"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Moquegua"
option.value = "9"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Tacna"
option.value = "10"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Amazon"
option.value = "11"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Cajamarca"
option.value = "12"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - San Martin"
option.value = "13"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Huanuco"
option.value = "14"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Pasco"
option.value = "15"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Junin"
option.value = "16"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Huancavelica"
option.value = "17"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Ayacucho"
option.value = "18"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Apurimac"
option.value = "19"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Cuzco"
option.value = "20"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Puno"
option.value = "21"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Loreto"
option.value = "22"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Ucayali"
option.value = "23"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Madre de Dios"
option.value = "24"
option.setAttribute("country", "PE");
this.StateInput.add(option);

//COUNTRY - PF
//1
option = document.createElement("option");
option.text = "1 - French Polynesia"
option.value = "1"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//CI
option = document.createElement("option");
option.text = "CI - Clipperton Island"
option.value = "CI"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//LI
option = document.createElement("option");
option.text = "LI - Leeward Islands"
option.value = "LI"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//MI
option = document.createElement("option");
option.text = "MI - Marquesas Islands"
option.value = "MI"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//TG
option = document.createElement("option");
option.text = "TG - Tuamotu and Gambier"
option.value = "TG"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//TI
option = document.createElement("option");
option.text = "TI - Tubuai Islands"
option.value = "TI"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//WI
option = document.createElement("option");
option.text = "WI - Windward Islands"
option.value = "WI"
option.setAttribute("country", "PF");
this.StateInput.add(option);

//COUNTRY - PG
//NCD
option = document.createElement("option");
option.text = "NCD - NATIONAL CAPITAL DIS"
option.value = "NCD"
option.setAttribute("country", "PG");
this.StateInput.add(option);

//COUNTRY - PH
//1
option = document.createElement("option");
option.text = "1 - Ilocos"
option.value = "1"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Cagayan Valley"
option.value = "2"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Central Luzon"
option.value = "3"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Calabarzon"
option.value = "4"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Bicol"
option.value = "5"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - West Visayas"
option.value = "6"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Central Visayas"
option.value = "7"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Eastern Visayas"
option.value = "8"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Zamboanga Peninsula"
option.value = "9"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Northern Mindanao"
option.value = "10"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Davao Region"
option.value = "11"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Soccsksargen"
option.value = "12"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - National Capitol Reg"
option.value = "13"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Cordillera Admin Reg"
option.value = "14"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - ARMM"
option.value = "15"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Caraga"
option.value = "16"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Mimaropa"
option.value = "17"
option.setAttribute("country", "PH");
this.StateInput.add(option);

//COUNTRY - PK
//BA
option = document.createElement("option");
option.text = "BA - Balochistan"
option.value = "BA"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//GB
option = document.createElement("option");
option.text = "GB - Gilgit-Baltistan"
option.value = "GB"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//IS
option = document.createElement("option");
option.text = "IS - Islamabad"
option.value = "IS"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//JK
option = document.createElement("option");
option.text = "JK - Azad Kashmir"
option.value = "JK"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//KP
option = document.createElement("option");
option.text = "KP - Khyber Pakhtunkhwa"
option.value = "KP"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//PB
option = document.createElement("option");
option.text = "PB - Punjab"
option.value = "PB"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//SD
option = document.createElement("option");
option.text = "SD - Sindh"
option.value = "SD"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//TA
option = document.createElement("option");
option.text = "TA - Federally Administer"
option.value = "TA"
option.setAttribute("country", "PK");
this.StateInput.add(option);

//COUNTRY - PL
//DSL
option = document.createElement("option");
option.text = "DSL - Dolnoslaskie"
option.value = "DSL"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//K-P
option = document.createElement("option");
option.text = "K-P - Kujawsko-Pomorskie"
option.value = "K-P"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//LBL
option = document.createElement("option");
option.text = "LBL - Lubelskie"
option.value = "LBL"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//LBS
option = document.createElement("option");
option.text = "LBS - Lubuskie"
option.value = "LBS"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//LDZ
option = document.createElement("option");
option.text = "LDZ - Lodzkie"
option.value = "LDZ"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//MAL
option = document.createElement("option");
option.text = "MAL - Malopolskie"
option.value = "MAL"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//MAZ
option = document.createElement("option");
option.text = "MAZ - Mazowieckie"
option.value = "MAZ"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//OPO
option = document.createElement("option");
option.text = "OPO - Opolskie"
option.value = "OPO"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//PDK
option = document.createElement("option");
option.text = "PDK - Podkarpackie"
option.value = "PDK"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//PDL
option = document.createElement("option");
option.text = "PDL - Podlaskie"
option.value = "PDL"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//POM
option = document.createElement("option");
option.text = "POM - Pomorskie"
option.value = "POM"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//SLS
option = document.createElement("option");
option.text = "SLS - Slaskie"
option.value = "SLS"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//SWK
option = document.createElement("option");
option.text = "SWK - Swietokrzyskie"
option.value = "SWK"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//W-M
option = document.createElement("option");
option.text = "W-M - Warminsko-mazurskie"
option.value = "W-M"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//WLK
option = document.createElement("option");
option.text = "WLK - Wielkopolskie"
option.value = "WLK"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//Z-P
option = document.createElement("option");
option.text = "Z-P - Zachodnio-Pomorskie"
option.value = "Z-P"
option.setAttribute("country", "PL");
this.StateInput.add(option);

//COUNTRY - PR
//PR
option = document.createElement("option");
option.text = "PR - Puerto Rico"
option.value = "PR"
option.setAttribute("country", "PR");
this.StateInput.add(option);

//COUNTRY - PT
//10
option = document.createElement("option");
option.text = "10 - Minho-Lima"
option.value = "10"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Cavado"
option.value = "11"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Ave"
option.value = "12"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Grande Porto"
option.value = "13"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Tamega"
option.value = "14"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Entre Douro e Vouga"
option.value = "15"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Douro"
option.value = "16"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Alto Tras-os-Montes"
option.value = "17"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Baixo Vouga"
option.value = "20"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Baixo Mondego"
option.value = "21"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Pinhal Litoral"
option.value = "22"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Pinhal Interior N."
option.value = "23"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Pinhal Interior Sul"
option.value = "24"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Dao-Lafoes"
option.value = "25"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Serra da Estrela"
option.value = "26"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Beira Interior Norte"
option.value = "27"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Beira Interior Sul"
option.value = "28"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Cova da Beira"
option.value = "29"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Oeste"
option.value = "30"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Grande Lisboa"
option.value = "31"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Peninsula de Setubal"
option.value = "32"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Medio Tejo"
option.value = "33"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Leziria do Tejo"
option.value = "34"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Alentejo Litoral"
option.value = "40"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Alto Alentejo"
option.value = "41"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Alentejo Central"
option.value = "42"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Baixo Alentejo"
option.value = "43"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Algarve"
option.value = "50"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//60
option = document.createElement("option");
option.text = "60 - Reg. Aut. dos Açores"
option.value = "60"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//70
option = document.createElement("option");
option.text = "70 - Reg. Aut. da Madeira"
option.value = "70"
option.setAttribute("country", "PT");
this.StateInput.add(option);

//COUNTRY - PW
//AN
option = document.createElement("option");
option.text = "AN - Hatohobei"
option.value = "AN"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//AR
option = document.createElement("option");
option.text = "AR - Airai"
option.value = "AR"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//KA
option = document.createElement("option");
option.text = "KA - Kayangel"
option.value = "KA"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//KO
option = document.createElement("option");
option.text = "KO - Koror"
option.value = "KO"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//ME
option = document.createElement("option");
option.text = "ME - Melekeok"
option.value = "ME"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//NC
option = document.createElement("option");
option.text = "NC - Ngarchelong"
option.value = "NC"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//ND
option = document.createElement("option");
option.text = "ND - Ngaraard"
option.value = "ND"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//NL
option = document.createElement("option");
option.text = "NL - Ngeremlengui"
option.value = "NL"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//NM
option = document.createElement("option");
option.text = "NM - Ngardmau"
option.value = "NM"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//NP
option = document.createElement("option");
option.text = "NP - Ngatpang"
option.value = "NP"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//NS
option = document.createElement("option");
option.text = "NS - Ngchesar"
option.value = "NS"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//NW
option = document.createElement("option");
option.text = "NW - Ngiwal"
option.value = "NW"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//PE
option = document.createElement("option");
option.text = "PE - Pelepiu"
option.value = "PE"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//PW
option = document.createElement("option");
option.text = "PW - Aimeliik"
option.value = "PW"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Sonsorol"
option.value = "SO"
option.setAttribute("country", "PW");
this.StateInput.add(option);

//COUNTRY - PY
//10
option = document.createElement("option");
option.text = "10 - Alto Parana"
option.value = "10"
option.setAttribute("country", "PY");
this.StateInput.add(option);

//COUNTRY - QA
//DA
option = document.createElement("option");
option.text = "DA - Ad Dawhah"
option.value = "DA"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//KH
option = document.createElement("option");
option.text = "KH - Al Khawr"
option.value = "KH"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//MS
option = document.createElement("option");
option.text = "MS - Madinat ach Shamal"
option.value = "MS"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//RA
option = document.createElement("option");
option.text = "RA - Ar Rayyan"
option.value = "RA"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//US
option = document.createElement("option");
option.text = "US - Umm Salal"
option.value = "US"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Al Wakrah"
option.value = "WA"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//ZA
option = document.createElement("option");
option.text = "ZA - Al Daayen"
option.value = "ZA"
option.setAttribute("country", "QA");
this.StateInput.add(option);

//COUNTRY - RO
//1
option = document.createElement("option");
option.text = "1 - Alba"
option.value = "1"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Arad"
option.value = "2"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Arges"
option.value = "3"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Bacau"
option.value = "4"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Bihor"
option.value = "5"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Bistrita-Nasaud"
option.value = "6"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Botosani"
option.value = "7"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Braila"
option.value = "8"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Brasov"
option.value = "9"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Bucuresti"
option.value = "10"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Buzau"
option.value = "11"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Calarasi"
option.value = "12"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Caras-Severin"
option.value = "13"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Cluj"
option.value = "14"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Constanta"
option.value = "15"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Covasna"
option.value = "16"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Dimbovita"
option.value = "17"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Dolj"
option.value = "18"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Galati"
option.value = "19"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Gorj"
option.value = "20"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Giurgiu"
option.value = "21"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Harghita"
option.value = "22"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Hunedoara"
option.value = "23"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Ialomita"
option.value = "24"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Iasi"
option.value = "25"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Maramures"
option.value = "26"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Mehedinti"
option.value = "27"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Mures"
option.value = "28"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Neamt"
option.value = "29"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Olt"
option.value = "30"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Prahova"
option.value = "31"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Salaj"
option.value = "32"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Satu Mare"
option.value = "33"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Sibiu"
option.value = "34"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Suceava"
option.value = "35"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Teleorman"
option.value = "36"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Timis"
option.value = "37"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Tulcea"
option.value = "38"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Vaslui"
option.value = "39"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Vilcea"
option.value = "40"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Vrancea"
option.value = "41"
option.setAttribute("country", "RO");
this.StateInput.add(option);

//COUNTRY - RU
//1
option = document.createElement("option");
option.text = "1 - Adigeja Republic"
option.value = "1"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Highlands-Altay Rep."
option.value = "2"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Republ.of Bashkortos"
option.value = "3"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Buryat Republic"
option.value = "4"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Dagestan Republic"
option.value = "5"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Ingushetija Republic"
option.value = "6"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Kabardino-Balkar.Rep"
option.value = "7"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Kalmyk Republic"
option.value = "8"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Karach.-Cherkessk Re"
option.value = "9"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Karelian Republic"
option.value = "10"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Komi Republic"
option.value = "11"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Marijskaya Republic"
option.value = "12"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Mordovian Republic"
option.value = "13"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Yakutiya-Saha Rrepub"
option.value = "14"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - North-Osetiya Republ"
option.value = "15"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Tatarstan Republic"
option.value = "16"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Tuva Republic"
option.value = "17"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - The Udmurt Republic"
option.value = "18"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Chakassky Republic"
option.value = "19"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Chechenskaya Republ."
option.value = "20"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Chuvash Republic"
option.value = "21"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Altay Territory"
option.value = "22"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Krasnodar Territory"
option.value = "23"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Krasnoyarsk Territor"
option.value = "24"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Primorye Territory"
option.value = "25"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Stavropol Territory"
option.value = "26"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Khabarovsk Territory"
option.value = "27"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - The Amur Area"
option.value = "28"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - The Arkhangelsk Area"
option.value = "29"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - The Astrakhan Area"
option.value = "30"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - The Belgorod Area"
option.value = "31"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - The Bryansk Area"
option.value = "32"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - The Vladimir Area"
option.value = "33"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - The Volgograd Area"
option.value = "34"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - The Vologda Area"
option.value = "35"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - The Voronezh Area"
option.value = "36"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - The Ivanovo Area"
option.value = "37"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - The Irkutsk Area"
option.value = "38"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - The Kaliningrad Area"
option.value = "39"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - The Kaluga Area"
option.value = "40"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - The Kamchatka Area"
option.value = "41"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - The Kemerovo Area"
option.value = "42"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - The Kirov Area"
option.value = "43"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - The Kostroma Area"
option.value = "44"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - The Kurgan Area"
option.value = "45"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - The Kursk Area"
option.value = "46"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - The Leningrad Area"
option.value = "47"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - The Lipetsk Area"
option.value = "48"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - The Magadan Area"
option.value = "49"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - The Moscow Area"
option.value = "50"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//51
option = document.createElement("option");
option.text = "51 - The Murmansk Area"
option.value = "51"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - The Nizhniy Novgorod"
option.value = "52"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//53
option = document.createElement("option");
option.text = "53 - The Novgorod Area"
option.value = "53"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//54
option = document.createElement("option");
option.text = "54 - The Novosibirsk Area"
option.value = "54"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//55
option = document.createElement("option");
option.text = "55 - The Omsk Area"
option.value = "55"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//56
option = document.createElement("option");
option.text = "56 - The Orenburg Area"
option.value = "56"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//57
option = document.createElement("option");
option.text = "57 - The Oryol Area"
option.value = "57"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//58
option = document.createElement("option");
option.text = "58 - The Penza Area"
option.value = "58"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//59
option = document.createElement("option");
option.text = "59 - The Perm Area"
option.value = "59"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//60
option = document.createElement("option");
option.text = "60 - The Pskov Area"
option.value = "60"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//61
option = document.createElement("option");
option.text = "61 - The Rostov Area"
option.value = "61"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//62
option = document.createElement("option");
option.text = "62 - The Ryazan Area"
option.value = "62"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//63
option = document.createElement("option");
option.text = "63 - The Samara Area"
option.value = "63"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//64
option = document.createElement("option");
option.text = "64 - The Saratov Area"
option.value = "64"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//65
option = document.createElement("option");
option.text = "65 - The Sakhalin Area"
option.value = "65"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//66
option = document.createElement("option");
option.text = "66 - The Sverdlovsk Area"
option.value = "66"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//67
option = document.createElement("option");
option.text = "67 - The Smolensk Area"
option.value = "67"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//68
option = document.createElement("option");
option.text = "68 - The Tambov Area"
option.value = "68"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//69
option = document.createElement("option");
option.text = "69 - The Tver Area"
option.value = "69"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//70
option = document.createElement("option");
option.text = "70 - The Tomsk Area"
option.value = "70"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//71
option = document.createElement("option");
option.text = "71 - The Tula Area"
option.value = "71"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//72
option = document.createElement("option");
option.text = "72 - The Tyumen Area"
option.value = "72"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//73
option = document.createElement("option");
option.text = "73 - The Ulyanovsk Area"
option.value = "73"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//74
option = document.createElement("option");
option.text = "74 - The Chelyabinsk Area"
option.value = "74"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//75
option = document.createElement("option");
option.text = "75 - The Chita Area"
option.value = "75"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//76
option = document.createElement("option");
option.text = "76 - The Yaroslavl Area"
option.value = "76"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//77
option = document.createElement("option");
option.text = "77 - c.Moscow"
option.value = "77"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//78
option = document.createElement("option");
option.text = "78 - c.St-Peterburg"
option.value = "78"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//79
option = document.createElement("option");
option.text = "79 - The Jewish Auton.are"
option.value = "79"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//80
option = document.createElement("option");
option.text = "80 - Aginsk Buryat Aut.di"
option.value = "80"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//81
option = document.createElement("option");
option.text = "81 - Komy Permjats.Aut.di"
option.value = "81"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//82
option = document.createElement("option");
option.text = "82 - Korjacs Auton.distri"
option.value = "82"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//83
option = document.createElement("option");
option.text = "83 - Nenekchky Auton.dist"
option.value = "83"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//84
option = document.createElement("option");
option.text = "84 - The Taymir Auton.dis"
option.value = "84"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//85
option = document.createElement("option");
option.text = "85 - Ust-Ordinsky Buryat"
option.value = "85"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//86
option = document.createElement("option");
option.text = "86 - Chanti-Mansyjsky Aut"
option.value = "86"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//87
option = document.createElement("option");
option.text = "87 - Chukotka Auton. dist"
option.value = "87"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//88
option = document.createElement("option");
option.text = "88 - Evensky Auton.distri"
option.value = "88"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//89
option = document.createElement("option");
option.text = "89 - Jamalo-Nenekchky Aut"
option.value = "89"
option.setAttribute("country", "RU");
this.StateInput.add(option);

//COUNTRY - SA
//1
option = document.createElement("option");
option.text = "1 - Ar Riy##"
option.value = "1"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Makkah"
option.value = "2"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Al Mad#nah"
option.value = "3"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Ash Sharq#yah"
option.value = "4"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Al Qa##m"
option.value = "5"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - ##'il"
option.value = "6"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Al #ud#d ash Sham#li"
option.value = "8"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - J#zan"
option.value = "9"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Najr#n"
option.value = "10"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Al B#hah"
option.value = "11"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Al Jawf"
option.value = "12"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - EASTERN PROVINCE"
option.value = "13"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - `As#r"
option.value = "14"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Tab#k"
option.value = "7"
option.setAttribute("country", "SA");
this.StateInput.add(option);

//COUNTRY - SE
//1
option = document.createElement("option");
option.text = "1 - Blekinge County"
option.value = "1"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Dalarnas County"
option.value = "2"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Gotland County"
option.value = "3"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Gaevleborg County"
option.value = "4"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Halland County"
option.value = "5"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Jaemtland County"
option.value = "6"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Joenkoeping County"
option.value = "7"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Kalmar County"
option.value = "8"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Kronoberg County"
option.value = "9"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Norrbotten County"
option.value = "10"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Skaane County"
option.value = "11"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Stockholm County"
option.value = "12"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Soedermanland County"
option.value = "13"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Uppsala County"
option.value = "14"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Vaermland County"
option.value = "15"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Vaesterbotten County"
option.value = "16"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Vaesternorrland Cnty"
option.value = "17"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Vaestmanland County"
option.value = "18"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Vaestra Goetaland C."
option.value = "19"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Oerebro County"
option.value = "20"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Oestergoetland Cnty"
option.value = "21"
option.setAttribute("country", "SE");
this.StateInput.add(option);

//COUNTRY - SG
//SG
option = document.createElement("option");
option.text = "SG - Singapore"
option.value = "SG"
option.setAttribute("country", "SG");
this.StateInput.add(option);

//COUNTRY - SI
//1
option = document.createElement("option");
option.text = "1 - Ajdovscina"
option.value = "1"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Brezice"
option.value = "2"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Celje"
option.value = "3"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Cerknica"
option.value = "4"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Crnomelj"
option.value = "5"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Dravograd"
option.value = "6"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Gornja Radgona"
option.value = "7"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Grosuplje"
option.value = "8"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Hrastnik Lasko"
option.value = "9"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Idrija"
option.value = "10"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Ilirska Bistrica"
option.value = "11"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Izola"
option.value = "12"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Jesenice"
option.value = "13"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Kamnik"
option.value = "14"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Kocevje"
option.value = "15"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Koper"
option.value = "16"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Kranj"
option.value = "17"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Krsko"
option.value = "18"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Lenart"
option.value = "19"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Lendava"
option.value = "20"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Litija"
option.value = "21"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Ljubljana-Bezigrad"
option.value = "22"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Ljubljana-Center"
option.value = "23"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Ljubljana-Moste-Polj"
option.value = "24"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Ljubljana-Siska"
option.value = "25"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Ljubljana-Vic-Rudnik"
option.value = "26"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Ljutomer"
option.value = "27"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Logatec"
option.value = "28"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Maribor"
option.value = "29"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Metlika"
option.value = "30"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Mozirje"
option.value = "31"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Murska Sobota"
option.value = "32"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Nova Gorica"
option.value = "33"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Novo Mesto"
option.value = "34"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Ormoz"
option.value = "35"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Pesnica"
option.value = "36"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Piran"
option.value = "37"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Postojna"
option.value = "38"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Ptuj"
option.value = "39"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Radlje Ob Dravi"
option.value = "40"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Radovljica"
option.value = "41"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Ravne Na Koroskem"
option.value = "42"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Ribnica"
option.value = "43"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Ruse"
option.value = "44"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Sentjur Pri Celju"
option.value = "45"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Sevnica"
option.value = "46"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Sezana"
option.value = "47"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Skofja Loka"
option.value = "48"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - Slovenj Gradec"
option.value = "49"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Slovenska Bistrica"
option.value = "50"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//51
option = document.createElement("option");
option.text = "51 - Slovenske Konjice"
option.value = "51"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - Smarje Pri Jelsah"
option.value = "52"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//53
option = document.createElement("option");
option.text = "53 - Tolmin"
option.value = "53"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//54
option = document.createElement("option");
option.text = "54 - Trbovlje"
option.value = "54"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//55
option = document.createElement("option");
option.text = "55 - Trebnje"
option.value = "55"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//56
option = document.createElement("option");
option.text = "56 - Trzic"
option.value = "56"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//57
option = document.createElement("option");
option.text = "57 - Velenje"
option.value = "57"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//58
option = document.createElement("option");
option.text = "58 - Vrhnika"
option.value = "58"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//59
option = document.createElement("option");
option.text = "59 - Zagorje Ob Savi"
option.value = "59"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//60
option = document.createElement("option");
option.text = "60 - Zalec"
option.value = "60"
option.setAttribute("country", "SI");
this.StateInput.add(option);

//COUNTRY - SK
//1
option = document.createElement("option");
option.text = "1 - Bratislava"
option.value = "1"
option.setAttribute("country", "SK");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Zapadoslovensky"
option.value = "2"
option.setAttribute("country", "SK");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Stredoslovensky"
option.value = "3"
option.setAttribute("country", "SK");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Vychodoslovensky"
option.value = "4"
option.setAttribute("country", "SK");
this.StateInput.add(option);

//BC
option = document.createElement("option");
option.text = "BC - BANKSA BYSTRICA"
option.value = "BC"
option.setAttribute("country", "SK");
this.StateInput.add(option);

//COUNTRY - SL
//FNA
option = document.createElement("option");
option.text = "FNA - Free Town"
option.value = "FNA"
option.setAttribute("country", "SL");
this.StateInput.add(option);

//COUNTRY - SN
//DB
option = document.createElement("option");
option.text = "DB - Diourbel"
option.value = "DB"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//DK
option = document.createElement("option");
option.text = "DK - Dakar"
option.value = "DK"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//FK
option = document.createElement("option");
option.text = "FK - Fatick"
option.value = "FK"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//KA
option = document.createElement("option");
option.text = "KA - Kaffrine"
option.value = "KA"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//KD
option = document.createElement("option");
option.text = "KD - Kolda"
option.value = "KD"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//KE
option = document.createElement("option");
option.text = "KE - Kedougou"
option.value = "KE"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//KL
option = document.createElement("option");
option.text = "KL - Kaolack"
option.value = "KL"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//LG
option = document.createElement("option");
option.text = "LG - Louga"
option.value = "LG"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Matam"
option.value = "MT"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//SE
option = document.createElement("option");
option.text = "SE - Sedhiou"
option.value = "SE"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//SL
option = document.createElement("option");
option.text = "SL - Saint Louis"
option.value = "SL"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//TC
option = document.createElement("option");
option.text = "TC - Tambacounda"
option.value = "TC"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//TH
option = document.createElement("option");
option.text = "TH - Thies"
option.value = "TH"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//ZG
option = document.createElement("option");
option.text = "ZG - Ziguinchor"
option.value = "ZG"
option.setAttribute("country", "SN");
this.StateInput.add(option);

//COUNTRY - SR
//BR
option = document.createElement("option");
option.text = "BR - Brokopondo"
option.value = "BR"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//CM
option = document.createElement("option");
option.text = "CM - Commewijne"
option.value = "CM"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//CR
option = document.createElement("option");
option.text = "CR - Coronie"
option.value = "CR"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Marowijne"
option.value = "MA"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//NI
option = document.createElement("option");
option.text = "NI - Nickeie"
option.value = "NI"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//PM
option = document.createElement("option");
option.text = "PM - Paramaribo"
option.value = "PM"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//PR
option = document.createElement("option");
option.text = "PR - Para"
option.value = "PR"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Saramacca"
option.value = "SA"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//SI
option = document.createElement("option");
option.text = "SI - Sipaliwini"
option.value = "SI"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Wanica"
option.value = "WA"
option.setAttribute("country", "SR");
this.StateInput.add(option);

//COUNTRY - SV
//AH
option = document.createElement("option");
option.text = "AH - Ahuachapán"
option.value = "AH"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - Cabañas"
option.value = "CA"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//CH
option = document.createElement("option");
option.text = "CH - Chalatenango"
option.value = "CH"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//CU
option = document.createElement("option");
option.text = "CU - Cuscatlán"
option.value = "CU"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//LI
option = document.createElement("option");
option.text = "LI - La Libertad"
option.value = "LI"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//MO
option = document.createElement("option");
option.text = "MO - Morazán"
option.value = "MO"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - La Paz"
option.value = "PA"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Santa Ana"
option.value = "SA"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//SM
option = document.createElement("option");
option.text = "SM - San Miguel"
option.value = "SM"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Sonsonate"
option.value = "SO"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//SS
option = document.createElement("option");
option.text = "SS - San Salvador"
option.value = "SS"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//SV
option = document.createElement("option");
option.text = "SV - San Vicente"
option.value = "SV"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//UN
option = document.createElement("option");
option.text = "UN - La Unión"
option.value = "UN"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//US
option = document.createElement("option");
option.text = "US - Usulután"
option.value = "US"
option.setAttribute("country", "SV");
this.StateInput.add(option);

//COUNTRY - SX
//1
option = document.createElement("option");
option.text = "1 - "
option.value = "1"
option.setAttribute("country", "SX");
this.StateInput.add(option);

//COUNTRY - SY
//AL
option = document.createElement("option");
option.text = "AL - ALEPPO"
option.value = "AL"
option.setAttribute("country", "SY");
this.StateInput.add(option);

//COUNTRY - TC
//TC
option = document.createElement("option");
option.text = "TC - Turks and Caicos Isl"
option.value = "TC"
option.setAttribute("country", "TC");
this.StateInput.add(option);

//COUNTRY - TH
//1
option = document.createElement("option");
option.text = "1 - Amnat Charoen"
option.value = "1"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Ang Thong"
option.value = "2"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Buriram"
option.value = "3"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Chachoengsao"
option.value = "4"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Chai Nat"
option.value = "5"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Chaiyaphum"
option.value = "6"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Chanthaburi"
option.value = "7"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Chiang Mai"
option.value = "8"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Chiang Rai"
option.value = "9"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Chon Buri"
option.value = "10"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Chumphon"
option.value = "11"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Kalasin"
option.value = "12"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Kamphaeng Phet"
option.value = "13"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Kanchanaburi"
option.value = "14"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Khon Kaen"
option.value = "15"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Krabi"
option.value = "16"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Krung Thep"
option.value = "17"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Mahanakhon"
option.value = "18"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Lampang"
option.value = "19"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Lamphun"
option.value = "20"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Loei"
option.value = "21"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Lop Buri"
option.value = "22"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Mae Hong Son"
option.value = "23"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Maha Sarakham"
option.value = "24"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Mukdahan"
option.value = "25"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Nakhon Nayok"
option.value = "26"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Nakhon Pathom"
option.value = "27"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Nakhon Phanom"
option.value = "28"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Nakhon Ratchasima"
option.value = "29"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Nakhon Sawan"
option.value = "30"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Nakhon Si Thammarat"
option.value = "31"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Nan"
option.value = "32"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Narathiwat"
option.value = "33"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Nong Bua Lamphu"
option.value = "34"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Nong Khai"
option.value = "35"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Nonthaburi"
option.value = "36"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Pathum Thani"
option.value = "37"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Pattani"
option.value = "38"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Phangnga"
option.value = "39"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Phatthalung"
option.value = "40"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Phayao"
option.value = "41"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Phetchabun"
option.value = "42"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Phetchaburi"
option.value = "43"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Phichit"
option.value = "44"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Phitsanulok"
option.value = "45"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - Phra Nakhon Si Ayut."
option.value = "46"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Phrae"
option.value = "47"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Phuket"
option.value = "48"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - Prachin Buri"
option.value = "49"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - BANGKOK"
option.value = "50"
option.setAttribute("country", "TH");
this.StateInput.add(option);

//COUNTRY - TM
//TMA
option = document.createElement("option");
option.text = "TMA - Ahal Province"
option.value = "TMA"
option.setAttribute("country", "TM");
this.StateInput.add(option);

//TMB
option = document.createElement("option");
option.text = "TMB - Balkan Province"
option.value = "TMB"
option.setAttribute("country", "TM");
this.StateInput.add(option);

//TMD
option = document.createElement("option");
option.text = "TMD - Dasoguz Province"
option.value = "TMD"
option.setAttribute("country", "TM");
this.StateInput.add(option);

//TML
option = document.createElement("option");
option.text = "TML - Lebap Province"
option.value = "TML"
option.setAttribute("country", "TM");
this.StateInput.add(option);

//TMM
option = document.createElement("option");
option.text = "TMM - Mary Province"
option.value = "TMM"
option.setAttribute("country", "TM");
this.StateInput.add(option);

//TMS
option = document.createElement("option");
option.text = "TMS - Ashgabat City"
option.value = "TMS"
option.setAttribute("country", "TM");
this.StateInput.add(option);

//COUNTRY - TO
//4
option = document.createElement("option");
option.text = "4 - TONGATAPU"
option.value = "4"
option.setAttribute("country", "TO");
this.StateInput.add(option);

//COUNTRY - TR
//1
option = document.createElement("option");
option.text = "1 - Adana"
option.value = "1"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//2
option = document.createElement("option");
option.text = "2 - Adiyaman"
option.value = "2"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//3
option = document.createElement("option");
option.text = "3 - Afyon"
option.value = "3"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//4
option = document.createElement("option");
option.text = "4 - Agri"
option.value = "4"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//5
option = document.createElement("option");
option.text = "5 - Amasya"
option.value = "5"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//6
option = document.createElement("option");
option.text = "6 - Ankara"
option.value = "6"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//7
option = document.createElement("option");
option.text = "7 - Antalya"
option.value = "7"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//8
option = document.createElement("option");
option.text = "8 - Artvin"
option.value = "8"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//9
option = document.createElement("option");
option.text = "9 - Aydin"
option.value = "9"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//10
option = document.createElement("option");
option.text = "10 - Balikesir"
option.value = "10"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//11
option = document.createElement("option");
option.text = "11 - Bilecik"
option.value = "11"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//12
option = document.createElement("option");
option.text = "12 - Bingöl"
option.value = "12"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//13
option = document.createElement("option");
option.text = "13 - Bitlis"
option.value = "13"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//14
option = document.createElement("option");
option.text = "14 - Bolu"
option.value = "14"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//15
option = document.createElement("option");
option.text = "15 - Burdur"
option.value = "15"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//16
option = document.createElement("option");
option.text = "16 - Bursa"
option.value = "16"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//17
option = document.createElement("option");
option.text = "17 - Canakkale"
option.value = "17"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//18
option = document.createElement("option");
option.text = "18 - Cankiri"
option.value = "18"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//19
option = document.createElement("option");
option.text = "19 - Corum"
option.value = "19"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//20
option = document.createElement("option");
option.text = "20 - Denizli"
option.value = "20"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//21
option = document.createElement("option");
option.text = "21 - Diyarbakir"
option.value = "21"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//22
option = document.createElement("option");
option.text = "22 - Edirne"
option.value = "22"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//23
option = document.createElement("option");
option.text = "23 - Elazig"
option.value = "23"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//24
option = document.createElement("option");
option.text = "24 - Erzincan"
option.value = "24"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//25
option = document.createElement("option");
option.text = "25 - Erzurum"
option.value = "25"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//26
option = document.createElement("option");
option.text = "26 - Eskisehir"
option.value = "26"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//27
option = document.createElement("option");
option.text = "27 - Gaziantep"
option.value = "27"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//28
option = document.createElement("option");
option.text = "28 - Giresun"
option.value = "28"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//29
option = document.createElement("option");
option.text = "29 - Guemueshane"
option.value = "29"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//30
option = document.createElement("option");
option.text = "30 - Hakkari"
option.value = "30"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//31
option = document.createElement("option");
option.text = "31 - Hatay"
option.value = "31"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//32
option = document.createElement("option");
option.text = "32 - Isparta"
option.value = "32"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//33
option = document.createElement("option");
option.text = "33 - Icel"
option.value = "33"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//34
option = document.createElement("option");
option.text = "34 - Istanbul"
option.value = "34"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//35
option = document.createElement("option");
option.text = "35 - Izmir"
option.value = "35"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//36
option = document.createElement("option");
option.text = "36 - Kars"
option.value = "36"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//37
option = document.createElement("option");
option.text = "37 - Kastamonu"
option.value = "37"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//38
option = document.createElement("option");
option.text = "38 - Kayseri"
option.value = "38"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//39
option = document.createElement("option");
option.text = "39 - Kirklareli"
option.value = "39"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//40
option = document.createElement("option");
option.text = "40 - Kirshehir"
option.value = "40"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//41
option = document.createElement("option");
option.text = "41 - Kocaeli"
option.value = "41"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//42
option = document.createElement("option");
option.text = "42 - Konya"
option.value = "42"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//43
option = document.createElement("option");
option.text = "43 - Kuetahya"
option.value = "43"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//44
option = document.createElement("option");
option.text = "44 - Malatya"
option.value = "44"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//45
option = document.createElement("option");
option.text = "45 - Manisa"
option.value = "45"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//46
option = document.createElement("option");
option.text = "46 - K.Marash"
option.value = "46"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//47
option = document.createElement("option");
option.text = "47 - Mardin"
option.value = "47"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//48
option = document.createElement("option");
option.text = "48 - Mugla"
option.value = "48"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//49
option = document.createElement("option");
option.text = "49 - Mush"
option.value = "49"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//50
option = document.createElement("option");
option.text = "50 - Nevshehir"
option.value = "50"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//51
option = document.createElement("option");
option.text = "51 - Nigde"
option.value = "51"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//52
option = document.createElement("option");
option.text = "52 - Ordu"
option.value = "52"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//53
option = document.createElement("option");
option.text = "53 - Rize"
option.value = "53"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//54
option = document.createElement("option");
option.text = "54 - Sakarya"
option.value = "54"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//55
option = document.createElement("option");
option.text = "55 - Samsun"
option.value = "55"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//56
option = document.createElement("option");
option.text = "56 - Siirt"
option.value = "56"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//57
option = document.createElement("option");
option.text = "57 - Sinop"
option.value = "57"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//58
option = document.createElement("option");
option.text = "58 - Sivas"
option.value = "58"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//59
option = document.createElement("option");
option.text = "59 - Tekirdag"
option.value = "59"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//60
option = document.createElement("option");
option.text = "60 - Tokat"
option.value = "60"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//61
option = document.createElement("option");
option.text = "61 - Trabzon"
option.value = "61"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//62
option = document.createElement("option");
option.text = "62 - Tunceli"
option.value = "62"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//63
option = document.createElement("option");
option.text = "63 - Shanliurfa"
option.value = "63"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//64
option = document.createElement("option");
option.text = "64 - Ushak"
option.value = "64"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//65
option = document.createElement("option");
option.text = "65 - Van"
option.value = "65"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//66
option = document.createElement("option");
option.text = "66 - Yozgat"
option.value = "66"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//67
option = document.createElement("option");
option.text = "67 - Zonguldak"
option.value = "67"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//68
option = document.createElement("option");
option.text = "68 - Aksaray"
option.value = "68"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//69
option = document.createElement("option");
option.text = "69 - Bayburt"
option.value = "69"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//70
option = document.createElement("option");
option.text = "70 - Karaman"
option.value = "70"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//71
option = document.createElement("option");
option.text = "71 - Kirikkale"
option.value = "71"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//72
option = document.createElement("option");
option.text = "72 - Batman"
option.value = "72"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//73
option = document.createElement("option");
option.text = "73 - Shirnak"
option.value = "73"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//74
option = document.createElement("option");
option.text = "74 - Bartin"
option.value = "74"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//75
option = document.createElement("option");
option.text = "75 - Ardahan"
option.value = "75"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//76
option = document.createElement("option");
option.text = "76 - Igdir"
option.value = "76"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//77
option = document.createElement("option");
option.text = "77 - Yalova"
option.value = "77"
option.setAttribute("country", "TR");
this.StateInput.add(option);

//COUNTRY - TT
//ARI
option = document.createElement("option");
option.text = "ARI - Arima"
option.value = "ARI"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//CHA
option = document.createElement("option");
option.text = "CHA - Chaguanas"
option.value = "CHA"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//CTT
option = document.createElement("option");
option.text = "CTT - Couva/Tabaquite/Talp"
option.value = "CTT"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//DMN
option = document.createElement("option");
option.text = "DMN - Diego Martin"
option.value = "DMN"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//ETO
option = document.createElement("option");
option.text = "ETO - Eastern Tobago"
option.value = "ETO"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//PED
option = document.createElement("option");
option.text = "PED - Penal/Debe"
option.value = "PED"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//POS
option = document.createElement("option");
option.text = "POS - Port of Spain"
option.value = "POS"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//PRT
option = document.createElement("option");
option.text = "PRT - Princes Town"
option.value = "PRT"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//PTF
option = document.createElement("option");
option.text = "PTF - Point Fortin"
option.value = "PTF"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//RCM
option = document.createElement("option");
option.text = "RCM - Mayaro/Rio Claro"
option.value = "RCM"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//SFO
option = document.createElement("option");
option.text = "SFO - San Fernando"
option.value = "SFO"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//SGE
option = document.createElement("option");
option.text = "SGE - Sangre Grande"
option.value = "SGE"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//SIP
option = document.createElement("option");
option.text = "SIP - Siparia"
option.value = "SIP"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//SJL
option = document.createElement("option");
option.text = "SJL - San Juan/Laventille"
option.value = "SJL"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//TUP
option = document.createElement("option");
option.text = "TUP - Tunapuna/Piarco"
option.value = "TUP"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//WTO
option = document.createElement("option");
option.text = "WTO - Western Tobago"
option.value = "WTO"
option.setAttribute("country", "TT");
this.StateInput.add(option);

//COUNTRY - TW
//FJN
option = document.createElement("option");
option.text = "FJN - Fu-chien"
option.value = "FJN"
option.setAttribute("country", "TW");
this.StateInput.add(option);

//HSQ
option = document.createElement("option");
option.text = "HSQ - HSINCHU"
option.value = "HSQ"
option.setAttribute("country", "TW");
this.StateInput.add(option);

//KSH
option = document.createElement("option");
option.text = "KSH - Kao-hsiung"
option.value = "KSH"
option.setAttribute("country", "TW");
this.StateInput.add(option);

//TPE
option = document.createElement("option");
option.text = "TPE - T´ai-pei"
option.value = "TPE"
option.setAttribute("country", "TW");
this.StateInput.add(option);

//TWN
option = document.createElement("option");
option.text = "TWN - Taiwan"
option.value = "TWN"
option.setAttribute("country", "TW");
this.StateInput.add(option);

//COUNTRY - TZ
//5
option = document.createElement("option");
option.text = "5 - KAGERA"
option.value = "5"
option.setAttribute("country", "TZ");
this.StateInput.add(option);

//COUNTRY - UA
//CHG
option = document.createElement("option");
option.text = "CHG - Chernigivs'ka"
option.value = "CHG"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//CHR
option = document.createElement("option");
option.text = "CHR - Cherkas'ka"
option.value = "CHR"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//CHV
option = document.createElement("option");
option.text = "CHV - Chernovits'ka"
option.value = "CHV"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//DNP
option = document.createElement("option");
option.text = "DNP - Dnipropetrovs'ka"
option.value = "DNP"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//DON
option = document.createElement("option");
option.text = "DON - Donets'ka"
option.value = "DON"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//HAR
option = document.createElement("option");
option.text = "HAR - Harkivs'ka"
option.value = "HAR"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//HML
option = document.createElement("option");
option.text = "HML - Hmel'nits'ka"
option.value = "HML"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//HRS
option = document.createElement("option");
option.text = "HRS - Hersons'ka"
option.value = "HRS"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//IVF
option = document.createElement("option");
option.text = "IVF - Ivano-Frankivs'ka"
option.value = "IVF"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//KIE
option = document.createElement("option");
option.text = "KIE - Kievs'ka"
option.value = "KIE"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//KIR
option = document.createElement("option");
option.text = "KIR - Kirovograds'ka"
option.value = "KIR"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//KRM
option = document.createElement("option");
option.text = "KRM - Respublika Krim"
option.value = "KRM"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//L'V
option = document.createElement("option");
option.text = "L'V - L'vivsbka"
option.value = "L'V"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//LUG
option = document.createElement("option");
option.text = "LUG - Lugans'ka"
option.value = "LUG"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//MIK
option = document.createElement("option");
option.text = "MIK - Mikolaivs'ka"
option.value = "MIK"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//M_K
option = document.createElement("option");
option.text = "M_K - m.Kiev"
option.value = "M_K"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//M_S
option = document.createElement("option");
option.text = "M_S - m.Sevastopil'"
option.value = "M_S"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//ODS
option = document.createElement("option");
option.text = "ODS - Odes'ka"
option.value = "ODS"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//POL
option = document.createElement("option");
option.text = "POL - Poltavs'ka"
option.value = "POL"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//RIV
option = document.createElement("option");
option.text = "RIV - Rivnens'ka"
option.value = "RIV"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//SUM
option = document.createElement("option");
option.text = "SUM - Sums'ka"
option.value = "SUM"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//TER
option = document.createElement("option");
option.text = "TER - Ternopil's'ka"
option.value = "TER"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//VIN
option = document.createElement("option");
option.text = "VIN - Vinnits'ka"
option.value = "VIN"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//VOL
option = document.createElement("option");
option.text = "VOL - Volins'ka"
option.value = "VOL"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//ZAK
option = document.createElement("option");
option.text = "ZAK - Zakarpats'ka"
option.value = "ZAK"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//ZAP
option = document.createElement("option");
option.text = "ZAP - Zaporiz'ka"
option.value = "ZAP"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//ZHI
option = document.createElement("option");
option.text = "ZHI - Zhitomirs'ka"
option.value = "ZHI"
option.setAttribute("country", "UA");
this.StateInput.add(option);

//COUNTRY - UG
//ZZ
option = document.createElement("option");
option.text = "ZZ - KAMPALA"
option.value = "ZZ"
option.setAttribute("country", "UG");
this.StateInput.add(option);

//COUNTRY - US
//AK
option = document.createElement("option");
option.text = "AK - Alaska"
option.value = "AK"
option.setAttribute("country", "US");
this.StateInput.add(option);

//AL
option = document.createElement("option");
option.text = "AL - Alabama"
option.value = "AL"
option.setAttribute("country", "US");
this.StateInput.add(option);

//AP
option = document.createElement("option");
option.text = "AP - US APO Pacific"
option.value = "AP"
option.setAttribute("country", "US");
this.StateInput.add(option);

//AR
option = document.createElement("option");
option.text = "AR - Arkansas"
option.value = "AR"
option.setAttribute("country", "US");
this.StateInput.add(option);

//AS
option = document.createElement("option");
option.text = "AS - American Samoa"
option.value = "AS"
option.setAttribute("country", "US");
this.StateInput.add(option);

//AZ
option = document.createElement("option");
option.text = "AZ - Arizona"
option.value = "AZ"
option.setAttribute("country", "US");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - California"
option.value = "CA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//CO
option = document.createElement("option");
option.text = "CO - Colorado"
option.value = "CO"
option.setAttribute("country", "US");
this.StateInput.add(option);

//CT
option = document.createElement("option");
option.text = "CT - Connecticut"
option.value = "CT"
option.setAttribute("country", "US");
this.StateInput.add(option);

//DC
option = document.createElement("option");
option.text = "DC - District of Columbia"
option.value = "DC"
option.setAttribute("country", "US");
this.StateInput.add(option);

//DE
option = document.createElement("option");
option.text = "DE - Delaware"
option.value = "DE"
option.setAttribute("country", "US");
this.StateInput.add(option);

//FL
option = document.createElement("option");
option.text = "FL - Florida"
option.value = "FL"
option.setAttribute("country", "US");
this.StateInput.add(option);

//GA
option = document.createElement("option");
option.text = "GA - Georgia"
option.value = "GA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//GU
option = document.createElement("option");
option.text = "GU - Guam"
option.value = "GU"
option.setAttribute("country", "US");
this.StateInput.add(option);

//HI
option = document.createElement("option");
option.text = "HI - Hawaii"
option.value = "HI"
option.setAttribute("country", "US");
this.StateInput.add(option);

//IA
option = document.createElement("option");
option.text = "IA - Iowa"
option.value = "IA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//ID
option = document.createElement("option");
option.text = "ID - Idaho"
option.value = "ID"
option.setAttribute("country", "US");
this.StateInput.add(option);

//IL
option = document.createElement("option");
option.text = "IL - Illinois"
option.value = "IL"
option.setAttribute("country", "US");
this.StateInput.add(option);

//IN
option = document.createElement("option");
option.text = "IN - Indiana"
option.value = "IN"
option.setAttribute("country", "US");
this.StateInput.add(option);

//KS
option = document.createElement("option");
option.text = "KS - Kansas"
option.value = "KS"
option.setAttribute("country", "US");
this.StateInput.add(option);

//KY
option = document.createElement("option");
option.text = "KY - Kentucky"
option.value = "KY"
option.setAttribute("country", "US");
this.StateInput.add(option);

//LA
option = document.createElement("option");
option.text = "LA - Louisiana"
option.value = "LA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Massachusetts"
option.value = "MA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MD
option = document.createElement("option");
option.text = "MD - Maryland"
option.value = "MD"
option.setAttribute("country", "US");
this.StateInput.add(option);

//ME
option = document.createElement("option");
option.text = "ME - Maine"
option.value = "ME"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MI
option = document.createElement("option");
option.text = "MI - Michigan"
option.value = "MI"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MN
option = document.createElement("option");
option.text = "MN - Minnesota"
option.value = "MN"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MO
option = document.createElement("option");
option.text = "MO - Missouri"
option.value = "MO"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MP
option = document.createElement("option");
option.text = "MP - Northern Mariana Isl"
option.value = "MP"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MS
option = document.createElement("option");
option.text = "MS - Mississippi"
option.value = "MS"
option.setAttribute("country", "US");
this.StateInput.add(option);

//MT
option = document.createElement("option");
option.text = "MT - Montana"
option.value = "MT"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NC
option = document.createElement("option");
option.text = "NC - North Carolina"
option.value = "NC"
option.setAttribute("country", "US");
this.StateInput.add(option);

//ND
option = document.createElement("option");
option.text = "ND - North Dakota"
option.value = "ND"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NE
option = document.createElement("option");
option.text = "NE - Nebraska"
option.value = "NE"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NH
option = document.createElement("option");
option.text = "NH - New Hampshire"
option.value = "NH"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NJ
option = document.createElement("option");
option.text = "NJ - New Jersey"
option.value = "NJ"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NM
option = document.createElement("option");
option.text = "NM - New Mexico"
option.value = "NM"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NV
option = document.createElement("option");
option.text = "NV - Nevada"
option.value = "NV"
option.setAttribute("country", "US");
this.StateInput.add(option);

//NY
option = document.createElement("option");
option.text = "NY - New York"
option.value = "NY"
option.setAttribute("country", "US");
this.StateInput.add(option);

//OH
option = document.createElement("option");
option.text = "OH - Ohio"
option.value = "OH"
option.setAttribute("country", "US");
this.StateInput.add(option);

//OK
option = document.createElement("option");
option.text = "OK - Oklahoma"
option.value = "OK"
option.setAttribute("country", "US");
this.StateInput.add(option);

//OR
option = document.createElement("option");
option.text = "OR - Oregon"
option.value = "OR"
option.setAttribute("country", "US");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Pennsylvania"
option.value = "PA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//PR
option = document.createElement("option");
option.text = "PR - Puerto Rico"
option.value = "PR"
option.setAttribute("country", "US");
this.StateInput.add(option);

//RI
option = document.createElement("option");
option.text = "RI - Rhode Island"
option.value = "RI"
option.setAttribute("country", "US");
this.StateInput.add(option);

//SC
option = document.createElement("option");
option.text = "SC - South Carolina"
option.value = "SC"
option.setAttribute("country", "US");
this.StateInput.add(option);

//SD
option = document.createElement("option");
option.text = "SD - South Dakota"
option.value = "SD"
option.setAttribute("country", "US");
this.StateInput.add(option);

//TN
option = document.createElement("option");
option.text = "TN - Tennessee"
option.value = "TN"
option.setAttribute("country", "US");
this.StateInput.add(option);

//TX
option = document.createElement("option");
option.text = "TX - Texas"
option.value = "TX"
option.setAttribute("country", "US");
this.StateInput.add(option);

//UT
option = document.createElement("option");
option.text = "UT - Utah"
option.value = "UT"
option.setAttribute("country", "US");
this.StateInput.add(option);

//VA
option = document.createElement("option");
option.text = "VA - Virginia"
option.value = "VA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//VI
option = document.createElement("option");
option.text = "VI - Virgin Islands"
option.value = "VI"
option.setAttribute("country", "US");
this.StateInput.add(option);

//VT
option = document.createElement("option");
option.text = "VT - Vermont"
option.value = "VT"
option.setAttribute("country", "US");
this.StateInput.add(option);

//WA
option = document.createElement("option");
option.text = "WA - Washington"
option.value = "WA"
option.setAttribute("country", "US");
this.StateInput.add(option);

//WI
option = document.createElement("option");
option.text = "WI - Wisconsin"
option.value = "WI"
option.setAttribute("country", "US");
this.StateInput.add(option);

//WV
option = document.createElement("option");
option.text = "WV - West Virginia"
option.value = "WV"
option.setAttribute("country", "US");
this.StateInput.add(option);

//WY
option = document.createElement("option");
option.text = "WY - Wyoming"
option.value = "WY"
option.setAttribute("country", "US");
this.StateInput.add(option);

// Select
option = document.createElement("option");
option.text = "--select--"
option.value = "0"
option.setAttribute("country", "US");
option.setAttribute("selected", "selected");
this.StateInput.add(option);

//COUNTRY - UY
//AR
option = document.createElement("option");
option.text = "AR - Artigas"
option.value = "AR"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//CA
option = document.createElement("option");
option.text = "CA - Canelones"
option.value = "CA"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//CL
option = document.createElement("option");
option.text = "CL - Cerro Largo"
option.value = "CL"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//CO
option = document.createElement("option");
option.text = "CO - Colonia"
option.value = "CO"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//DU
option = document.createElement("option");
option.text = "DU - Durazno"
option.value = "DU"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//FD
option = document.createElement("option");
option.text = "FD - Florida"
option.value = "FD"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//FS
option = document.createElement("option");
option.text = "FS - Flores"
option.value = "FS"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//LA
option = document.createElement("option");
option.text = "LA - Lavalleja"
option.value = "LA"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//MA
option = document.createElement("option");
option.text = "MA - Maldonado"
option.value = "MA"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//MO
option = document.createElement("option");
option.text = "MO - Montevideo"
option.value = "MO"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//PA
option = document.createElement("option");
option.text = "PA - Paysandú"
option.value = "PA"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//RN
option = document.createElement("option");
option.text = "RN - Rio Negro"
option.value = "RN"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//RO
option = document.createElement("option");
option.text = "RO - Rocha"
option.value = "RO"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//RV
option = document.createElement("option");
option.text = "RV - Rivera"
option.value = "RV"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Salto"
option.value = "SA"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - San José"
option.value = "SJ"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//SO
option = document.createElement("option");
option.text = "SO - Soriano"
option.value = "SO"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//TA
option = document.createElement("option");
option.text = "TA - Tacuarembó"
option.value = "TA"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//TT
option = document.createElement("option");
option.text = "TT - Treinta y Tres"
option.value = "TT"
option.setAttribute("country", "UY");
this.StateInput.add(option);

//COUNTRY - UZ
//AN
option = document.createElement("option");
option.text = "AN - Andijon"
option.value = "AN"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//BU
option = document.createElement("option");
option.text = "BU - Buxoro"
option.value = "BU"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//FA
option = document.createElement("option");
option.text = "FA - Farg ona"
option.value = "FA"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//JI
option = document.createElement("option");
option.text = "JI - Jizzax"
option.value = "JI"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//NG
option = document.createElement("option");
option.text = "NG - Namangan"
option.value = "NG"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//NW
option = document.createElement("option");
option.text = "NW - Navoi"
option.value = "NW"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//QA
option = document.createElement("option");
option.text = "QA - Kashkadarya"
option.value = "QA"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//QR
option = document.createElement("option");
option.text = "QR - Karakalpakstan"
option.value = "QR"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//SA
option = document.createElement("option");
option.text = "SA - Samarkand"
option.value = "SA"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//SI
option = document.createElement("option");
option.text = "SI - Sirdaryo"
option.value = "SI"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//SU
option = document.createElement("option");
option.text = "SU - Surxondaryo"
option.value = "SU"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//TK
option = document.createElement("option");
option.text = "TK - Tashkent City"
option.value = "TK"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//TO
option = document.createElement("option");
option.text = "TO - Tashkent"
option.value = "TO"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//XO
option = document.createElement("option");
option.text = "XO - Xorazm"
option.value = "XO"
option.setAttribute("country", "UZ");
this.StateInput.add(option);

//COUNTRY - VC
//4
option = document.createElement("option");
option.text = "4 - SAINT GEORGE"
option.value = "4"
option.setAttribute("country", "VC");
this.StateInput.add(option);

//COUNTRY - VE
//AMA
option = document.createElement("option");
option.text = "AMA - Amazon"
option.value = "AMA"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//ANZ
option = document.createElement("option");
option.text = "ANZ - Anzoategui"
option.value = "ANZ"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//APU
option = document.createElement("option");
option.text = "APU - Apure"
option.value = "APU"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//ARA
option = document.createElement("option");
option.text = "ARA - Aragua"
option.value = "ARA"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//BAR
option = document.createElement("option");
option.text = "BAR - Barinas"
option.value = "BAR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//BOL
option = document.createElement("option");
option.text = "BOL - Bolivar"
option.value = "BOL"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//CAD
option = document.createElement("option");
option.text = "CAD - Capital District"
option.value = "CAD"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//CAR
option = document.createElement("option");
option.text = "CAR - Carabobo"
option.value = "CAR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//COJ
option = document.createElement("option");
option.text = "COJ - Cojedes"
option.value = "COJ"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//DA
option = document.createElement("option");
option.text = "DA - Delta Amacuro"
option.value = "DA"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//DF
option = document.createElement("option");
option.text = "DF - Distrito Federal"
option.value = "DF"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//FAL
option = document.createElement("option");
option.text = "FAL - Falcon"
option.value = "FAL"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//GUA
option = document.createElement("option");
option.text = "GUA - Guarico"
option.value = "GUA"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//LAR
option = document.createElement("option");
option.text = "LAR - Lara"
option.value = "LAR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//MER
option = document.createElement("option");
option.text = "MER - Merida"
option.value = "MER"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//MIR
option = document.createElement("option");
option.text = "MIR - Miranda"
option.value = "MIR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//MON
option = document.createElement("option");
option.text = "MON - Monagas"
option.value = "MON"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//NE
option = document.createElement("option");
option.text = "NE - Nueva Esparta"
option.value = "NE"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//POR
option = document.createElement("option");
option.text = "POR - Portuguesa"
option.value = "POR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//SUC
option = document.createElement("option");
option.text = "SUC - Sucre"
option.value = "SUC"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//TAC
option = document.createElement("option");
option.text = "TAC - Tachira"
option.value = "TAC"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//TRU
option = document.createElement("option");
option.text = "TRU - Trujillo"
option.value = "TRU"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//VAR
option = document.createElement("option");
option.text = "VAR - Vargas"
option.value = "VAR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//YAR
option = document.createElement("option");
option.text = "YAR - Yaracuy"
option.value = "YAR"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//ZUL
option = document.createElement("option");
option.text = "ZUL - Zulia"
option.value = "ZUL"
option.setAttribute("country", "VE");
this.StateInput.add(option);

//COUNTRY - VG
//VG
option = document.createElement("option");
option.text = "VG - Brittish Virgin"
option.value = "VG"
option.setAttribute("country", "VG");
this.StateInput.add(option);

//COUNTRY - VI
//SC
option = document.createElement("option");
option.text = "SC - Saint Croix"
option.value = "SC"
option.setAttribute("country", "VI");
this.StateInput.add(option);

//SJ
option = document.createElement("option");
option.text = "SJ - Saint John"
option.value = "SJ"
option.setAttribute("country", "VI");
this.StateInput.add(option);

//ST
option = document.createElement("option");
option.text = "ST - Saint Thomas"
option.value = "ST"
option.setAttribute("country", "VI");
this.StateInput.add(option);

//COUNTRY - YE
//1
option = document.createElement("option");
option.text = "1 - Sana'a"
option.value = "1"
option.setAttribute("country", "YE");
this.StateInput.add(option);

//COUNTRY - ZA
//EC
option = document.createElement("option");
option.text = "EC - Eastern Cape"
option.value = "EC"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//FS
option = document.createElement("option");
option.text = "FS - Freestate"
option.value = "FS"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//GP
option = document.createElement("option");
option.text = "GP - Gauteng"
option.value = "GP"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//KZN
option = document.createElement("option");
option.text = "KZN - Kwazulu/Natal"
option.value = "KZN"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//MP
option = document.createElement("option");
option.text = "MP - Mpumalanga"
option.value = "MP"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//NC
option = document.createElement("option");
option.text = "NC - Northern Cape"
option.value = "NC"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//NP
option = document.createElement("option");
option.text = "NP - Northern Province"
option.value = "NP"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//NW
option = document.createElement("option");
option.text = "NW - North-West"
option.value = "NW"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//WC
option = document.createElement("option");
option.text = "WC - Western Cape"
option.value = "WC"
option.setAttribute("country", "ZA");
this.StateInput.add(option);

//COUNTRIES

//COUNTRY - AE
option = document.createElement("option");
option.text = "AE - United Arab Emirates";
option.value = "AE";
this.CountryInput.add(option);

//COUNTRY - AG
option = document.createElement("option");
option.text = "AG - Antigua and Barbuda";
option.value = "AG";
this.CountryInput.add(option);

//COUNTRY - AI
option = document.createElement("option");
option.text = "AI - Anguilla";
option.value = "AI";
this.CountryInput.add(option);

//COUNTRY - AR
option = document.createElement("option");
option.text = "AR - Argentina";
option.value = "AR";
this.CountryInput.add(option);

//COUNTRY - AT
option = document.createElement("option");
option.text = "AT - Austria";
option.value = "AT";
this.CountryInput.add(option);

//COUNTRY - AU
option = document.createElement("option");
option.text = "AU - Australia";
option.value = "AU";
this.CountryInput.add(option);

//COUNTRY - AW
option = document.createElement("option");
option.text = "AW - Aruba";
option.value = "AW";
this.CountryInput.add(option);

//COUNTRY - BB
option = document.createElement("option");
option.text = "BB - Barbados";
option.value = "BB";
this.CountryInput.add(option);

//COUNTRY - BD
option = document.createElement("option");
option.text = "BD - Bangladesh";
option.value = "BD";
this.CountryInput.add(option);

//COUNTRY - BE
option = document.createElement("option");
option.text = "BE - Belgium";
option.value = "BE";
this.CountryInput.add(option);

//COUNTRY - BF
option = document.createElement("option");
option.text = "BF - Burkina Faso";
option.value = "BF";
this.CountryInput.add(option);

//COUNTRY - BG
option = document.createElement("option");
option.text = "BG - Bulgaria";
option.value = "BG";
this.CountryInput.add(option);

//COUNTRY - BH
option = document.createElement("option");
option.text = "BH - Bahrain";
option.value = "BH";
this.CountryInput.add(option);

//COUNTRY - BM
option = document.createElement("option");
option.text = "BM - Bermuda";
option.value = "BM";
this.CountryInput.add(option);

//COUNTRY - BO
option = document.createElement("option");
option.text = "BO - Bolivia";
option.value = "BO";
this.CountryInput.add(option);

//COUNTRY - BQ
option = document.createElement("option");
option.text = "BQ - Bonaire";
option.value = "BQ";
this.CountryInput.add(option);

//COUNTRY - BR
option = document.createElement("option");
option.text = "BR - Brazil";
option.value = "BR";
this.CountryInput.add(option);

//COUNTRY - BS
option = document.createElement("option");
option.text = "BS - Bahamas";
option.value = "BS";
this.CountryInput.add(option);

//COUNTRY - BW
option = document.createElement("option");
option.text = "BW - Botswana";
option.value = "BW";
this.CountryInput.add(option);

//COUNTRY - BZ
option = document.createElement("option");
option.text = "BZ - Belize";
option.value = "BZ";
this.CountryInput.add(option);

//COUNTRY - CA
option = document.createElement("option");
option.text = "CA - Canada";
option.value = "CA";
this.CountryInput.add(option);

//COUNTRY - CH
option = document.createElement("option");
option.text = "CH - Switzerland";
option.value = "CH";
this.CountryInput.add(option);

//COUNTRY - CI
option = document.createElement("option");
option.text = "CI - Cote d//lvoire";
option.value = "CI";
this.CountryInput.add(option);

//COUNTRY - CL
option = document.createElement("option");
option.text = "CL - Chile";
option.value = "CL";
this.CountryInput.add(option);

//COUNTRY - CN
option = document.createElement("option");
option.text = "CN - China";
option.value = "CN";
this.CountryInput.add(option);

//COUNTRY - CO
option = document.createElement("option");
option.text = "CO - Colombia";
option.value = "CO";
this.CountryInput.add(option);

//COUNTRY - CR
option = document.createElement("option");
option.text = "CR - Costa Rica";
option.value = "CR";
this.CountryInput.add(option);

//COUNTRY - CW
option = document.createElement("option");
option.text = "CW - Curacao";
option.value = "CW";
this.CountryInput.add(option);

//COUNTRY - CY
option = document.createElement("option");
option.text = "CY - Cyprus";
option.value = "CY";
this.CountryInput.add(option);

//COUNTRY - CZ
option = document.createElement("option");
option.text = "CZ - Czechia";
option.value = "CZ";
this.CountryInput.add(option);

//COUNTRY - DE
option = document.createElement("option");
option.text = "DE - Djibouti";
option.value = "DE";
this.CountryInput.add(option);

//COUNTRY - DK
option = document.createElement("option");
option.text = "DK - Denbark";
option.value = "DK";
this.CountryInput.add(option);

//COUNTRY - DM
option = document.createElement("option");
option.text = "DM - Dominica";
option.value = "DM";
this.CountryInput.add(option);

//COUNTRY - DO
option = document.createElement("option");
option.text = "DO - Dominican Republic";
option.value = "DO";
this.CountryInput.add(option);

//COUNTRY - DZ
option = document.createElement("option");
option.text = "DZ - Algeria";
option.value = "DZ";
this.CountryInput.add(option);

//COUNTRY - EC
option = document.createElement("option");
option.text = "EC - Ecuador";
option.value = "EC";
this.CountryInput.add(option);

//COUNTRY - EE
option = document.createElement("option");
option.text = "EE - Estonia";
option.value = "EE";
this.CountryInput.add(option);

//COUNTRY - EG
option = document.createElement("option");
option.text = "EG - Egypt";
option.value = "EG";
this.CountryInput.add(option);

//COUNTRY - ES
option = document.createElement("option");
option.text = "ES - Spain";
option.value = "ES";
this.CountryInput.add(option);

//COUNTRY - FI
option = document.createElement("option");
option.text = "FI - Finland";
option.value = "FI";
this.CountryInput.add(option);

//COUNTRY - FJ
option = document.createElement("option");
option.text = "FJ - Fiji";
option.value = "FJ";
this.CountryInput.add(option);

//COUNTRY - FM
option = document.createElement("option");
option.text = "FM - Micronesia";
option.value = "FM";
this.CountryInput.add(option);

//COUNTRY - FR
option = document.createElement("option");
option.text = "FR - France";
option.value = "FR";
this.CountryInput.add(option);

//COUNTRY - GB
option = document.createElement("option");
option.text = "GB - Great Britain";
option.value = "GB";
this.CountryInput.add(option);

//COUNTRY - GD
option = document.createElement("option");
option.text = "GD - Grenada";
option.value = "GD";
this.CountryInput.add(option);

//COUNTRY - GE
option = document.createElement("option");
option.text = "GE - Georgia";
option.value = "GE";
this.CountryInput.add(option);

//COUNTRY - GH
option = document.createElement("option");
option.text = "GH - Ghana";
option.value = "GH";
this.CountryInput.add(option);

//COUNTRY - GP
option = document.createElement("option");
option.text = "GP - Guadeloupe";
option.value = "GP";
this.CountryInput.add(option);

//COUNTRY - GR
option = document.createElement("option");
option.text = "GR - Greece";
option.value = "GR";
this.CountryInput.add(option);

//COUNTRY - GT
option = document.createElement("option");
option.text = "GT - Guatemala";
option.value = "GT";
this.CountryInput.add(option);

//COUNTRY - GU
option = document.createElement("option");
option.text = "GU - Guam";
option.value = "GU";
this.CountryInput.add(option);

//COUNTRY - GY
option = document.createElement("option");
option.text = "GY - Guyana";
option.value = "GY";
this.CountryInput.add(option);

//COUNTRY - HK
option = document.createElement("option");
option.text = "HK - Hong Kong";
option.value = "HK";
this.CountryInput.add(option);

//COUNTRY - HN
option = document.createElement("option");
option.text = "HN - Honduras";
option.value = "HN";
this.CountryInput.add(option);

//COUNTRY - HR
option = document.createElement("option");
option.text = "HR - Croatia";
option.value = "HR";
this.CountryInput.add(option);

//COUNTRY - HT
option = document.createElement("option");
option.text = "HT - Haiti";
option.value = "HT";
this.CountryInput.add(option);

//COUNTRY - HU
option = document.createElement("option");
option.text = "HU - Hungary";
option.value = "HU";
this.CountryInput.add(option);

//COUNTRY - ID
option = document.createElement("option");
option.text = "ID - Indonesia";
option.value = "ID";
this.CountryInput.add(option);

//COUNTRY - IE
option = document.createElement("option");
option.text = "IE - Ireland";
option.value = "IE";
this.CountryInput.add(option);

//COUNTRY - IL
option = document.createElement("option");
option.text = "IL - Israel";
option.value = "IL";
this.CountryInput.add(option);

//COUNTRY - IN
option = document.createElement("option");
option.text = "IN - India";
option.value = "IN";
this.CountryInput.add(option);

//COUNTRY - IQ
option = document.createElement("option");
option.text = "IQ - Iraq";
option.value = "IQ";
this.CountryInput.add(option);

//COUNTRY - IT
option = document.createElement("option");
option.text = "IT - Italy";
option.value = "IT";
this.CountryInput.add(option);

//COUNTRY - JM
option = document.createElement("option");
option.text = "JM - Jamaica";
option.value = "JM";
this.CountryInput.add(option);

//COUNTRY - JO
option = document.createElement("option");
option.text = "JO - Jordan";
option.value = "JO";
this.CountryInput.add(option);

//COUNTRY - JP
option = document.createElement("option");
option.text = "JP - Japan";
option.value = "JP";
this.CountryInput.add(option);

//COUNTRY - KE
option = document.createElement("option");
option.text = "KE - Kenya";
option.value = "KE";
this.CountryInput.add(option);

//COUNTRY - KN
option = document.createElement("option");
option.text = "KN - Saint Kitts and Nevis";
option.value = "KN";
this.CountryInput.add(option);

//COUNTRY - KR
option = document.createElement("option");
option.text = "KR - The Republic of Korea";
option.value = "KR";
this.CountryInput.add(option);

//COUNTRY - KW
option = document.createElement("option");
option.text = "KW - Kuwait";
option.value = "KW";
this.CountryInput.add(option);

//COUNTRY - KY
option = document.createElement("option");
option.text = "KY - Cayman Islands";
option.value = "KY";
this.CountryInput.add(option);

//COUNTRY - KZ
option = document.createElement("option");
option.text = "KZ - Kazakhstan";
option.value = "KZ";
this.CountryInput.add(option);

//COUNTRY - LB
option = document.createElement("option");
option.text = "LB - Lebanon";
option.value = "LB";
this.CountryInput.add(option);

//COUNTRY - LC
option = document.createElement("option");
option.text = "LC - Saint Lucia";
option.value = "LC";
this.CountryInput.add(option);

//COUNTRY - LK
option = document.createElement("option");
option.text = "LK - Sri Lanka";
option.value = "LK";
this.CountryInput.add(option);

//COUNTRY - LR
option = document.createElement("option");
option.text = "LR - Liberia";
option.value = "LR";
this.CountryInput.add(option);

//COUNTRY - LT
option = document.createElement("option");
option.text = "LT - Lithuania";
option.value = "LT";
this.CountryInput.add(option);

//COUNTRY - LU
option = document.createElement("option");
option.text = "LU - Luxembourg";
option.value = "LU";
this.CountryInput.add(option);

//COUNTRY - LV
option = document.createElement("option");
option.text = "LV - Latvia";
option.value = "LV";
this.CountryInput.add(option);

//COUNTRY - MF
option = document.createElement("option");
option.text = "MF - Saint Martin";
option.value = "MF";
this.CountryInput.add(option);

//COUNTRY - MH
option = document.createElement("option");
option.text = "MH - Marshall Islands";
option.value = "MH";
this.CountryInput.add(option);

//COUNTRY - MK
option = document.createElement("option");
option.text = "MK - Republic of NOrth Macedonia";
option.value = "MK";
this.CountryInput.add(option);

//COUNTRY - ML
option = document.createElement("option");
option.text = "ML - Mali";
option.value = "ML";
this.CountryInput.add(option);

//COUNTRY - MQ
option = document.createElement("option");
option.text = "MQ - Martinique";
option.value = "MQ";
this.CountryInput.add(option);

//COUNTRY - MT
option = document.createElement("option");
option.text = "MT - Malta";
option.value = "MT";
this.CountryInput.add(option);

//COUNTRY - MX
option = document.createElement("option");
option.text = "MX - Mexico";
option.value = "MX";
this.CountryInput.add(option);

//COUNTRY - MY
option = document.createElement("option");
option.text = "MY - Malaysia";
option.value = "MY";
this.CountryInput.add(option);

//COUNTRY - MZ
option = document.createElement("option");
option.text = "MZ - Mozambique";
option.value = "MZ";
this.CountryInput.add(option);

//COUNTRY - NG
option = document.createElement("option");
option.text = "NG - Nigeria";
option.value = "NG";
this.CountryInput.add(option);

//COUNTRY - NI
option = document.createElement("option");
option.text = "NI - Nicaragua";
option.value = "NI";
this.CountryInput.add(option);

//COUNTRY - NL
option = document.createElement("option");
option.text = "NL - Netherlands";
option.value = "NL";
this.CountryInput.add(option);

//COUNTRY - NO
option = document.createElement("option");
option.text = "NO - Norway";
option.value = "NO";
this.CountryInput.add(option);

//COUNTRY - NZ
option = document.createElement("option");
option.text = "NZ - New Zealand";
option.value = "NZ";
this.CountryInput.add(option);

//COUNTRY - OM
option = document.createElement("option");
option.text = "OM - Oman";
option.value = "OM";
this.CountryInput.add(option);

//COUNTRY - PA
option = document.createElement("option");
option.text = "PA - Panama";
option.value = "PA";
this.CountryInput.add(option);

//COUNTRY - PE
option = document.createElement("option");
option.text = "PE - Peru";
option.value = "PE";
this.CountryInput.add(option);

//COUNTRY - PF
option = document.createElement("option");
option.text = "PF - French Polynesia";
option.value = "PF";
this.CountryInput.add(option);

//COUNTRY - PG
option = document.createElement("option");
option.text = "PG - Papua New Guinea";
option.value = "PG";
this.CountryInput.add(option);

//COUNTRY - PH
option = document.createElement("option");
option.text = "PH - Philippines";
option.value = "PH";
this.CountryInput.add(option);

//COUNTRY - PK
option = document.createElement("option");
option.text = "PK - Pakistan";
option.value = "PK";
this.CountryInput.add(option);

//COUNTRY - PL
option = document.createElement("option");
option.text = "PL - Poland";
option.value = "PL";
this.CountryInput.add(option);

//COUNTRY - PR
option = document.createElement("option");
option.text = "PR - Puerto Rico";
option.value = "PR";
this.CountryInput.add(option);

//COUNTRY - PT
option = document.createElement("option");
option.text = "PT - Portugal";
option.value = "PT";
this.CountryInput.add(option);

//COUNTRY - PW
option = document.createElement("option");
option.text = "PW - Palau";
option.value = "PW";
this.CountryInput.add(option);

//COUNTRY - PY
option = document.createElement("option");
option.text = "PY - Paraguay";
option.value = "PY";
this.CountryInput.add(option);

//COUNTRY - QA
option = document.createElement("option");
option.text = "QA - Qatar";
option.value = "QA";
this.CountryInput.add(option);

//COUNTRY - RO
option = document.createElement("option");
option.text = "RO - Romania";
option.value = "RO";
this.CountryInput.add(option);

//COUNTRY - RU
option = document.createElement("option");
option.text = "RU - Russian Federation";
option.value = "RU";
this.CountryInput.add(option);

//COUNTRY - SA
option = document.createElement("option");
option.text = "SA - Saudi Arabia";
option.value = "SA";
this.CountryInput.add(option);

//COUNTRY - SE
option = document.createElement("option");
option.text = "SE - Sweden";
option.value = "SE";
this.CountryInput.add(option);

//COUNTRY - SG
option = document.createElement("option");
option.text = "SG - Singapore";
option.value = "SG";
this.CountryInput.add(option);

//COUNTRY - SI
option = document.createElement("option");
option.text = "SI - Slovenia";
option.value = "SI";
this.CountryInput.add(option);

//COUNTRY - SK
option = document.createElement("option");
option.text = "SK - Slovakia";
option.value = "SK";
this.CountryInput.add(option);

//COUNTRY - SL
option = document.createElement("option");
option.text = "SL - Sierra Leone";
option.value = "SL";
this.CountryInput.add(option);

//COUNTRY - SN
option = document.createElement("option");
option.text = "SN - Senegal";
option.value = "SN";
this.CountryInput.add(option);

//COUNTRY - SR
option = document.createElement("option");
option.text = "SR - Suriname";
option.value = "SR";
this.CountryInput.add(option);

//COUNTRY - SV
option = document.createElement("option");
option.text = "SV - El Salvador";
option.value = "SV";
this.CountryInput.add(option);

//COUNTRY - SX
option = document.createElement("option");
option.text = "SX - Sint Maarten";
option.value = "SX";
this.CountryInput.add(option);

//COUNTRY - SY
option = document.createElement("option");
option.text = "SY - Syrian Arab Republic";
option.value = "SY";
this.CountryInput.add(option);

//COUNTRY - TC
option = document.createElement("option");
option.text = "TC - Turks and Caicos Islands";
option.value = "TC";
this.CountryInput.add(option);

//COUNTRY - TH
option = document.createElement("option");
option.text = "TH - Thailand";
option.value = "TH";
this.CountryInput.add(option);

//COUNTRY - TM
option = document.createElement("option");
option.text = "TM - Turkmenistan";
option.value = "TM";
this.CountryInput.add(option);

//COUNTRY - TO
option = document.createElement("option");
option.text = "TO - Tonga";
option.value = "TO";
this.CountryInput.add(option);

//COUNTRY - TR
option = document.createElement("option");
option.text = "TR - Turkey";
option.value = "TR";
this.CountryInput.add(option);

//COUNTRY - TT
option = document.createElement("option");
option.text = "TT - Trinidad and Tobago";
option.value = "TT";
this.CountryInput.add(option);

//COUNTRY - TW
option = document.createElement("option");
option.text = "TW - Taiwan";
option.value = "TW";
this.CountryInput.add(option);

//COUNTRY - TZ
option = document.createElement("option");
option.text = "TZ - Tanzania";
option.value = "TZ";
this.CountryInput.add(option);

//COUNTRY - UA
option = document.createElement("option");
option.text = "UA - Ukraine";
option.value = "UA";
this.CountryInput.add(option);

//COUNTRY - UG
option = document.createElement("option");
option.text = "UG - Uganda";
option.value = "UG";
this.CountryInput.add(option);

//COUNTRY - US
option = document.createElement("option");
option.text = "US - United States";
option.value = "US";
this.CountryInput.add(option);

//COUNTRY - UY
option = document.createElement("option");
option.text = "UY - Uruguay";
option.value = "UY";
this.CountryInput.add(option);

//COUNTRY - UZ
option = document.createElement("option");
option.text = "UZ - Uzbekistan";
option.value = "UZ";
this.CountryInput.add(option);

//COUNTRY - VC
option = document.createElement("option");
option.text = "VC - Saint Vincent and the Grenadines";
option.value = "VC";
this.CountryInput.add(option);

//COUNTRY - VE
option = document.createElement("option");
option.text = "VE - Venezuela";
option.value = "VE";
this.CountryInput.add(option);

//COUNTRY - VG
option = document.createElement("option");
option.text = "VG - Virgin Islands (British)";
option.value = "VG";
this.CountryInput.add(option);

//COUNTRY - VI
option = document.createElement("option");
option.text = "VI - Virgin Islands (U.S.)";
option.value = "VI";
this.CountryInput.add(option);

//COUNTRY - YE
option = document.createElement("option");
option.text = "YE - Yemen";
option.value = "YE";
this.CountryInput.add(option);

//COUNTRY - ZA
option = document.createElement("option");
option.text = "ZA - South Africa";
option.value = "ZA";
this.CountryInput.add(option);


	this.SaveButton = DOM.c(
		this, 
		"INPUT",
		{
			"type"	: "button",
			"id"	: "save",
			"name"	: "save", 
			"class"	: "default",
			"value"	: "Create Account"
		}
	)
	
	this.SaveButton.onclick = function(){
		this.Controller.RootElement.Post();
	}
	
	// Create the controls.
	
	// Account Controls
	this.FirstNameControl 				= new FormInputControl("First Name",				"firstname",			this.FirstNameGroup,			true);
	this.MiddleNameControl				= new FormInputControl("Middle Name",				"middlename",			this.MiddleNameGroup,			false);
	this.LastNameControl 				= new FormInputControl("Last Name",					"lastname",				this.LastNameGroup,				true);
	this.EmailControl					= new FormInputControl("Email",						"email",				this.EmailGroup,				true);
	this.UsernameControl				= new FormInputControl("Username",					"username",				this.UsernameGroup,				true);
	this.PasswordControl 				= new FormInputControl("Password",					"password",				this.PasswordGroup,				true);
	this.ConfirmControl 				= new FormInputControl("Confirm Password",			"confirm",				this.ConfirmGroup,				true);
	
	// Contact Controls
	this.StreetAddress1Control			= new FormInputControl("Street Address 1",			"streetaddress1",		this.Address1Group,				true);
	this.StreetAddress2Control			= new FormInputControl("Street Address 2",			"streetaddress2",		this.Address2Group,				false);
	this.CityControl					= new FormInputControl("City",						"city",					this.CityGroup,					true);
	this.StateControl					= new FormInputControl("State",						"state",				this.StateGroup,				true);
	this.ZipControl						= new FormInputControl("Postal Code",				"zip",					this.ZipGroup,					true);
	this.CountryControl					= new FormInputControl("Country",					"country",				this.CountryGroup,				true);
	this.PrimaryPhoneControl			= new FormInputControl("Primary Phone",				"primaryphone",			this.PrimaryPhoneGroup,			true);
	this.MobilePhoneControl				= new FormInputControl("Mobile Phone",				"mobilephone",			this.MobilePhoneGroup,			false);
	
	// Employee Group
	this.JobTitleControl				= new FormInputControl("Job Title",					"jobtitle",				this.JobTitleGroup,				false);
	
	// Other Controls
	this.CompanyControl					= new FormInputControl("Company",					"company",				this.CompanyGroup,				true);
	
	// Log the controls into the Form object.
	// Each control represents a parameter value that will be sent to/received from the server.
	
	this.RootElement.InputControls.push(this.FirstNameControl);
	this.RootElement.InputControls.push(this.MiddleNameControl);
	this.RootElement.InputControls.push(this.LastNameControl);
	this.RootElement.InputControls.push(this.EmailControl);
	this.RootElement.InputControls.push(this.UsernameControl);
	this.RootElement.InputControls.push(this.PasswordControl);
	this.RootElement.InputControls.push(this.ConfirmControl);
	
	this.RootElement.InputControls.push(this.StreetAddress1Control);
	this.RootElement.InputControls.push(this.StreetAddress2Control);
	this.RootElement.InputControls.push(this.CityControl);
	this.RootElement.InputControls.push(this.StateControl);
	this.RootElement.InputControls.push(this.ZipControl);
	this.RootElement.InputControls.push(this.CountryControl);
	this.RootElement.InputControls.push(this.PrimaryPhoneControl);
	this.RootElement.InputControls.push(this.MobilePhoneControl);
	
	this.RootElement.InputControls.push(this.JobTitleControl);
	
	this.RootElement.InputControls.push(this.CompanyControl);
	
	//this.RootElement.InputControls.push(this.OptionContainer);
	// Attach the inputs to the controls.
		
	this.FirstNameControl.AddInput(this.FirstNameInput);
	this.MiddleNameControl.AddInput(this.MiddleNameInput);
	this.LastNameControl.AddInput(this.LastNameInput);
	this.EmailControl.AddInput(this.EmailInput);
	this.UsernameControl.AddInput(this.UsernameInput);
	this.PasswordControl.AddInput(this.PasswordInput);
	this.ConfirmControl.AddInput(this.ConfirmInput);
	
	this.StreetAddress1Control.AddInput(this.StreetAddress1Input);
	this.StreetAddress2Control.AddInput(this.StreetAddress2Input);
	this.CityControl.AddInput(this.CityInput);
	this.StateControl.AddInput(this.StateInput);
	this.ZipControl.AddInput(this.ZipInput);
	this.CountryControl.AddInput(this.CountryInput);
	this.PrimaryPhoneControl.AddInput(this.PrimaryPhoneInput);
	this.MobilePhoneControl.AddInput(this.MobilePhoneInput);
	
	this.JobTitleControl.AddInput(this.JobTitleInput);
	
	this.CompanyControl.AddInput(this.CompanyInput);
	//this.CompanyControl.AddInput(this.OptionContainer);
	
	// Buttons
	
	this.ButtonGroup.appendChild(this.SaveButton);
	
	// Append the form to the container.
	
	try{
		this.Container.appendChild(this.RootElement);
	}catch(e){ }
	
	// Client-side error checking.
	
	this.RootElement.UpdatePasswordConfirm = function(){
		if(this.Controller.ConfirmInput.value.length > 0){
			if (this.Controller.ConfirmInput.value == this.Controller.PasswordInput.value){
				this.Controller.ConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.ConfirmControl.HasErrors()){
					this.Controller.ConfirmControl.AddError("Confirmation does not match.");
				}
			}
		}else{
			this.Controller.ConfirmControl.ClearErrors();
		}
	}
	
	this.PasswordInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	this.ConfirmInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	
	// Set Success Actions
	
	this.RootElement.addEventListener(
		"post-successful", 
		function(){
			this.Controller.CompleteRegistration();
		}
	);
	
	this.RootElement.SetSuccessEvent("post-successful", this.RootElement);
	
	// Overloads
	
	this.RootElement.Post = function(){
	
		// move into view
		
		DOM.scrollTo(this);
		
		// clear all the errors
		
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		// set form to "working" 
		DOM.addClass(this, "working");		
		
		if(!this.Controller.DoEasyErrors()){
			DOM.removeClass(this, "working");
			return false;
		}
		
		// post		
		AJAXHelper.Post(
			this.PostURL, 
			this.Controller.RootElement.GetData(),
			this.PostType,
			"post-complete", 
			this
		);
		
	}
	
}


RegistrationFormControl.prototype.DoEasyErrors = function(){
	
	var s = true;
	
	if(this.FirstNameInput.value.length == 0){
		s = false;
		this.FirstNameControl.AddError("First Name is required.");
	}
	
	if(this.LastNameInput.value.length == 0){
		s = false;
		this.LastNameControl.AddError("Last Name is required.");
	}
	
	if(this.EmailInput.value.length == 0){
		s = false;
		this.EmailControl.AddError("Email is required.");
	}
	
	if(this.PasswordInput.value.length == 0){
		s = false;
		this.PasswordControl.AddError("Password is required.");
	}
	
	if(this.ConfirmInput.value != this.PasswordInput.value){
		s = false;
		this.ConfirmControl.AddError("Confirmation does not match.");
	}

	if(this.UsernameInput.value.length == 0){
		s = false;
		this.UsernameControl.AddError("Username is required.");
	}
	
	if(this.StreetAddress1Input.value.length == 0){
		s = false;
		this.StreetAddress1Control.AddError("Street Address 1 is required.");
	}
	
	if(this.CityInput.value.length == 0){
		s = false;
		this.CityControl.AddError("City is required.");
	}
	
	if(this.StateInput.value.length == 0){
		s = false;
		this.StateControl.AddError("State is required.");
	}
	
	if(this.ZipInput.value.length == 0){
		s = false;
		this.ZipControl.AddError("Postal Code is required.");
	}	
	
	if(this.CountryInput.value.length == 0){
		s = false;
		this.CountryControl.AddError("Country is required.");
	}	

	if(this.PrimaryPhoneInput.value.length == 0){
		s = false;
		this.PrimaryPhoneControl.AddError("Primary Phone is required.");
	}

	if(this.CompanyInput.value.length == 0){
		s = false;
		this.CompanyControl.AddError("Company is required.");
	}
	
	if(this.CountryInput.value.length > 2){
		s = false;
		this.CountryControl.AddError("Country must be 2 letter country code.");
	}

	if((!/\d{5}/.test(this.ZipInput.value) && (this.CountryInput.value == "US" || this.CountryInput.value == "MX")) ||
	((!/[a-zA-Z]\d[a-zA-Z] \d[a-zA-Z]\d/.test(this.ZipInput.value)) && this.CountryInput.value == "CA")){
		s = false;
		this.ZipControl.AddError("Postal Code Is Not In Correct Format");
	}

	if(!/\w+@\w+\.\w+/.test(this.EmailInput.value)){
		s = false;
		this.EmailControl.AddError("Email must be a valid email address.");
	}

	if(!/\d{3}-\d{3}-\d{4}/.test(this.PrimaryPhoneInput.value) && !/\d{10}/.test(this.PrimaryPhoneInput.value)){
		s = false;
		this.PrimaryPhoneControl.AddError("Primary phone must be a 10 digit phone number.");
	}
	
	if(!/\d{3}-\d{3}-\d{4}/.test(this.MobilePhoneInput.value) && !/\d{10}/.test(this.MobilePhoneInput.value) && this.MobilePhoneInput.value.length > 0){
		s = false;
		this.MobilePhoneControl.AddError("Mobile phone must be a 10 digit phone number.");
	}
	
	if(this.StateInput.value == 0){
		s = false;
		this.StateControl.AddError("Please select a state.");
	}
	
	return s;
	
}


RegistrationFormControl.prototype.CompleteRegistration = function () {
    
    // if we got an SSO token, redirect; otherwise, just proceed with displaying the success message
    if (this.RootElement.ReturnData.token != null) {
        window.location.href = "https://generac-fs-dev.asentialms.com/_util/DoSSO.aspx?token=" + this.RootElement.ReturnData.token;
    }
    else {
        this.RootElement.addEventListener(
            "animationend",
            function () {
                this.Controller.Container.appendChild(
                    new UserRegistrationCompletedControl(
                        this.Controller.FirstNameControl.GetValue(),
                        this.Controller.UsernameControl.GetValue()
                        )
                );
                DOM.scrollTo(this.Controller.Container);
                this.Controller.Container.removeChild(this);
            }
        );

        DOM.addClass(this.RootElement, "vanish");
    }
}

function getSelectedOption(sel) {
    var opt;
    for ( var i = 0, len = sel.options.length; i < len; i++ ) {
        opt = sel.options[i];
        if ( opt.selected === true ) {
            break;
        }
    }
    return opt;
}

function changeCountry(){
	
	  $("#state option[country='" + $("#country option:selected").val() + "']").prop('selected', true);
	  $("#state option").each(function(){
		  if($(this).attr("country") != $("#country option:selected").val()){
			$(this).hide();
		  }else{
			  
			 $(this).show();
		  }			 
		  
	  });
		  
	  
}

UserRegistrationCompletedControl = function(
	name,
	email
	){
	
	this.RootElement = DOM.c(
		this, 
		"DIV",
		{
			"class":"info-control evolve"
		}
	);
	
	this.Something = DOM.c(
		null, 
		"DIV", 
		{
			"class":"primary"
		}, 
		this.RootElement, 
		"Thank you for registering for a Generac LMS account, " + name + ". Your LMS username is: " + email
		+ ". You may log into the LMS by going to "
	);
	
	DOM.c(
		null, 
		"A", 
		{
			"href":"https://generac-fs-dev.asentialms.com"			
		}, 
		this.Something, 
		"https://generac-fs-dev.asentialms.com"
	);
	
	return this.RootElement;
	
}

