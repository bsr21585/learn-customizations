﻿<%@ WebService Language="C#" Class="GeneracRegistrationForm" %>

using System;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.WebControls;
using System.Xml;

[WebService(Description = "Web services example.", Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class GeneracRegistrationForm : System.Web.Services.WebService 
{
    public GeneracRegistrationForm()
    {}
	
	#region Register
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ReturnObject Register(string username, string firstname, string middlename, string lastname, string password, string email, string streetaddress1, string streetaddress2, string city, string state, string zip, string country, string primaryphone, string mobilephone, string company, string jobtitle)
    {
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		string stepThroughLog = String.Empty;
		
        ReturnObject retObj = new ReturnObject();
        DateTime? dtAccountExpiresQR = null;
        string courseCodesQR = null;
        string[] courseCodesQRArr = null;
		
		retObj.status.result = "success";
		retObj.status.code = "1";
		retObj.status.messages = new List<object>();
        retObj.token = null;

        try
        {
            
            // REGISTER THE USER USING THE ASENTIA API
            try
            {				
                // USER ACCOUNT REGISTRATION
				stepThroughLog += DateTime.Now.ToString() + " - User registration with API.\r\n";

				string crmResponse = "";
                string techid = null;
                string dealerid = null;
                try
                {
                    // CALL EXTERNAL API to get techid and dealer id
                    crmResponse = _CallPIWrapper(firstname, middlename, lastname, jobtitle, email, streetaddress1, streetaddress2, city, state, zip, country, primaryphone, mobilephone, company);
                }
                catch (WebException webEx)
                {
                    throw new Exception("An error occurred when contacting the CRM Service. Please contact an administrator.");
                }

                stepThroughLog += DateTime.Now.ToString() + " - Call external API finished.\r\n";


                CRMData responseObject = Newtonsoft.Json.JsonConvert.DeserializeObject<CRMData>(crmResponse);

                if (responseObject.ReturnMessage[0].MessageStatus == "E")
                {
                    throw new Exception(responseObject.ReturnMessage[0].Message);
                }

                techid = responseObject.TechID;
                dealerid = responseObject.DealerID;

                string streetaddress = streetaddress1;

                if (!String.IsNullOrWhiteSpace(streetaddress2))
                {
                    streetaddress = streetaddress + ", " + streetaddress2;
                }
                
                try
                { this._DoUserRegistration(username, firstname, middlename, lastname, password, techid, email, dealerid, streetaddress, city, state, zip, country, primaryphone, mobilephone, company, jobtitle); }
                catch (WebException webEx) // RETRY 1 TIME
                { this._DoUserRegistration(username, firstname, middlename, lastname, password, techid, email, dealerid, streetaddress, city, state, zip, country, primaryphone, mobilephone, company, jobtitle); }
                
                
				
				stepThroughLog += DateTime.Now.ToString() + " - User registration with API finished.\r\n";

                
                
                // LOG THE USER INTO ASENTIA AND REDIRECT
				/*stepThroughLog += DateTime.Now.ToString() + " - Logging user into Asentia with API.\r\n";
					
                try
                { retObj.token = this._GetUserSSOToken(idUser); }
                catch (WebException webEx) // RETRY 1 TIME
                { retObj.token = this._GetUserSSOToken(idUser); }
					
			    stepThroughLog += DateTime.Now.ToString() + " - Logging user into Asentia with API finished.\r\n";*/
                
            }
            catch (WebException ex)
            {
                // write the exception to a log with the response
				if (ex.Response != null)
				{
					WebResponse errResp = ex.Response;
				
					using (Stream respStream = errResp.GetResponseStream())
					{
						StreamReader reader = new StreamReader(respStream);
						string text = reader.ReadToEnd();

                        this._WriteExceptionToLog(ex.Message, ex.StackTrace, text, stepThroughLog);	
					}
				}
                else // write the exception to a log without the response
				{
                    this._WriteExceptionToLog(ex.Message, ex.StackTrace, null, stepThroughLog);	
				}
				
				// bubble up the exception
                throw new Exception("A fatal error occurred when contacting the user registration service. Please contact an administrator.");
            }
        }        
        catch (Exception ex)
        {
            retObj.status.result = "fail";
            retObj.status.code = "0";
            retObj.status.messages = new List<object>();

            string[] exceptionMessages = ex.Message.Split('|');

            foreach (string s in exceptionMessages)
            {
                MessageObject msgObj = new MessageObject();
                msgObj.identifier = "general";
                msgObj.text = s;
                retObj.status.messages.Add(msgObj);
            }
        }
		
		// return
		return retObj;
    }
    #endregion

    #region _DoUserRegistration
    private void _DoUserRegistration(string username, string firstname, string middlename, string lastname, string password, string techid, string email, string dealerid, string streetaddress, string city, string state, string zip, string country, string primaryphone, string mobilephone, string company, string jobtitle)
    {
        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key>1234567890</key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<user id=\\\"0\\\">";
        xmlPayload += "<firstName><![CDATA[" + firstname + "]]></firstName>";
        xmlPayload += "<middleName><![CDATA[" + middlename + "]]></middleName>";
        xmlPayload += "<lastName><![CDATA[" + lastname + "]]></lastName>";
        xmlPayload += "<email><![CDATA[" + email + "]]></email>";
        xmlPayload += "<username><![CDATA[" + username + "]]></username>";
        xmlPayload += "<password><![CDATA[" + password + "]]></password>";
        xmlPayload += "<mustChangePassword>0</mustChangePassword>";
        xmlPayload += "<idTimezone>15</idTimezone>";
        xmlPayload += "<languageString>en-US</languageString>";
        xmlPayload += "<isActive>true</isActive>";
        xmlPayload += "<company><![CDATA[" + company + "]]></company>";
        xmlPayload += "<address><![CDATA[" + streetaddress + "]]></address>";
        xmlPayload += "<city><![CDATA[" + city + "]]></city>";
        xmlPayload += "<province><![CDATA[" + state + "]]></province>";
        xmlPayload += "<postalcode><![CDATA[" + zip + "]]></postalcode>";
		xmlPayload += "<country><![CDATA[" + country + "]]></country>";
        xmlPayload += "<phonePrimary><![CDATA[" + primaryphone + "]]></phonePrimary>";
        xmlPayload += "<phoneMobile><![CDATA[" + mobilephone + "]]></phoneMobile>";
        xmlPayload += "<jobTitle><![CDATA[" + jobtitle + "]]></jobTitle>";
		xmlPayload += "<field00><![CDATA[" + techid + "]]></field00>";
		xmlPayload += "<field15><![CDATA[" + dealerid + "]]></field15>";
        xmlPayload += "</user>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://generac-fs.asentialms.com/_util/API/Default.asmx/SaveUser");
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://generac-fs-dev.asentialms.com/_util/API/Default.asmx/SaveUser");

        // encode payload into bytes
        byte[] bytes;
        bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // throw an error if there is one
        if (statusNode.InnerText == "Error")
        { throw new Exception(descriptionNode.InnerText); }
    }
    #endregion
	
	#region _CallPIWrapper
    private string _CallPIWrapper(string firstname, string middlename, string lastname, string jobtitle, string email, string streetaddress1, string streetaddress2, string city, string state, string zip, string country, string primaryphone, string mobilephone, string company)
    {
        string responseString = String.Empty;
        string payload = String.Empty;
        try
        {
            // prepare the payload
            payload = "{";
            payload += "\"Technician\": ";
            payload += "{";
            payload += "\"FirstName\":\"" + firstname + "\", ";
            payload += "\"LastName\":\"" + lastname + "\", ";
            payload += "\"MiddleName\":\"" + middlename + "\", ";
            payload += "\"JobTitle\":\"" + jobtitle + "\", ";
            payload += "\"StreetAddress1\":\"" + streetaddress1 + "\", ";
            payload += "\"StreetAddress2\":\"" + streetaddress2 + "\", ";
            payload += "\"ZipCode\":\"" + zip + "\", ";
            payload += "\"City\":\"" + city + "\", ";
            payload += "\"Country\":\"" + country + "\", ";
            payload += "\"State\":\"" + state + "\", ";
            payload += "\"Phone\":\"" + primaryphone + "\", ";
            payload += "\"Phone2\":\"" + mobilephone + "\", ";
            payload += "\"Email\":\"" + email + "\"";
            payload += "}, ";
            payload += "\"Dealer\": ";
            payload += "{ ";
            payload += "\"CompanyName\":\"" + company + "\", ";
            payload += "\"StreetAddress1\":\"" + streetaddress1 + "\", ";
            payload += "\"StreetAddress2\":\"" + streetaddress2 + "\", ";
            payload += "\"ZipCode\":\"" + zip + "\", ";
            payload += "\"City\":\"" + city + "\", ";
            payload += "\"Country\":\"" + country + "\", ";
            payload += "\"State\":\"" + state + "\" ";
            payload += "}";
            payload += "}";


            // create the request
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://soagridtest.generac.com/WebServices/SAPCRMTechnician/api/Technician/Create");
            
            // encode payload into bytes
            byte[] bytes;
            bytes = System.Text.Encoding.UTF8.GetBytes(payload);

            // set the payload type, length, and method
            request.ContentType = "application/json; encoding='utf-8'";
            request.ContentLength = bytes.Length;
            request.Headers["auth-key"] = "9C4E50DD-657D-4697-8B64-9D658BB0D071";
            request.Method = "POST";

            // write the request stream (payload)
            Stream requestStream = request.GetRequestStream();
            requestStream.Write(bytes, 0, bytes.Length);
            requestStream.Close();

            // get the response
            
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    { responseString = new StreamReader(responseStream).ReadToEnd(); }
                }
                else
                { throw new Exception(response.StatusDescription); }
            }
        }
        catch (Exception)
        {
            throw new Exception("An error occurred when contacting the CRM Service. Please contact an administrator.");
        }

        return responseString;
    }
    #endregion

    #region _DoGetUserId
    private int _DoGetUserId(string email)
    {
        int idUser = 0;

        // prepare the payload
        string xmlPayload = "{";
        xmlPayload += "payload: ";
        xmlPayload += "\"<request>";
        xmlPayload += "<securityContext><key>#c4r33r5p0t54p1!</key></securityContext><responseFormat>xml</responseFormat>";
        xmlPayload += "<payload>";
        xmlPayload += "<getUser>";
        xmlPayload += "<username><![CDATA[" + email + "]]></username>";
        xmlPayload += "</getUser>";
        xmlPayload += "</payload>";
        xmlPayload += "</request>\"";
        xmlPayload += "}";

        // create the request
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://generac-fs-dev.asentialms.com/_util/API/Default.asmx/GetUser");

        // encode payload into bytes                
        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

        // set the payload type, length, and method
        request.ContentType = "application/json; encoding='utf-8'";
        request.ContentLength = bytes.Length;
        request.Method = "POST";

        // write the request stream (payload)
        Stream requestStream = request.GetRequestStream();
        requestStream.Write(bytes, 0, bytes.Length);
        requestStream.Close();

        // get the response
        string responseString = String.Empty;

        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        {
            if (response.StatusCode == HttpStatusCode.OK)
            {
                using (Stream responseStream = response.GetResponseStream())
                { responseString = new StreamReader(responseStream).ReadToEnd(); }
            }
            else
            { throw new WebException(); }
        }

        // get the response string in Json format, parsing it to get the xml string under element d 
        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

        // parse the xml string to a xml object, get the description node from the xml object in this case. 
        XmlDocument xmlRoot = new XmlDocument();
        xmlRoot.LoadXml(responseString);

        // get the status and description
        XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
        XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

        // do nothing if there is an error, it just means that we cannot register the user in courses, it should not be a fatal exception
        if (statusNode.InnerText == "Error")
        { }
        else // no error on getUser, proceed
        {
            if (xmlRoot.SelectSingleNode("response/payload/Users/User/Id") != null)
            {
                idUser = Convert.ToInt32(xmlRoot.SelectSingleNode("response/payload/Users/User/Id").InnerText);
            }
        }

        // return the user id
        return idUser;
    }
    #endregion

    #region _WriteExceptionToLog
    private void _WriteExceptionToLog(string message, string stackTrace, string response, string stepThroughLog)
    {
        // write the exception to a file
        DateTime dtNow = DateTime.Now;
        string exceptionfileName = "RegistrationException_" + String.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}.log", dtNow);
        string exceptionFileFullPath = Path.Combine(HttpRuntime.AppDomainAppPath + @"static\Generac\ExceptionLogs\", exceptionfileName);

        using (StreamWriter file = new StreamWriter(exceptionFileFullPath))
        {
            file.WriteLine("EXCEPTION OCCURED ON " + dtNow.ToString());
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION MESSAGE:");
            file.WriteLine(message);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("EXCEPTION STACK TRACE:");
            file.WriteLine(stackTrace);
            file.WriteLine("");
            file.WriteLine("");
            file.WriteLine("API RESPONSE:");

            if (!String.IsNullOrWhiteSpace(response))
            { file.WriteLine(response); }
            else
            { file.WriteLine("NO RESPONSE RECEIVED FROM THE API"); }
			
			file.WriteLine("");
            file.WriteLine("");
			file.WriteLine("STEP THROUGH LOG:");
            file.WriteLine(stepThroughLog);            
        }
    }
    #endregion

    #region Helper Structs
    public struct ReturnObject
    { 
        public StatusObject status;
        public string token;
    }

    public struct StatusObject
    {
        public string result;
        public string code;
        public List<object> messages;
    }       
	
	public struct MessageObject
    {
        public string identifier;
        public string text;
    }
    
    public struct CRMData
    {
        public string DealerID;
        public string TechID;
        public CRMReturnMessage[] ReturnMessage;
    }        	
    
    public struct CRMReturnMessage
    {
        public string MessageStatus;
        public string Message;
    }
    #endregion
}
