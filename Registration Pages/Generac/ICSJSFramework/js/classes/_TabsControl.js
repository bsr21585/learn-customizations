TabsControl = function(
	controller, 
	identifier, 
	className, 
	appendTo, 
	data,
	eventName, // tab changes dispatch events
	eventTarget // to this object
	){
	
	this.ObjectType = "TabsControl";
	
	this.RootElement = DOM.c(
		this, 
		"UL",
		{
			"class":"tabs-control"
		},
		appendTo
	);
	
	this.RootElement.ObjectType = "TabsControl.RootElement";
	
	this.RootElement.AppendTab = function(T){
		
		try{
			this.appendChild(T);
		} catch(e){}
		
	}
	
	this.RootElement.RemoveTab = function(T){
		
		try{
			this.removeChild(T);
		} catch(e){}
		
	}
	
	this.RootElement.GetTabAtIndex = function(index){
		
		return this.childNodes[index];
		
	}
	
	this.RootElement.GetTabWithIdentifier = function(identifier){
		
		for (var i = 0; i < this.childNodes.length; i++){
			
			if (this.childNodes[i].getAttribute("identifier") == identifier){
				
				return this.childNodes[i];
				
			}
		}
		
	}
	
	this.RootElement.DoTabChange = function(identifier){
		
		for (var i = 0; i < this.childNodes.length; i++){
			
			DOM.removeClass(this.GetTabAtIndex(i), "on");
			
		}
		
		DOM.addClass(this.GetTabWithIdentifier(identifier), "on");
		
		// dispatch the event
		
		try{
			
			eventTarget.dispatchEvent(
				new CustomEvent(
					eventName,
					{detail:identifier}
				)
			);
			
		} catch(e){}
		
	}
	
	
	// build tabs from data if it exists
	
	if(data){
		
		for (var i = 0; i < data.length; i++){
			
			this.RootElement.AppendTab(
				new Tab(
					this.RootElement, 
					data[i].identifier, 
					data[i].label 
				)
			);
			
		}
		
	}
}



Tab = function(
	controller, // this should always be the UL (TabsControl.RootElement)
	identifier, 
	label
	){
	
	this.ObjectType = "Tab";
	
	this.RootElement = DOM.c(
		controller, 
		"LI"
	)
	
	this.RootElement.setAttribute("identifier", identifier);
	
	this.RootElement.onclick = function(){
	
		// this / Tab  / TabsControl / UL
		this.Controller.Controller.RootElement.DoTabChange(identifier);

	}
	
	this.Label = DOM.c(
		this, 
		"DIV",
		null,
		this.RootElement, 
		label
	);
	
	return this.RootElement;
	
}