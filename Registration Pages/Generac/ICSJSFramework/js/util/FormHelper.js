FormHelper = function(){
	
}

/*
A is an array of form input controls
M is an array of messages from the error object

Loop backwards so we can remove items from the array without  causing problems.

*/
FormHelper.DoErrors = function(A, M){
	
	for (var i = A.length - 1; i >= 0; i--){
		
		console.log("FormControl: " + A[i].RootElement.id);
		
		for (var j = M.length - 1; j >= 0; j--){
			
			console.log(i + ", " + j);
			
			if (A[i].RootElement.id == M[j].identifier){
				
				console.log("Matched.");
				A[i].AddError(M[j].text);
				M.splice(j, 1);
				
			}
			
		}
		
	}	
	
	// if general errors remain, alert
	if (M.length > 0){
		
		var l = DOM.c(this, "ul");
		
		for (i = 0; i < M.length; i++){
			
			l.appendChild(DOM.c(null, "li", null, null, M[i].text));
			
		}
		
		var errorAlert = new AlertModal(
			"Warning", // title
			M.length.toString() + " error(s) detected.", // heading
			null, // message
			l // details
		);
	}
	
}

FormHelper.PushData = function(A, M){
	
	for (var p in M){ // for each property in the Object
		
		for (var i = 0; i < A.length; i++){
			
			if (p == A[i].RootElement.id){
				
				A[i].PushData(M[p]);
				
			}
			
		}
		
	}
	
	
		
		
	
}