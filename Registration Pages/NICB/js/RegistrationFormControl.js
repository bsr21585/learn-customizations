function RemoveSpacesFromInput(inputElement) {				
		inputElement.value = inputElement.value.replace(" ", "");
}

RegistrationFormControl = function(appendTo){
	
	this.ObjectType = "NICTAUserRegistrationForm";
	this.Container 	= appendTo;
	
	this.RootElement = new Form(
		this, 
		null, 
		null, 
		"registration-form",
		null,
		"Please wait while we create your NICTA LMS user account."
	).RootElement;
		
	this.RootElement.PostURL 	= "/static/NICB/svc/RegistrationForm.asmx/Register";
	this.RootElement.PostType = "json";
	this.RootElement.ReturnData = null;
	
	// Create the instructions container.
	DOM.c(
		null, 
		"DIV", 
		{
			"id":"registrationinstructions",			
			"class":"registrationinstructions"
		}, 
		this.RootElement, 
		"Fill out the form fields below to register for an account."
	);	
	
	// Create the groupings.
	
	this.NameGroup 					= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.EmailGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);	
	this.PasswordGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);	
	this.AccessCodeGroup 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.JobTitleGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.DepartmentGroup 			= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	this.ButtonGroup 				= DOM.c(this, "DIV", {"class":"input-group"},	this.RootElement);
	
	// Create the inputs.
	
	this.FirstNameInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"firstname", 			"name":"firstname", 			"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.LastNameInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"lastname", 			"name":"lastname", 				"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.EmailInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"email", 				"name":"email", 				"type":"text", 		"maxlength":512, "class":"InputLong"});	
	this.PasswordInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"password", 			"name":"password", 				"type":"password", 	"maxlength":255, "class":"InputNormal"});
	this.ConfirmInput 				= DOM.c(this.RootElement, "INPUT", 	{"id":"confirm", 			"name":"confirm", 				"type":"password", 	"maxlength":255, "class":"InputNormal"});	
	this.AccessCodeInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"accesscode", 		"name":"accesscode", 			"type":"text", 		"maxlength":255, "class":"InputNormal"});
	this.JobTitleInput 				= DOM.c(this.RootElement, "SELECT", {"id":"jobtitle", 			"name":"jobtitle", 				"type":"select", 	"maxlength":255, "class":"InputNormal"});
	this.DepartmentInput 			= DOM.c(this.RootElement, "INPUT", 	{"id":"department", 		"name":"department", 			"type":"text", 		"maxlength":255, "class":"InputLong"});
	
	this.SaveButton = DOM.c(
		this, 
		"INPUT",
		{
			"type"	: "button",
			"id"	: "save",
			"name"	: "save", 
			"class"	: "default",
			"value"	: "Create Account"
		}
	)
	
	this.SaveButton.onclick = function(){
		this.Controller.RootElement.Post();
	}
	
	// Create the controls.
	
	this.FirstNameControl 				= new FormInputControl("First Name",				"firstname",			this.NameGroup,					true);
	this.LastNameControl 				= new FormInputControl("Last Name",					"lastname",				this.NameGroup,					true);
	this.EmailControl 					= new FormInputControl("Email",						"email",				this.EmailGroup, 				true);		
	this.PasswordControl 				= new FormInputControl("Password",					"password",				this.PasswordGroup,				true);
	this.ConfirmControl 				= new FormInputControl("Confirm Password",			"confirm",				this.PasswordGroup,				true);	
	this.AccessCodeControl				= new FormInputControl("Company Code",				"accesscode",			this.AccessCodeGroup,			true);
	this.JobTitleControl				= new FormInputControl("Job Title",					"jobtitle",				this.JobTitleGroup,				true);
	this.DepartmentControl				= new FormInputControl("Department",				"department",			this.DepartmentGroup,			false);
	
	// Log the controls into the Form object.
	// Each control represents a parameter value that will be sent to/received from the server.
	
	this.RootElement.InputControls.push(this.FirstNameControl);
	this.RootElement.InputControls.push(this.LastNameControl);
	this.RootElement.InputControls.push(this.EmailControl);		
	this.RootElement.InputControls.push(this.PasswordControl);
	this.RootElement.InputControls.push(this.ConfirmControl);	
	this.RootElement.InputControls.push(this.AccessCodeControl);
	this.RootElement.InputControls.push(this.JobTitleControl);
	this.RootElement.InputControls.push(this.DepartmentControl);
	
	// Attach items to the select inputs.
	
	this.JobTitleInput.AddItem("Special Investigator", "Special Investigator", true);
	this.JobTitleInput.AddItem("Claims Adjuster", "Claims Adjuster", false);
	this.JobTitleInput.AddItem("Underwriting", "Underwriting", false);
	this.JobTitleInput.AddItem("Other", "Other", false);	

	// Attach the inputs to the controls.
		
	this.FirstNameControl.AddInput(this.FirstNameInput);
	this.LastNameControl.AddInput(this.LastNameInput);
	this.EmailControl.AddInput(this.EmailInput);		
	this.PasswordControl.AddInput(this.PasswordInput);
	this.ConfirmControl.AddInput(this.ConfirmInput);		
	this.AccessCodeControl.AddInput(this.AccessCodeInput);
	this.JobTitleControl.AddInput(this.JobTitleInput);
	this.DepartmentControl.AddInput(this.DepartmentInput);
	
	// Buttons
	
	this.ButtonGroup.appendChild(this.SaveButton);
	
	// Append the form to the container.
	
	try{
		this.Container.appendChild(this.RootElement);
	}catch(e){ }
	
	// Client-side error checking.
	
	this.RootElement.UpdatePasswordConfirm = function(){
		if(this.Controller.ConfirmInput.value.length > 0){
			if (this.Controller.ConfirmInput.value == this.Controller.PasswordInput.value){
				this.Controller.ConfirmControl.ClearErrors();
			}else{
				if(!this.Controller.ConfirmControl.HasErrors()){
					this.Controller.ConfirmControl.AddError("Confirmation does not match.");
				}
			}
		}else{
			this.Controller.ConfirmControl.ClearErrors();
		}
	}
	
	this.PasswordInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	this.ConfirmInput.onkeyup = function(e){
		this.Controller.UpdatePasswordConfirm();
	}
	
	// Set Success Actions
	
	this.RootElement.addEventListener(
		"post-successful", 
		function(){
			this.Controller.CompleteRegistration();
		}
	);
	
	this.RootElement.SetSuccessEvent("post-successful", this.RootElement);
	
	// Overloads
	
	this.RootElement.Post = function(){
	
		// move into view
		
		DOM.scrollTo(this);
		
		// clear all the errors
		
		for (var i = 0; i < this.InputControls.length; i++){
			this.InputControls[i].ClearErrors();
		}
		
		// set form to "working" 
		DOM.addClass(this, "working");		
		
		if(!this.Controller.DoEasyErrors()){
			DOM.removeClass(this, "working");
			return false;
		}
		
		// post		
		AJAXHelper.Post(
			this.PostURL, 
			this.Controller.RootElement.GetData(),
			this.PostType,
			"post-complete", 
			this
		);
		
	}
}


RegistrationFormControl.prototype.DoEasyErrors = function(){
	
	var s = true;
	
	if(this.FirstNameInput.value.length == 0){
		s = false;
		this.FirstNameControl.AddError("First Name is required.")
	}
	
	if(this.LastNameInput.value.length == 0){
		s = false;
		this.LastNameControl.AddError("Last Name is required.")
	}
	
	if(this.EmailInput.value.length == 0){
		s = false;
		this.EmailControl.AddError("Email is required.")
	}
	
	if(this.PasswordInput.value.length == -0){
		s = false;
		this.PasswordControl.AddError("Password is required.")
	}
	
	if(this.ConfirmInput.value != this.PasswordInput.value){
		s = false;
		this.ConfirmControl.AddError("Confirmation does not match.")
	}	
	
	if(this.AccessCodeInput.value.length == 0){
		s = false;
		this.AccessCodeControl.AddError("Company Code is required.")
	}	
		
	return s;
	
}


RegistrationFormControl.prototype.CompleteRegistration = function () {
    
    // if we got an SSO token, redirect; otherwise, just proceed with displaying the success message
    if (this.RootElement.ReturnData.token != null) {
        window.location.href = "https://nicta.asentialms.com/_util/DoSSO.aspx?token=" + this.RootElement.ReturnData.token;
    }
    else {
        this.RootElement.addEventListener(
            "animationend",
            function () {
                this.Controller.Container.appendChild(
                    new UserRegistrationCompletedControl(
                        this.Controller.FirstNameControl.GetValue(),
                        this.Controller.EmailControl.GetValue()
                        )
                );
                DOM.scrollTo(this.Controller.Container);
                this.Controller.Container.removeChild(this);
            }
        );

        DOM.addClass(this.RootElement, "vanish");
    }
}


UserRegistrationCompletedControl = function(
	name,
	email
	){
	
	this.RootElement = DOM.c(
		this, 
		"DIV",
		{
			"class":"info-control evolve"
		}
	);
	
	this.Something = DOM.c(
		null, 
		"DIV", 
		{
			"class":"primary"
		}, 
		this.RootElement, 
		"Thank you for registering for a NICTA LMS account, " + name + ". Your LMS username is: " + email
		+ ". You may log into the LMS by going to "
	);
	
	DOM.c(
		null, 
		"A", 
		{
			"href":"https://nicta.asentialms.com"			
		}, 
		this.Something, 
		"https://nicta.asentialms.com"
	);
	
	return this.RootElement;
	
}

