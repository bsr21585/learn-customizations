// assumes that it will be attached to the DOM element "modal" and will call the page overlay automatically.

Modal = function(
	controller,  // the "closer" (close event is dispatched here)
	title, 
	isClosable,
	className, 
	pageControl
	){
	
	this.Title = title;
	this.IsClosable = isClosable;
	this.Controller = controller;
	this.ClassName = className;
	
	this.RootElement = DOM.c(
		this, 
		"DIV"
	);
	
	//instantiate the titlebar
	this.TitleBar = new ModalTitleBar(
		this
	)
	
	// attach the titlebar to the DOM
	this.RootElement.appendChild(this.TitleBar.RootElement);
	
	this.PageControl = pageControl;
	
	this.Content = DOM.c(
		this,
		"DIV",
		{
			"class":"content-container"
		},
		this.RootElement
	)
	// attach the page control
	this.Content.appendChild(this.PageControl.RootElement);
		
	// append to the page
	DOM.get("modal").appendChild(this.RootElement);
	
	// trigger overlay
	PageOverlay.On();
	
	// trigger display
	DOM.addClass(DOM.get("modal"), "modal");
	DOM.addClass(DOM.get("modal"), this.ClassName);
	
	//
	DOM.get("modal").Cleanup = function(){
		DOM.get("modal").removeChild(DOM.get("modal").childNodes[0]);
		DOM.removeAllClasses(DOM.get("modal"));
		this.removeEventListener("animationend", DOM.get("modal").Cleanup);
	}
	
}

Modal.prototype.Close = function(){
	
	
	DOM.addClass(DOM.get("modal"), "modal-out");
	
	// trigger overlay
	PageOverlay.Off();
	
	DOM.get("modal").addEventListener(
		"animationend", 
		DOM.get("modal").Cleanup
	);
}

Modal.prototype.SetCancelEvent = function(eventName, eventTarget){

	try{
		this.PageControl.SetCancelEvent(eventName, eventTarget);
	} catch(e){
		try{
			this.PageControl.RootElement.SetCancelEvent(eventName, eventTarget);
		}catch(e){}
	}
}

/*
Modal.prototype.Cleanup = function(){
	
	DOM.get("modal").removeChild(this.RootElement);
	DOM.removeClass(DOM.get("modal"), "modal");
	DOM.removeClass(DOM.get("modal"), this.ClassName);
	
}
*/
ModalTitleBar = function(
	controller
	){
	
	this.Controller = controller;
	
	this.RootElement = DOM.c(
		this, 
		"DIV", 
		{
			"class":"title-container"
		}
	);
	
	this.Title = DOM.c(
		this,
		"DIV",
		{
			"class":"title"
		},
		this.RootElement,
		this.Controller.Title
	);
	
	if (this.Controller.IsClosable){
		
		this.CloseButton = DOM.c(
			this, 
			"IMG",
			{
				"class":"close-x",
				"src":"/static/NICB/ICSJSFramework/images/modal-x.png"
			},
			this.RootElement
		);
		
		this.CloseButton.onclick = function(){
			
			this.Controller.Controller.Close();
			
		}
	}
	
}
