﻿<%@ WebService Language="C#" Class="NICTARegistrationForm" %>

using System;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.UI.WebControls;
using System.Xml;

[WebService(Description = "Web services example.", Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
[ScriptService]
public class NICTARegistrationForm : System.Web.Services.WebService 
{
    public NICTARegistrationForm()
    {}
	
	#region Register
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public ReturnObject Register(string firstname, string lastname, string email, string password, string confirm, string accesscode, string jobtitle, string department)
    {
		ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
		
        ReturnObject retObj = new ReturnObject();  
		string companyNameQR = String.Empty;
		
		retObj.status.result = "success";
		retObj.status.code = "1";
		retObj.status.messages = new List<MessageObject>();
        retObj.token = null;

        try
        {
            // VALIDATE THE REGISTRATION CODE

            using (SqlConnection connection = new SqlConnection("Server=asentia-sql02;Database=NICTA-RegistrationCodes;User Id=nictareg;password=#fr4nt1c!"))
            using (SqlCommand command = new SqlCommand("[ValidateRegistrationCode]", connection))
            {
                command.CommandType = CommandType.StoredProcedure;

                // add the parameters
                SqlParameter code = new SqlParameter("@Code", SqlDbType.NVarChar, 255);
                code.Direction = ParameterDirection.Input;
                code.Value = accesscode;

                SqlParameter isValid = new SqlParameter("@IsValid", SqlDbType.Bit, 1);
                isValid.Direction = ParameterDirection.Output;

                SqlParameter returnMessage = new SqlParameter("@ReturnMessage", SqlDbType.NVarChar, 512);
                returnMessage.Direction = ParameterDirection.Output;

                SqlParameter companyName = new SqlParameter("@CompanyName", SqlDbType.NVarChar, 255);
                companyName.Direction = ParameterDirection.Output;

                command.Parameters.Add(code);
                command.Parameters.Add(isValid);
                command.Parameters.Add(returnMessage);
                command.Parameters.Add(companyName);

                // execute the query
                connection.Open();
                command.ExecuteNonQuery();

                // get the results of the query
                bool isValidQR = (bool)isValid.Value;
                string returnMessageQR = returnMessage.Value.ToString();
				companyNameQR = companyName.Value.ToString();

                if (!isValidQR)
                { throw new Exception(returnMessageQR); }

                // close the connection
                connection.Close();
            }
            
            // REGISTER THE USER USING THE ASENTIA API

            try
            {
                // USER ACCOUNT REGISTRATION
                
                // prepare the payload
                string xmlPayload = "{";
                xmlPayload += "payload: ";
                xmlPayload += "\"<request>";
                xmlPayload += "<securityContext><key>#n1cbn1ct4@p1!</key></securityContext><responseFormat>xml</responseFormat>";
                xmlPayload += "<payload>";
                xmlPayload += "<user id=\\\"0\\\">";
                xmlPayload += "<firstName><![CDATA[" + firstname + "]]></firstName>";
                xmlPayload += "<lastName><![CDATA[" + lastname + "]]></lastName>";
                xmlPayload += "<email><![CDATA[" + email + "]]></email>";
                xmlPayload += "<username><![CDATA[" + email + "]]></username>";
                xmlPayload += "<password><![CDATA[" + password + "]]></password>";
                xmlPayload += "<mustChangePassword>0</mustChangePassword>";
                xmlPayload += "<idTimezone>15</idTimezone>";
                xmlPayload += "<languageString>en-US</languageString>";
                xmlPayload += "<isActive>true</isActive>";
                xmlPayload += "<company><![CDATA[" + companyNameQR + "]]></company>";
				xmlPayload += "<jobTitle><![CDATA[" + jobtitle + "]]></jobTitle>";
				xmlPayload += "<department><![CDATA[" + department + "]]></department>";
                xmlPayload += "<field00><![CDATA[" + accesscode + "]]></field00>";
                xmlPayload += "</user>";
                xmlPayload += "</payload>";
                xmlPayload += "</request>\"";
                xmlPayload += "}";
                
                // create the request
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://nicta.asentialms.com/_util/API/Default.asmx/SaveUser");                

                // encode payload into bytes
                byte[] bytes;
                bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

                // set the payload type, length, and method
                request.ContentType = "application/json; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";

                // write the request stream (payload)
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                // get the response
                string responseString = String.Empty;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        { responseString = new StreamReader(responseStream).ReadToEnd(); }
                    }
                    else
                    { throw new WebException(); }
                }

                // get the response string in Json format, parsing it to get the xml string under element d 
                responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

                // parse the xml string to a xml object, get the description node from the xml object in this case. 
                XmlDocument xmlRoot = new XmlDocument();
                xmlRoot.LoadXml(responseString);

                // get the status and description
                XmlNode statusNode = xmlRoot.SelectSingleNode("response/status/message");
                XmlNode descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

                // throw an error if there is one
                if (statusNode.InnerText == "Error")
                {
                    throw new Exception(descriptionNode.InnerText);
                }

                // RECORD THE REGISTRATION IN THE USE TABLE

                using (SqlConnection connection = new SqlConnection("Server=asentia-sql02;Database=NICTA-RegistrationCodes;User Id=nictareg;password=#fr4nt1c!"))
                using (SqlCommand command = new SqlCommand("[UpdateRegistrationCodeUse]", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    // add the parameters
                    SqlParameter code = new SqlParameter("@Code", SqlDbType.NVarChar, 255);
                    code.Direction = ParameterDirection.Input;
                    code.Value = accesscode;

                    SqlParameter username = new SqlParameter("@Username", SqlDbType.NVarChar, 512);
                    username.Direction = ParameterDirection.Input;
                    username.Value = email;

                    command.Parameters.Add(code);
                    command.Parameters.Add(username);

                    // execute the query
                    connection.Open();
                    command.ExecuteNonQuery();

                    // close the connection
                    connection.Close();
                }
                
                // GET THE REGISTERED USER'S ID AND LOG THEM IN
                                
                // GET THE USER ID

                // prepare the payload
                xmlPayload = "{";
                xmlPayload += "payload: ";
                xmlPayload += "\"<request>";
                xmlPayload += "<securityContext><key>#n1cbn1ct4@p1!</key></securityContext><responseFormat>xml</responseFormat>";
                xmlPayload += "<payload>";
                xmlPayload += "<getUser>";
                xmlPayload += "<username><![CDATA[" + email + "]]></username>";
                xmlPayload += "</getUser>";
                xmlPayload += "</payload>";
                xmlPayload += "</request>\"";
                xmlPayload += "}";

                // create the request
                request = (HttpWebRequest)WebRequest.Create("https://nicta.asentialms.com/_util/API/Default.asmx/GetUser");

                // encode payload into bytes                
                bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

                // set the payload type, length, and method
                request.ContentType = "application/json; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";

                // write the request stream (payload)
                requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();

                // get the response
                responseString = String.Empty;

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        using (Stream responseStream = response.GetResponseStream())
                        { responseString = new StreamReader(responseStream).ReadToEnd(); }
                    }
                    else
                    { throw new WebException(); }
                }

                // get the response string in Json format, parsing it to get the xml string under element d 
                responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

                // parse the xml string to a xml object, get the description node from the xml object in this case. 
                xmlRoot = new XmlDocument();
                xmlRoot.LoadXml(responseString);

                // get the status and description
                statusNode = xmlRoot.SelectSingleNode("response/status/message");
                descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

                // do nothing if there is an error, it just means that we cannot register the user in courses, it should not be a fatal exception
                if (statusNode.InnerText == "Error")
                { }
                else // no error on getUser, proceed
                {
                    if (xmlRoot.SelectSingleNode("response/payload/Users/User/Id") != null)
                    {
                        int idUser = Convert.ToInt32(xmlRoot.SelectSingleNode("response/payload/Users/User/Id").InnerText);

                        // LOG THE USER INTO ASENTIA AND REDIRECT

                        // prepare the payload
                        xmlPayload = "{";
                        xmlPayload += "payload: ";
                        xmlPayload += "\"<request>";
                        xmlPayload += "<securityContext><key>#n1cbn1ct4@p1!</key></securityContext><responseFormat>xml</responseFormat>";
                        xmlPayload += "<payload>";
                        xmlPayload += "<getSSOToken>";
                        xmlPayload += "<id>" + idUser.ToString() + "</id>";
                        xmlPayload += "</getSSOToken>";
                        xmlPayload += "</payload>";
                        xmlPayload += "</request>\"";
                        xmlPayload += "}";

                        // create the request
                        request = (HttpWebRequest)WebRequest.Create("https://nicta.asentialms.com/_util/API/Default.asmx/GetSSOToken");

                        // encode payload into bytes                
                        bytes = System.Text.Encoding.UTF8.GetBytes(xmlPayload);

                        // set the payload type, length, and method
                        request.ContentType = "application/json; encoding='utf-8'";
                        request.ContentLength = bytes.Length;
                        request.Method = "POST";

                        // write the request stream (payload)
                        requestStream = request.GetRequestStream();
                        requestStream.Write(bytes, 0, bytes.Length);
                        requestStream.Close();

                        // get the response
                        responseString = String.Empty;

                        using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                        {
                            if (response.StatusCode == HttpStatusCode.OK)
                            {
                                using (Stream responseStream = response.GetResponseStream())
                                { responseString = new StreamReader(responseStream).ReadToEnd(); }
                            }
                            else
                            { throw new WebException(); }
                        }

                        // get the response string in Json format, parsing it to get the xml string under element d 
                        responseString = Newtonsoft.Json.Linq.JObject.Parse(responseString)["d"].ToString();

                        // parse the xml string to a xml object, get the description node from the xml object in this case. 
                        xmlRoot = new XmlDocument();
                        xmlRoot.LoadXml(responseString);

                        // get the status and description
                        statusNode = xmlRoot.SelectSingleNode("response/status/message");
                        descriptionNode = xmlRoot.SelectSingleNode("response/status/description");

                        // do nothing if there is an error, it just means that we cannot re-direct the user into the LMS, it should not be a fatal exception
                        if (statusNode.InnerText == "Error")
                        { }
                        else // no error on getSSOToken, proceed with redirect
                        {
                            string token = xmlRoot.SelectSingleNode("response/payload/SSOToken/Token").InnerText;
                            retObj.token = token;
                        }
                    }
                }
            }
            catch (WebException ex)
            {
                throw new Exception("A fatal error occurred when contacting the user registration service. Please contact an administrator.");
                
                //WebResponse errResp = ex.Response;
                //using (Stream respStream = errResp.GetResponseStream())
                //{
                    //StreamReader reader = new StreamReader(respStream);
                    //string text = reader.ReadToEnd();

                    //throw new Exception(ex.StackTrace + "|" + ex.Message + " | " + text);
                //}
            }
        }        
        catch (Exception ex)
        {
            retObj.status.result = "fail";
            retObj.status.code = "0";
            retObj.status.messages = new List<MessageObject>();

            string[] exceptionMessages = ex.Message.Split('|');

            foreach (string s in exceptionMessages)
            {
                MessageObject msgObj = new MessageObject();
                msgObj.identifier = "general";
                msgObj.text = s;
                retObj.status.messages.Add(msgObj);
            }
        }
		
		// return
		return retObj;
    }
    #endregion
	
    #region Helper Structs	
	public struct ReturnObject
    { 
        public StatusObject status;
        public string token;
    }
	
    public struct StatusObject
    {
        public string result;
        public string code;
        public List<MessageObject> messages;
    }       
	
	public struct MessageObject
    {
        public string identifier;
        public string text;
    }        	
    #endregion
}
